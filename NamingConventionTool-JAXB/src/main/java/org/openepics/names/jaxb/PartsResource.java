/*
 * Copyright (c) 2020 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.jaxb;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.Contact;
import io.swagger.annotations.ExternalDocs;
import io.swagger.annotations.Info;
import io.swagger.annotations.SwaggerDefinition;

/**
 * This resource provides name parts data. Intended usage is for System Structure and Device Structure.
 *
 * @author Lars Johansson
 */
@Path("parts")
@Api(value = "/parts")
@Produces({"application/json"})
@SwaggerDefinition(
        info = @Info(
                description = "\n"
                        + "    This is a documentation for all REST interfaces of Naming service. \n"
                        + "    You can find out more about Naming service in Naming application,  \n"
                        + "        https://naming.esss.lu.se",
                version = "1.0.2",
                title = "Naming service API documentation",
                termsOfService = "http://swagger.io/terms/",
                contact = @Contact(
                        name = "Support",
                        email = "Icsscontrolsystemsupport@esss.se")
                ),
        externalDocs = @ExternalDocs(
                value = "ESS Naming Convention",
                url = "https://chess.esss.lu.se/enovia/link/ESS-0000757/21308.51166.45568.45993/valid"))
public interface PartsResource {

    /**
     * Finds all name parts by mnemonic.
     * Note mnemonic (exact match, case sensitive).
     *
     * @param mnemonic mnemonic to look for
     * @return a list of name parts
     */
    @GET
    @Path("mnemonic/{mnemonic}")
    @ApiOperation(
            value = "Finds all name parts by mnemonic",
            notes = "Note mnemonic (exact match, case sensitive). Returns a list of name parts",
            response = PartElement.class,
            responseContainer = "List")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public List<PartElement> getAllPartsByMnemonic(@PathParam("mnemonic") String mnemonic);

    /**
     * Finds all name parts by mnemonic search.
     * Note mnemonic (search, case sensitive, regex).
     *
     * @param mnemonic mnemonic to search for
     * @return a list of name parts
     */
    @GET
    @Path("mnemonic/search/{mnemonic}")
    @ApiOperation(
            value = "Finds all name parts by mnemonic search",
            notes = "Note mnemonic (search, case sensitive, regex). Returns a list of name parts",
            response = PartElement.class,
            responseContainer = "List")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public List<PartElement> getAllPartsByMnemonicSearch(@PathParam("mnemonic") String mnemonic);

    /**
     * Finds all name parts by mnemonic path search.
     * Note mnemonic path (search, case sensitive, regex).
     *
     * @param mnemonicPath mnemonic path to search for
     * @return a list of name parts
     */
    @GET
    @Path("mnemonicPath/search/{mnemonicPath}")
    @ApiOperation(
            value = "Finds all name parts by mnemonic path search",
            notes = "Note mnemonic path (search, case sensitive, regex). Returns a list of name parts",
            response = PartElement.class,
            responseContainer = "List")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public List<PartElement> getAllPartsByMnemonicPathSearch(@PathParam("mnemonicPath") String mnemonicPath);

}
