/*
 * Copyright (c) 2020 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.jaxb;

import java.util.UUID;

import javax.xml.bind.annotation.XmlRootElement;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * Data transfer object representing name parts for JSON and XML serialization.
 * Intended usage is for System Structure and Device Structure.
 *
 * @author Lars Johansson
 */
@XmlRootElement
@ApiModel
public class PartElement {

    private String type;
    private UUID uuid;
    private String level;
    private String name;
    private String mnemonic;
    private String description;
    private String status;
    private String namePath;
    private String mnemonicPath;

    /**
     * Constructor.
     */
    public PartElement() {
        super();
    }

    /**
     * Constructor.
     *
     * @param uuid the uuid
     * @param name the name
     * @param status the status
     */
    public PartElement(UUID uuid, String name, String status) {
        super();
        this.uuid = uuid;
        this.name = name;
        this.status = status;
    }

    /**
     * Constructor.
     *
     * @param type the type
     * @param uuid the uuid
     * @param level the level
     * @param name the name
     * @param mnemonic the mnemonic
     * @param description the description
     * @param status the status
     * @param namePath the name path
     * @param mnemonicPath the mnemonic path
     */
    public PartElement(String type, UUID uuid, String level, String name, String mnemonic,
            String description, String status, String namePath, String mnemonicPath) {
        super();
        this.type = type;
        this.uuid = uuid;
        this.level = level;
        this.name = name;
        this.mnemonic = mnemonic;
        this.mnemonicPath = mnemonicPath;
        this.description = description;
        this.status = status;
    }

    /**
     * @return the type
     */
    @ApiModelProperty(required = true)
    public String getType() {
        return type;
    }
    /**
     * @param type the type to set
     */
    public void setType(String type) {
        this.type = type;
    }
    /**
     * @return the uuid
     */
    @ApiModelProperty(required = true, value = "UUID")
    public UUID getUuid() {
        return uuid;
    }
    /**
     * @param uuid the uuid to set
     */
    public void setUuid(UUID uuid) {
        this.uuid = uuid;
    }
    /**
     * @return the level
     */
    @ApiModelProperty(required = true)
    public String getLevel() {
        return level;
    }
    /**
     * @param level the level to set
     */
    public void setLevel(String level) {
        this.level = level;
    }
    /**
     * @return the name
     */
    @ApiModelProperty(required = true)
    public String getName() {
        return name;
    }
    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }
    /**
     * @return the mnemonic
     */
    @ApiModelProperty(required = true)
    public String getMnemonic() {
        return mnemonic;
    }
    /**
     * @param mnemonic the mnemonic to set
     */
    public void setMnemonic(String mnemonic) {
        this.mnemonic = mnemonic;
    }
    /**
     * @return the description
     */
    @ApiModelProperty(required = true)
    public String getDescription() {
        return description;
    }
    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }
    /**
     * @return the status
     */
    @ApiModelProperty(required = true)
    public String getStatus() {
        return status;
    }
    /**
     * @param status the status to set
     */
    public void setStatus(String status) {
        this.status = status;
    }
    /**
     * @return the name path
     */
    @ApiModelProperty(required = true)
    public String getNamePath() {
        return namePath;
    }
    /**
     * @param mnemonicPath the name path to set
     */
    public void setNamePath(String namePath) {
        this.namePath = namePath;
    }
    /**
     * @return the mnemonic path
     */
    @ApiModelProperty(required = true)
    public String getMnemonicPath() {
        return mnemonicPath;
    }
    /**
     * @param mnemonicPath the mnemonic path to set
     */
    public void setMnemonicPath(String mnemonicPath) {
        this.mnemonicPath = mnemonicPath;
    }

}
