FROM jboss/wildfly:8.2.1.Final
LABEL maintainer="anders.harrisson@esss.se"

# PostgreSQL jdbc driver module
RUN mkdir -p /opt/jboss/wildfly/modules/org/postgresql/main
ADD --chown=jboss:jboss https://artifactory.esss.lu.se/artifactory/repo1/org/postgresql/postgresql/9.4.1212/postgresql-9.4.1212.jar /opt/jboss/wildfly/modules/org/postgresql/main/
ADD --chown=jboss:jboss https://artifactory.esss.lu.se/artifactory/wildfly-modules/org.postgresql/postgresql/postgresql-9.4.1212.xml /opt/jboss/wildfly/modules/org/postgresql/main/module.xml
ADD --chown=jboss:jboss https://artifactory.esss.lu.se/artifactory/repo1/biz/paluch/logging/logstash-gelf/1.12.0/logstash-gelf-1.12.0-logging-module.zip /tmp/gelf-logging-module.zip
RUN unzip -q /tmp/gelf-logging-module.zip -d /tmp/ && mv /tmp/logstash-gelf-*/* /opt/jboss/wildfly/modules/ && rmdir /tmp/logstash-gelf-* && rm /tmp/gelf-logging-module.zip

# Set Environment variables used in JAVA_OPTS
ENV JBOSS_MODULES_SYSTEM_PKGS=org.jboss.byteman \
    JAVA_XMS=1024m \
    JAVA_XMX=2048m \
    JAVA_METASPACE=96M \
    JAVA_MAX_METASPACE=256m
ENV JAVA_OPTS="-Xms${JAVA_XMS} -Xmx${JAVA_XMX} -XX:MetaspaceSize=${JAVA_METASPACE} -XX:MaxMetaspaceSize=${JAVA_MAX_METASPACE} -Djava.net.preferIPv4Stack=true -Djboss.modules.system.pkgs=${JBOSS_MODULES_SYSTEM_PKGS} -Djava.awt.headless=true"

COPY --chown=jboss:jboss src/main/resources/wildfly/standalone.xml /opt/jboss/wildfly/standalone/configuration/

COPY target/names-*.war /opt/jboss/wildfly/standalone/deployments/
