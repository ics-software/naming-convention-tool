/*
 * Copyright (C) 2020 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.business;

import java.util.ArrayList;
import java.util.List;

import org.openepics.names.model.Device;
import org.openepics.names.model.DeviceRevision;
import org.openepics.names.model.NamePart;
import org.openepics.names.model.NamePartRevision;
import org.openepics.names.model.NamePartRevisionStatus;
import org.openepics.names.model.UserAccount;
import org.openepics.names.model.UtilityModel;
import org.powermock.reflect.Whitebox;

/**
 * Purpose to provide test fixture that can be used in multiple test classes and tests.
 *
 * <br>
 * Intended usage is
 * <ol>
 * <li> <tt>getInstance</tt> method followed by </tt>setUp</tt> method to set up test fixture.
 * <li> <tt>get</tt> methods for tests
 * <li> <tt>tearDown</tt> method to tear down test fixture.
 * </ol>
 *
 * @author Lars Johansson
 *
 * @see UtilityBusiness
 * @see UtilityModel
 */
public class UtilityBusinessTestFixture2 {

    /*
       Test fixture for section, device type, device based on database backup Sep 10 2020.
           -----------------------------------------
           Accelerator
           -----------------------------------------
           conventionNameEqClass  conventionName
           -----------------------------------------
           RFQ-10:EMR-TT-1        RFQ-010:EMR-TT-001
           RFQ-10:EMR-TT-10       RFQ-010:EMR-TT-010
           RFQ-10:EMR-TT-11       RFQ-010:EMR-TT-011
           RFQ-10:EMR-TT-12       RFQ-010:EMR-TT-012
           RFQ-10:EMR-TT-13       RFQ-010:EMR-TT-013
           RFQ-10:EMR-TT-14       RFQ-010:EMR-TT-014
           RFQ-10:EMR-TT-15       RFQ-010:EMR-TT-015
           RFQ-10:EMR-TT-16       RFQ-010:EMR-TT-016
           RFQ-10:EMR-TT-17       RFQ-010:EMR-TT-017
           RFQ-10:EMR-TT-18       RFQ-010:EMR-TT-018
           RFQ-10:EMR-TT-19       RFQ-010:EMR-TT-019
           -----------------------------------------
           mnemonic path
           -----------------------------------------
           Acc
           Acc-RFQ
           Acc-RFQ-010
           EMR
           EMR
           EMR-TT
           EMR
           EMR-TT2

       Note
           includes name revision hierarchy/history for section, device type, device
           section, device type, device are associated
           purpose for selection of names
               include name, eq name (instance index), history
    */

    public static final String ACCELERATOR  = "Accelerator";
    public static final String INITIAL_DATA = "Initial data";

    public static final String ELECTROMAGNETIC_RESONATORS = "Electromagnetic Resonators";

    public static final String ASDF = "asdf";
    public static final String ZXCV = "zxcv";

    private static final String DATE_2014_04_04_17_52_59 = "2014-04-04 17:52:59";
    private static final String DATE_2014_04_04_17_53_02 = "2014-04-04 17:53:02";
    private static final String DATE_2017_07_05_15_44_17 = "2017-07-05 15:44:17";
    private static final String DATE_2017_07_05_15_44_18 = "2017-07-05 15:44:18";
    private static final String DATE_2018_02_12_17_08_31 = "2018-02-12 17:08:31";
    private static final String DATE_2018_02_12_17_08_34 = "2018-02-12 17:08:34";

    private static final String ID = "id";

    // ----------------------------------------------------------------------------------------------------

    private static UtilityBusinessTestFixture2 instance;

    /**
     * Returns the singleton instance of this class.
     *
     * @return the singletone instance
     */
    public static synchronized UtilityBusinessTestFixture2 getInstance() {
        if (instance == null) {
            instance = new UtilityBusinessTestFixture2();
        }
        return instance;
    }

    /**
     * Constructor to be used only from within this class.
     */
    private UtilityBusinessTestFixture2() {
    }

    /**
     * One-time initialization code.
     */
    public void setUp() {
        userAccountEditor    = UtilityModel.createUserAccountEditor   (ASDF);
        userAccountSuperUser = UtilityModel.createUserAccountSuperUser(ZXCV);

        setupNameRevisionSection();
        setupNameRevisionDeviceType();
        setupNameRevisionDevice();
    }

    /**
     * One-time cleanup code.
     */
    public void tearDown() {
        userAccountEditor    = null;
        userAccountSuperUser = null;

        tearDownNameRevisionSection();
        tearDownNameRevisionDeviceType();
        tearDownNameRevisionDevice();
    }

    // ----------------------------------------------------------------------------------------------------

    private UserAccount userAccountEditor;
    private UserAccount userAccountSuperUser;

    public UserAccount getUserAccountEditor()    { return userAccountEditor;    }
    public UserAccount getUserAccountSuperUser() { return userAccountSuperUser; }

    // ----------------------------------------------------------------------------------------------------
    // section
    //     System Group        Accelerator                       Acc        name part id 1
    //     System              Radio Frequency Quadrupole        RFQ        name part id 7
    //     Subsystem           RFQ-010                           010        name part id 178
    //     ----------
    //     RFQ-010
    public static final String UUID_NAMEPART_1_SYSTEMGROUP_ACC  = "4262e1e7-2444-412e-83d7-aeabf58262c6";
    public static final String UUID_NAMEPART_7_SYSTEM_RFQ       = "121958b6-0592-41d3-8cec-0f05f9c01db8";
    public static final String UUID_NAMEPART_178_SUBSYSSTEM_010 = "ce4b9809-6994-45e0-852a-a52d4ecbd52e";

    private NamePartRevision namePartRevision1SystemGroupAcc;
    private NamePartRevision namePartRevision2SystemGroupAcc;
    private NamePartRevision namePartRevision3SystemGroupAcc;
    private NameRevision     nameRevision1SystemGroupAcc;
    private NameRevision     nameRevision2SystemGroupAcc;
    private NameRevision     nameRevision3SystemGroupAcc;

    public NamePartRevision getNamePartRevision1SystemGroupAcc() { return namePartRevision1SystemGroupAcc; }
    public NamePartRevision getNamePartRevision2SystemGroupAcc() { return namePartRevision2SystemGroupAcc; }
    public NamePartRevision getNamePartRevision3SystemGroupAcc() { return namePartRevision3SystemGroupAcc; }
    public NameRevision     getNameRevision1SystemGroupAcc()     { return nameRevision1SystemGroupAcc; }
    public NameRevision     getNameRevision2SystemGroupAcc()     { return nameRevision2SystemGroupAcc; }
    public NameRevision     getNameRevision3SystemGroupAcc()     { return nameRevision3SystemGroupAcc; }

    // ----------
    private NamePartRevision namePartRevisionSystemRFQ;
    private NameRevision     nameRevisionSystemRFQ;

    public NamePartRevision getNamePartRevisionSystemRFQ()       { return namePartRevisionSystemRFQ; }
    public NameRevision     getNameRevisionSystemRFQ()           { return nameRevisionSystemRFQ; }
    // ----------
    private NamePartRevision namePartRevision1Subsystem010;
    private NamePartRevision namePartRevision2Subsystem010;
    private NameRevision     nameRevision1Subsystem010;
    private NameRevision     nameRevision2Subsystem010;

    public NamePartRevision getNamePartRevision1Subsystem010()   { return namePartRevision1Subsystem010; }
    public NamePartRevision getNamePartRevision2Subsystem010()   { return namePartRevision2Subsystem010; }
    public NameRevision     getNameRevision1Subsystem010()       { return nameRevision1Subsystem010; }
    public NameRevision     getNameRevision2Subsystem010()       { return nameRevision2Subsystem010; }

    // ----------------------------------------------------------------------------------------------------
    // device type
    //     Discipline          Electromagnetic Resonators        EMR
    //     Device Group        Control
    //     Device Type         Temperature Transmitter           TT
    //     ----------
    //     EMR-TT
    public static final String UUID_NAMEPART_239_DISCIPLINE_EMR  = "a4abce9e-9a3d-4483-8296-ec9fc1845fe0";
    public static final String UUID_NAMEPART_247_DEVICEGROUP     = "aa94fa6a-f6bb-49b0-a8e5-2e5eaffbe855";
    public static final String UUID_NAMEPART_1640_DEVICETYPE_TT2 = "fad5e98d-76cd-4400-aeb6-cfa1840cc9ea";
    public static final String UUID_NAMEPART_1978_DEVICEGROUP    = "dcf6b448-f0dd-4f7d-a836-14b66fa19667";
    public static final String UUID_NAMEPART_2016_DEVICETYPE_TT  = "9715e97d-5a60-47fb-bf95-c47e927efc1b";

    private NamePartRevision namePartRevisionDisciplineEMR;
    private NameRevision     nameRevisionDisciplineEMR;

    public NamePartRevision getNamePartRevisionDisciplineEMR()  { return namePartRevisionDisciplineEMR; }
    public NameRevision     getNameRevisionDisciplineEMR()      { return nameRevisionDisciplineEMR; }

    // ----------
    private NamePartRevision namePartRevision1DeviceGroup1;
    private NamePartRevision namePartRevision2DeviceGroup1;
    private NameRevision     nameRevision1DeviceGroup1;
    private NameRevision     nameRevision2DeviceGroup1;

    public NamePartRevision getNamePartRevision1DeviceGroup1()   { return namePartRevision1DeviceGroup1; }
    public NamePartRevision getNamePartRevision2DeviceGroup1()   { return namePartRevision2DeviceGroup1; }
    public NameRevision     getNameRevision1DeviceGroup1()       { return nameRevision1DeviceGroup1; }
    public NameRevision     getNameRevision2DeviceGroup1()       { return nameRevision2DeviceGroup1; }
    // ----------
    private NamePartRevision namePartRevision1DeviceTypeTT2;
    private NamePartRevision namePartRevision2DeviceTypeTT2;
    private NamePartRevision namePartRevision3DeviceTypeTT2;
    private NameRevision     nameRevision1DeviceTypeTT2;
    private NameRevision     nameRevision2DeviceTypeTT2;
    private NameRevision     nameRevision3DeviceTypeTT2;

    public NamePartRevision getNamePartRevision1DeviceTypeTT2() { return namePartRevision1DeviceTypeTT2; }
    public NamePartRevision getNamePartRevision2DeviceTypeTT2() { return namePartRevision2DeviceTypeTT2; }
    public NamePartRevision getNamePartRevision3DeviceTypeTT2() { return namePartRevision3DeviceTypeTT2; }
    public NameRevision     getNameRevision1DeviceTypeTT2()     { return nameRevision1DeviceTypeTT2; }
    public NameRevision     getNameRevision2DeviceTypeTT2()     { return nameRevision2DeviceTypeTT2; }
    public NameRevision     getNameRevision3DeviceTypeTT2()     { return nameRevision3DeviceTypeTT2; }
    // --------------------
    private NamePartRevision namePartRevisionDeviceGroup2;
    private NameRevision     nameRevisionDeviceGroup2;

    public NamePartRevision getNamePartRevisionDeviceGroup2()    { return namePartRevisionDeviceGroup2; }
    public NameRevision     getNameRevisionDeviceGroup2()        { return nameRevisionDeviceGroup2; }
    // ----------
    private NamePartRevision namePartRevisionDeviceTypeTT;
    private NameRevision     nameRevisionDeviceTypeTT;

    public NamePartRevision getNamePartRevisionDeviceTypeTT()   { return namePartRevisionDeviceTypeTT; }
    public NameRevision     getNameRevisionDeviceTypeTT()       { return nameRevisionDeviceTypeTT; }

    // ----------------------------------------------------------------------------------------------------
    // device
    //     Instance Index
    //     ----------
    //     conventionNameEqClass  conventionName
    //     RFQ-10:EMR-TT-1        RFQ-010:EMR-TT-001
    //     RFQ-10:EMR-TT-10       RFQ-010:EMR-TT-010
    //     RFQ-10:EMR-TT-11       RFQ-010:EMR-TT-011
    //     RFQ-10:EMR-TT-12       RFQ-010:EMR-TT-012
    //     RFQ-10:EMR-TT-13       RFQ-010:EMR-TT-013
    //     RFQ-10:EMR-TT-14       RFQ-010:EMR-TT-014
    //     RFQ-10:EMR-TT-15       RFQ-010:EMR-TT-015
    //     RFQ-10:EMR-TT-16       RFQ-010:EMR-TT-016
    //     RFQ-10:EMR-TT-17       RFQ-010:EMR-TT-017
    //     RFQ-10:EMR-TT-18       RFQ-010:EMR-TT-018
    //     RFQ-10:EMR-TT-19       RFQ-010:EMR-TT-019
    public static final String UUID_DEVICE_12735_INSTANCEINDEX = "7223e5b9-e683-46bc-bc7c-e23cb01b18f5";
    public static final String UUID_DEVICE_12798_INSTANCEINDEX = "b134f7a7-d86b-461c-b028-bae8ebdb0823";
    public static final String UUID_DEVICE_12742_INSTANCEINDEX = "68d4b813-3af2-40c0-8d12-14dc4cf4203a";
    public static final String UUID_DEVICE_12803_INSTANCEINDEX = "5f751ed9-811f-4f16-a95f-faddfb82f9ef";
    public static final String UUID_DEVICE_12778_INSTANCEINDEX = "d884df5e-350b-427b-bbf2-37589ec6cca2";
    public static final String UUID_DEVICE_12722_INSTANCEINDEX = "fad6d0ac-6032-4c87-90da-ec536f57bb51";
    public static final String UUID_DEVICE_12703_INSTANCEINDEX = "d3a58f4c-0aa8-405a-b8ee-fd67540cab55";
    public static final String UUID_DEVICE_12733_INSTANCEINDEX = "74d620aa-ba55-41d7-a1e9-023e5d6664bd";
    public static final String UUID_DEVICE_12706_INSTANCEINDEX = "6d9d68ef-53b2-4378-afa7-f63d232c9c36";
    public static final String UUID_DEVICE_12795_INSTANCEINDEX = "12a98cd0-a117-472b-85c9-2877db79bfc1";
    public static final String UUID_DEVICE_12770_INSTANCEINDEX = "4bb6f624-ba90-4702-8995-a4da1a452607";

    private DeviceRevision deviceRevision1Device001;
    private DeviceRevision deviceRevision2Device001;
    private DeviceRevision deviceRevision3Device001;
    private NameRevision nameRevision1Device001;
    private NameRevision nameRevision2Device001;
    private NameRevision nameRevision3Device001;
    // ----------
    private DeviceRevision deviceRevision1Device010;
    private DeviceRevision deviceRevision2Device010;
    private DeviceRevision deviceRevision3Device010;
    private NameRevision nameRevision1Device010;
    private NameRevision nameRevision2Device010;
    private NameRevision nameRevision3Device010;
    // ----------
    private DeviceRevision deviceRevision1Device011;
    private DeviceRevision deviceRevision2Device011;
    private DeviceRevision deviceRevision3Device011;
    private NameRevision nameRevision1Device011;
    private NameRevision nameRevision2Device011;
    private NameRevision nameRevision3Device011;
    // ----------
    private DeviceRevision deviceRevision1Device012;
    private DeviceRevision deviceRevision2Device012;
    private DeviceRevision deviceRevision3Device012;
    private NameRevision nameRevision1Device012;
    private NameRevision nameRevision2Device012;
    private NameRevision nameRevision3Device012;
    // ----------
    private DeviceRevision deviceRevision1Device013;
    private DeviceRevision deviceRevision2Device013;
    private DeviceRevision deviceRevision3Device013;
    private NameRevision nameRevision1Device013;
    private NameRevision nameRevision2Device013;
    private NameRevision nameRevision3Device013;
    // ----------
    private DeviceRevision deviceRevision1Device014;
    private DeviceRevision deviceRevision2Device014;
    private DeviceRevision deviceRevision3Device014;
    private NameRevision nameRevision1Device014;
    private NameRevision nameRevision2Device014;
    private NameRevision nameRevision3Device014;
    // ----------
    private DeviceRevision deviceRevision1Device015;
    private DeviceRevision deviceRevision2Device015;
    private DeviceRevision deviceRevision3Device015;
    private NameRevision nameRevision1Device015;
    private NameRevision nameRevision2Device015;
    private NameRevision nameRevision3Device015;
    // ----------
    private DeviceRevision deviceRevision1Device016;
    private DeviceRevision deviceRevision2Device016;
    private DeviceRevision deviceRevision3Device016;
    private NameRevision nameRevision1Device016;
    private NameRevision nameRevision2Device016;
    private NameRevision nameRevision3Device016;
    // ----------
    private DeviceRevision deviceRevision1Device017;
    private DeviceRevision deviceRevision2Device017;
    private DeviceRevision deviceRevision3Device017;
    private NameRevision nameRevision1Device017;
    private NameRevision nameRevision2Device017;
    private NameRevision nameRevision3Device017;
    // ----------
    private DeviceRevision deviceRevision1Device018;
    private DeviceRevision deviceRevision2Device018;
    private DeviceRevision deviceRevision3Device018;
    private NameRevision nameRevision1Device018;
    private NameRevision nameRevision2Device018;
    private NameRevision nameRevision3Device018;
    // ----------
    private DeviceRevision deviceRevision1Device019;
    private DeviceRevision deviceRevision2Device019;
    private DeviceRevision deviceRevision3Device019;
    private NameRevision nameRevision1Device019;
    private NameRevision nameRevision2Device019;
    private NameRevision nameRevision3Device019;

    public DeviceRevision getDeviceRevision1Device001() { return deviceRevision1Device001; }
    public DeviceRevision getDeviceRevision2Device001() { return deviceRevision2Device001; }
    public DeviceRevision getDeviceRevision3Device001() { return deviceRevision3Device001; }
    public NameRevision   getNameRevision1Device001()   { return nameRevision1Device001; }
    public NameRevision   getNameRevision2Device001()   { return nameRevision2Device001; }
    public NameRevision   getNameRevision3Device001()   { return nameRevision3Device001; }
    // ----------
    public DeviceRevision getDeviceRevision1Device010() { return deviceRevision1Device010; }
    public DeviceRevision getDeviceRevision2Device010() { return deviceRevision2Device010; }
    public DeviceRevision getDeviceRevision3Device010() { return deviceRevision3Device010; }
    public NameRevision   getNameRevision1Device010()   { return nameRevision1Device010; }
    public NameRevision   getNameRevision2Device010()   { return nameRevision2Device010; }
    public NameRevision   getNameRevision3Device010()   { return nameRevision3Device010; }
    // ----------
    public DeviceRevision getDeviceRevision1Device011() { return deviceRevision1Device011; }
    public DeviceRevision getDeviceRevision2Device011() { return deviceRevision2Device011; }
    public DeviceRevision getDeviceRevision3Device011() { return deviceRevision3Device011; }
    public NameRevision   getNameRevision1Device011()   { return nameRevision1Device011; }
    public NameRevision   getNameRevision2Device011()   { return nameRevision2Device011; }
    public NameRevision   getNameRevision3Device011()   { return nameRevision3Device011; }
    // ----------
    public DeviceRevision getDeviceRevision1Device012() { return deviceRevision1Device012; }
    public DeviceRevision getDeviceRevision2Device012() { return deviceRevision2Device012; }
    public DeviceRevision getDeviceRevision3Device012() { return deviceRevision3Device012; }
    public NameRevision   getNameRevision1Device012()   { return nameRevision1Device012; }
    public NameRevision   getNameRevision2Device012()   { return nameRevision2Device012; }
    public NameRevision   getNameRevision3Device012()   { return nameRevision3Device012; }
    // ----------
    public DeviceRevision getDeviceRevision1Device013() { return deviceRevision1Device013; }
    public DeviceRevision getDeviceRevision2Device013() { return deviceRevision2Device013; }
    public DeviceRevision getDeviceRevision3Device013() { return deviceRevision3Device013; }
    public NameRevision   getNameRevision1Device013()   { return nameRevision1Device013; }
    public NameRevision   getNameRevision2Device013()   { return nameRevision2Device013; }
    public NameRevision   getNameRevision3Device013()   { return nameRevision3Device013; }
    // ----------
    public DeviceRevision getDeviceRevision1Device014() { return deviceRevision1Device014; }
    public DeviceRevision getDeviceRevision2Device014() { return deviceRevision2Device014; }
    public DeviceRevision getDeviceRevision3Device014() { return deviceRevision3Device014; }
    public NameRevision   getNameRevision1Device014()   { return nameRevision1Device014; }
    public NameRevision   getNameRevision2Device014()   { return nameRevision2Device014; }
    public NameRevision   getNameRevision3Device014()   { return nameRevision3Device014; }
    // ----------
    public DeviceRevision getDeviceRevision1Device015() { return deviceRevision1Device015; }
    public DeviceRevision getDeviceRevision2Device015() { return deviceRevision2Device015; }
    public DeviceRevision getDeviceRevision3Device015() { return deviceRevision3Device015; }
    public NameRevision   getNameRevision1Device015()   { return nameRevision1Device015; }
    public NameRevision   getNameRevision2Device015()   { return nameRevision2Device015; }
    public NameRevision   getNameRevision3Device015()   { return nameRevision3Device015; }
    // ----------
    public DeviceRevision getDeviceRevision1Device016() { return deviceRevision1Device016; }
    public DeviceRevision getDeviceRevision2Device016() { return deviceRevision2Device016; }
    public DeviceRevision getDeviceRevision3Device016() { return deviceRevision3Device016; }
    public NameRevision   getNameRevision1Device016()   { return nameRevision1Device016; }
    public NameRevision   getNameRevision2Device016()   { return nameRevision2Device016; }
    public NameRevision   getNameRevision3Device016()   { return nameRevision3Device016; }
    // ----------
    public DeviceRevision getDeviceRevision1Device017() { return deviceRevision1Device017; }
    public DeviceRevision getDeviceRevision2Device017() { return deviceRevision2Device017; }
    public DeviceRevision getDeviceRevision3Device017() { return deviceRevision3Device017; }
    public NameRevision   getNameRevision1Device017()   { return nameRevision1Device017; }
    public NameRevision   getNameRevision2Device017()   { return nameRevision2Device017; }
    public NameRevision   getNameRevision3Device017()   { return nameRevision3Device017; }
    // ----------
    public DeviceRevision getDeviceRevision1Device018() { return deviceRevision1Device018; }
    public DeviceRevision getDeviceRevision2Device018() { return deviceRevision2Device018; }
    public DeviceRevision getDeviceRevision3Device018() { return deviceRevision3Device018; }
    public NameRevision   getNameRevision1Device018()   { return nameRevision1Device018; }
    public NameRevision   getNameRevision2Device018()   { return nameRevision2Device018; }
    public NameRevision   getNameRevision3Device018()   { return nameRevision3Device018; }
    // ----------
    public DeviceRevision getDeviceRevision1Device019() { return deviceRevision1Device019; }
    public DeviceRevision getDeviceRevision2Device019() { return deviceRevision2Device019; }
    public DeviceRevision getDeviceRevision3Device019() { return deviceRevision3Device019; }
    public NameRevision   getNameRevision1Device019()   { return nameRevision1Device019; }
    public NameRevision   getNameRevision2Device019()   { return nameRevision2Device019; }
    public NameRevision   getNameRevision3Device019()   { return nameRevision3Device019; }

    // ----------------------------------------------------------------------------------------------------

    /**
     * Return list of name revisions for test fixture.
     *
     * @return list of name revisions for test fixture
     */
    public List<NameRevision> getNameRevisions() {
        List<NameRevision> nameRevisions = new ArrayList<>();

        // section
        nameRevisions.add(getNameRevision1SystemGroupAcc());
        nameRevisions.add(getNameRevision2SystemGroupAcc());
        nameRevisions.add(getNameRevision3SystemGroupAcc());
        nameRevisions.add(getNameRevisionSystemRFQ());
        nameRevisions.add(getNameRevision1Subsystem010());
        nameRevisions.add(getNameRevision2Subsystem010());

        // device type
        nameRevisions.add(getNameRevisionDisciplineEMR());
        nameRevisions.add(getNameRevision1DeviceGroup1());
        nameRevisions.add(getNameRevision2DeviceGroup1());
        nameRevisions.add(getNameRevision1DeviceTypeTT2());
        nameRevisions.add(getNameRevision2DeviceTypeTT2());
        nameRevisions.add(getNameRevision3DeviceTypeTT2());
        nameRevisions.add(getNameRevisionDeviceGroup2());
        nameRevisions.add(getNameRevisionDeviceTypeTT());

        // device
        nameRevisions.add(getNameRevision1Device001());
        nameRevisions.add(getNameRevision2Device001());
        nameRevisions.add(getNameRevision3Device001());
        nameRevisions.add(getNameRevision1Device010());
        nameRevisions.add(getNameRevision2Device010());
        nameRevisions.add(getNameRevision3Device010());
        nameRevisions.add(getNameRevision1Device011());
        nameRevisions.add(getNameRevision2Device011());
        nameRevisions.add(getNameRevision3Device011());
        nameRevisions.add(getNameRevision1Device012());
        nameRevisions.add(getNameRevision2Device012());
        nameRevisions.add(getNameRevision3Device012());
        nameRevisions.add(getNameRevision1Device013());
        nameRevisions.add(getNameRevision2Device013());
        nameRevisions.add(getNameRevision3Device013());
        nameRevisions.add(getNameRevision1Device014());
        nameRevisions.add(getNameRevision2Device014());
        nameRevisions.add(getNameRevision3Device014());
        nameRevisions.add(getNameRevision1Device015());
        nameRevisions.add(getNameRevision2Device015());
        nameRevisions.add(getNameRevision3Device015());
        nameRevisions.add(getNameRevision1Device016());
        nameRevisions.add(getNameRevision2Device016());
        nameRevisions.add(getNameRevision3Device016());
        nameRevisions.add(getNameRevision1Device017());
        nameRevisions.add(getNameRevision2Device017());
        nameRevisions.add(getNameRevision3Device017());
        nameRevisions.add(getNameRevision1Device018());
        nameRevisions.add(getNameRevision2Device018());
        nameRevisions.add(getNameRevision3Device018());
        nameRevisions.add(getNameRevision1Device019());
        nameRevisions.add(getNameRevision2Device019());
        nameRevisions.add(getNameRevision3Device019());

        return nameRevisions;
    }

    /**
     * Set up test structure - section - name part revisions, name revisions.
     */
    private void setupNameRevisionSection() {
        NamePart namePartSystemGroupAcc = UtilityModel.createNamePartSection(UUID_NAMEPART_1_SYSTEMGROUP_ACC);
        NamePart namePartSystemRFQ      = UtilityModel.createNamePartSection(UUID_NAMEPART_7_SYSTEM_RFQ);
        NamePart namePartSubsystem010   = UtilityModel.createNamePartSection(UUID_NAMEPART_178_SUBSYSSTEM_010);

        Whitebox.setInternalState(namePartSystemGroupAcc, ID, 1L);
        Whitebox.setInternalState(namePartSystemRFQ,      ID, 7L);
        Whitebox.setInternalState(namePartSubsystem010,   ID, 178L);

        namePartRevision1SystemGroupAcc = UtilityModel.createNamePartRevision(
                namePartSystemGroupAcc, UtilityBusiness.parseDateOrNewDate(DATE_2014_04_04_17_52_59), null,
                INITIAL_DATA,
                false, null, ACCELERATOR, "Acc",
                null, "ACC");
        namePartRevision2SystemGroupAcc = UtilityModel.createNamePartRevision(
                namePartSystemGroupAcc, UtilityBusiness.parseDateOrNewDate("2015-04-30 10:47:54"), null,
                "Mnemonics for Super section is not part of names "
                        + "and have been removed in order not to confuse users of the naming convention",
                false, null, ACCELERATOR, null,
                null, null);
        namePartRevision3SystemGroupAcc = UtilityModel.createNamePartRevision(
                namePartSystemGroupAcc, UtilityBusiness.parseDateOrNewDate("2020-07-15 10:34:28"), null,
                "The mnemonic was added accordging to the new naming convention. Alfio",
                false, null, ACCELERATOR, "Acc",
                null, "ACC");

        namePartRevision1SystemGroupAcc.setStatus(NamePartRevisionStatus.APPROVED);
        namePartRevision1SystemGroupAcc.setProcessDate(UtilityBusiness.parseDateOrNewDate(DATE_2014_04_04_17_52_59));
        namePartRevision1SystemGroupAcc.setProcessorComment(null);
        namePartRevision2SystemGroupAcc.setStatus(NamePartRevisionStatus.APPROVED);
        namePartRevision2SystemGroupAcc.setProcessDate(UtilityBusiness.parseDateOrNewDate("2015-05-06 14:24:11"));
        namePartRevision2SystemGroupAcc.setProcessorComment("Mnemonics for Super Section are not part of "
                + "device names and have been removed to avoid confusion. ");
        namePartRevision3SystemGroupAcc.setStatus(NamePartRevisionStatus.APPROVED);
        namePartRevision3SystemGroupAcc.setProcessDate(UtilityBusiness.parseDateOrNewDate("2020-07-15 10:36:45"));
        namePartRevision3SystemGroupAcc.setProcessorComment("approved by alfio");

        namePartRevisionSystemRFQ = UtilityModel.createNamePartRevision(
                namePartSystemRFQ, UtilityBusiness.parseDateOrNewDate(DATE_2014_04_04_17_52_59), null,
                INITIAL_DATA,
                false, namePartSystemGroupAcc, "Radio Frequency Quadrupole", "RFQ",
                null, "RFQ");

        namePartRevisionSystemRFQ.setStatus(NamePartRevisionStatus.APPROVED);
        namePartRevisionSystemRFQ.setProcessDate(UtilityBusiness.parseDateOrNewDate(DATE_2014_04_04_17_52_59));
        namePartRevisionSystemRFQ.setProcessorComment(null);

        namePartRevision1Subsystem010 = UtilityModel.createNamePartRevision(
                namePartSubsystem010, UtilityBusiness.parseDateOrNewDate("2014-04-04 17:53:01"), null,
                INITIAL_DATA,
                false, namePartSystemRFQ, "RFQ-01", "01",
                null, "1");
        namePartRevision2Subsystem010 = UtilityModel.createNamePartRevision(
                namePartSubsystem010, UtilityBusiness.parseDateOrNewDate("2016-03-23 13:58:42"), null,
                "Trailing zero",
                false, namePartSystemRFQ, "RFQ-010", "010",
                null, "10");

        namePartRevision1Subsystem010.setStatus(NamePartRevisionStatus.APPROVED);
        namePartRevision1Subsystem010.setProcessDate(UtilityBusiness.parseDateOrNewDate("2014-04-04 17:53:01"));
        namePartRevision1Subsystem010.setProcessorComment(null);
        namePartRevision2Subsystem010.setStatus(NamePartRevisionStatus.APPROVED);
        namePartRevision2Subsystem010.setProcessDate(UtilityBusiness.parseDateOrNewDate("2016-06-28 09:56:31"));
        namePartRevision2Subsystem010.setProcessorComment("Approved by Daniel Piso");

        Whitebox.setInternalState(namePartRevision1SystemGroupAcc, ID, 1L);
        Whitebox.setInternalState(namePartRevision2SystemGroupAcc, ID, 1050L);
        Whitebox.setInternalState(namePartRevision3SystemGroupAcc, ID, 5007L);
        Whitebox.setInternalState(namePartRevisionSystemRFQ,       ID, 7L);
        Whitebox.setInternalState(namePartRevision1Subsystem010,   ID, 178L);
        Whitebox.setInternalState(namePartRevision2Subsystem010,   ID, 1650L);

        nameRevision1SystemGroupAcc = UtilityBusiness.createNameRevision(namePartRevision1SystemGroupAcc);
        nameRevision2SystemGroupAcc = UtilityBusiness.createNameRevision(namePartRevision1SystemGroupAcc);
        nameRevision3SystemGroupAcc = UtilityBusiness.createNameRevision(namePartRevision1SystemGroupAcc);
        nameRevisionSystemRFQ       = UtilityBusiness.createNameRevision(namePartRevisionSystemRFQ);
        nameRevision1Subsystem010   = UtilityBusiness.createNameRevision(namePartRevision1Subsystem010);
        nameRevision2Subsystem010   = UtilityBusiness.createNameRevision(namePartRevision2Subsystem010);
    }

    /**
     * Set up test structure - device type - name part revisions, name revisions.
     */
    private void setupNameRevisionDeviceType() {
        NamePart namePartDisciplineEMR = UtilityModel.createNamePartDeviceType(UUID_NAMEPART_239_DISCIPLINE_EMR);
        NamePart namePartDeviceGroup1  = UtilityModel.createNamePartDeviceType(UUID_NAMEPART_247_DEVICEGROUP);
        NamePart namePartDeviceTypeTT2 = UtilityModel.createNamePartDeviceType(UUID_NAMEPART_1640_DEVICETYPE_TT2);
        NamePart namePartDeviceGroup2  = UtilityModel.createNamePartDeviceType(UUID_NAMEPART_1978_DEVICEGROUP);
        NamePart namePartDeviceTypeTT  = UtilityModel.createNamePartDeviceType(UUID_NAMEPART_2016_DEVICETYPE_TT);

        Whitebox.setInternalState(namePartDisciplineEMR, ID, 239L);
        Whitebox.setInternalState(namePartDeviceGroup1,  ID, 247L);
        Whitebox.setInternalState(namePartDeviceTypeTT2, ID, 1640L);
        Whitebox.setInternalState(namePartDeviceGroup2,  ID, 1978L);
        Whitebox.setInternalState(namePartDeviceTypeTT,  ID, 2016L);

        namePartRevisionDisciplineEMR = UtilityModel.createNamePartRevision(
                namePartDisciplineEMR, UtilityBusiness.parseDateOrNewDate(DATE_2014_04_04_17_53_02), null,
                INITIAL_DATA,
                false, null, ELECTROMAGNETIC_RESONATORS, "EMR",
                null, "EMR");

        namePartRevisionDisciplineEMR.setStatus(NamePartRevisionStatus.APPROVED);
        namePartRevisionDisciplineEMR.setProcessDate(UtilityBusiness.parseDateOrNewDate(DATE_2014_04_04_17_53_02));
        namePartRevisionDisciplineEMR.setProcessorComment(null);

        namePartRevision1DeviceGroup1 = UtilityModel.createNamePartRevision(
                namePartDeviceGroup1, UtilityBusiness.parseDateOrNewDate(DATE_2014_04_04_17_53_02), null,
                INITIAL_DATA,
                false, namePartDisciplineEMR, "Misc", "Misc",
                null, "M1SC");
        namePartRevision2DeviceGroup1 = UtilityModel.createNamePartRevision(
                namePartDeviceGroup1, UtilityBusiness.parseDateOrNewDate("2015-04-30 11:07:54"), userAccountSuperUser,
                null,
                false, namePartDisciplineEMR, "Misc", null,
                null, null);

        namePartRevision1DeviceGroup1.setStatus(NamePartRevisionStatus.APPROVED);
        namePartRevision1DeviceGroup1.setProcessDate(UtilityBusiness.parseDateOrNewDate(DATE_2014_04_04_17_53_02));
        namePartRevision1DeviceGroup1.setProcessorComment(null);
        namePartRevision2DeviceGroup1.setStatus(NamePartRevisionStatus.APPROVED);
        namePartRevision2DeviceGroup1.setProcessDate(UtilityBusiness.parseDateOrNewDate("2015-04-30 11:29:06"));
        namePartRevision2DeviceGroup1.setProcessorComment(
                "Mnemonics for Device Group (category) is not part of names and have been removed "
                        + "in order not to confuse users of the naming convention");

        namePartRevision1DeviceTypeTT2 = UtilityModel.createNamePartRevision(
                namePartDeviceTypeTT2, UtilityBusiness.parseDateOrNewDate("2017-07-03 14:15:57"), userAccountEditor,
                "Needed for the RFQ cavity",
                false, namePartDeviceGroup1, ELECTROMAGNETIC_RESONATORS, "TT",
                null, "TT");
        namePartRevision2DeviceTypeTT2 = UtilityModel.createNamePartRevision(
                namePartDeviceTypeTT2, UtilityBusiness.parseDateOrNewDate("2018-02-12 17:02:11"), userAccountSuperUser,
                "Modified until recreated under control device group",
                false, namePartDeviceGroup1, ELECTROMAGNETIC_RESONATORS, "TT2",
                null, "TT2");
        namePartRevision3DeviceTypeTT2 = UtilityModel.createNamePartRevision(
                namePartDeviceTypeTT2, UtilityBusiness.parseDateOrNewDate("2018-02-12 17:15:42"), userAccountSuperUser,
                "Delete device types from Misc group that was recreated in the Control device group",
                false, namePartDeviceGroup1, ELECTROMAGNETIC_RESONATORS, "TT2",
                null, "TT2");

        namePartRevision1DeviceTypeTT2.setStatus(NamePartRevisionStatus.APPROVED);
        namePartRevision1DeviceTypeTT2.setProcessDate(UtilityBusiness.parseDateOrNewDate("2017-07-05 15:41:56"));
        namePartRevision1DeviceTypeTT2.setProcessorComment("Daniel Piso Fernandez");
        namePartRevision2DeviceTypeTT2.setStatus(NamePartRevisionStatus.APPROVED);
        namePartRevision2DeviceTypeTT2.setProcessDate(UtilityBusiness.parseDateOrNewDate(DATE_2018_02_12_17_08_31));
        namePartRevision2DeviceTypeTT2.setProcessorComment("Approved temporary names");
        namePartRevision3DeviceTypeTT2.setStatus(NamePartRevisionStatus.APPROVED);
        namePartRevision3DeviceTypeTT2.setProcessDate(UtilityBusiness.parseDateOrNewDate("2018-02-12 17:16:08"));
        namePartRevision3DeviceTypeTT2.setProcessorComment("approved delete");

        namePartRevisionDeviceGroup2 = UtilityModel.createNamePartRevision(
                namePartDeviceGroup2, UtilityBusiness.parseDateOrNewDate("2018-01-24 16:08:55"), userAccountEditor,
                "As requested by Janet Schmidt",
                false, namePartDisciplineEMR, "Control", null,
                null, null);

        namePartRevisionDeviceGroup2.setStatus(NamePartRevisionStatus.APPROVED);
        namePartRevisionDeviceGroup2.setProcessDate(UtilityBusiness.parseDateOrNewDate("2018-01-31 08:15:58"));
        namePartRevisionDeviceGroup2.setProcessorComment(
                "These names are needed now, so I am approving, but please add a description to these later.");

        namePartRevisionDeviceTypeTT = UtilityModel.createNamePartRevision(
                namePartDeviceTypeTT, UtilityBusiness.parseDateOrNewDate("2018-02-12 17:12:23"), userAccountEditor,
                "Added from Misc device group",
                false, namePartDeviceGroup2, "Temperature Transmitter", "TT",
                null, "TT");

        namePartRevisionDeviceTypeTT.setStatus(NamePartRevisionStatus.APPROVED);
        namePartRevisionDeviceTypeTT.setProcessDate(UtilityBusiness.parseDateOrNewDate("2018-02-12 17:12:51"));
        namePartRevisionDeviceTypeTT.setProcessorComment("Approve names added from misc device group");

        Whitebox.setInternalState(namePartRevisionDisciplineEMR,  ID, 239L);
        Whitebox.setInternalState(namePartRevision1DeviceGroup1,  ID, 247L);
        Whitebox.setInternalState(namePartRevision2DeviceGroup1,  ID, 1079L);
        Whitebox.setInternalState(namePartRevision1DeviceTypeTT2, ID, 2780L);
        Whitebox.setInternalState(namePartRevision2DeviceTypeTT2, ID, 3272L);
        Whitebox.setInternalState(namePartRevision3DeviceTypeTT2, ID, 3279L);
        Whitebox.setInternalState(namePartRevisionDeviceGroup2,   ID, 3212L);
        Whitebox.setInternalState(namePartRevisionDeviceTypeTT,   ID, 3277L);

        nameRevisionDisciplineEMR  = UtilityBusiness.createNameRevision(namePartRevisionDisciplineEMR);
        nameRevision1DeviceGroup1  = UtilityBusiness.createNameRevision(namePartRevision1DeviceGroup1);
        nameRevision2DeviceGroup1  = UtilityBusiness.createNameRevision(namePartRevision2DeviceGroup1);
        nameRevision1DeviceTypeTT2 = UtilityBusiness.createNameRevision(namePartRevision1DeviceTypeTT2);
        nameRevision2DeviceTypeTT2 = UtilityBusiness.createNameRevision(namePartRevision2DeviceTypeTT2);
        nameRevision3DeviceTypeTT2 = UtilityBusiness.createNameRevision(namePartRevision3DeviceTypeTT2);
        nameRevisionDeviceGroup2   = UtilityBusiness.createNameRevision(namePartRevisionDeviceGroup2);
        nameRevisionDeviceTypeTT   = UtilityBusiness.createNameRevision(namePartRevisionDeviceTypeTT);
    }

    /**
     * Set up test structure - device - name part revisions, name revisions.
     */
    private void setupNameRevisionDevice() {
        Device device001 = UtilityModel.createDevice(UUID_DEVICE_12735_INSTANCEINDEX);
        Device device010 = UtilityModel.createDevice(UUID_DEVICE_12798_INSTANCEINDEX);
        Device device011 = UtilityModel.createDevice(UUID_DEVICE_12742_INSTANCEINDEX);
        Device device012 = UtilityModel.createDevice(UUID_DEVICE_12803_INSTANCEINDEX);
        Device device013 = UtilityModel.createDevice(UUID_DEVICE_12778_INSTANCEINDEX);
        Device device014 = UtilityModel.createDevice(UUID_DEVICE_12722_INSTANCEINDEX);
        Device device015 = UtilityModel.createDevice(UUID_DEVICE_12703_INSTANCEINDEX);
        Device device016 = UtilityModel.createDevice(UUID_DEVICE_12733_INSTANCEINDEX);
        Device device017 = UtilityModel.createDevice(UUID_DEVICE_12706_INSTANCEINDEX);
        Device device018 = UtilityModel.createDevice(UUID_DEVICE_12795_INSTANCEINDEX);
        Device device019 = UtilityModel.createDevice(UUID_DEVICE_12770_INSTANCEINDEX);

        Whitebox.setInternalState(device001, ID, 12735L);
        Whitebox.setInternalState(device010, ID, 12798L);
        Whitebox.setInternalState(device011, ID, 12742L);
        Whitebox.setInternalState(device012, ID, 12803L);
        Whitebox.setInternalState(device013, ID, 12778L);
        Whitebox.setInternalState(device014, ID, 12722L);
        Whitebox.setInternalState(device015, ID, 12703L);
        Whitebox.setInternalState(device016, ID, 12733L);
        Whitebox.setInternalState(device017, ID, 12706L);
        Whitebox.setInternalState(device018, ID, 12795L);
        Whitebox.setInternalState(device019, ID, 12770L);

        // section id 178
        NamePart section     = namePartRevision2Subsystem010.getNamePart();
        // device type id 1640, 2016
        NamePart deviceType1 = namePartRevision3DeviceTypeTT2.getNamePart();
        NamePart deviceType2 = namePartRevisionDeviceTypeTT.getNamePart();

        deviceRevision1Device001 = UtilityModel.createDeviceRevision(
                device001, UtilityBusiness.parseDateOrNewDate(DATE_2017_07_05_15_44_17), userAccountEditor, false,
                section, deviceType1, "001", "RFQ-010:EMR-TT-001",
                "RFQ-10:EMR-TT-1", null, null);
        deviceRevision2Device001 = UtilityModel.createDeviceRevision(
                device001, UtilityBusiness.parseDateOrNewDate(DATE_2018_02_12_17_08_31), userAccountSuperUser, false,
                section, deviceType1, "001", "RFQ-010:EMR-TT2-001",
                "RFQ-10:EMR-TT2-1", null, null);
        deviceRevision3Device001 = UtilityModel.createDeviceRevision(
                device001, UtilityBusiness.parseDateOrNewDate("2018-02-12 17:13:48"), userAccountSuperUser, false,
                section, deviceType2, "001", "RFQ-010:EMR-TT-001",
                "RFQ-10:EMR-TT-1", null, null);

        deviceRevision1Device010 = UtilityModel.createDeviceRevision(
                device010, UtilityBusiness.parseDateOrNewDate(DATE_2017_07_05_15_44_18), userAccountEditor, false,
                section, deviceType1, "010", "RFQ-010:EMR-TT-010",
                "RFQ-10:EMR-TT-10", null, null);
        deviceRevision2Device010 = UtilityModel.createDeviceRevision(
                device010, UtilityBusiness.parseDateOrNewDate("2018-02-12 17:08:35"), userAccountSuperUser, false,
                section, deviceType1, "010", "RFQ-010:EMR-TT2-010",
                "RFQ-10:EMR-TT2-10", null, null);
        deviceRevision3Device010 = UtilityModel.createDeviceRevision(
                device010, UtilityBusiness.parseDateOrNewDate("2018-02-12 17:13:38"), userAccountSuperUser, false,
                section, deviceType2, "010", "RFQ-010:EMR-TT-010",
                "RFQ-10:EMR-TT-10", null, null);

        deviceRevision1Device011 = UtilityModel.createDeviceRevision(
                device011, UtilityBusiness.parseDateOrNewDate(DATE_2017_07_05_15_44_18), userAccountEditor, false,
                section, deviceType1, "011", "RFQ-010:EMR-TT-011",
                "RFQ-10:EMR-TT-11", null, null);
        deviceRevision2Device011 = UtilityModel.createDeviceRevision(
                device011, UtilityBusiness.parseDateOrNewDate("2018-02-12 17:08:39"), userAccountSuperUser, false,
                section, deviceType1, "011", "RFQ-010:EMR-TT2-011",
                "RFQ-10:EMR-TT2-11", null, null);
        deviceRevision3Device011 = UtilityModel.createDeviceRevision(
                device011, UtilityBusiness.parseDateOrNewDate("2018-02-12 17:13:41"), userAccountSuperUser, false,
                section, deviceType2, "011", "RFQ-010:EMR-TT-011",
                "RFQ-10:EMR-TT-11", null, null);

        deviceRevision1Device012 = UtilityModel.createDeviceRevision(
                device012, UtilityBusiness.parseDateOrNewDate(DATE_2017_07_05_15_44_18), userAccountEditor, false,
                section, deviceType1, "012", "RFQ-010:EMR-TT-012",
                "RFQ-10:EMR-TT-12", null, null);
        deviceRevision2Device012 = UtilityModel.createDeviceRevision(
                device012, UtilityBusiness.parseDateOrNewDate(DATE_2018_02_12_17_08_34), userAccountSuperUser, false,
                section, deviceType1, "012", "RFQ-010:EMR-TT2-012",
                "RFQ-10:EMR-TT2-12", null, null);
        deviceRevision3Device012 = UtilityModel.createDeviceRevision(
                device012, UtilityBusiness.parseDateOrNewDate("2018-02-12 17:13:43"), userAccountSuperUser, false,
                section, deviceType2, "012", "RFQ-010:EMR-TT-012",
                "RFQ-10:EMR-TT-12", null, null);

        deviceRevision1Device013 = UtilityModel.createDeviceRevision(
                device013, UtilityBusiness.parseDateOrNewDate(DATE_2017_07_05_15_44_18), userAccountEditor, false,
                section, deviceType1, "013", "RFQ-010:EMR-TT-013",
                "RFQ-10:EMR-TT-13", null, null);
        deviceRevision2Device013 = UtilityModel.createDeviceRevision(
                device013, UtilityBusiness.parseDateOrNewDate(DATE_2018_02_12_17_08_34), userAccountSuperUser, false,
                section, deviceType1, "013", "RFQ-010:EMR-TT2-013",
                "RFQ-10:EMR-TT2-13", null, null);
        deviceRevision3Device013 = UtilityModel.createDeviceRevision(
                device013, UtilityBusiness.parseDateOrNewDate("2018-02-12 17:13:42"), userAccountSuperUser, false,
                section, deviceType2, "013", "RFQ-010:EMR-TT-013",
                "RFQ-10:EMR-TT-13", null, null);

        deviceRevision1Device014 = UtilityModel.createDeviceRevision(
                device014, UtilityBusiness.parseDateOrNewDate(DATE_2017_07_05_15_44_17), userAccountEditor, false,
                section, deviceType1, "014", "RFQ-010:EMR-TT-014",
                "RFQ-10:EMR-TT-14", null, null);
        deviceRevision2Device014 = UtilityModel.createDeviceRevision(
                device014, UtilityBusiness.parseDateOrNewDate(DATE_2018_02_12_17_08_34), userAccountSuperUser, false,
                section, deviceType1, "014", "RFQ-010:EMR-TT2-014",
                "RFQ-10:EMR-TT2-14", null, null);
        deviceRevision3Device014 = UtilityModel.createDeviceRevision(
                device014, UtilityBusiness.parseDateOrNewDate("2018-02-12 17:13:33"), userAccountSuperUser, false,
                section, deviceType2, "014", "RFQ-010:EMR-TT-014",
                "RFQ-10:EMR-TT-14", null, null);

        deviceRevision1Device015 = UtilityModel.createDeviceRevision(
                device015, UtilityBusiness.parseDateOrNewDate(DATE_2017_07_05_15_44_17), userAccountEditor, false,
                section, deviceType1, "015", "RFQ-010:EMR-TT-015",
                "RFQ-10:EMR-TT-15", null, null);
        deviceRevision2Device015 = UtilityModel.createDeviceRevision(
                device015, UtilityBusiness.parseDateOrNewDate("2018-02-12 17:08:39"), userAccountSuperUser, false,
                section, deviceType1, "015", "RFQ-010:EMR-TT2-015",
                "RFQ-10:EMR-TT2-15", null, null);
        deviceRevision3Device015 = UtilityModel.createDeviceRevision(
                device015, UtilityBusiness.parseDateOrNewDate("2018-02-12 17:13:44"), userAccountSuperUser, false,
                section, deviceType2, "015", "RFQ-010:EMR-TT-015",
                "RFQ-10:EMR-TT-15", null, null);

        deviceRevision1Device016 = UtilityModel.createDeviceRevision(
                device016, UtilityBusiness.parseDateOrNewDate(DATE_2017_07_05_15_44_17), userAccountEditor, false,
                section, deviceType1, "016", "RFQ-010:EMR-TT-016",
                "RFQ-10:EMR-TT-16", null, null);
        deviceRevision2Device016 = UtilityModel.createDeviceRevision(
                device016, UtilityBusiness.parseDateOrNewDate("2018-02-12 17:08:38"), userAccountSuperUser, false,
                section, deviceType1, "016", "RFQ-010:EMR-TT2-016",
                "RFQ-10:EMR-TT2-16", null, null);
        deviceRevision3Device016 = UtilityModel.createDeviceRevision(
                device016, UtilityBusiness.parseDateOrNewDate("2018-02-12 17:13:50"), userAccountSuperUser, false,
                section, deviceType2, "016", "RFQ-010:EMR-TT-016",
                "RFQ-10:EMR-TT-16", null, null);

        deviceRevision1Device017 = UtilityModel.createDeviceRevision(
                device017, UtilityBusiness.parseDateOrNewDate(DATE_2017_07_05_15_44_17), userAccountEditor, false,
                section, deviceType1, "017", "RFQ-010:EMR-TT-017",
                "RFQ-10:EMR-TT-17", null, null);
        deviceRevision2Device017 = UtilityModel.createDeviceRevision(
                device017, UtilityBusiness.parseDateOrNewDate("2018-02-12 17:08:37"), userAccountSuperUser, false,
                section, deviceType1, "017", "RFQ-010:EMR-TT2-017",
                "RFQ-10:EMR-TT2-17", null, null);
        deviceRevision3Device017 = UtilityModel.createDeviceRevision(
                device017, UtilityBusiness.parseDateOrNewDate("2018-02-12 17:13:33"), userAccountSuperUser, false,
                section, deviceType2, "017", "RFQ-010:EMR-TT-017",
                "RFQ-10:EMR-TT-17", null, null);

        deviceRevision1Device018 = UtilityModel.createDeviceRevision(
                device018, UtilityBusiness.parseDateOrNewDate(DATE_2017_07_05_15_44_18), userAccountEditor, false,
                section, deviceType1, "018", "RFQ-010:EMR-TT-018",
                "RFQ-10:EMR-TT-18", null, null);
        deviceRevision2Device018 = UtilityModel.createDeviceRevision(
                device018, UtilityBusiness.parseDateOrNewDate(DATE_2018_02_12_17_08_31), userAccountSuperUser, false,
                section, deviceType1, "018", "RFQ-010:EMR-TT2-018",
                "RFQ-10:EMR-TT2-18", null, null);
        deviceRevision3Device018 = UtilityModel.createDeviceRevision(
                device018, UtilityBusiness.parseDateOrNewDate("2018-02-12 17:13:30"), userAccountSuperUser, false,
                section, deviceType2, "018", "RFQ-010:EMR-TT-018",
                "RFQ-10:EMR-TT-18", null, null);

        deviceRevision1Device019 = UtilityModel.createDeviceRevision(
                device019, UtilityBusiness.parseDateOrNewDate(DATE_2017_07_05_15_44_18), userAccountEditor, false,
                section, deviceType1, "019", "RFQ-010:EMR-TT-019",
                "RFQ-10:EMR-TT-19", null, null);
        deviceRevision2Device019 = UtilityModel.createDeviceRevision(
                device019, UtilityBusiness.parseDateOrNewDate("2018-02-12 17:08:32"), userAccountSuperUser, false,
                section, deviceType1, "019", "RFQ-010:EMR-TT2-019",
                "RFQ-10:EMR-TT2-19", null, null);
        deviceRevision3Device019 = UtilityModel.createDeviceRevision(
                device019, UtilityBusiness.parseDateOrNewDate("2018-02-12 17:13:43"), userAccountSuperUser, false,
                section, deviceType2, "019", "RFQ-010:EMR-TT-019",
                "RFQ-10:EMR-TT-19", null, null);

        Whitebox.setInternalState(deviceRevision1Device001, ID, 18177L);
        Whitebox.setInternalState(deviceRevision2Device001, ID, 70947L);
        Whitebox.setInternalState(deviceRevision3Device001, ID, 71364L);
        Whitebox.setInternalState(deviceRevision1Device010, ID, 18240L);
        Whitebox.setInternalState(deviceRevision2Device010, ID, 71043L);
        Whitebox.setInternalState(deviceRevision3Device010, ID, 71279L);
        Whitebox.setInternalState(deviceRevision1Device011, ID, 18184L);
        Whitebox.setInternalState(deviceRevision2Device011, ID, 71137L);
        Whitebox.setInternalState(deviceRevision3Device011, ID, 71312L);
        Whitebox.setInternalState(deviceRevision1Device012, ID, 18245L);
        Whitebox.setInternalState(deviceRevision2Device012, ID, 71022L);
        Whitebox.setInternalState(deviceRevision3Device012, ID, 71324L);
        Whitebox.setInternalState(deviceRevision1Device013, ID, 18220L);
        Whitebox.setInternalState(deviceRevision2Device013, ID, 71027L);
        Whitebox.setInternalState(deviceRevision3Device013, ID, 71319L);
        Whitebox.setInternalState(deviceRevision1Device014, ID, 18164L);
        Whitebox.setInternalState(deviceRevision2Device014, ID, 71035L);
        Whitebox.setInternalState(deviceRevision3Device014, ID, 71238L);
        Whitebox.setInternalState(deviceRevision1Device015, ID, 18145L);
        Whitebox.setInternalState(deviceRevision2Device015, ID, 71141L);
        Whitebox.setInternalState(deviceRevision3Device015, ID, 71332L);
        Whitebox.setInternalState(deviceRevision1Device016, ID, 18175L);
        Whitebox.setInternalState(deviceRevision2Device016, ID, 71117L);
        Whitebox.setInternalState(deviceRevision3Device016, ID, 71376L);
        Whitebox.setInternalState(deviceRevision1Device017, ID, 18148L);
        Whitebox.setInternalState(deviceRevision2Device017, ID, 71093L);
        Whitebox.setInternalState(deviceRevision3Device017, ID, 71236L);
        Whitebox.setInternalState(deviceRevision1Device018, ID, 18237L);
        Whitebox.setInternalState(deviceRevision2Device018, ID, 70938L);
        Whitebox.setInternalState(deviceRevision3Device018, ID, 71210L);
        Whitebox.setInternalState(deviceRevision1Device019, ID, 18212L);
        Whitebox.setInternalState(deviceRevision2Device019, ID, 70973L);
        Whitebox.setInternalState(deviceRevision3Device019, ID, 71320L);

        nameRevision1Device001 = UtilityBusiness.createNameRevision(deviceRevision1Device001);
        nameRevision2Device001 = UtilityBusiness.createNameRevision(deviceRevision2Device001);
        nameRevision3Device001 = UtilityBusiness.createNameRevision(deviceRevision3Device001);
        nameRevision1Device010 = UtilityBusiness.createNameRevision(deviceRevision1Device010);
        nameRevision2Device010 = UtilityBusiness.createNameRevision(deviceRevision2Device010);
        nameRevision3Device010 = UtilityBusiness.createNameRevision(deviceRevision3Device010);
        nameRevision1Device011 = UtilityBusiness.createNameRevision(deviceRevision1Device011);
        nameRevision2Device011 = UtilityBusiness.createNameRevision(deviceRevision2Device011);
        nameRevision3Device011 = UtilityBusiness.createNameRevision(deviceRevision3Device011);
        nameRevision1Device012 = UtilityBusiness.createNameRevision(deviceRevision1Device012);
        nameRevision2Device012 = UtilityBusiness.createNameRevision(deviceRevision2Device012);
        nameRevision3Device012 = UtilityBusiness.createNameRevision(deviceRevision3Device012);
        nameRevision1Device013 = UtilityBusiness.createNameRevision(deviceRevision1Device013);
        nameRevision2Device013 = UtilityBusiness.createNameRevision(deviceRevision2Device013);
        nameRevision3Device013 = UtilityBusiness.createNameRevision(deviceRevision3Device013);
        nameRevision1Device014 = UtilityBusiness.createNameRevision(deviceRevision1Device014);
        nameRevision2Device014 = UtilityBusiness.createNameRevision(deviceRevision2Device014);
        nameRevision3Device014 = UtilityBusiness.createNameRevision(deviceRevision3Device014);
        nameRevision1Device015 = UtilityBusiness.createNameRevision(deviceRevision1Device015);
        nameRevision2Device015 = UtilityBusiness.createNameRevision(deviceRevision2Device015);
        nameRevision3Device015 = UtilityBusiness.createNameRevision(deviceRevision3Device015);
        nameRevision1Device016 = UtilityBusiness.createNameRevision(deviceRevision1Device016);
        nameRevision2Device016 = UtilityBusiness.createNameRevision(deviceRevision2Device016);
        nameRevision3Device016 = UtilityBusiness.createNameRevision(deviceRevision3Device016);
        nameRevision1Device017 = UtilityBusiness.createNameRevision(deviceRevision1Device017);
        nameRevision2Device017 = UtilityBusiness.createNameRevision(deviceRevision2Device017);
        nameRevision3Device017 = UtilityBusiness.createNameRevision(deviceRevision3Device017);
        nameRevision1Device018 = UtilityBusiness.createNameRevision(deviceRevision1Device018);
        nameRevision2Device018 = UtilityBusiness.createNameRevision(deviceRevision2Device018);
        nameRevision3Device018 = UtilityBusiness.createNameRevision(deviceRevision3Device018);
        nameRevision1Device019 = UtilityBusiness.createNameRevision(deviceRevision1Device019);
        nameRevision2Device019 = UtilityBusiness.createNameRevision(deviceRevision2Device019);
        nameRevision3Device019 = UtilityBusiness.createNameRevision(deviceRevision3Device019);
    }

    /**
     * Tear down test structure - section - name part revisions, name revisions.
     */
    private void tearDownNameRevisionSection() {
        namePartRevision1SystemGroupAcc = null;
        namePartRevision2SystemGroupAcc = null;
        namePartRevision3SystemGroupAcc = null;
        nameRevision1SystemGroupAcc = null;
        nameRevision2SystemGroupAcc = null;
        nameRevision3SystemGroupAcc = null;
        // ----------
        namePartRevisionSystemRFQ = null;
        nameRevisionSystemRFQ = null;
        // ----------
        namePartRevision1Subsystem010 = null;
        namePartRevision2Subsystem010 = null;
        nameRevision1Subsystem010 = null;
        nameRevision2Subsystem010 = null;
    }

    /**
     * Tear down test structure - device type - name part revisions, name revisions.
     */
    private void tearDownNameRevisionDeviceType() {
        namePartRevisionDisciplineEMR = null;
        nameRevisionDisciplineEMR = null;
        // ----------
        namePartRevision1DeviceGroup1 = null;
        namePartRevision2DeviceGroup1 = null;
        nameRevision1DeviceGroup1 = null;
        nameRevision2DeviceGroup1 = null;
        // ----------
        namePartRevision1DeviceTypeTT2 = null;
        namePartRevision2DeviceTypeTT2 = null;
        namePartRevision3DeviceTypeTT2 = null;
        nameRevision1DeviceTypeTT2 = null;
        nameRevision2DeviceTypeTT2 = null;
        nameRevision3DeviceTypeTT2 = null;
        // --------------------
        namePartRevisionDeviceGroup2 = null;
        nameRevisionDeviceGroup2 = null;
        // ----------
        namePartRevisionDeviceTypeTT = null;
        nameRevisionDeviceTypeTT = null;
    }

    /**
     * Tear down test structure - device - device revisions, name revisions.
     */
    private void tearDownNameRevisionDevice() {
        deviceRevision1Device001 = null;
        deviceRevision2Device001 = null;
        deviceRevision3Device001 = null;
        nameRevision1Device001 = null;
        nameRevision2Device001 = null;
        nameRevision3Device001 = null;
        // ----------
        deviceRevision1Device010 = null;
        deviceRevision2Device010 = null;
        deviceRevision3Device010 = null;
        nameRevision1Device010 = null;
        nameRevision2Device010 = null;
        nameRevision3Device010 = null;
        // ----------
        deviceRevision1Device011 = null;
        deviceRevision2Device011 = null;
        deviceRevision3Device011 = null;
        nameRevision1Device011 = null;
        nameRevision2Device011 = null;
        nameRevision3Device011 = null;
        // ----------
        deviceRevision1Device012 = null;
        deviceRevision2Device012 = null;
        deviceRevision3Device012 = null;
        nameRevision1Device012 = null;
        nameRevision2Device012 = null;
        nameRevision3Device012 = null;
        // ----------
        deviceRevision1Device013 = null;
        deviceRevision2Device013 = null;
        deviceRevision3Device013 = null;
        nameRevision1Device013 = null;
        nameRevision2Device013 = null;
        nameRevision3Device013 = null;
        // ----------
        deviceRevision1Device014 = null;
        deviceRevision2Device014 = null;
        deviceRevision3Device014 = null;
        nameRevision1Device014 = null;
        nameRevision2Device014 = null;
        nameRevision3Device014 = null;
        // ----------
        deviceRevision1Device015 = null;
        deviceRevision2Device015 = null;
        deviceRevision3Device015 = null;
        nameRevision1Device015 = null;
        nameRevision2Device015 = null;
        nameRevision3Device015 = null;
        // ----------
        deviceRevision1Device016 = null;
        deviceRevision2Device016 = null;
        deviceRevision3Device016 = null;
        nameRevision1Device016 = null;
        nameRevision2Device016 = null;
        nameRevision3Device016 = null;
        // ----------
        deviceRevision1Device017 = null;
        deviceRevision2Device017 = null;
        deviceRevision3Device017 = null;
        nameRevision1Device017 = null;
        nameRevision2Device017 = null;
        nameRevision3Device017 = null;
        // ----------
        deviceRevision1Device018 = null;
        deviceRevision2Device018 = null;
        deviceRevision3Device018 = null;
        nameRevision1Device018 = null;
        nameRevision2Device018 = null;
        nameRevision3Device018 = null;
        // ----------
        deviceRevision1Device019 = null;
        deviceRevision2Device019 = null;
        deviceRevision3Device019 = null;
        nameRevision1Device019 = null;
        nameRevision2Device019 = null;
        nameRevision3Device019 = null;
    }

}
