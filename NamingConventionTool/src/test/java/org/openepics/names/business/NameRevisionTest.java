/*
 * Copyright (C) 2020 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.business;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * Unit tests for NameRevision class.
 *
 * @author Lars Johansson
 *
 * @see NameRevision
 * @see UtilityBusinessTestFixture
 */
public class NameRevisionTest {

    // Note that test fixture is handled by utility class.

    private static UtilityBusinessTestFixture testFixture;

    /**
     * One-time initialization code.
     */
    @BeforeClass
    public static void oneTimeSetUp() {
        testFixture = UtilityBusinessTestFixture.getInstance();
        testFixture.setUp();
    }

    /**
     * One-time cleanup code.
     */
    @AfterClass
    public static void oneTimeTearDown() {
        testFixture.tearDown();
        testFixture = null;
    }

    // ----------------------------------------------------------------------------------------------------

    /**
     * Test getParent method of NameRevision.
     */
    @Test
    public void getParent() {
        assertNull(testFixture.getNameRevisionSection11().getParent(NameType.SYSTEM_STRUCTURE));
        assertNull(testFixture.getNameRevisionSection12().getParent(NameType.SYSTEM_STRUCTURE));
        assertNull(testFixture.getNameRevisionSection13().getParent(NameType.SYSTEM_STRUCTURE));
        assertEquals(
                UtilityBusinessTestFixture.UUID_NAMEPART_1,
                testFixture.getNameRevisionSection21().getParent(NameType.SYSTEM_STRUCTURE).getUuid().toString());
        assertEquals(
                UtilityBusinessTestFixture.UUID_NAMEPART_1,
                testFixture.getNameRevisionSection22().getParent(NameType.SYSTEM_STRUCTURE).getUuid().toString());
        assertEquals(
                UtilityBusinessTestFixture.UUID_NAMEPART_1,
                testFixture.getNameRevisionSection23().getParent(NameType.SYSTEM_STRUCTURE).getUuid().toString());
        assertEquals(
                UtilityBusinessTestFixture.UUID_NAMEPART_1,
                testFixture.getNameRevisionSection24().getParent(NameType.SYSTEM_STRUCTURE).getUuid().toString());
        assertEquals(
                UtilityBusinessTestFixture.UUID_NAMEPART_13,
                testFixture.getNameRevisionSection31().getParent(NameType.SYSTEM_STRUCTURE).getUuid().toString());
        assertEquals(
                UtilityBusinessTestFixture.UUID_NAMEPART_13,
                testFixture.getNameRevisionSection32().getParent(NameType.SYSTEM_STRUCTURE).getUuid().toString());
        assertEquals(
                UtilityBusinessTestFixture.UUID_NAMEPART_13,
                testFixture.getNameRevisionSection33().getParent(NameType.SYSTEM_STRUCTURE).getUuid().toString());

        assertNull(testFixture.getNameRevisionSection11().getParent(NameType.DEVICE_STRUCTURE));
        assertNull(testFixture.getNameRevisionSection12().getParent(NameType.DEVICE_STRUCTURE));
        assertNull(testFixture.getNameRevisionSection13().getParent(NameType.DEVICE_STRUCTURE));
        assertNull(testFixture.getNameRevisionSection21().getParent(NameType.DEVICE_STRUCTURE));
        assertNull(testFixture.getNameRevisionSection22().getParent(NameType.DEVICE_STRUCTURE));
        assertNull(testFixture.getNameRevisionSection23().getParent(NameType.DEVICE_STRUCTURE));
        assertNull(testFixture.getNameRevisionSection24().getParent(NameType.DEVICE_STRUCTURE));
        assertNull(testFixture.getNameRevisionSection31().getParent(NameType.DEVICE_STRUCTURE));
        assertNull(testFixture.getNameRevisionSection32().getParent(NameType.DEVICE_STRUCTURE));
        assertNull(testFixture.getNameRevisionSection33().getParent(NameType.DEVICE_STRUCTURE));

        assertNull(testFixture.getNameRevisionDeviceType1().getParent(NameType.SYSTEM_STRUCTURE));
        assertNull(testFixture.getNameRevisionDeviceType2().getParent(NameType.SYSTEM_STRUCTURE));
        assertNull(testFixture.getNameRevisionDeviceType3().getParent(NameType.SYSTEM_STRUCTURE));
        assertNull(testFixture.getNameRevisionDeviceType1().getParent(NameType.DEVICE_STRUCTURE));
        assertNull(testFixture.getNameRevisionDeviceType2().getParent(NameType.DEVICE_STRUCTURE));
        assertNull(testFixture.getNameRevisionDeviceType3().getParent(NameType.DEVICE_STRUCTURE));

        assertEquals(
                UtilityBusinessTestFixture.UUID_NAMEPART_2692,
                testFixture.getNameRevisionDevice1().getParent(NameType.SYSTEM_STRUCTURE).getUuid().toString());
        assertEquals(
                UtilityBusinessTestFixture.UUID_NAMEPART_2692,
                testFixture.getNameRevisionDevice2().getParent(NameType.SYSTEM_STRUCTURE).getUuid().toString());
        assertEquals(
                UtilityBusinessTestFixture.UUID_NAMEPART_2692,
                testFixture.getNameRevisionDevice3().getParent(NameType.SYSTEM_STRUCTURE).getUuid().toString());
        assertEquals(
                UtilityBusinessTestFixture.UUID_NAMEPART_2765,
                testFixture.getNameRevisionDevice1().getParent(NameType.DEVICE_STRUCTURE).getUuid().toString());
        assertEquals(
                UtilityBusinessTestFixture.UUID_NAMEPART_2765,
                testFixture.getNameRevisionDevice2().getParent(NameType.DEVICE_STRUCTURE).getUuid().toString());
        assertEquals(
                UtilityBusinessTestFixture.UUID_NAMEPART_2886,
                testFixture.getNameRevisionDevice3().getParent(NameType.DEVICE_STRUCTURE).getUuid().toString());
    }

    /**
     * Test getUserAction method of NameRevision.
     */
    @Test
    public void getUserAction() {
        assertEquals(
                null,
                testFixture.getNameRevisionSection11().getUserAction().getUser());
        assertEquals(
                testFixture.getUserAccountSuperUser(),
                testFixture.getNameRevisionSection12().getUserAction().getUser());
        assertEquals(
                testFixture.getUserAccountSuperUser(),
                testFixture.getNameRevisionSection13().getUserAction().getUser());
        assertEquals(
                null,
                testFixture.getNameRevisionSection21().getUserAction().getUser());
        assertEquals(
                testFixture.getUserAccountSuperUser(),
                testFixture.getNameRevisionSection22().getUserAction().getUser());
        assertEquals(
                testFixture.getUserAccountSuperUser(),
                testFixture.getNameRevisionSection23().getUserAction().getUser());
        assertEquals(
                testFixture.getUserAccountSuperUser(),
                testFixture.getNameRevisionSection24().getUserAction().getUser());
        assertEquals(
                testFixture.getUserAccountSuperUser(),
                testFixture.getNameRevisionSection31().getUserAction().getUser());
        assertEquals(
                testFixture.getUserAccountEditor(),
                testFixture.getNameRevisionSection32().getUserAction().getUser());
        assertEquals(
                testFixture.getUserAccountEditor(),
                testFixture.getNameRevisionSection33().getUserAction().getUser());

        assertEquals(
                null,
                testFixture.getNameRevisionDeviceType1().getUserAction().getUser());
        assertEquals(
                testFixture.getUserAccountEditor(),
                testFixture.getNameRevisionDeviceType2().getUserAction().getUser());
        assertEquals(
                testFixture.getUserAccountSuperUser(),
                testFixture.getNameRevisionDeviceType3().getUserAction().getUser());

        assertEquals(
                testFixture.getUserAccountEditor(),
                testFixture.getNameRevisionDevice1().getUserAction().getUser());
        assertEquals(
                testFixture.getUserAccountEditor(),
                testFixture.getNameRevisionDevice2().getUserAction().getUser());
        assertEquals(
                testFixture.getUserAccountEditor(),
                testFixture.getNameRevisionDevice3().getUserAction().getUser());
    }

    /**
     * Test getProcess method of NameRevision.
     */
    @Test
    public void getProcess() {
        assertNotNull(testFixture.getNameRevisionSection11().getProcess());
        assertNotNull(testFixture.getNameRevisionSection12().getProcess());
        assertNotNull(testFixture.getNameRevisionSection13().getProcess());
        assertNotNull(testFixture.getNameRevisionSection21().getProcess());
        assertNotNull(testFixture.getNameRevisionSection22().getProcess());
        assertNotNull(testFixture.getNameRevisionSection23().getProcess());
        assertNotNull(testFixture.getNameRevisionSection24().getProcess());
        assertNotNull(testFixture.getNameRevisionSection31().getProcess());
        assertNotNull(testFixture.getNameRevisionSection32().getProcess());
        assertNotNull(testFixture.getNameRevisionSection33().getProcess());

        assertNotNull(testFixture.getNameRevisionDeviceType1().getProcess());
        assertNotNull(testFixture.getNameRevisionDeviceType2().getProcess());
        assertNotNull(testFixture.getNameRevisionDeviceType3().getProcess());

        assertNull(testFixture.getNameRevisionDevice1().getProcess());
        assertNull(testFixture.getNameRevisionDevice2().getProcess());
        assertNull(testFixture.getNameRevisionDevice3().getProcess());
    }

    /**
     * Test getStatus method of NameRevision.
     */
    @Test
    public void getStatus() {
        assertEquals(NameRevisionStatus.PENDING,  testFixture.getNameRevisionSection11().getStatus());
        assertEquals(NameRevisionStatus.PENDING,  testFixture.getNameRevisionSection12().getStatus());
        assertEquals(NameRevisionStatus.PENDING,  testFixture.getNameRevisionSection13().getStatus());
        assertEquals(NameRevisionStatus.PENDING,  testFixture.getNameRevisionSection21().getStatus());
        assertEquals(NameRevisionStatus.PENDING,  testFixture.getNameRevisionSection22().getStatus());
        assertEquals(NameRevisionStatus.PENDING,  testFixture.getNameRevisionSection23().getStatus());
        assertEquals(NameRevisionStatus.PENDING,  testFixture.getNameRevisionSection24().getStatus());
        assertEquals(NameRevisionStatus.PENDING,  testFixture.getNameRevisionSection31().getStatus());
        assertEquals(NameRevisionStatus.PENDING,  testFixture.getNameRevisionSection32().getStatus());
        assertEquals(NameRevisionStatus.PENDING,  testFixture.getNameRevisionSection33().getStatus());

        assertEquals(NameRevisionStatus.PENDING,  testFixture.getNameRevisionDeviceType1().getStatus());
        assertEquals(NameRevisionStatus.PENDING,  testFixture.getNameRevisionDeviceType2().getStatus());
        assertEquals(NameRevisionStatus.PENDING,  testFixture.getNameRevisionDeviceType3().getStatus());

        assertEquals(NameRevisionStatus.APPROVED, testFixture.getNameRevisionDevice1().getStatus());
        assertEquals(NameRevisionStatus.APPROVED, testFixture.getNameRevisionDevice2().getStatus());
        assertEquals(NameRevisionStatus.APPROVED, testFixture.getNameRevisionDevice3().getStatus());
    }

    /**
     * Test getDate method of NameRevision.
     */
    @Test
    public void getDate() {
        assertNotNull(testFixture.getNameRevisionSection11().getDate());
        assertNotNull(testFixture.getNameRevisionSection12().getDate());
        assertNotNull(testFixture.getNameRevisionSection13().getDate());
        assertNotNull(testFixture.getNameRevisionSection21().getDate());
        assertNotNull(testFixture.getNameRevisionSection22().getDate());
        assertNotNull(testFixture.getNameRevisionSection23().getDate());
        assertNotNull(testFixture.getNameRevisionSection24().getDate());
        assertNotNull(testFixture.getNameRevisionSection31().getDate());
        assertNotNull(testFixture.getNameRevisionSection32().getDate());
        assertNotNull(testFixture.getNameRevisionSection33().getDate());

        assertNotNull(testFixture.getNameRevisionDeviceType1().getDate());
        assertNotNull(testFixture.getNameRevisionDeviceType2().getDate());
        assertNotNull(testFixture.getNameRevisionDeviceType3().getDate());

        assertNotNull(testFixture.getNameRevisionDevice1().getDate());
        assertNotNull(testFixture.getNameRevisionDevice2().getDate());
        assertNotNull(testFixture.getNameRevisionDevice3().getDate());
    }

    /**
     * Test supersede method of NameRevision.
     */
    @Test
    public void supersede() {
        assertNotNull(testFixture.getNameRevisionSection11());
        assertNotNull(testFixture.getNameRevisionSection12());
        assertNotNull(testFixture.getNameRevisionSection13());
        assertNotNull(testFixture.getNameRevisionSection21());
        assertNotNull(testFixture.getNameRevisionSection22());
        assertNotNull(testFixture.getNameRevisionSection23());
        assertNotNull(testFixture.getNameRevisionSection24());
        assertNotNull(testFixture.getNameRevisionSection31());
        assertNotNull(testFixture.getNameRevisionSection32());
        assertNotNull(testFixture.getNameRevisionSection33());

        assertTrue(testFixture.getNameRevisionSection13().supersede(null));
        assertTrue(testFixture.getNameRevisionSection12().supersede(null));
        assertTrue(testFixture.getNameRevisionSection11().supersede(null));
        assertTrue(testFixture.getNameRevisionSection13().supersede(testFixture.getNameRevisionSection12()));
        assertTrue(testFixture.getNameRevisionSection13().supersede(testFixture.getNameRevisionSection11()));
        assertTrue(testFixture.getNameRevisionSection12().supersede(testFixture.getNameRevisionSection11()));

        assertTrue(testFixture.getNameRevisionSection24().supersede(null));
        assertTrue(testFixture.getNameRevisionSection23().supersede(null));
        assertTrue(testFixture.getNameRevisionSection22().supersede(null));
        assertTrue(testFixture.getNameRevisionSection21().supersede(null));
        assertTrue(testFixture.getNameRevisionSection24().supersede(testFixture.getNameRevisionSection23()));
        assertTrue(testFixture.getNameRevisionSection24().supersede(testFixture.getNameRevisionSection22()));
        assertTrue(testFixture.getNameRevisionSection24().supersede(testFixture.getNameRevisionSection21()));
        assertTrue(testFixture.getNameRevisionSection23().supersede(testFixture.getNameRevisionSection22()));
        assertTrue(testFixture.getNameRevisionSection23().supersede(testFixture.getNameRevisionSection21()));
        assertTrue(testFixture.getNameRevisionSection22().supersede(testFixture.getNameRevisionSection21()));

        assertTrue(testFixture.getNameRevisionSection33().supersede(null));
        assertTrue(testFixture.getNameRevisionSection32().supersede(null));
        assertTrue(testFixture.getNameRevisionSection31().supersede(null));
        assertTrue(testFixture.getNameRevisionSection33().supersede(testFixture.getNameRevisionSection32()));
        assertTrue(testFixture.getNameRevisionSection33().supersede(testFixture.getNameRevisionSection31()));
        assertTrue(testFixture.getNameRevisionSection32().supersede(testFixture.getNameRevisionSection31()));

        assertNotNull(testFixture.getNameRevisionDeviceType1());
        assertNotNull(testFixture.getNameRevisionDeviceType2());
        assertNotNull(testFixture.getNameRevisionDeviceType3());

        assertTrue(testFixture.getNameRevisionDeviceType3().supersede(null));
        assertTrue(testFixture.getNameRevisionDeviceType3().supersede(null));
        assertTrue(testFixture.getNameRevisionDeviceType2().supersede(null));
        assertTrue(testFixture.getNameRevisionDeviceType3().supersede(testFixture.getNameRevisionDeviceType2()));
        assertTrue(testFixture.getNameRevisionDeviceType3().supersede(testFixture.getNameRevisionDeviceType1()));
        assertTrue(testFixture.getNameRevisionDeviceType2().supersede(testFixture.getNameRevisionDeviceType1()));

        assertNotNull(testFixture.getNameRevisionDevice1());
        assertNotNull(testFixture.getNameRevisionDevice2());
        assertNotNull(testFixture.getNameRevisionDevice3());

        assertTrue(testFixture.getNameRevisionDevice3().supersede(null));
        assertTrue(testFixture.getNameRevisionDevice3().supersede(null));
        assertTrue(testFixture.getNameRevisionDevice2().supersede(null));
        assertTrue(testFixture.getNameRevisionDevice3().supersede(testFixture.getNameRevisionDevice2()));
        assertTrue(testFixture.getNameRevisionDevice3().supersede(testFixture.getNameRevisionDevice1()));
        assertTrue(testFixture.getNameRevisionDevice2().supersede(testFixture.getNameRevisionDevice1()));
    }

    /**
     * Test isSameAs method of NameRevision.
     */
    @Test
    public void isSameAs() {
        assertNotNull(testFixture.getNameRevisionSection11());
        assertNotNull(testFixture.getNameRevisionSection12());
        assertNotNull(testFixture.getNameRevisionSection13());
        assertNotNull(testFixture.getNameRevisionSection21());
        assertNotNull(testFixture.getNameRevisionSection22());
        assertNotNull(testFixture.getNameRevisionSection23());
        assertNotNull(testFixture.getNameRevisionSection24());
        assertNotNull(testFixture.getNameRevisionSection31());
        assertNotNull(testFixture.getNameRevisionSection32());
        assertNotNull(testFixture.getNameRevisionSection33());

        assertFalse(testFixture.getNameRevisionSection13().isSameAs(null));
        assertFalse(testFixture.getNameRevisionSection12().isSameAs(null));
        assertFalse(testFixture.getNameRevisionSection11().isSameAs(null));
        assertFalse(testFixture.getNameRevisionSection13().isSameAs(testFixture.getNameRevisionSection12()));
        assertFalse(testFixture.getNameRevisionSection13().isSameAs(testFixture.getNameRevisionSection11()));
        assertFalse(testFixture.getNameRevisionSection12().isSameAs(testFixture.getNameRevisionSection11()));

        assertFalse(testFixture.getNameRevisionSection24().isSameAs(null));
        assertFalse(testFixture.getNameRevisionSection23().isSameAs(null));
        assertFalse(testFixture.getNameRevisionSection22().isSameAs(null));
        assertFalse(testFixture.getNameRevisionSection21().isSameAs(null));
        assertFalse(testFixture.getNameRevisionSection24().isSameAs(testFixture.getNameRevisionSection23()));
        assertFalse(testFixture.getNameRevisionSection24().isSameAs(testFixture.getNameRevisionSection22()));
        assertFalse(testFixture.getNameRevisionSection24().isSameAs(testFixture.getNameRevisionSection21()));
        assertFalse(testFixture.getNameRevisionSection23().isSameAs(testFixture.getNameRevisionSection22()));
        assertFalse(testFixture.getNameRevisionSection23().isSameAs(testFixture.getNameRevisionSection21()));
        assertFalse(testFixture.getNameRevisionSection22().isSameAs(testFixture.getNameRevisionSection21()));

        assertFalse(testFixture.getNameRevisionSection33().isSameAs(null));
        assertFalse(testFixture.getNameRevisionSection32().isSameAs(null));
        assertFalse(testFixture.getNameRevisionSection31().isSameAs(null));
        assertFalse(testFixture.getNameRevisionSection33().isSameAs(testFixture.getNameRevisionSection32()));
        assertFalse(testFixture.getNameRevisionSection33().isSameAs(testFixture.getNameRevisionSection31()));
        assertFalse(testFixture.getNameRevisionSection32().isSameAs(testFixture.getNameRevisionSection31()));

        assertNotNull(testFixture.getNameRevisionDeviceType1());
        assertNotNull(testFixture.getNameRevisionDeviceType2());
        assertNotNull(testFixture.getNameRevisionDeviceType3());

        assertFalse(testFixture.getNameRevisionDeviceType3().isSameAs(null));
        assertFalse(testFixture.getNameRevisionDeviceType2().isSameAs(null));
        assertFalse(testFixture.getNameRevisionDeviceType1().isSameAs(null));
        assertFalse(testFixture.getNameRevisionDeviceType3().isSameAs(testFixture.getNameRevisionDeviceType2()));
        assertFalse(testFixture.getNameRevisionDeviceType3().isSameAs(testFixture.getNameRevisionDeviceType1()));
        assertFalse(testFixture.getNameRevisionDeviceType2().isSameAs(testFixture.getNameRevisionDeviceType1()));

        assertNotNull(testFixture.getNameRevisionDevice1());
        assertNotNull(testFixture.getNameRevisionDevice2());
        assertNotNull(testFixture.getNameRevisionDevice3());

        assertFalse(testFixture.getNameRevisionDevice3().isSameAs(null));
        assertFalse(testFixture.getNameRevisionDevice2().isSameAs(null));
        assertFalse(testFixture.getNameRevisionDevice1().isSameAs(null));
        assertFalse(testFixture.getNameRevisionDevice2().isSameAs(testFixture.getNameRevisionDevice1()));
        assertFalse(testFixture.getNameRevisionDevice3().isSameAs(testFixture.getNameRevisionDevice1()));
        assertFalse(testFixture.getNameRevisionDevice3().isSameAs(testFixture.getNameRevisionDevice2()));
    }

    /**
     * Test sameArtifact method of NameRevision.
     */
    @Test
    public void sameArtifact() {
        assertNotNull(testFixture.getNameRevisionSection11());
        assertNotNull(testFixture.getNameRevisionSection12());
        assertNotNull(testFixture.getNameRevisionSection13());
        assertNotNull(testFixture.getNameRevisionSection21());
        assertNotNull(testFixture.getNameRevisionSection22());
        assertNotNull(testFixture.getNameRevisionSection23());
        assertNotNull(testFixture.getNameRevisionSection24());
        assertNotNull(testFixture.getNameRevisionSection31());
        assertNotNull(testFixture.getNameRevisionSection32());
        assertNotNull(testFixture.getNameRevisionSection33());

        assertFalse(testFixture.getNameRevisionSection13().sameArtifact(null));
        assertFalse(testFixture.getNameRevisionSection12().sameArtifact(null));
        assertFalse(testFixture.getNameRevisionSection11().sameArtifact(null));
        assertTrue(testFixture.getNameRevisionSection13().sameArtifact(testFixture.getNameRevisionSection12()));
        assertTrue(testFixture.getNameRevisionSection13().sameArtifact(testFixture.getNameRevisionSection11()));
        assertTrue(testFixture.getNameRevisionSection12().sameArtifact(testFixture.getNameRevisionSection11()));

        assertFalse(testFixture.getNameRevisionSection24().sameArtifact(null));
        assertFalse(testFixture.getNameRevisionSection23().sameArtifact(null));
        assertFalse(testFixture.getNameRevisionSection22().sameArtifact(null));
        assertFalse(testFixture.getNameRevisionSection21().sameArtifact(null));
        assertTrue(testFixture.getNameRevisionSection24().sameArtifact(testFixture.getNameRevisionSection23()));
        assertTrue(testFixture.getNameRevisionSection24().sameArtifact(testFixture.getNameRevisionSection22()));
        assertTrue(testFixture.getNameRevisionSection24().sameArtifact(testFixture.getNameRevisionSection21()));
        assertTrue(testFixture.getNameRevisionSection23().sameArtifact(testFixture.getNameRevisionSection22()));
        assertTrue(testFixture.getNameRevisionSection23().sameArtifact(testFixture.getNameRevisionSection21()));
        assertTrue(testFixture.getNameRevisionSection22().sameArtifact(testFixture.getNameRevisionSection21()));

        assertFalse(testFixture.getNameRevisionSection33().sameArtifact(null));
        assertFalse(testFixture.getNameRevisionSection32().sameArtifact(null));
        assertFalse(testFixture.getNameRevisionSection31().sameArtifact(null));
        assertTrue(testFixture.getNameRevisionSection33().sameArtifact(testFixture.getNameRevisionSection32()));
        assertTrue(testFixture.getNameRevisionSection33().sameArtifact(testFixture.getNameRevisionSection31()));
        assertTrue(testFixture.getNameRevisionSection32().sameArtifact(testFixture.getNameRevisionSection31()));

        assertNotNull(testFixture.getNameRevisionDeviceType1());
        assertNotNull(testFixture.getNameRevisionDeviceType2());
        assertNotNull(testFixture.getNameRevisionDeviceType3());

        assertFalse(testFixture.getNameRevisionDeviceType3().sameArtifact(null));
        assertFalse(testFixture.getNameRevisionDeviceType2().sameArtifact(null));
        assertFalse(testFixture.getNameRevisionDeviceType1().sameArtifact(null));
        assertTrue(testFixture.getNameRevisionDeviceType3().sameArtifact(testFixture.getNameRevisionDeviceType2()));
        assertTrue(testFixture.getNameRevisionDeviceType3().sameArtifact(testFixture.getNameRevisionDeviceType1()));
        assertTrue(testFixture.getNameRevisionDeviceType2().sameArtifact(testFixture.getNameRevisionDeviceType1()));

        assertNotNull(testFixture.getNameRevisionDevice1());
        assertNotNull(testFixture.getNameRevisionDevice2());
        assertNotNull(testFixture.getNameRevisionDevice3());

        assertFalse(testFixture.getNameRevisionDevice3().sameArtifact(null));
        assertFalse(testFixture.getNameRevisionDevice2().sameArtifact(null));
        assertFalse(testFixture.getNameRevisionDevice1().sameArtifact(null));
        assertTrue(testFixture.getNameRevisionDevice3().sameArtifact(testFixture.getNameRevisionDevice2()));
        assertTrue(testFixture.getNameRevisionDevice3().sameArtifact(testFixture.getNameRevisionDevice1()));
        assertTrue(testFixture.getNameRevisionDevice2().sameArtifact(testFixture.getNameRevisionDevice1()));
    }

    /**
     * Test isValidationNeededForProposedChanges method of NameRevision.
     */
    @Test
    public void isValidationNeededForProposedChanges() {
        assertNotNull(testFixture.getNameRevisionSection11());
        assertNotNull(testFixture.getNameRevisionSection12());
        assertNotNull(testFixture.getNameRevisionSection13());
        assertNotNull(testFixture.getNameRevisionSection21());
        assertNotNull(testFixture.getNameRevisionSection22());
        assertNotNull(testFixture.getNameRevisionSection23());
        assertNotNull(testFixture.getNameRevisionSection24());
        assertNotNull(testFixture.getNameRevisionSection31());
        assertNotNull(testFixture.getNameRevisionSection32());
        assertNotNull(testFixture.getNameRevisionSection33());

        assertTrue (testFixture.getNameRevisionSection13()
                .isValidationNeededForProposedChanges(null));
        assertTrue (testFixture.getNameRevisionSection12()
                .isValidationNeededForProposedChanges(null));
        assertTrue (testFixture.getNameRevisionSection11()
                .isValidationNeededForProposedChanges(null));
        assertTrue (testFixture.getNameRevisionSection13()
                .isValidationNeededForProposedChanges(testFixture.getNameRevisionSection12()));
        assertFalse(testFixture.getNameRevisionSection13()
                .isValidationNeededForProposedChanges(testFixture.getNameRevisionSection11()));
        assertTrue (testFixture.getNameRevisionSection12()
                .isValidationNeededForProposedChanges(testFixture.getNameRevisionSection11()));

        assertTrue (testFixture.getNameRevisionSection24()
                .isValidationNeededForProposedChanges(null));
        assertTrue (testFixture.getNameRevisionSection23()
                .isValidationNeededForProposedChanges(null));
        assertTrue (testFixture.getNameRevisionSection22()
                .isValidationNeededForProposedChanges(null));
        assertTrue (testFixture.getNameRevisionSection21()
                .isValidationNeededForProposedChanges(null));
        assertFalse(testFixture.getNameRevisionSection24()
                .isValidationNeededForProposedChanges(testFixture.getNameRevisionSection23()));
        assertTrue (testFixture.getNameRevisionSection24()
                .isValidationNeededForProposedChanges(testFixture.getNameRevisionSection22()));
        assertTrue (testFixture.getNameRevisionSection24()
                .isValidationNeededForProposedChanges(testFixture.getNameRevisionSection21()));
        assertTrue (testFixture.getNameRevisionSection23()
                .isValidationNeededForProposedChanges(testFixture.getNameRevisionSection22()));
        assertTrue (testFixture.getNameRevisionSection23()
                .isValidationNeededForProposedChanges(testFixture.getNameRevisionSection21()));
        assertTrue (testFixture.getNameRevisionSection22()
                .isValidationNeededForProposedChanges(testFixture.getNameRevisionSection21()));

        assertTrue (testFixture.getNameRevisionSection33()
                .isValidationNeededForProposedChanges(null));
        assertTrue (testFixture.getNameRevisionSection32()
                .isValidationNeededForProposedChanges(null));
        assertTrue (testFixture.getNameRevisionSection31()
                .isValidationNeededForProposedChanges(null));
        assertTrue (testFixture.getNameRevisionSection33()
                .isValidationNeededForProposedChanges(testFixture.getNameRevisionSection32()));
        assertFalse(testFixture.getNameRevisionSection33()
                .isValidationNeededForProposedChanges(testFixture.getNameRevisionSection31()));
        assertTrue (testFixture.getNameRevisionSection32()
                .isValidationNeededForProposedChanges(testFixture.getNameRevisionSection31()));

        assertNotNull(testFixture.getNameRevisionDeviceType1());
        assertNotNull(testFixture.getNameRevisionDeviceType2());
        assertNotNull(testFixture.getNameRevisionDeviceType3());

        assertTrue (testFixture.getNameRevisionDeviceType3()
                .isValidationNeededForProposedChanges(null));
        assertTrue (testFixture.getNameRevisionDeviceType2()
                .isValidationNeededForProposedChanges(null));
        assertTrue (testFixture.getNameRevisionDeviceType1()
                .isValidationNeededForProposedChanges(null));
        assertTrue (testFixture.getNameRevisionDeviceType3()
                .isValidationNeededForProposedChanges(testFixture.getNameRevisionDeviceType2()));
        assertFalse(testFixture.getNameRevisionDeviceType3()
                .isValidationNeededForProposedChanges(testFixture.getNameRevisionDeviceType1()));
        assertTrue (testFixture.getNameRevisionDeviceType2()
                .isValidationNeededForProposedChanges(testFixture.getNameRevisionDeviceType1()));

        assertNotNull(testFixture.getNameRevisionDevice1());
        assertNotNull(testFixture.getNameRevisionDevice2());
        assertNotNull(testFixture.getNameRevisionDevice3());

        assertTrue(testFixture.getNameRevisionDevice3()
                .isValidationNeededForProposedChanges(null));
        assertTrue(testFixture.getNameRevisionDevice2()
                .isValidationNeededForProposedChanges(null));
        assertTrue(testFixture.getNameRevisionDevice1()
                .isValidationNeededForProposedChanges(null));
        assertTrue(testFixture.getNameRevisionDevice3()
                .isValidationNeededForProposedChanges(testFixture.getNameRevisionDevice2()));
        assertTrue(testFixture.getNameRevisionDevice3()
                .isValidationNeededForProposedChanges(testFixture.getNameRevisionDevice1()));
        assertTrue(testFixture.getNameRevisionDevice2()
                .isValidationNeededForProposedChanges(testFixture.getNameRevisionDevice1()));
    }

}
