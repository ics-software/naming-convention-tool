/*
 * Copyright (C) 2020 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.business;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.openepics.names.model.NamePartType;

/**
 * Unit tests for NameType class.
 *
 * @author Lars Johansson
 *
 * @see NameType
 */
public class NameTypeTest {

    private static final String SYSTEM_STRUCTURE_VALUE = "systemStructure";
    private static final String DEVICE_STRUCTURE_VALUE = "deviceStructure";
    private static final String DEVICE_NAME_VALUE      = "deviceName";

    /**
     * Test toString method of NameType.
     */
    @Test
    public void toStringTest() {
        assertEquals(SYSTEM_STRUCTURE_VALUE,    NameType.SYSTEM_STRUCTURE.toString());
        assertEquals(DEVICE_STRUCTURE_VALUE,    NameType.DEVICE_STRUCTURE.toString());
        assertEquals(DEVICE_NAME_VALUE,         NameType.DEVICE_REGISTRY.toString());
    }

    /**
     * Test get method of NameType.
     */
    @Test(expected = IllegalArgumentException.class)
    public void getEmpty() {
        NameType.get("");
    }

    /**
     * Test get method of NameType.
     */
    @Test(expected = IllegalArgumentException.class)
    public void getString() {
        NameType.get("asdf");
    }

    /**
     * Test isSystemStructure method of NameType.
     */
    @Test
    public void isSystemStructure() {
        assertTrue (NameType.SYSTEM_STRUCTURE.isSystemStructure());
        assertFalse(NameType.DEVICE_STRUCTURE.isSystemStructure());
        assertFalse(NameType.DEVICE_REGISTRY.isSystemStructure());
    }

    /**
     * Test isDeviceStructure method of NameType.
     */
    @Test
    public void isDeviceStructure() {
        assertFalse(NameType.SYSTEM_STRUCTURE.isDeviceStructure());
        assertTrue (NameType.DEVICE_STRUCTURE.isDeviceStructure());
        assertFalse(NameType.DEVICE_REGISTRY.isDeviceStructure());
    }

    /**
     * Test isDeviceRegistry method of NameType.
     */
    @Test
    public void isDeviceRegistry() {
        assertFalse(NameType.SYSTEM_STRUCTURE.isDeviceRegistry());
        assertFalse(NameType.DEVICE_STRUCTURE.isDeviceRegistry());
        assertTrue (NameType.DEVICE_REGISTRY.isDeviceRegistry());
    }

    /**
     * Test asNamePartType method of NameType.
     */
    @Test
    public void asNamePartType() {
        assertEquals(NamePartType.SECTION,      NameType.SYSTEM_STRUCTURE.asNamePartType());
        assertEquals(NamePartType.DEVICE_TYPE,  NameType.DEVICE_STRUCTURE.asNamePartType());
        assertNull(NameType.DEVICE_REGISTRY.asNamePartType());
    }

}
