/*
 * Copyright (C) 2020 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.business;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.openepics.names.model.Device;
import org.openepics.names.model.DeviceRevision;
import org.openepics.names.model.NamePart;
import org.openepics.names.model.NamePartRevision;
import org.openepics.names.model.UserAccount;
import org.openepics.names.model.UtilityModel;
import org.powermock.reflect.Whitebox;

/**
 * Purpose to provide test fixture that can be used in multiple test classes and tests.
 *
 * <br>
 * Intended usage is
 * <ol>
 * <li> <tt>getInstance</tt> method followed by </tt>setUp</tt> method to set up test fixture.
 * <li> <tt>get</tt> methods for tests
 * <li> <tt>tearDown</tt> method to tear down test fixture.
 * </ol>
 *
 * @author Lars Johansson
 *
 * @see UtilityBusiness
 * @see UtilityModel
 */
public class UtilityBusinessTestFixture {

    /*
       Test fixture for section, device type, device based on database backup Sep 10 2020.
           -----------------------------------------
           Accelerator
           -----------------------------------------
           conventionNameEqClass  conventionName
           -----------------------------------------
           ACC-HEBT-70DRF         Acc-HEBT-070Drf    (section)
           WTRC                   WtrC               (device type)
           TD-D22:CTR1-MCH-1      TD-D22:Ctrl-MCH-1  (device)

       Note
           includes name revision hierarchy/history for section, device type but not device
           section, device type, device are not associated
           purpose for selection of names
               include structures that are not connected (system structure, device structure, device registry)
    */

    public static final String ACCELERATOR           = "Accelerator";
    public static final String INITIAL_DATA          = "Initial data";
    public static final String MNEMONICEQCLASS_70DRF = "70DRF";
    public static final String MTCA_HUB              = "MTCA hub";
    public static final String NAME_07_DRIFT         = "07 Drift";
    public static final String TD_D22_CTRL_MCH_1     = "TD-D22:Ctrl-MCH-1";
    public static final String TD_D22_CTR1_MCH_1     = "TD-D22:CTR1-MCH-1";
    public static final String TD_D22_TS_MCH_01      = "TD-D22:TS-MCH-01";
    public static final String TD_D22_TS_MCH_1       = "TD-D22:TS-MCH-1";
    public static final String WATER_COOLING         = "Water cooling";

    public static final String ASDF = "asdf";
    public static final String ZXCV = "zxcv";

    private static final String ID = "id";

    // ----------------------------------------------------------------------------------------------------

    private static UtilityBusinessTestFixture instance;

    /**
     * Returns the singleton instance of this class.
     *
     * @return the singletone instance
     */
    public static synchronized UtilityBusinessTestFixture getInstance() {
        if (instance == null) {
            instance = new UtilityBusinessTestFixture();
        }
        return instance;
    }

    /**
     * Constructor to be used only from within this class.
     */
    private UtilityBusinessTestFixture() {
    }

    /**
     * One-time initialization code.
     */
    public void setUp() {
        userAccountEditor    = UtilityModel.createUserAccountEditor   (ASDF);
        userAccountSuperUser = UtilityModel.createUserAccountSuperUser(ZXCV);

        setupNameRevisionSection();
        setupNameRevisionDeviceType();
        setupNameRevisionDevice();
    }

    /**
     * One-time cleanup code.
     */
    public void tearDown() {
        userAccountEditor    = null;
        userAccountSuperUser = null;

        tearDownNameRevisionSection();
        tearDownNameRevisionDeviceType();
        tearDownNameRevisionDevice();
    }

    // ----------------------------------------------------------------------------------------------------

    private UserAccount userAccountEditor;
    private UserAccount userAccountSuperUser;

    public UserAccount getUserAccountEditor()    { return userAccountEditor;    }
    public UserAccount getUserAccountSuperUser() { return userAccountSuperUser; }

    // ----------------------------------------------------------------------------------------------------
    // namepartrevision section
    //     namepart id 1, 13, 1021
    //     hierarchy - 3 levels - system group, system, subsystem

    // namepart section
    public static final String UUID_NAMEPART_1    = "4262e1e7-2444-412e-83d7-aeabf58262c6";
    public static final String UUID_NAMEPART_13   = "034f1c3b-fe93-4541-bdf2-3d75e5551bb2";
    public static final String UUID_NAMEPART_1021 = "bf210a11-f71e-4a4e-ba2b-c2c8d8f16e95";

    private NamePartRevision namePartRevisionSection11;
    private NamePartRevision namePartRevisionSection12;
    private NamePartRevision namePartRevisionSection13;
    private NamePartRevision namePartRevisionSection21;
    private NamePartRevision namePartRevisionSection22;
    private NamePartRevision namePartRevisionSection23;
    private NamePartRevision namePartRevisionSection24;
    private NamePartRevision namePartRevisionSection31;
    private NamePartRevision namePartRevisionSection32;
    private NamePartRevision namePartRevisionSection33;

    private NameRevision nameRevisionNamePartRevisionSection11;
    private NameRevision nameRevisionNamePartRevisionSection12;
    private NameRevision nameRevisionNamePartRevisionSection13;
    private NameRevision nameRevisionNamePartRevisionSection21;
    private NameRevision nameRevisionNamePartRevisionSection22;
    private NameRevision nameRevisionNamePartRevisionSection23;
    private NameRevision nameRevisionNamePartRevisionSection24;
    private NameRevision nameRevisionNamePartRevisionSection31;
    private NameRevision nameRevisionNamePartRevisionSection32;
    private NameRevision nameRevisionNamePartRevisionSection33;

    public NamePartRevision getNamePartRevisionSection11() { return namePartRevisionSection11; }
    public NamePartRevision getNamePartRevisionSection12() { return namePartRevisionSection12; }
    public NamePartRevision getNamePartRevisionSection13() { return namePartRevisionSection13; }
    public NamePartRevision getNamePartRevisionSection21() { return namePartRevisionSection21; }
    public NamePartRevision getNamePartRevisionSection22() { return namePartRevisionSection22; }
    public NamePartRevision getNamePartRevisionSection23() { return namePartRevisionSection23; }
    public NamePartRevision getNamePartRevisionSection24() { return namePartRevisionSection24; }
    public NamePartRevision getNamePartRevisionSection31() { return namePartRevisionSection31; }
    public NamePartRevision getNamePartRevisionSection32() { return namePartRevisionSection32; }
    public NamePartRevision getNamePartRevisionSection33() { return namePartRevisionSection33; }

    public NameRevision     getNameRevisionSection11()     { return nameRevisionNamePartRevisionSection11; }
    public NameRevision     getNameRevisionSection12()     { return nameRevisionNamePartRevisionSection12; }
    public NameRevision     getNameRevisionSection13()     { return nameRevisionNamePartRevisionSection13; }
    public NameRevision     getNameRevisionSection21()     { return nameRevisionNamePartRevisionSection21; }
    public NameRevision     getNameRevisionSection22()     { return nameRevisionNamePartRevisionSection22; }
    public NameRevision     getNameRevisionSection23()     { return nameRevisionNamePartRevisionSection23; }
    public NameRevision     getNameRevisionSection24()     { return nameRevisionNamePartRevisionSection24; }
    public NameRevision     getNameRevisionSection31()     { return nameRevisionNamePartRevisionSection31; }
    public NameRevision     getNameRevisionSection32()     { return nameRevisionNamePartRevisionSection32; }
    public NameRevision     getNameRevisionSection33()     { return nameRevisionNamePartRevisionSection33; }

    // ----------------------------------------------------------------------------------------------------
    // namepartrevision device type
    //     namepart id 241
    //     hierarchy - 1 level - discipline

    // namepart devicetype
    public static final String UUID_NAMEPART_241  = "989669d0-79b5-4b12-b6ec-717f458da2b4";

    private NamePartRevision namePartRevisionDeviceType1;
    private NamePartRevision namePartRevisionDeviceType2;
    private NamePartRevision namePartRevisionDeviceType3;

    private NameRevision nameRevisionNamePartRevisionDeviceType1;
    private NameRevision nameRevisionNamePartRevisionDeviceType2;
    private NameRevision nameRevisionNamePartRevisionDeviceType3;

    public NamePartRevision getNamePartRevisionDeviceType1() { return namePartRevisionDeviceType1; }
    public NamePartRevision getNamePartRevisionDeviceType2() { return namePartRevisionDeviceType2; }
    public NamePartRevision getNamePartRevisionDeviceType3() { return namePartRevisionDeviceType3; }

    public NameRevision     getNameRevisionDeviceType1()     { return nameRevisionNamePartRevisionDeviceType1; }
    public NameRevision     getNameRevisionDeviceType2()     { return nameRevisionNamePartRevisionDeviceType2; }
    public NameRevision     getNameRevisionDeviceType3()     { return nameRevisionNamePartRevisionDeviceType3; }

    // ----------------------------------------------------------------------------------------------------
    // devicerevision
    //     device id 62012
    //     hierarchy - child with two parents

    // device
    public static final String UUID_DEVICE_62012  = "235be53f-dab3-46b5-a0fd-cc385f5c8520";
    // section
    public static final String UUID_NAMEPART_2692 = "c9d929fc-a38f-4c38-b434-7e520979f0ee";
    // device type
    public static final String UUID_NAMEPART_2765 = "d4bcd4a6-64c0-41d8-a2d1-a7bbbfe8fdda";
    public static final String UUID_NAMEPART_2886 = "45a1b48e-3f4f-4191-ac54-a1d12fc523d9";

    private DeviceRevision deviceRevision1;
    private DeviceRevision deviceRevision2;
    private DeviceRevision deviceRevision3;

    private NameRevision nameRevisionDeviceRevision1;
    private NameRevision nameRevisionDeviceRevision2;
    private NameRevision nameRevisionDeviceRevision3;

    public DeviceRevision getDeviceRevision1()     { return deviceRevision1; }
    public DeviceRevision getDeviceRevision2()     { return deviceRevision2; }
    public DeviceRevision getDeviceRevision3()     { return deviceRevision3; }

    public NameRevision   getNameRevisionDevice1() { return nameRevisionDeviceRevision1; }
    public NameRevision   getNameRevisionDevice2() { return nameRevisionDeviceRevision2; }
    public NameRevision   getNameRevisionDevice3() { return nameRevisionDeviceRevision3; }

    // ----------------------------------------------------------------------------------------------------

    /**
     * Return list of name revisions for test fixture.
     *
     * @return list of name revisions for test fixture
     */
    public List<NameRevision> getNameRevisions() {
        List<NameRevision> nameRevisions = new ArrayList<>();

        // section
        nameRevisions.add(getNameRevisionSection11());
        nameRevisions.add(getNameRevisionSection12());
        nameRevisions.add(getNameRevisionSection13());
        nameRevisions.add(getNameRevisionSection21());
        nameRevisions.add(getNameRevisionSection22());
        nameRevisions.add(getNameRevisionSection23());
        nameRevisions.add(getNameRevisionSection24());
        nameRevisions.add(getNameRevisionSection31());
        nameRevisions.add(getNameRevisionSection32());
        nameRevisions.add(getNameRevisionSection33());

        // device type
        nameRevisions.add(getNameRevisionDeviceType1());
        nameRevisions.add(getNameRevisionDeviceType2());
        nameRevisions.add(getNameRevisionDeviceType3());

        // device
        nameRevisions.add(getNameRevisionDevice1());
        nameRevisions.add(getNameRevisionDevice2());
        nameRevisions.add(getNameRevisionDevice3());

        return nameRevisions;
    }

    /**
     * Set up test structure - section - name part revisions, name revisions.
     */
    private void setupNameRevisionSection() {
        NamePart namePartSection1 = UtilityModel.createNamePartSection(UUID_NAMEPART_1);
        NamePart namePartSection2 = UtilityModel.createNamePartSection(UUID_NAMEPART_13);
        NamePart namePartSection3 = UtilityModel.createNamePartSection(UUID_NAMEPART_1021);

        Date date11request = UtilityBusiness.parseDateOrNewDate("2014-04-04 17:52:59");
        Date date11process = date11request;
        Date date12request = UtilityBusiness.parseDateOrNewDate("2015-04-30 10:47:5");
        Date date12process = UtilityBusiness.parseDateOrNewDate("2015-05-06 14:24:11");
        Date date13request = UtilityBusiness.parseDateOrNewDate("2020-07-15 10:34:28");
        Date date13process = UtilityBusiness.parseDateOrNewDate("2020-07-15 10:36:45");
        Date date21request = UtilityBusiness.parseDateOrNewDate("2014-04-04 17:52:59");
        Date date21process = date11request;
        Date date22request = UtilityBusiness.parseDateOrNewDate("2015-01-20 23:03:48");
        Date date22process = UtilityBusiness.parseDateOrNewDate("2015-01-21 10:40:11");
        Date date23request = UtilityBusiness.parseDateOrNewDate("2015-01-21 15:34:58");
        Date date23process = UtilityBusiness.parseDateOrNewDate("2015-01-21 15:35:42");
        Date date24request = UtilityBusiness.parseDateOrNewDate("2015-01-26 10:52:36");
        Date date24process = UtilityBusiness.parseDateOrNewDate("2015-03-03 11:07:43");
        Date date31request = UtilityBusiness.parseDateOrNewDate("2016-01-18 15:09:48");
        Date date31process = UtilityBusiness.parseDateOrNewDate("2016-03-23 14:25:49");
        Date date32request = date31process;
        Date date32process = UtilityBusiness.parseDateOrNewDate("2016-06-28 10:00:34");
        Date date33request = UtilityBusiness.parseDateOrNewDate("2017-05-03 11:13:04");
        Date date33process = UtilityBusiness.parseDateOrNewDate("2017-08-21 15:15:51");

        namePartRevisionSection11 = UtilityModel.createNamePartRevision(
                namePartSection1, date11request, null, INITIAL_DATA,
                false, null, ACCELERATOR, "Acc",
                null, "ACC",
                date11process,
                "Mnemonics for Super Section are not part of device names and have been removed to avoid confusion.");
        namePartRevisionSection12 = UtilityModel.createNamePartRevision(
                namePartSection1, date12request, userAccountSuperUser,
                "Mnemonics for Super section is not part of names and have been removed in order not to confuse users "
                        + "of the naming convention",
                false, null, ACCELERATOR, null,
                null, null,
                date12process, null);
        namePartRevisionSection13 = UtilityModel.createNamePartRevision(
                namePartSection1, date13request, userAccountSuperUser, "The mnemonic was added accordging to the new "
                        + "naming convention. Alfio ",
                false, null, ACCELERATOR, "Acc",
                "The ESS Linear Accelerator ", "ACC",
                date13process, "approved by alfio");

        namePartRevisionSection21 = UtilityModel.createNamePartRevision(
                namePartSection2, date21request, null, INITIAL_DATA,
                false, namePartSection1, "Upper High Beta", "UHB",
                null, "UHB",
                date21process, null);
        namePartRevisionSection22 = UtilityModel.createNamePartRevision(
                namePartSection2, date22request, userAccountSuperUser, null,
                false, namePartSection1, "Contingency", "Cont",
                null, "C0NT",
                date22process, "Intermediate step; will still need to be replaced by HEBT");
        namePartRevisionSection23 = UtilityModel.createNamePartRevision(
                namePartSection2, date23request, userAccountSuperUser, "Just to demo",
                false, namePartSection1, "High Energy Beam Transport", "HEBT",
                null, "HEBT",
                date23process, "Eugene should do this");
        namePartRevisionSection24 = UtilityModel.createNamePartRevision(
                namePartSection2, date24request, userAccountSuperUser,
                "Cont may be confused with Controls. ¶¶If it is foreseen that the HEBT will be used for acceleration "
                        + "in the future, beam transport is misleading.",
                false, namePartSection1, "High Energy Beam Transport", "HEBT",
                null, "HEBT",
                date24process, "Changed to make it consistent with the ACC PBS");

        namePartRevisionSection31 = UtilityModel.createNamePartRevision(
                namePartSection3, date31request, userAccountSuperUser, null,
                false, namePartSection2, NAME_07_DRIFT, "070Drf",
                null, MNEMONICEQCLASS_70DRF,
                date31process, null);
        namePartRevisionSection32 = UtilityModel.createNamePartRevision(
                namePartSection3, date32request, userAccountEditor, "HEBT SubSections renaming",
                false, namePartSection2, NAME_07_DRIFT, "070DRF",
                null, MNEMONICEQCLASS_70DRF,
                date32process, "Approved by Daniel Piso");
        namePartRevisionSection33 = UtilityModel.createNamePartRevision(
                namePartSection3, date33request, userAccountEditor,
                "Changing to CamelCase according to ESS Naming Convention",
                false, namePartSection2, NAME_07_DRIFT, "070Drf",
                null, MNEMONICEQCLASS_70DRF,
                date33process, "Approved by Daniel Piso");

        Whitebox.setInternalState(namePartRevisionSection11, ID, 1L);
        Whitebox.setInternalState(namePartRevisionSection12, ID, 1050L);
        Whitebox.setInternalState(namePartRevisionSection13, ID, 5007L);
        Whitebox.setInternalState(namePartRevisionSection21, ID, 13L);
        Whitebox.setInternalState(namePartRevisionSection22, ID, 745L);
        Whitebox.setInternalState(namePartRevisionSection23, ID, 795L);
        Whitebox.setInternalState(namePartRevisionSection24, ID, 801L);
        Whitebox.setInternalState(namePartRevisionSection31, ID, 1609L);
        Whitebox.setInternalState(namePartRevisionSection32, ID, 1698L);
        Whitebox.setInternalState(namePartRevisionSection33, ID, 2630L);

        nameRevisionNamePartRevisionSection11 = UtilityBusiness.createNameRevision(namePartRevisionSection11);
        nameRevisionNamePartRevisionSection12 = UtilityBusiness.createNameRevision(namePartRevisionSection12);
        nameRevisionNamePartRevisionSection13 = UtilityBusiness.createNameRevision(namePartRevisionSection13);
        nameRevisionNamePartRevisionSection21 = UtilityBusiness.createNameRevision(namePartRevisionSection21);
        nameRevisionNamePartRevisionSection22 = UtilityBusiness.createNameRevision(namePartRevisionSection22);
        nameRevisionNamePartRevisionSection23 = UtilityBusiness.createNameRevision(namePartRevisionSection23);
        nameRevisionNamePartRevisionSection24 = UtilityBusiness.createNameRevision(namePartRevisionSection24);
        nameRevisionNamePartRevisionSection31 = UtilityBusiness.createNameRevision(namePartRevisionSection31);
        nameRevisionNamePartRevisionSection32 = UtilityBusiness.createNameRevision(namePartRevisionSection32);
        nameRevisionNamePartRevisionSection33 = UtilityBusiness.createNameRevision(namePartRevisionSection33);
    }

    /**
     * Set up test structure - device type - name part revisions, name revisions.
     */
    private void setupNameRevisionDeviceType() {
        NamePart namePartDeviceType = UtilityModel.createNamePartDeviceType(UUID_NAMEPART_241);

        Date date1request = UtilityBusiness.parseDateOrNewDate("2014-04-04 17:53:02");
        Date date1process = date1request;
        Date date2request = UtilityBusiness.parseDateOrNewDate("2016-06-17 08:16:15");
        Date date2process = UtilityBusiness.parseDateOrNewDate("2016-06-17 08:38:56");
        Date date3request = UtilityBusiness.parseDateOrNewDate("2017-10-16 22:27:25");
        Date date3process = UtilityBusiness.parseDateOrNewDate("2017-12-01 10:47:48");

        namePartRevisionDeviceType1 =
                UtilityModel.createNamePartRevision(
                        namePartDeviceType, date1request, null, INITIAL_DATA,
                        false, null, WATER_COOLING, "WtrC",
                        null, "WTRC",
                        date1process, null);

        namePartRevisionDeviceType2 =
                UtilityModel.createNamePartRevision(
                        namePartDeviceType, date2request, userAccountEditor, null,
                        false, null, WATER_COOLING, "WTRC",
                        null, "WTRC",
                        date2process, null);

        namePartRevisionDeviceType3 =
                UtilityModel.createNamePartRevision(
                        namePartDeviceType, date3request, userAccountSuperUser,
                        "CamelCase according to the naming convention",
                        false, null, WATER_COOLING, "WtrC",
                        null, "WTRC",
                        date3process, null);

        Whitebox.setInternalState(namePartRevisionDeviceType1, ID, 241L);
        Whitebox.setInternalState(namePartRevisionDeviceType2, ID, 1949L);
        Whitebox.setInternalState(namePartRevisionDeviceType3, ID, 3008L);

        nameRevisionNamePartRevisionDeviceType1 = UtilityBusiness.createNameRevision(namePartRevisionDeviceType1);
        nameRevisionNamePartRevisionDeviceType2 = UtilityBusiness.createNameRevision(namePartRevisionDeviceType2);
        nameRevisionNamePartRevisionDeviceType3 = UtilityBusiness.createNameRevision(namePartRevisionDeviceType3);
    }

    /**
     * Set up test structure - device - name part revisions, name revisions.
     */
    private void setupNameRevisionDevice() {
        NamePart namePartSection     = UtilityModel.createNamePartSection   (UUID_NAMEPART_2692);
        NamePart namePartDeviceType1 = UtilityModel.createNamePartDeviceType(UUID_NAMEPART_2765);
        NamePart namePartDeviceType2 = UtilityModel.createNamePartDeviceType(UUID_NAMEPART_2886);

        Device device = UtilityModel.createDevice(UUID_DEVICE_62012);

        Date date1 = UtilityBusiness.parseDateOrNewDate("2020-01-27 16:00:41");
        Date date2 = UtilityBusiness.parseDateOrNewDate("2020-03-31 13:01:10");
        Date date3 = UtilityBusiness.parseDateOrNewDate("2020-04-16 11:58:46");

        deviceRevision1 =
                UtilityModel.createDeviceRevision(
                        device, date1, userAccountEditor, false,
                        namePartSection, namePartDeviceType1, "01", TD_D22_TS_MCH_01,
                        TD_D22_TS_MCH_1, MTCA_HUB);
        deviceRevision2 =
                UtilityModel.createDeviceRevision(
                        device, date2, userAccountEditor, false,
                        namePartSection, namePartDeviceType1, "1", TD_D22_TS_MCH_1,
                        TD_D22_TS_MCH_1, MTCA_HUB);
        deviceRevision3 =
                UtilityModel.createDeviceRevision(
                        device, date3, userAccountEditor, false,
                        namePartSection, namePartDeviceType2, "1", TD_D22_CTRL_MCH_1,
                        TD_D22_CTR1_MCH_1, MTCA_HUB);

        Whitebox.setInternalState(deviceRevision1, ID, 104755L);
        Whitebox.setInternalState(deviceRevision2, ID, 110818L);
        Whitebox.setInternalState(deviceRevision3, ID, 113713L);

        nameRevisionDeviceRevision1 = UtilityBusiness.createNameRevision(deviceRevision1);
        nameRevisionDeviceRevision2 = UtilityBusiness.createNameRevision(deviceRevision2);
        nameRevisionDeviceRevision3 = UtilityBusiness.createNameRevision(deviceRevision3);
    }

    /**
     * Tear down test structure - section - name part revisions, name revisions.
     */
    private void tearDownNameRevisionSection() {
        namePartRevisionSection11 = null;
        namePartRevisionSection12 = null;
        namePartRevisionSection13 = null;
        namePartRevisionSection21 = null;
        namePartRevisionSection22 = null;
        namePartRevisionSection23 = null;
        namePartRevisionSection24 = null;
        namePartRevisionSection31 = null;
        namePartRevisionSection32 = null;
        namePartRevisionSection33 = null;

        nameRevisionNamePartRevisionSection11 = null;
        nameRevisionNamePartRevisionSection12 = null;
        nameRevisionNamePartRevisionSection13 = null;
        nameRevisionNamePartRevisionSection21 = null;
        nameRevisionNamePartRevisionSection22 = null;
        nameRevisionNamePartRevisionSection23 = null;
        nameRevisionNamePartRevisionSection24 = null;
        nameRevisionNamePartRevisionSection31 = null;
        nameRevisionNamePartRevisionSection32 = null;
        nameRevisionNamePartRevisionSection33 = null;
    }

    /**
     * Tear down test structure - device type - name part revisions, name revisions.
     */
    private void tearDownNameRevisionDeviceType() {
        namePartRevisionDeviceType1 = null;
        namePartRevisionDeviceType2 = null;
        namePartRevisionDeviceType3 = null;

        nameRevisionNamePartRevisionDeviceType1 = null;
        nameRevisionNamePartRevisionDeviceType2 = null;
        nameRevisionNamePartRevisionDeviceType3 = null;
    }

    /**
     * Tear down test structure - device - device revisions, name revisions.
     */
    private void tearDownNameRevisionDevice() {
        deviceRevision1 = null;
        deviceRevision2 = null;
        deviceRevision3 = null;

        nameRevisionDeviceRevision1 = null;
        nameRevisionDeviceRevision2 = null;
        nameRevisionDeviceRevision3 = null;
    }

}
