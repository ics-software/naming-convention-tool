/*
 * Copyright (C) 2021 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.business;

import java.util.ArrayList;
import java.util.List;

import org.openepics.names.model.Device;
import org.openepics.names.model.DeviceRevision;
import org.openepics.names.model.NamePart;
import org.openepics.names.model.NamePartRevision;
import org.openepics.names.model.NamePartRevisionStatus;
import org.openepics.names.model.UserAccount;
import org.openepics.names.model.UtilityModel;
import org.powermock.reflect.Whitebox;

/**
 * Purpose to provide test fixture that can be used in multiple test classes and tests.
 *
 * <br>
 * Intended usage is
 * <ol>
 * <li> <tt>getInstance</tt> method followed by </tt>setUp</tt> method to set up test fixture.
 * <li> <tt>get</tt> methods for tests
 * <li> <tt>tearDown</tt> method to tear down test fixture.
 * </ol>
 *
 * @author Lars Johansson
 *
 * @see UtilityBusiness
 * @see UtilityModel
 */
public class UtilityBusinessTestFixture4 {

    /*
    Test fixture for section, device type, device based on database backup Mar 23 2021.
        -----------------------------------------
        purposes of this text fixture to have
            1. multiple entries (names), i.e. history, for same uuid
            2. multiple entries including deleted entry (name) for same uuid

        -----------------------------------------
        1. device_id = 4108, uuid = 2edcacdb-e618-4d64-8e95-303ac014ed87
        -----------------------------------------
        conventionName                conventionNameEqClass                section_id                devicetype_id
        ---------------------------------------------------------------------------------------------------------------
        FEB-050ROW:PBI-MTCA-215100    FEB-50R0W:PB1-MTCA-21510             1189                      1452
        FEB-050Row:PBI-MTCA-215100    FEB-50R0W:PB1-MTCA-21510             1189                      1452
        FEB-050Row:PBI-MTCA-004       FEB-50R0W:PB1-MTCA-4                 1189                      1452
        FEB-050Row:PBI-MTCA-040       FEB-50R0W:PB1-MTCA-40                1189                      1452
        FEB-050Row:PBI-MTCA-040       FEB-50R0W:PB1-MTCA-40                1189                      1452
        FEB-050Row:PBI-MTCA-9040      FEB-50R0W:PB1-MTCA-9040              1189                      1452
        FEB-050Row:PBI-MTCA-040       FEB-50R0W:PB1-MTCA-40                1189                      1452
        FEB-050Row:PBI-MTCA-050x      FEB-50R0W:PB1-MTCA-50X               1189                      1452
        FEB-050Row:PBI-MTCA-050       FEB-50R0W:PB1-MTCA-50                1189                      1452
        FEB-050Row:PBI-MTCA-050       FEB-50R0W:PB1-MTCA-50                1189                      1452
        FEB-050Row:PBI-MTCA-050       FEB-50R0W:PB1-MTCA-50                1189                      1452
        FEB-050Row:PBI-MTCA-050       FEB-50R0W:PB1-MTCA-50                1189                      1452
        FEB-050Row:PBI-MTCA-050       FEB-50R0W:PB1-MTCA-50                1189                      1452
        PBI-BCM01:PBI-MTCA-200        PB1-BCM1:PB1-MTCA-200                2778                      1452
        PBI-BCM01:Ctrl-MTCA3U-200     PB1-BCM1:CTR1-MTCA3U-200             2778                      2875
        PBI-BCM01:Ctrl-MTCA3U-201     PB1-BCM1:CTR1-MTCA3U-201             2778                      2875
        PBI-BCM01:Ctrl-MTCA3U-201     PB1-BCM1:CTR1-MTCA3U-201             2778                      2875
        -----------------------------------------
        mnemonic path
        -----------------------------------------
        FEB
        FEB-050Row
        PBI
        PBI-BCM01
                       (empty)
        PBI
        PBI-MTCA
        Ctrl
        Ctrl-MTCA3U
        -----------------------------------------
        names have history, have been moved from Accelerator to Central Services

        -----------------------------------------
        2. device_id = 70025, uuid = 37377f9a-4a79-4132-98a9-e8bdbc855ee5
        -----------------------------------------
        id            conventionName                conventionNameEqClass            section_id            devicetype_id
        ---------------------------------------------------------------------------------------------------------------
        119719        Tgt-TSS1080:Ctrl-SProt-8      TGT-TSS1080:CTR1-SPR0T-8         2607                  3025
        119780        Tgt-TSS1080:Ctrl-SProt-08     TGT-TSS1080:CTR1-SPR0T-8         2607                  3025
        119949        Tgt-TSS1080:Ctrl-SProt-08     TGT-TSS1080:CTR1-SPR0T-8         2607                  3025

    Note
        includes name revision hierarchy/history for section, device type, device.
        section, device type, device are associated
        purpose for selection of names
            include name, eq name, move of name in system structure and device structure, history
 */

    // ----------

    private static final String PBI_BCM_01_BCM_U_TCA_CRATE_B = "PBI BCM-01 - BCM uTCA crate B";
    private static final String PBI_BCM_01_BCM_U_TCA_CRATE_2 = "PBI BCM-01 - BCM uTCA crate 2";

    private static final String FEB_50R0W_PB1_MTCA_50        = "FEB-50R0W:PB1-MTCA-50";
    private static final String FEB_50R0W_PB1_MTCA_40        = "FEB-50R0W:PB1-MTCA-40";
    private static final String FEB_050_ROW_PBI_MTCA_050     = "FEB-050Row:PBI-MTCA-050";
    private static final String FEB_050_ROW_PBI_MTCA_040     = "FEB-050Row:PBI-MTCA-040";

    private static final String DATE_2014_04_04_17_52_59 = "2014-04-04 17:52:59";

    private static final String TGT_TSS1080_CTR1_SPR0T_8              = "TGT-TSS1080:CTR1-SPR0T-8";
    private static final String TARGET_STATION                        = "Target Station";
    private static final String SURGE_PROTECTION_RFQ_CTRL             = "Surge Protection - RFQ Ctrl";
    private static final String PROTON_BEAM_INSTRUMENTATION           = "Proton Beam Instrumentation";
    private static final String MNEMONICS_FOR_SUPER_SECTION_IS_NOT_PART_OF_NAMES_AND_HAVE_BEEN_REMOVED_IN_ORDER_NOT_TO_CONFUSE_USERS_OF_THE_NAMING_CONVENTION =
            "Mnemonics for Super section is not part of names and have been removed in order not to confuse users of the naming convention";
    private static final String INITIAL_DATA                          = "Initial data";
    private static final String CONTROL_SYSTEM                        = "Control System";
    private static final String CENTRAL_SERVICES                      = "Central Services";
    private static final String AUTOMATICALLY_CANCELLED_BEFORE_MODIFY = "Automatically cancelled before modify";
    private static final String APPROVED_BY_DANIEL_PISO               = "Approved by Daniel Piso";
    private static final String APPROVED_BY_ALFIO                     = "Approved by Alfio";
    private static final String ACCELERATOR                           = "Accelerator";

    public static final String ASDF = "asdf";
    public static final String ZXCV = "zxcv";

    private static final String ID = "id";

    // ----------------------------------------------------------------------------------------------------

    private static UtilityBusinessTestFixture4 instance;

    /**
     * Returns the singleton instance of this class.
     *
     * @return the singletone instance
     */
    public static synchronized UtilityBusinessTestFixture4 getInstance() {
        if (instance == null) {
            instance = new UtilityBusinessTestFixture4();
        }
        return instance;
    }

    /**
     * Constructor to be used only from within this class.
     */
    private UtilityBusinessTestFixture4() {
    }

    /**
     * One-time initialization code.
     */
    public void setUp() {
        userAccountEditor    = UtilityModel.createUserAccountEditor   (ASDF);
        userAccountSuperUser = UtilityModel.createUserAccountSuperUser(ZXCV);

        setupNameRevisionSection();
        setupNameRevisionDeviceType();
        setupNameRevisionDevice();
    }

    /**
     * One-time cleanup code.
     */
    public void tearDown() {
        userAccountEditor    = null;
        userAccountSuperUser = null;

        tearDownNameRevisionSection();
        tearDownNameRevisionDeviceType();
        tearDownNameRevisionDevice();
    }

    // ----------------------------------------------------------------------------------------------------

    private UserAccount userAccountEditor;
    private UserAccount userAccountSuperUser;

    public UserAccount getUserAccountEditor()    { return userAccountEditor;    }
    public UserAccount getUserAccountSuperUser() { return userAccountSuperUser; }

    // ----------------------------------------------------------------------------------------------------
    // section
    //     Subsystem           05 Rack Row                       050Row     name part id 1189     2 name part revisions
    //     System              Front End Building                FEB        name part id 1052     1 name part revision
    //     System Group        Accelerator                       Acc        name part id 1        3 name part revisions
    public static final String UUID_NAMEPART_1189_SUBSYSSTEM_050ROW         = "7e67365b-3d01-4acb-873f-4af7e29eeca5";
    public static final String UUID_NAMEPART_1052_SYSTEM_FEB                = "08e4b66b-d503-4888-b9ae-4d6b0524231b";
    public static final String UUID_NAMEPART_1_SYSTEMGROUP_ACC              = "4262e1e7-2444-412e-83d7-aeabf58262c6";

    private List<NamePartRevision> namePartRevisionsSystemGroupAcc1;
    private List<NameRevision>     nameRevisionsSystemGroupAcc1;

    private List<NamePartRevision> namePartRevisionsSystemFEB1052;
    private List<NameRevision>     nameRevisionsSystemFEB1052;

    private List<NamePartRevision> namePartRevisionsSubsystem050ROW1189;
    private List<NameRevision>     nameRevisionsSubsystem050ROW1189;

    // section
    //     Subsystem           PBI BCM-01                        BCM01      name part id 2778     1 name part revision
    //     System              Proton Beam Instrumentation       PBI        name part id 2774     2 name part revisions
    //     System Group        Central Services                             name part id 4        3 name part revisions
    public static final String UUID_NAMEPART_2778_SUBSYSSTEM_BCM01          = "59dca0b0-3eb5-4898-9fa5-20d323db1050";
    public static final String UUID_NAMEPART_2774_SYSTEM_PBI                = "b33b67d8-c518-4b98-9b62-312114747a66";
    public static final String UUID_NAMEPART_4_SYSTEMGROUP_CENTRAL_SERVICES = "a796360b-2634-4499-944b-8bd5c46aea16";

    private List<NamePartRevision> namePartRevisionsSystemGroupCentralServices4;
    private List<NameRevision>     nameRevisionsSystemGroupCentralServices4;

    private List<NamePartRevision> namePartRevisionsSystemPBI2774;
    private List<NameRevision>     nameRevisionsSystemPBI2774;

    private List<NamePartRevision> namePartRevisionsSubsystemBCM012778;
    private List<NameRevision>     nameRevisionsSubsystemBCM012778;

    // section
    //     Subsystem           Target Safety System             TSS1080      name part id 2607     1 name part revision
    //     System              Target Station                   Tgt          name part id 2566     1 name part revision
    //     System Group        Target Station                                name part id 2        4 name part revisions
    public static final String UUID_NAMEPART_2607_SUBSYSSTEM_TSS1080        = "cefc2035-0e6d-4aa6-ab67-67519bd42d64";
    public static final String UUID_NAMEPART_2566_SYSTEM_TGT                = "cc33ed85-cc40-4fab-bfe2-dc5b8d73994d";
    public static final String UUID_NAMEPART_2_SYSTEMGROUP_TARGET_STATION   = "c6e84e09-29d7-49a0-8ecd-92a6eff86da4";

    private List<NamePartRevision> namePartRevisionsSystemGroupTargetStation2;
    private List<NameRevision>     nameRevisionsSystemGroupTargetStation2;

    private List<NamePartRevision> namePartRevisionsSystemTGT2566;
    private List<NameRevision>     nameRevisionsSystemTGT2566;

    private List<NamePartRevision> namePartRevisionsSubsystemTSS10802607;
    private List<NameRevision>     nameRevisionsSubsystemTSS10802607;

    // ----------------------------------------------------------------------------------------------------
    // device type
    //     Device Type         uTCA crate                        MTCA       name part id 1452     2 name part revisions
    //     Device Group        Control electronics                          name part id 1435     4 name part revisions
    //     Discipline          Proton Beam Instrumentation       PBI        name part id 237      1 name part revision
    public static final String UUID_NAMEPART_1452_DEVICETYPE_MTCA                    = "aeb02f70-afea-489c-b3ae-c37cc7951fdf";
    public static final String UUID_NAMEPART_1435_DEVICEGROUP_CONTROL_ELECTRONICS    = "5fa9dcf1-2f6f-4805-99b7-e7327735f0a6";
    public static final String UUID_NAMEPART_237_DISCIPLINE_PBI                      = "bd8e2820-b42e-4ec7-97f9-b0b898af8f1f";

    private List<NamePartRevision> namePartRevisionsDisciplinePBI237;
    private List<NameRevision>     nameRevisionsDisciplinePBI237;

    private List<NamePartRevision> namePartRevisionsDeviceGroupControlElectronics1435;
    private List<NameRevision>     nameRevisionsDeviceGroupControllerElectronics1435;

    private List<NamePartRevision> namePartRevisionsDeviceTypeMTCA1452;
    private List<NameRevision>     nameRevisionsDeviceTypeMTCA1452;

    // device type
    //     Device Type         MTCA 3U crate                     MTCA3U     name part id 2875     1 name part revision
    //     Device Group        Enclosures                                   name part id 2870     1 name part revision
    //     Discipline          Control System                    Ctrl       name part id 502      3 name part revisions
    public static final String UUID_NAMEPART_2875_DEVICETYPE_MTCA3U                  = "909559a0-6500-4845-8671-e1cf852fc090";
    public static final String UUID_NAMEPART_2870_DEVICEGROUP_ENCLOSURES             = "f1a6825a-2dd4-4ed2-9ad4-80eb708152d2";
    public static final String UUID_NAMEPART_502_DISCIPLINE_CTRL                     = "ebe8a9ec-000e-40de-a2cd-9a2ccdb1d6b1";

    private List<NamePartRevision> namePartRevisionsDisciplineCTRL502;
    private List<NameRevision>     nameRevisionsDisciplineCTRL502;

    private List<NamePartRevision> namePartRevisionsDeviceGroupEnclosures2870;
    private List<NameRevision>     nameRevisionsDeviceGroupEnclosures2870;

    private List<NamePartRevision> namePartRevisionsDeviceTypeMTCA3U2875;
    private List<NameRevision>     nameRevisionsDeviceTypeMTCA3U2875;

    // device type
    //     Device Type         Surge Protector                   SProt      name part id 3025     2 name part revisions
    //     Device Group        Supporting Equipment                         name part id 1007     2 name part revisions
    //     Discipline          Control System                    Ctrl       name part id 502      3 name part revisions
    public static final String UUID_NAMEPART_3025_DEVICETYPE_SPROT                   = "41262505-8a7b-4ee3-8481-07a552bbd4ec";
    public static final String UUID_NAMEPART_1007_DEVICEGROUP_SUPPORTING_EQUIPMENT   = "dd7aa22f-75e4-4ae5-aefa-178abf72a99b";
    //     see above

    private List<NamePartRevision> namePartRevisionsDeviceGroupSupportingEquipment1007;
    private List<NameRevision>     nameRevisionsDeviceGroupSupportingEquipment1007;

    private List<NamePartRevision> namePartRevisionsDeviceTypeSPROT3025;
    private List<NameRevision>     nameRevisionsDeviceTypeSPROT3025;

    // ----------------------------------------------------------------------------------------------------
    // device
    //     Instance Index                                                   device id 4108        17 name part revisions
    //     conventionName                conventionNameEqClass                section_id                devicetype_id
    //     -------------------------------------------------------------------------------------------------------------
    //     FEB-050ROW:PBI-MTCA-215100    FEB-50R0W:PB1-MTCA-21510             1189                      1452
    //     FEB-050Row:PBI-MTCA-215100    FEB-50R0W:PB1-MTCA-21510             1189                      1452
    //     FEB-050Row:PBI-MTCA-004       FEB-50R0W:PB1-MTCA-4                 1189                      1452
    //     FEB-050Row:PBI-MTCA-040       FEB-50R0W:PB1-MTCA-40                1189                      1452
    //     FEB-050Row:PBI-MTCA-040       FEB-50R0W:PB1-MTCA-40                1189                      1452
    //     FEB-050Row:PBI-MTCA-9040      FEB-50R0W:PB1-MTCA-9040              1189                      1452
    //     FEB-050Row:PBI-MTCA-040       FEB-50R0W:PB1-MTCA-40                1189                      1452
    //     FEB-050Row:PBI-MTCA-050x      FEB-50R0W:PB1-MTCA-50X               1189                      1452
    //     FEB-050Row:PBI-MTCA-050       FEB-50R0W:PB1-MTCA-50                1189                      1452
    //     FEB-050Row:PBI-MTCA-050       FEB-50R0W:PB1-MTCA-50                1189                      1452
    //     FEB-050Row:PBI-MTCA-050       FEB-50R0W:PB1-MTCA-50                1189                      1452
    //     FEB-050Row:PBI-MTCA-050       FEB-50R0W:PB1-MTCA-50                1189                      1452
    //     FEB-050Row:PBI-MTCA-050       FEB-50R0W:PB1-MTCA-50                1189                      1452
    //     PBI-BCM01:PBI-MTCA-200        PB1-BCM1:PB1-MTCA-200                2778                      1452
    //     PBI-BCM01:Ctrl-MTCA3U-200     PB1-BCM1:CTR1-MTCA3U-200             2778                      2875
    //     PBI-BCM01:Ctrl-MTCA3U-201     PB1-BCM1:CTR1-MTCA3U-201             2778                      2875
    //     PBI-BCM01:Ctrl-MTCA3U-201     PB1-BCM1:CTR1-MTCA3U-201             2778                      2875
    public static final String UUID_DEVICE_4108_INSTANCEINDEX_FEB_050ROW_PBI_MTCA_050 = "2edcacdb-e618-4d64-8e95-303ac014ed87";

    private List<DeviceRevision> deviceRevisions4108InstanceIndex_FEB_050ROW_PBI_MTCA_050;
    private List<NameRevision>   nameRevisions4108InstanceIndex_FEB_050ROW_PBI_MTCA_050;

    // device
    //                   Instance Index                device id 70025     3 name part revisions
    //     id            conventionName                conventionNameEqClass          section_id        devicetype_id
    //     -------------------------------------------------------------------------------------------------------------
    //     119719        Tgt-TSS1080:Ctrl-SProt-8      TGT-TSS1080:CTR1-SPR0T-8       2607              3025
    //     119780        Tgt-TSS1080:Ctrl-SProt-08     TGT-TSS1080:CTR1-SPR0T-8       2607              3025
    //     119949        Tgt-TSS1080:Ctrl-SProt-08     TGT-TSS1080:CTR1-SPR0T-8       2607              3025
    public static final String UUID_DEVICE_70025_INSTANCEINDEX_TGT_TSS1080_CTRL_SPROT_08 = "37377f9a-4a79-4132-98a9-e8bdbc855ee5";

    private List<DeviceRevision> deviceRevisions70025InstanceIndex_TGT_TSS1080_CTRL_SPROT_08;
    private List<NameRevision>   nameRevisions70025InstanceIndex_TGT_TSS1080_CTRL_SPROT_08;

    // ----------------------------------------------------------------------------------------------------

    /**
     * Return list of name revisions for test fixture.
     *
     * @return list of name revisions for test fixture
     */
    public List<NameRevision> getNameRevisions() {
        List<NameRevision> nameRevisions = new ArrayList<>();

        // section
        nameRevisions.addAll(nameRevisionsSystemGroupAcc1);
        nameRevisions.addAll(nameRevisionsSystemFEB1052);
        nameRevisions.addAll(nameRevisionsSubsystem050ROW1189);
        // ----------
        nameRevisions.addAll(nameRevisionsSystemGroupCentralServices4);
        nameRevisions.addAll(nameRevisionsSystemPBI2774);
        nameRevisions.addAll(nameRevisionsSubsystemBCM012778);
        // ----------
        nameRevisions.addAll(nameRevisionsSystemGroupTargetStation2);
        nameRevisions.addAll(nameRevisionsSystemTGT2566);
        nameRevisions.addAll(nameRevisionsSubsystemTSS10802607);

        // device type
        nameRevisions.addAll(nameRevisionsDisciplinePBI237);
        nameRevisions.addAll(nameRevisionsDeviceGroupControllerElectronics1435);
        nameRevisions.addAll(nameRevisionsDeviceTypeMTCA1452);
        // ----------
        nameRevisions.addAll(nameRevisionsDisciplineCTRL502);
        nameRevisions.addAll(nameRevisionsDeviceGroupEnclosures2870);
        nameRevisions.addAll(nameRevisionsDeviceTypeMTCA3U2875);
        // ----------
        nameRevisions.addAll(nameRevisionsDeviceGroupSupportingEquipment1007);
        nameRevisions.addAll(nameRevisionsDeviceTypeSPROT3025);

        // device
        nameRevisions.addAll(nameRevisions4108InstanceIndex_FEB_050ROW_PBI_MTCA_050);
        // ----------
        nameRevisions.addAll(nameRevisions70025InstanceIndex_TGT_TSS1080_CTRL_SPROT_08);

        return nameRevisions;
    }

    /**
     * Set up test structure - section - name part revisions, name revisions.
     */
    private void setupNameRevisionSection() {
        namePartRevisionsSystemGroupAcc1 = new ArrayList<>();
        nameRevisionsSystemGroupAcc1 = new ArrayList<>();

        namePartRevisionsSystemFEB1052 = new ArrayList<>();
        nameRevisionsSystemFEB1052 = new ArrayList<>();

        namePartRevisionsSubsystem050ROW1189 = new ArrayList<>();
        nameRevisionsSubsystem050ROW1189 = new ArrayList<>();

        // ----------

        namePartRevisionsSystemGroupCentralServices4 = new ArrayList<>();
        nameRevisionsSystemGroupCentralServices4 = new ArrayList<>();

        namePartRevisionsSystemPBI2774 = new ArrayList<>();
        nameRevisionsSystemPBI2774 = new ArrayList<>();

        namePartRevisionsSubsystemBCM012778 = new ArrayList<>();
        nameRevisionsSubsystemBCM012778 = new ArrayList<>();

        // ----------

        namePartRevisionsSystemGroupTargetStation2 = new ArrayList<>();
        nameRevisionsSystemGroupTargetStation2 = new ArrayList<>();

        namePartRevisionsSystemTGT2566 = new ArrayList<>();
        nameRevisionsSystemTGT2566 = new ArrayList<>();

        namePartRevisionsSubsystemTSS10802607 = new ArrayList<>();
        nameRevisionsSubsystemTSS10802607 = new ArrayList<>();

        // ----------

        NamePart namePartSystemGroupACC  = UtilityModel.createNamePartSection(UUID_NAMEPART_1_SYSTEMGROUP_ACC);
        NamePart namePartSystemFEB       = UtilityModel.createNamePartSection(UUID_NAMEPART_1052_SYSTEM_FEB);
        NamePart namePartSubsystem050ROW = UtilityModel.createNamePartSection(UUID_NAMEPART_1189_SUBSYSSTEM_050ROW);

        Whitebox.setInternalState(namePartSystemGroupACC,             ID, 1L);
        Whitebox.setInternalState(namePartSystemFEB,                  ID, 1052L);
        Whitebox.setInternalState(namePartSubsystem050ROW,            ID, 1189L);

        // 3, 1, 2 - name part revisions

        namePartRevisionsSystemGroupAcc1.add(    UtilityModel.createNamePartRevision(namePartSystemGroupACC,  UtilityBusiness.parseDateOrNewDate(DATE_2014_04_04_17_52_59), userAccountSuperUser, INITIAL_DATA, false, null, ACCELERATOR, "Acc",    null, "ACC",   UtilityBusiness.parseDateOrNewDate(DATE_2014_04_04_17_52_59), null, NamePartRevisionStatus.APPROVED));
        namePartRevisionsSystemGroupAcc1.add(    UtilityModel.createNamePartRevision(namePartSystemGroupACC,  UtilityBusiness.parseDateOrNewDate("2015-04-30 10:47:54"), userAccountSuperUser, MNEMONICS_FOR_SUPER_SECTION_IS_NOT_PART_OF_NAMES_AND_HAVE_BEEN_REMOVED_IN_ORDER_NOT_TO_CONFUSE_USERS_OF_THE_NAMING_CONVENTION, false, null, ACCELERATOR, null,     null, null,    UtilityBusiness.parseDateOrNewDate("2015-05-06 14:24:11"), "Mnemonics for Super Section are not part of device names and have been removed to avoid confusion. ",   NamePartRevisionStatus.APPROVED));
        namePartRevisionsSystemGroupAcc1.add(    UtilityModel.createNamePartRevision(namePartSystemGroupACC,  UtilityBusiness.parseDateOrNewDate("2020-07-15 10:34:28"), userAccountSuperUser, "The mnemonic was added accordging to the new naming convention. Alfio", false, null, ACCELERATOR, "Acc",    "The ESS Linear Accelerator", "ACC",   UtilityBusiness.parseDateOrNewDate("2020-07-15 10:36:45"), "approved by alfio",   NamePartRevisionStatus.APPROVED));
        namePartRevisionsSystemFEB1052.add(      UtilityModel.createNamePartRevision(namePartSystemFEB,       UtilityBusiness.parseDateOrNewDate("2016-04-22 10:39:46"), userAccountSuperUser, "Added FEB, needed for the Rack Row subsections", false, namePartSystemGroupACC, "Front End Building", "FEB",    null, "FEB",   UtilityBusiness.parseDateOrNewDate("2016-06-28 09:57:15"), APPROVED_BY_DANIEL_PISO,   NamePartRevisionStatus.APPROVED));
        namePartRevisionsSubsystem050ROW1189.add(UtilityModel.createNamePartRevision(namePartSubsystem050ROW, UtilityBusiness.parseDateOrNewDate("2016-07-04 15:20:18"), userAccountSuperUser, null, false, namePartSystemFEB, "05 Rack Row", "050ROW", null, "50ROW", UtilityBusiness.parseDateOrNewDate("2016-07-06 10:26:55"), APPROVED_BY_DANIEL_PISO,   NamePartRevisionStatus.APPROVED));
        namePartRevisionsSubsystem050ROW1189.add(UtilityModel.createNamePartRevision(namePartSubsystem050ROW, UtilityBusiness.parseDateOrNewDate("2017-05-03 10:49:07"), userAccountSuperUser, "Changing to CamelCase according to ESS Naming Convention", false, namePartSystemFEB, "05 Rack Row", "050Row", null, "50ROW", UtilityBusiness.parseDateOrNewDate("2017-08-21 15:15:50"), APPROVED_BY_DANIEL_PISO,   NamePartRevisionStatus.APPROVED));

        Whitebox.setInternalState(namePartRevisionsSystemGroupAcc1.get(0),     ID, 1L);
        Whitebox.setInternalState(namePartRevisionsSystemGroupAcc1.get(1),     ID, 1050L);
        Whitebox.setInternalState(namePartRevisionsSystemGroupAcc1.get(2),     ID, 5007L);
        Whitebox.setInternalState(namePartRevisionsSystemFEB1052.get(0),       ID, 1720L);
        Whitebox.setInternalState(namePartRevisionsSubsystem050ROW1189.get(0), ID, 2073L);
        Whitebox.setInternalState(namePartRevisionsSubsystem050ROW1189.get(1), ID, 2577L);

        nameRevisionsSystemGroupAcc1.add(UtilityBusiness.createNameRevision(namePartRevisionsSystemGroupAcc1.get(0)));
        nameRevisionsSystemGroupAcc1.add(UtilityBusiness.createNameRevision(namePartRevisionsSystemGroupAcc1.get(1)));
        nameRevisionsSystemGroupAcc1.add(UtilityBusiness.createNameRevision(namePartRevisionsSystemGroupAcc1.get(2)));
        nameRevisionsSystemFEB1052.add(UtilityBusiness.createNameRevision(namePartRevisionsSystemFEB1052.get(0)));
        nameRevisionsSubsystem050ROW1189.add(UtilityBusiness.createNameRevision(namePartRevisionsSubsystem050ROW1189.get(0)));
        nameRevisionsSubsystem050ROW1189.add(UtilityBusiness.createNameRevision(namePartRevisionsSubsystem050ROW1189.get(1)));

        // ----------

        NamePart namePartSystemGroupCentralServices = UtilityModel.createNamePartSection(UUID_NAMEPART_4_SYSTEMGROUP_CENTRAL_SERVICES);
        NamePart namePartSystemPBI                  = UtilityModel.createNamePartSection(UUID_NAMEPART_2774_SYSTEM_PBI);
        NamePart namePartSubsystemBCM01             = UtilityModel.createNamePartSection(UUID_NAMEPART_2778_SUBSYSSTEM_BCM01);

        Whitebox.setInternalState(namePartSystemGroupCentralServices, ID, 4L);
        Whitebox.setInternalState(namePartSystemPBI,                  ID, 2774L);
        Whitebox.setInternalState(namePartSubsystemBCM01,             ID, 2778L);

        // 3, 2, 1 - name part revisions

        namePartRevisionsSystemGroupCentralServices4.add(UtilityModel.createNamePartRevision(namePartSystemGroupCentralServices, UtilityBusiness.parseDateOrNewDate(DATE_2014_04_04_17_52_59), userAccountSuperUser, INITIAL_DATA, false, null, CENTRAL_SERVICES, "CS",     null, "CS",     UtilityBusiness.parseDateOrNewDate(DATE_2014_04_04_17_52_59), null, NamePartRevisionStatus.APPROVED));
        namePartRevisionsSystemGroupCentralServices4.add(UtilityModel.createNamePartRevision(namePartSystemGroupCentralServices, UtilityBusiness.parseDateOrNewDate("2015-04-30 10:48:09"), userAccountSuperUser, MNEMONICS_FOR_SUPER_SECTION_IS_NOT_PART_OF_NAMES_AND_HAVE_BEEN_REMOVED_IN_ORDER_NOT_TO_CONFUSE_USERS_OF_THE_NAMING_CONVENTION, false, null, CENTRAL_SERVICES, "CS",     null, "CS",     UtilityBusiness.parseDateOrNewDate("2015-04-30 10:48:33"), null, NamePartRevisionStatus.CANCELLED));
        namePartRevisionsSystemGroupCentralServices4.add(UtilityModel.createNamePartRevision(namePartSystemGroupCentralServices, UtilityBusiness.parseDateOrNewDate("2015-04-30 10:48:33"), userAccountSuperUser, MNEMONICS_FOR_SUPER_SECTION_IS_NOT_PART_OF_NAMES_AND_HAVE_BEEN_REMOVED_IN_ORDER_NOT_TO_CONFUSE_USERS_OF_THE_NAMING_CONVENTION, false, null, CENTRAL_SERVICES, null,     null, null,     UtilityBusiness.parseDateOrNewDate("2015-04-30 11:44:13"), "Mnemonics for Super Section is not part of names and have been removed in order not to confuse users of the naming convention",   NamePartRevisionStatus.APPROVED));
        namePartRevisionsSystemPBI2774.add(              UtilityModel.createNamePartRevision(namePartSystemPBI,                  UtilityBusiness.parseDateOrNewDate("2020-02-26 13:06:21"), userAccountSuperUser, "Placeholder for the section \"PBI\" so subsections can be entered. The name will be changed to PBI once Jira NT-256 is resolved. Wait with approval until the correct name is in place.", false, namePartSystemGroupCentralServices, PROTON_BEAM_INSTRUMENTATION, "PBItmp", "Placeholder for the section \"PBI\". The name will be changed to PBI once Jira NT-256 is resolved.", "PB1TMP", UtilityBusiness.parseDateOrNewDate("2020-03-27 14:03:15"), AUTOMATICALLY_CANCELLED_BEFORE_MODIFY,   NamePartRevisionStatus.CANCELLED));
        namePartRevisionsSystemPBI2774.add(              UtilityModel.createNamePartRevision(namePartSystemPBI,                  UtilityBusiness.parseDateOrNewDate("2020-03-27 14:03:15"), userAccountSuperUser, "Change according to ICSHWI-3839", false, namePartSystemGroupCentralServices, PROTON_BEAM_INSTRUMENTATION, "PBI",    "Proton beam instrumentation", "PB1",    UtilityBusiness.parseDateOrNewDate("2020-03-27 14:14:32"), APPROVED_BY_ALFIO,   NamePartRevisionStatus.APPROVED));
        namePartRevisionsSubsystemBCM012778.add(         UtilityModel.createNamePartRevision(namePartSubsystemBCM01,             UtilityBusiness.parseDateOrNewDate("2020-02-26 13:13:51"), userAccountSuperUser, null, false, namePartSystemPBI, "PBI BCM-01", "BCM01",  "PBI BCM-01 - PBI control group", "BCM1",   UtilityBusiness.parseDateOrNewDate("2020-03-27 14:14:47"), APPROVED_BY_ALFIO,   NamePartRevisionStatus.APPROVED));

        Whitebox.setInternalState(namePartRevisionsSystemGroupCentralServices4.get(0), ID, 4L);
        Whitebox.setInternalState(namePartRevisionsSystemGroupCentralServices4.get(1), ID, 1051L);
        Whitebox.setInternalState(namePartRevisionsSystemGroupCentralServices4.get(2), ID, 1052L);
        Whitebox.setInternalState(namePartRevisionsSystemPBI2774.get(0),               ID, 4723L);
        Whitebox.setInternalState(namePartRevisionsSystemPBI2774.get(1),               ID, 4826L);
        Whitebox.setInternalState(namePartRevisionsSubsystemBCM012778.get(0),          ID, 4727L);

        nameRevisionsSystemGroupCentralServices4.add(UtilityBusiness.createNameRevision(namePartRevisionsSystemGroupCentralServices4.get(0)));
        nameRevisionsSystemGroupCentralServices4.add(UtilityBusiness.createNameRevision(namePartRevisionsSystemGroupCentralServices4.get(1)));
        nameRevisionsSystemGroupCentralServices4.add(UtilityBusiness.createNameRevision(namePartRevisionsSystemGroupCentralServices4.get(2)));
        nameRevisionsSystemPBI2774.add(UtilityBusiness.createNameRevision(namePartRevisionsSystemPBI2774.get(0)));
        nameRevisionsSystemPBI2774.add(UtilityBusiness.createNameRevision(namePartRevisionsSystemPBI2774.get(1)));
        nameRevisionsSubsystemBCM012778.add(UtilityBusiness.createNameRevision(namePartRevisionsSubsystemBCM012778.get(0)));

        // ----------

        NamePart namePartSystemGroupTargetStation = UtilityModel.createNamePartSection(UUID_NAMEPART_2_SYSTEMGROUP_TARGET_STATION);
        NamePart namePartSystemTGT                = UtilityModel.createNamePartSection(UUID_NAMEPART_2566_SYSTEM_TGT);
        NamePart namePartSubsystemTSS1080         = UtilityModel.createNamePartSection(UUID_NAMEPART_2607_SUBSYSSTEM_TSS1080);

        Whitebox.setInternalState(namePartSystemGroupCentralServices, ID, 2L);
        Whitebox.setInternalState(namePartSystemPBI,                  ID, 2566L);
        Whitebox.setInternalState(namePartSubsystemBCM01,             ID, 2607L);

        // 4, 1, 1 - name part revisions

        namePartRevisionsSystemGroupTargetStation2.add(UtilityModel.createNamePartRevision(namePartSystemGroupTargetStation, UtilityBusiness.parseDateOrNewDate("2014-04-04 17:52:59"), userAccountSuperUser, "Initial data", false, null,                             TARGET_STATION, "TS",      "Target Safety System", "TS",      UtilityBusiness.parseDateOrNewDate("2014-04-04 17:52:59"), null, NamePartRevisionStatus.APPROVED));
        namePartRevisionsSystemGroupTargetStation2.add(UtilityModel.createNamePartRevision(namePartSystemGroupTargetStation, UtilityBusiness.parseDateOrNewDate("2015-03-04 09:56:00"), userAccountSuperUser, "Temporarily changed to TgtS until a naming rule has been changed so that the we can use TS for temperatur sensor.", false, null,                             TARGET_STATION, "TgtS",    TARGET_STATION, "TGTS",    UtilityBusiness.parseDateOrNewDate("2015-03-04 09:56:42"), "could not approve it", NamePartRevisionStatus.REJECTED));
        namePartRevisionsSystemGroupTargetStation2.add(UtilityModel.createNamePartRevision(namePartSystemGroupTargetStation, UtilityBusiness.parseDateOrNewDate("2015-03-04 09:57:42"), userAccountSuperUser, "Another try ", false, null,                             TARGET_STATION, "TgtS",    null, "TGTS",    UtilityBusiness.parseDateOrNewDate("2015-03-04 09:58:42"), "Could not approve", NamePartRevisionStatus.REJECTED));
        namePartRevisionsSystemGroupTargetStation2.add(UtilityModel.createNamePartRevision(namePartSystemGroupTargetStation, UtilityBusiness.parseDateOrNewDate("2015-04-30 10:49:51"), userAccountSuperUser, null, false, null,                             TARGET_STATION, null,      null, null,      UtilityBusiness.parseDateOrNewDate("2015-05-06 14:25:07"), "Mnemonics for Super Section are not part of device names and have been removed to avoid confusion. ", NamePartRevisionStatus.APPROVED));
        namePartRevisionsSystemTGT2566.add(            UtilityModel.createNamePartRevision(namePartSystemTGT,                UtilityBusiness.parseDateOrNewDate("2019-11-28 13:44:11"), userAccountSuperUser, "New section according to new Target Naming Convention", false, namePartSystemGroupTargetStation, TARGET_STATION, "Tgt",     null, "TGT",     UtilityBusiness.parseDateOrNewDate("2019-11-28 13:44:32"), "New section according to new Target Naming Convention", NamePartRevisionStatus.APPROVED));
        namePartRevisionsSubsystemTSS10802607.add(     UtilityModel.createNamePartRevision(namePartSubsystemTSS1080,         UtilityBusiness.parseDateOrNewDate("2019-11-28 14:41:15"), userAccountSuperUser, "New Subsection added according to new Target Naming Convention", false, namePartSystemTGT,         "Target Safety System", "TSS1080", null, "TSS1080", UtilityBusiness.parseDateOrNewDate("2019-11-28 15:15:25"), "New Subsection added according to new Target Naming Convention", NamePartRevisionStatus.APPROVED));

        Whitebox.setInternalState(namePartRevisionsSystemGroupTargetStation2.get(0), ID, 2L);
        Whitebox.setInternalState(namePartRevisionsSystemGroupTargetStation2.get(1), ID, 989L);
        Whitebox.setInternalState(namePartRevisionsSystemGroupTargetStation2.get(2), ID, 990L);
        Whitebox.setInternalState(namePartRevisionsSystemGroupTargetStation2.get(3), ID, 1057L);
        Whitebox.setInternalState(namePartRevisionsSystemTGT2566.get(0),             ID, 4361L);
        Whitebox.setInternalState(namePartRevisionsSubsystemTSS10802607.get(0),      ID, 4402L);

        nameRevisionsSystemGroupTargetStation2.add(UtilityBusiness.createNameRevision(namePartRevisionsSystemGroupTargetStation2.get(0)));
        nameRevisionsSystemGroupTargetStation2.add(UtilityBusiness.createNameRevision(namePartRevisionsSystemGroupTargetStation2.get(1)));
        nameRevisionsSystemGroupTargetStation2.add(UtilityBusiness.createNameRevision(namePartRevisionsSystemGroupTargetStation2.get(2)));
        nameRevisionsSystemGroupTargetStation2.add(UtilityBusiness.createNameRevision(namePartRevisionsSystemGroupTargetStation2.get(3)));
        nameRevisionsSystemTGT2566.add(UtilityBusiness.createNameRevision(namePartRevisionsSystemTGT2566.get(0)));
        nameRevisionsSubsystemTSS10802607.add(UtilityBusiness.createNameRevision(namePartRevisionsSubsystemTSS10802607.get(0)));
    }

    /**
     * Set up test structure - device type - name part revisions, name revisions.
     */
    private void setupNameRevisionDeviceType() {
        namePartRevisionsDisciplinePBI237 = new ArrayList<>();
        nameRevisionsDisciplinePBI237 = new ArrayList<>();

        namePartRevisionsDeviceGroupControlElectronics1435 = new ArrayList<>();
        nameRevisionsDeviceGroupControllerElectronics1435 = new ArrayList<>();

        namePartRevisionsDeviceTypeMTCA1452 = new ArrayList<>();
        nameRevisionsDeviceTypeMTCA1452 = new ArrayList<>();

        // ----------

        namePartRevisionsDisciplineCTRL502 = new ArrayList<>();
        nameRevisionsDisciplineCTRL502 = new ArrayList<>();

        namePartRevisionsDeviceGroupEnclosures2870 = new ArrayList<>();
        nameRevisionsDeviceGroupEnclosures2870 = new ArrayList<>();

        namePartRevisionsDeviceTypeMTCA3U2875 = new ArrayList<>();
        nameRevisionsDeviceTypeMTCA3U2875 = new ArrayList<>();

        // ----------

        namePartRevisionsDeviceGroupSupportingEquipment1007 = new ArrayList<>();
        nameRevisionsDeviceGroupSupportingEquipment1007 = new ArrayList<>();

        namePartRevisionsDeviceTypeSPROT3025 = new ArrayList<>();
        nameRevisionsDeviceTypeSPROT3025 = new ArrayList<>();

        // ----------

        NamePart namePartDisciplinePBI237                  = UtilityModel.createNamePartDeviceType(UUID_NAMEPART_237_DISCIPLINE_PBI);
        NamePart namePartDeviceGroupControlElectronics1435 = UtilityModel.createNamePartDeviceType(UUID_NAMEPART_1435_DEVICEGROUP_CONTROL_ELECTRONICS);
        NamePart namePartDeviceTypeMTCA1452                = UtilityModel.createNamePartDeviceType(UUID_NAMEPART_1452_DEVICETYPE_MTCA);

        Whitebox.setInternalState(namePartDisciplinePBI237,                   ID, 237L);
        Whitebox.setInternalState(namePartDeviceGroupControlElectronics1435,  ID, 1435L);
        Whitebox.setInternalState(namePartDeviceTypeMTCA1452,                 ID, 1452L);

        // 1, 4, 2 - name part revisions

        namePartRevisionsDisciplinePBI237.add(                 UtilityModel.createNamePartRevision(namePartDisciplinePBI237,                  UtilityBusiness.parseDateOrNewDate("2014-04-04 17:53:02"), userAccountSuperUser, INITIAL_DATA, false, null, PROTON_BEAM_INSTRUMENTATION, "PBI",  null, "PB1",  UtilityBusiness.parseDateOrNewDate("2014-04-04 17:53:02"), null, NamePartRevisionStatus.APPROVED));
        namePartRevisionsDeviceGroupControlElectronics1435.add(UtilityModel.createNamePartRevision(namePartDeviceGroupControlElectronics1435, UtilityBusiness.parseDateOrNewDate("2016-10-14 16:01:40"), userAccountSuperUser, null, false, namePartDisciplinePBI237, "Control", null,   null, null,   UtilityBusiness.parseDateOrNewDate("2016-10-20 11:21:09"), APPROVED_BY_DANIEL_PISO,   NamePartRevisionStatus.APPROVED));
        namePartRevisionsDeviceGroupControlElectronics1435.add(UtilityModel.createNamePartRevision(namePartDeviceGroupControlElectronics1435, UtilityBusiness.parseDateOrNewDate("2017-10-12 12:30:56"), userAccountSuperUser, null, false, namePartDisciplinePBI237, "Control electronics", null,   null, null,   UtilityBusiness.parseDateOrNewDate("2017-10-12 13:07:45"), AUTOMATICALLY_CANCELLED_BEFORE_MODIFY,   NamePartRevisionStatus.CANCELLED));
        namePartRevisionsDeviceGroupControlElectronics1435.add(UtilityModel.createNamePartRevision(namePartDeviceGroupControlElectronics1435, UtilityBusiness.parseDateOrNewDate("2017-10-12 13:07:45"), userAccountSuperUser, null, false, namePartDisciplinePBI237, "BPM front end unit", null,   "BPMFE", null,   UtilityBusiness.parseDateOrNewDate("2017-10-12 13:08:40"), AUTOMATICALLY_CANCELLED_BEFORE_MODIFY,   NamePartRevisionStatus.CANCELLED));
        namePartRevisionsDeviceGroupControlElectronics1435.add(UtilityModel.createNamePartRevision(namePartDeviceGroupControlElectronics1435, UtilityBusiness.parseDateOrNewDate("2017-10-12 13:08:40"), userAccountSuperUser, null, false, namePartDisciplinePBI237, "Control electronics", null,   null, null,   UtilityBusiness.parseDateOrNewDate("2017-10-23 14:35:13"), "Approve requests from Inigo Alonso.",   NamePartRevisionStatus.APPROVED));
        namePartRevisionsDeviceTypeMTCA1452.add(               UtilityModel.createNamePartRevision(namePartDeviceTypeMTCA1452,                UtilityBusiness.parseDateOrNewDate("2016-11-08 13:54:10"), userAccountSuperUser, null, false, namePartDeviceGroupControlElectronics1435, "uTCA crate", "MTCA", null, "MTCA", UtilityBusiness.parseDateOrNewDate("2016-11-09 13:31:07"), "Additions are consistent with naming policy agreed by HW&I group.",   NamePartRevisionStatus.APPROVED));
        namePartRevisionsDeviceTypeMTCA1452.add(               UtilityModel.createNamePartRevision(namePartDeviceTypeMTCA1452,                UtilityBusiness.parseDateOrNewDate("2020-04-07 10:45:56"), userAccountSuperUser, "Moved to Ctrl discipline", true,  namePartDeviceGroupControlElectronics1435, "uTCA crate", "MTCA", null, "MTCA", UtilityBusiness.parseDateOrNewDate("2020-04-08 09:21:41"), "approved by alfio",   NamePartRevisionStatus.APPROVED));

        Whitebox.setInternalState(namePartRevisionsDisciplinePBI237.get(0),                      ID, 237L);
        Whitebox.setInternalState(namePartRevisionsDeviceGroupControlElectronics1435.get(0),     ID, 2370L);
        Whitebox.setInternalState(namePartRevisionsDeviceGroupControlElectronics1435.get(1),     ID, 2957L);
        Whitebox.setInternalState(namePartRevisionsDeviceGroupControlElectronics1435.get(2),     ID, 2993L);
        Whitebox.setInternalState(namePartRevisionsDeviceGroupControlElectronics1435.get(3),     ID, 2994L);
        Whitebox.setInternalState(namePartRevisionsDeviceTypeMTCA1452.get(0),                    ID, 2390L);
        Whitebox.setInternalState(namePartRevisionsDeviceTypeMTCA1452.get(1),                    ID, 4902L);

        nameRevisionsDisciplinePBI237.add(UtilityBusiness.createNameRevision(namePartRevisionsDisciplinePBI237.get(0)));
        nameRevisionsDeviceGroupControllerElectronics1435.add(UtilityBusiness.createNameRevision(namePartRevisionsDeviceGroupControlElectronics1435.get(0)));
        nameRevisionsDeviceGroupControllerElectronics1435.add(UtilityBusiness.createNameRevision(namePartRevisionsDeviceGroupControlElectronics1435.get(1)));
        nameRevisionsDeviceGroupControllerElectronics1435.add(UtilityBusiness.createNameRevision(namePartRevisionsDeviceGroupControlElectronics1435.get(2)));
        nameRevisionsDeviceGroupControllerElectronics1435.add(UtilityBusiness.createNameRevision(namePartRevisionsDeviceGroupControlElectronics1435.get(3)));
        nameRevisionsDeviceTypeMTCA1452.add(UtilityBusiness.createNameRevision(namePartRevisionsDeviceTypeMTCA1452.get(0)));
        nameRevisionsDeviceTypeMTCA1452.add(UtilityBusiness.createNameRevision(namePartRevisionsDeviceTypeMTCA1452.get(1)));

        // ----------

        NamePart namePartDisciplineCTRL502         = UtilityModel.createNamePartSection(UUID_NAMEPART_502_DISCIPLINE_CTRL);
        NamePart namePartDeviceGroupEnclosures2870 = UtilityModel.createNamePartSection(UUID_NAMEPART_2870_DEVICEGROUP_ENCLOSURES);
        NamePart namePartDeviceTypeMTCA3U2875      = UtilityModel.createNamePartSection(UUID_NAMEPART_2875_DEVICETYPE_MTCA3U);

        Whitebox.setInternalState(namePartDisciplineCTRL502,         ID, 502L);
        Whitebox.setInternalState(namePartDeviceGroupEnclosures2870, ID, 2870L);
        Whitebox.setInternalState(namePartDeviceTypeMTCA3U2875,      ID, 2875L);

        // 3, 1, 1 - name part revisions

        namePartRevisionsDisciplineCTRL502.add(        UtilityModel.createNamePartRevision(namePartDisciplineCTRL502,         UtilityBusiness.parseDateOrNewDate("2014-11-19 11:36:10"), userAccountSuperUser, "Discipline for control system devices", false, null, CONTROL_SYSTEM, "CTR",    null, "CTR",    UtilityBusiness.parseDateOrNewDate("2014-11-19 11:39:25"), null, NamePartRevisionStatus.APPROVED));
        namePartRevisionsDisciplineCTRL502.add(        UtilityModel.createNamePartRevision(namePartDisciplineCTRL502,         UtilityBusiness.parseDateOrNewDate("2015-01-19 11:39:27"), userAccountSuperUser, "Renamed ", false, null, CONTROL_SYSTEM, "CSys",   null, "CSYS",   UtilityBusiness.parseDateOrNewDate("2015-01-19 11:39:54"), null, NamePartRevisionStatus.APPROVED));
        namePartRevisionsDisciplineCTRL502.add(        UtilityModel.createNamePartRevision(namePartDisciplineCTRL502,         UtilityBusiness.parseDateOrNewDate("2015-01-22 11:00:46"), userAccountSuperUser, null, false, null, CONTROL_SYSTEM, "Ctrl",   null, "CTR1",   UtilityBusiness.parseDateOrNewDate("2015-01-22 11:00:59"), null, NamePartRevisionStatus.APPROVED));
        namePartRevisionsDeviceGroupEnclosures2870.add(UtilityModel.createNamePartRevision(namePartDeviceGroupEnclosures2870, UtilityBusiness.parseDateOrNewDate("2020-03-31 21:35:04"), userAccountSuperUser, "JN 20200331", false, namePartDisciplineCTRL502, "Enclosures", null,     "Racks, enclosures, crates, etc...", null,     UtilityBusiness.parseDateOrNewDate("2020-04-03 10:17:29"), APPROVED_BY_ALFIO,   NamePartRevisionStatus.APPROVED));
        namePartRevisionsDeviceTypeMTCA3U2875.add(     UtilityModel.createNamePartRevision(namePartDeviceTypeMTCA3U2875,      UtilityBusiness.parseDateOrNewDate("2020-04-01 08:56:57"), userAccountSuperUser, null, false, namePartDeviceGroupEnclosures2870, "MTCA 3U crate", "MTCA3U", "MTCA 3U crate", "MTCA3U", UtilityBusiness.parseDateOrNewDate("2020-04-03 10:17:32"), APPROVED_BY_ALFIO,   NamePartRevisionStatus.APPROVED));

        Whitebox.setInternalState(namePartRevisionsDisciplineCTRL502.get(0),         ID, 584L);
        Whitebox.setInternalState(namePartRevisionsDisciplineCTRL502.get(1),         ID, 677L);
        Whitebox.setInternalState(namePartRevisionsDisciplineCTRL502.get(2),         ID, 799L);
        Whitebox.setInternalState(namePartRevisionsDeviceGroupEnclosures2870.get(0), ID, 4830L);
        Whitebox.setInternalState(namePartRevisionsDeviceTypeMTCA3U2875.get(0),      ID, 4835L);

        nameRevisionsDisciplineCTRL502.add(UtilityBusiness.createNameRevision(namePartRevisionsDisciplineCTRL502.get(0)));
        nameRevisionsDisciplineCTRL502.add(UtilityBusiness.createNameRevision(namePartRevisionsDisciplineCTRL502.get(1)));
        nameRevisionsDisciplineCTRL502.add(UtilityBusiness.createNameRevision(namePartRevisionsDisciplineCTRL502.get(2)));
        nameRevisionsDeviceGroupEnclosures2870.add(UtilityBusiness.createNameRevision(namePartRevisionsDeviceGroupEnclosures2870.get(0)));
        nameRevisionsDeviceTypeMTCA3U2875.add(UtilityBusiness.createNameRevision(namePartRevisionsDeviceTypeMTCA3U2875.get(0)));

        // ----------

        NamePart namePartDeviceGroupSupportingEquipment1007 = UtilityModel.createNamePartSection(UUID_NAMEPART_1007_DEVICEGROUP_SUPPORTING_EQUIPMENT);
        NamePart namePartDeviceTypeSPROT3025                = UtilityModel.createNamePartSection(UUID_NAMEPART_3025_DEVICETYPE_SPROT);

        Whitebox.setInternalState(namePartDeviceGroupSupportingEquipment1007, ID, 1007L);
        Whitebox.setInternalState(namePartDeviceTypeSPROT3025,                ID, 3025L);

        // see above (3), 2, 2 - name part revisions

        namePartRevisionsDeviceGroupSupportingEquipment1007.add(UtilityModel.createNamePartRevision(namePartDeviceGroupSupportingEquipment1007, UtilityBusiness.parseDateOrNewDate("2015-12-22 14:34:28"), userAccountSuperUser, null,                                                            false, namePartDisciplineCTRL502,             "Supporting Equipment", null,    null,                                                                                        null,   UtilityBusiness.parseDateOrNewDate("2015-12-22 14:37:17"), "Moved to another device group",              NamePartRevisionStatus.APPROVED));
        namePartRevisionsDeviceGroupSupportingEquipment1007.add(UtilityModel.createNamePartRevision(namePartDeviceGroupSupportingEquipment1007, UtilityBusiness.parseDateOrNewDate("2016-05-09 15:00:01"), userAccountSuperUser, null,                                                            true,  namePartDisciplineCTRL502,             "Supporting Equipment", null,    null,                                                                                        null,   UtilityBusiness.parseDateOrNewDate("2016-05-09 15:01:07"), "Naming Service CleanUp. Approved by Daniel", NamePartRevisionStatus.REJECTED));
        namePartRevisionsDeviceTypeSPROT3025.add(               UtilityModel.createNamePartRevision(namePartDeviceTypeSPROT3025,                UtilityBusiness.parseDateOrNewDate("2021-01-21 17:33:09"), userAccountSuperUser, "Used in TSS. The system monitors the status of these devices.", false, namePartDeviceGroupSupportingEquipment1007, "Surge Protector",      "SProt", "Electrical power supply safety device, typically found in an automation mation cabinet.",  "SPR0T", UtilityBusiness.parseDateOrNewDate("2021-01-22 15:11:15"), "Automatically cancelled before modify",      NamePartRevisionStatus.CANCELLED));
        namePartRevisionsDeviceTypeSPROT3025.add(               UtilityModel.createNamePartRevision(namePartDeviceTypeSPROT3025,                UtilityBusiness.parseDateOrNewDate("2021-01-22 15:11:15"), userAccountSuperUser, "Typo in the description corrected.",                            false, namePartDeviceGroupSupportingEquipment1007, "Surge Protector",      "SProt", "Electrical power supply safety device, typically found in an automation cabinet.",         "SPR0T", UtilityBusiness.parseDateOrNewDate("2021-01-22 15:23:27"), "Approved by Alfio.",                         NamePartRevisionStatus.APPROVED));

        Whitebox.setInternalState(namePartRevisionsDeviceGroupSupportingEquipment1007.get(0),     ID, 1590L);
        Whitebox.setInternalState(namePartRevisionsDeviceGroupSupportingEquipment1007.get(1),     ID, 1880L);
        Whitebox.setInternalState(namePartRevisionsDeviceTypeSPROT3025.get(0),                    ID, 5151L);
        Whitebox.setInternalState(namePartRevisionsDeviceTypeSPROT3025.get(1),                    ID, 5155L);

        nameRevisionsDeviceGroupSupportingEquipment1007.add(UtilityBusiness.createNameRevision(namePartRevisionsDeviceGroupSupportingEquipment1007.get(0)));
        nameRevisionsDeviceGroupSupportingEquipment1007.add(UtilityBusiness.createNameRevision(namePartRevisionsDeviceGroupSupportingEquipment1007.get(1)));
        nameRevisionsDeviceTypeSPROT3025.add(UtilityBusiness.createNameRevision(namePartRevisionsDeviceTypeSPROT3025.get(0)));
        nameRevisionsDeviceTypeSPROT3025.add(UtilityBusiness.createNameRevision(namePartRevisionsDeviceTypeSPROT3025.get(1)));
    }

    /**
     * Set up test structure - device - name part revisions, name revisions.
     */
    private void setupNameRevisionDevice() {
        deviceRevisions4108InstanceIndex_FEB_050ROW_PBI_MTCA_050     = new ArrayList<>();
        nameRevisions4108InstanceIndex_FEB_050ROW_PBI_MTCA_050       = new ArrayList<>();

        // ----------

        deviceRevisions70025InstanceIndex_TGT_TSS1080_CTRL_SPROT_08  = new ArrayList<>();
        nameRevisions70025InstanceIndex_TGT_TSS1080_CTRL_SPROT_08    = new ArrayList<>();

        // ----------

        Device device4108InstanceIndex_FEB_050ROW_PBI_MTCA_050     = UtilityModel.createDevice(UUID_DEVICE_4108_INSTANCEINDEX_FEB_050ROW_PBI_MTCA_050);

        Whitebox.setInternalState(device4108InstanceIndex_FEB_050ROW_PBI_MTCA_050,     ID, 4108L);

        NamePart namePartDeviceType1452 = namePartRevisionsDeviceTypeMTCA1452.get(0).getNamePart();
        NamePart namePartDeviceType2875 = namePartRevisionsDeviceTypeMTCA3U2875.get(0).getNamePart();
        NamePart namePartSubsystem1189  = namePartRevisionsSubsystem050ROW1189.get(0).getNamePart();
        NamePart namePartSubsystem2778  = namePartRevisionsSubsystemBCM012778.get(0).getNamePart();

        // 17 - device revisions

        deviceRevisions4108InstanceIndex_FEB_050ROW_PBI_MTCA_050.add(UtilityModel.createDeviceRevision(device4108InstanceIndex_FEB_050ROW_PBI_MTCA_050, UtilityBusiness.parseDateOrNewDate("2016-11-09 13:58:13"), userAccountEditor, false, namePartSubsystem1189, namePartDeviceType1452, "215100", "FEB-050ROW:PBI-MTCA-215100", "FEB-50R0W:PB1-MTCA-215100", "PBI ICBLM", null));
        deviceRevisions4108InstanceIndex_FEB_050ROW_PBI_MTCA_050.add(UtilityModel.createDeviceRevision(device4108InstanceIndex_FEB_050ROW_PBI_MTCA_050, UtilityBusiness.parseDateOrNewDate("2017-08-31 09:24:21"), userAccountEditor, false, namePartSubsystem1189, namePartDeviceType1452, "215100", "FEB-050Row:PBI-MTCA-215100", "FEB-50R0W:PB1-MTCA-215100", "PBI ICBLM", null));
        deviceRevisions4108InstanceIndex_FEB_050ROW_PBI_MTCA_050.add(UtilityModel.createDeviceRevision(device4108InstanceIndex_FEB_050ROW_PBI_MTCA_050, UtilityBusiness.parseDateOrNewDate("2017-11-28 10:03:30"), userAccountEditor, false, namePartSubsystem1189, namePartDeviceType1452, "004", "FEB-050Row:PBI-MTCA-004", "FEB-50R0W:PB1-MTCA-4", "PBI BCM-08", null));
        deviceRevisions4108InstanceIndex_FEB_050ROW_PBI_MTCA_050.add(UtilityModel.createDeviceRevision(device4108InstanceIndex_FEB_050ROW_PBI_MTCA_050, UtilityBusiness.parseDateOrNewDate("2017-12-04 09:40:14"), userAccountEditor, false, namePartSubsystem1189, namePartDeviceType1452, "040", FEB_050_ROW_PBI_MTCA_040, FEB_50R0W_PB1_MTCA_40, "PBI BCM-08", null));
        deviceRevisions4108InstanceIndex_FEB_050ROW_PBI_MTCA_050.add(UtilityModel.createDeviceRevision(device4108InstanceIndex_FEB_050ROW_PBI_MTCA_050, UtilityBusiness.parseDateOrNewDate("2018-03-07 16:35:24"), userAccountEditor, false, namePartSubsystem1189, namePartDeviceType1452, "040", FEB_050_ROW_PBI_MTCA_040, FEB_50R0W_PB1_MTCA_40, PBI_BCM_01_BCM_U_TCA_CRATE_2, null));
        deviceRevisions4108InstanceIndex_FEB_050ROW_PBI_MTCA_050.add(UtilityModel.createDeviceRevision(device4108InstanceIndex_FEB_050ROW_PBI_MTCA_050, UtilityBusiness.parseDateOrNewDate("2018-03-09 13:35:09"), userAccountEditor, false, namePartSubsystem1189, namePartDeviceType1452, "9040", "FEB-050Row:PBI-MTCA-9040", "FEB-50R0W:PB1-MTCA-9040", PBI_BCM_01_BCM_U_TCA_CRATE_2, null));
        deviceRevisions4108InstanceIndex_FEB_050ROW_PBI_MTCA_050.add(UtilityModel.createDeviceRevision(device4108InstanceIndex_FEB_050ROW_PBI_MTCA_050, UtilityBusiness.parseDateOrNewDate("2018-03-09 13:55:44"), userAccountEditor, false, namePartSubsystem1189, namePartDeviceType1452, "040", FEB_050_ROW_PBI_MTCA_040, FEB_50R0W_PB1_MTCA_40, PBI_BCM_01_BCM_U_TCA_CRATE_2, null));
        deviceRevisions4108InstanceIndex_FEB_050ROW_PBI_MTCA_050.add(UtilityModel.createDeviceRevision(device4108InstanceIndex_FEB_050ROW_PBI_MTCA_050, UtilityBusiness.parseDateOrNewDate("2018-05-30 12:48:05"), userAccountEditor, false, namePartSubsystem1189, namePartDeviceType1452, "050x", "FEB-050Row:PBI-MTCA-050x", "FEB-50R0W:PB1-MTCA-50X", PBI_BCM_01_BCM_U_TCA_CRATE_2, null));
        deviceRevisions4108InstanceIndex_FEB_050ROW_PBI_MTCA_050.add(UtilityModel.createDeviceRevision(device4108InstanceIndex_FEB_050ROW_PBI_MTCA_050, UtilityBusiness.parseDateOrNewDate("2018-05-30 12:52:28"), userAccountEditor, false, namePartSubsystem1189, namePartDeviceType1452, "050", FEB_050_ROW_PBI_MTCA_050, FEB_50R0W_PB1_MTCA_50, PBI_BCM_01_BCM_U_TCA_CRATE_2, null));
        deviceRevisions4108InstanceIndex_FEB_050ROW_PBI_MTCA_050.add(UtilityModel.createDeviceRevision(device4108InstanceIndex_FEB_050ROW_PBI_MTCA_050, UtilityBusiness.parseDateOrNewDate("2018-05-31 09:54:14"), userAccountEditor, false, namePartSubsystem1189, namePartDeviceType1452, "050", FEB_050_ROW_PBI_MTCA_050, FEB_50R0W_PB1_MTCA_50, "PBI FBCM-01 - LEBT fast BCM uTCA crate", null));
        deviceRevisions4108InstanceIndex_FEB_050ROW_PBI_MTCA_050.add(UtilityModel.createDeviceRevision(device4108InstanceIndex_FEB_050ROW_PBI_MTCA_050, UtilityBusiness.parseDateOrNewDate("2018-05-31 10:03:16"), userAccountEditor, false, namePartSubsystem1189, namePartDeviceType1452, "050", FEB_050_ROW_PBI_MTCA_050, FEB_50R0W_PB1_MTCA_50, PBI_BCM_01_BCM_U_TCA_CRATE_2, null));
        deviceRevisions4108InstanceIndex_FEB_050ROW_PBI_MTCA_050.add(UtilityModel.createDeviceRevision(device4108InstanceIndex_FEB_050ROW_PBI_MTCA_050, UtilityBusiness.parseDateOrNewDate("2018-11-15 13:08:32"), userAccountEditor, false, namePartSubsystem1189, namePartDeviceType1452, "050", FEB_050_ROW_PBI_MTCA_050, FEB_50R0W_PB1_MTCA_50, "PBI BCM-01 - BCM uTCA 2 crate", null));
        deviceRevisions4108InstanceIndex_FEB_050ROW_PBI_MTCA_050.add(UtilityModel.createDeviceRevision(device4108InstanceIndex_FEB_050ROW_PBI_MTCA_050, UtilityBusiness.parseDateOrNewDate("2019-12-06 12:35:51"), userAccountEditor, false, namePartSubsystem1189, namePartDeviceType1452, "050", FEB_050_ROW_PBI_MTCA_050, FEB_50R0W_PB1_MTCA_50, PBI_BCM_01_BCM_U_TCA_CRATE_B, null));
        deviceRevisions4108InstanceIndex_FEB_050ROW_PBI_MTCA_050.add(UtilityModel.createDeviceRevision(device4108InstanceIndex_FEB_050ROW_PBI_MTCA_050, UtilityBusiness.parseDateOrNewDate("2020-03-31 10:47:26"), userAccountEditor, false, namePartSubsystem2778, namePartDeviceType1452, "200", "PBI-BCM01:PBI-MTCA-200", "PB1-BCM1:PB1-MTCA-200", PBI_BCM_01_BCM_U_TCA_CRATE_B, null));
        deviceRevisions4108InstanceIndex_FEB_050ROW_PBI_MTCA_050.add(UtilityModel.createDeviceRevision(device4108InstanceIndex_FEB_050ROW_PBI_MTCA_050, UtilityBusiness.parseDateOrNewDate("2020-04-06 08:21:00"), userAccountEditor, false, namePartSubsystem2778, namePartDeviceType2875, "200", "PBI-BCM01:Ctrl-MTCA3U-200", "PB1-BCM1:CTR1-MTCA3U-200", PBI_BCM_01_BCM_U_TCA_CRATE_B, null));
        deviceRevisions4108InstanceIndex_FEB_050ROW_PBI_MTCA_050.add(UtilityModel.createDeviceRevision(device4108InstanceIndex_FEB_050ROW_PBI_MTCA_050, UtilityBusiness.parseDateOrNewDate("2020-04-07 09:42:59"), userAccountEditor, false, namePartSubsystem2778, namePartDeviceType2875, "201", "PBI-BCM01:Ctrl-MTCA3U-201", "PB1-BCM1:CTR1-MTCA3U-201", PBI_BCM_01_BCM_U_TCA_CRATE_B, null));
        deviceRevisions4108InstanceIndex_FEB_050ROW_PBI_MTCA_050.add(UtilityModel.createDeviceRevision(device4108InstanceIndex_FEB_050ROW_PBI_MTCA_050, UtilityBusiness.parseDateOrNewDate("2020-04-15 09:28:59"), userAccountEditor, false, namePartSubsystem2778, namePartDeviceType2875, "201", "PBI-BCM01:Ctrl-MTCA3U-201", "PB1-BCM1:CTR1-MTCA3U-201", "PBI BCM-01 - BCM uTCA 2 crate", null));

        Whitebox.setInternalState(deviceRevisions4108InstanceIndex_FEB_050ROW_PBI_MTCA_050.get(0),   ID, 6543L);
        Whitebox.setInternalState(deviceRevisions4108InstanceIndex_FEB_050ROW_PBI_MTCA_050.get(1),   ID, 23348L);
        Whitebox.setInternalState(deviceRevisions4108InstanceIndex_FEB_050ROW_PBI_MTCA_050.get(2),   ID, 36636L);
        Whitebox.setInternalState(deviceRevisions4108InstanceIndex_FEB_050ROW_PBI_MTCA_050.get(3),   ID, 42812L);
        Whitebox.setInternalState(deviceRevisions4108InstanceIndex_FEB_050ROW_PBI_MTCA_050.get(4),   ID, 73122L);
        Whitebox.setInternalState(deviceRevisions4108InstanceIndex_FEB_050ROW_PBI_MTCA_050.get(5),   ID, 85393L);
        Whitebox.setInternalState(deviceRevisions4108InstanceIndex_FEB_050ROW_PBI_MTCA_050.get(6),   ID, 89039L);
        Whitebox.setInternalState(deviceRevisions4108InstanceIndex_FEB_050ROW_PBI_MTCA_050.get(7),   ID, 92290L);
        Whitebox.setInternalState(deviceRevisions4108InstanceIndex_FEB_050ROW_PBI_MTCA_050.get(8),   ID, 92313L);
        Whitebox.setInternalState(deviceRevisions4108InstanceIndex_FEB_050ROW_PBI_MTCA_050.get(9),   ID, 92446L);
        Whitebox.setInternalState(deviceRevisions4108InstanceIndex_FEB_050ROW_PBI_MTCA_050.get(10),  ID, 92544L);
        Whitebox.setInternalState(deviceRevisions4108InstanceIndex_FEB_050ROW_PBI_MTCA_050.get(11),  ID, 95904L);
        Whitebox.setInternalState(deviceRevisions4108InstanceIndex_FEB_050ROW_PBI_MTCA_050.get(12),  ID, 104108L);
        Whitebox.setInternalState(deviceRevisions4108InstanceIndex_FEB_050ROW_PBI_MTCA_050.get(13),  ID, 109390L);
        Whitebox.setInternalState(deviceRevisions4108InstanceIndex_FEB_050ROW_PBI_MTCA_050.get(14),  ID, 112748L);
        Whitebox.setInternalState(deviceRevisions4108InstanceIndex_FEB_050ROW_PBI_MTCA_050.get(15),  ID, 113113L);
        Whitebox.setInternalState(deviceRevisions4108InstanceIndex_FEB_050ROW_PBI_MTCA_050.get(16),  ID, 113331L);

        nameRevisions4108InstanceIndex_FEB_050ROW_PBI_MTCA_050.add(UtilityBusiness.createNameRevision(deviceRevisions4108InstanceIndex_FEB_050ROW_PBI_MTCA_050.get(0)));
        nameRevisions4108InstanceIndex_FEB_050ROW_PBI_MTCA_050.add(UtilityBusiness.createNameRevision(deviceRevisions4108InstanceIndex_FEB_050ROW_PBI_MTCA_050.get(1)));
        nameRevisions4108InstanceIndex_FEB_050ROW_PBI_MTCA_050.add(UtilityBusiness.createNameRevision(deviceRevisions4108InstanceIndex_FEB_050ROW_PBI_MTCA_050.get(2)));
        nameRevisions4108InstanceIndex_FEB_050ROW_PBI_MTCA_050.add(UtilityBusiness.createNameRevision(deviceRevisions4108InstanceIndex_FEB_050ROW_PBI_MTCA_050.get(3)));
        nameRevisions4108InstanceIndex_FEB_050ROW_PBI_MTCA_050.add(UtilityBusiness.createNameRevision(deviceRevisions4108InstanceIndex_FEB_050ROW_PBI_MTCA_050.get(4)));
        nameRevisions4108InstanceIndex_FEB_050ROW_PBI_MTCA_050.add(UtilityBusiness.createNameRevision(deviceRevisions4108InstanceIndex_FEB_050ROW_PBI_MTCA_050.get(5)));
        nameRevisions4108InstanceIndex_FEB_050ROW_PBI_MTCA_050.add(UtilityBusiness.createNameRevision(deviceRevisions4108InstanceIndex_FEB_050ROW_PBI_MTCA_050.get(6)));
        nameRevisions4108InstanceIndex_FEB_050ROW_PBI_MTCA_050.add(UtilityBusiness.createNameRevision(deviceRevisions4108InstanceIndex_FEB_050ROW_PBI_MTCA_050.get(7)));
        nameRevisions4108InstanceIndex_FEB_050ROW_PBI_MTCA_050.add(UtilityBusiness.createNameRevision(deviceRevisions4108InstanceIndex_FEB_050ROW_PBI_MTCA_050.get(8)));
        nameRevisions4108InstanceIndex_FEB_050ROW_PBI_MTCA_050.add(UtilityBusiness.createNameRevision(deviceRevisions4108InstanceIndex_FEB_050ROW_PBI_MTCA_050.get(9)));
        nameRevisions4108InstanceIndex_FEB_050ROW_PBI_MTCA_050.add(UtilityBusiness.createNameRevision(deviceRevisions4108InstanceIndex_FEB_050ROW_PBI_MTCA_050.get(10)));
        nameRevisions4108InstanceIndex_FEB_050ROW_PBI_MTCA_050.add(UtilityBusiness.createNameRevision(deviceRevisions4108InstanceIndex_FEB_050ROW_PBI_MTCA_050.get(11)));
        nameRevisions4108InstanceIndex_FEB_050ROW_PBI_MTCA_050.add(UtilityBusiness.createNameRevision(deviceRevisions4108InstanceIndex_FEB_050ROW_PBI_MTCA_050.get(12)));
        nameRevisions4108InstanceIndex_FEB_050ROW_PBI_MTCA_050.add(UtilityBusiness.createNameRevision(deviceRevisions4108InstanceIndex_FEB_050ROW_PBI_MTCA_050.get(13)));
        nameRevisions4108InstanceIndex_FEB_050ROW_PBI_MTCA_050.add(UtilityBusiness.createNameRevision(deviceRevisions4108InstanceIndex_FEB_050ROW_PBI_MTCA_050.get(14)));
        nameRevisions4108InstanceIndex_FEB_050ROW_PBI_MTCA_050.add(UtilityBusiness.createNameRevision(deviceRevisions4108InstanceIndex_FEB_050ROW_PBI_MTCA_050.get(15)));
        nameRevisions4108InstanceIndex_FEB_050ROW_PBI_MTCA_050.add(UtilityBusiness.createNameRevision(deviceRevisions4108InstanceIndex_FEB_050ROW_PBI_MTCA_050.get(16)));

        // ----------

        Device device70025InstanceIndex_TGT_TSS1080_CTRL_SPROT_08     = UtilityModel.createDevice(UUID_DEVICE_70025_INSTANCEINDEX_TGT_TSS1080_CTRL_SPROT_08);

        Whitebox.setInternalState(device70025InstanceIndex_TGT_TSS1080_CTRL_SPROT_08,     ID, 70025L);

        NamePart namePartDeviceType3025 = namePartRevisionsDeviceTypeSPROT3025.get(0).getNamePart();
        NamePart namePartSubsystem2607  = namePartRevisionsSubsystemTSS10802607.get(0).getNamePart();

        // 3 - device revisions

        deviceRevisions70025InstanceIndex_TGT_TSS1080_CTRL_SPROT_08.add(UtilityModel.createDeviceRevision(device70025InstanceIndex_TGT_TSS1080_CTRL_SPROT_08, UtilityBusiness.parseDateOrNewDate("2021-02-03 08:01:42"), userAccountEditor, false, namePartSubsystem2607, namePartDeviceType3025, "8",  "Tgt-TSS1080:Ctrl-SProt-8",  TGT_TSS1080_CTR1_SPR0T_8, SURGE_PROTECTION_RFQ_CTRL, null));
        deviceRevisions70025InstanceIndex_TGT_TSS1080_CTRL_SPROT_08.add(UtilityModel.createDeviceRevision(device70025InstanceIndex_TGT_TSS1080_CTRL_SPROT_08, UtilityBusiness.parseDateOrNewDate("2021-02-03 11:11:58"), userAccountEditor, false, namePartSubsystem2607, namePartDeviceType3025, "08", "Tgt-TSS1080:Ctrl-SProt-08", TGT_TSS1080_CTR1_SPR0T_8, SURGE_PROTECTION_RFQ_CTRL, null));
        deviceRevisions70025InstanceIndex_TGT_TSS1080_CTRL_SPROT_08.add(UtilityModel.createDeviceRevision(device70025InstanceIndex_TGT_TSS1080_CTRL_SPROT_08, UtilityBusiness.parseDateOrNewDate("2021-02-09 10:00:33"), userAccountEditor, true,  namePartSubsystem2607, namePartDeviceType3025, "08", "Tgt-TSS1080:Ctrl-SProt-08", TGT_TSS1080_CTR1_SPR0T_8, SURGE_PROTECTION_RFQ_CTRL, null));

        Whitebox.setInternalState(deviceRevisions70025InstanceIndex_TGT_TSS1080_CTRL_SPROT_08.get(0),   ID, 119719L);
        Whitebox.setInternalState(deviceRevisions70025InstanceIndex_TGT_TSS1080_CTRL_SPROT_08.get(1),   ID, 119780L);
        Whitebox.setInternalState(deviceRevisions70025InstanceIndex_TGT_TSS1080_CTRL_SPROT_08.get(2),   ID, 119949L);

        nameRevisions70025InstanceIndex_TGT_TSS1080_CTRL_SPROT_08.add(UtilityBusiness.createNameRevision(deviceRevisions70025InstanceIndex_TGT_TSS1080_CTRL_SPROT_08.get(0)));
        nameRevisions70025InstanceIndex_TGT_TSS1080_CTRL_SPROT_08.add(UtilityBusiness.createNameRevision(deviceRevisions70025InstanceIndex_TGT_TSS1080_CTRL_SPROT_08.get(1)));
        nameRevisions70025InstanceIndex_TGT_TSS1080_CTRL_SPROT_08.add(UtilityBusiness.createNameRevision(deviceRevisions70025InstanceIndex_TGT_TSS1080_CTRL_SPROT_08.get(2)));
    }

    /**
     * Tear down test structure - section - name part revisions, name revisions.
     */
    private void tearDownNameRevisionSection() {
        namePartRevisionsSystemGroupAcc1.clear();
        namePartRevisionsSystemGroupAcc1 = null;
        nameRevisionsSystemGroupAcc1.clear();
        nameRevisionsSystemGroupAcc1 = null;

        namePartRevisionsSystemFEB1052.clear();
        namePartRevisionsSystemFEB1052 = null;
        nameRevisionsSystemFEB1052.clear();
        nameRevisionsSystemFEB1052 = null;

        namePartRevisionsSubsystem050ROW1189.clear();
        namePartRevisionsSubsystem050ROW1189 = null;
        nameRevisionsSubsystem050ROW1189.clear();
        nameRevisionsSubsystem050ROW1189 = null;

        // ----------

        namePartRevisionsSystemGroupCentralServices4.clear();
        namePartRevisionsSystemGroupCentralServices4 = null;
        nameRevisionsSystemGroupCentralServices4.clear();
        nameRevisionsSystemGroupCentralServices4 = null;

        namePartRevisionsSystemPBI2774.clear();
        namePartRevisionsSystemPBI2774 = null;
        nameRevisionsSystemPBI2774.clear();
        nameRevisionsSystemPBI2774 = null;

        namePartRevisionsSubsystemBCM012778.clear();
        namePartRevisionsSubsystemBCM012778 = null;
        nameRevisionsSubsystemBCM012778.clear();
        nameRevisionsSubsystemBCM012778 = null;

        // ----------

        namePartRevisionsSystemGroupTargetStation2.clear();
        namePartRevisionsSystemGroupTargetStation2 = null;
        nameRevisionsSystemGroupTargetStation2.clear();
        nameRevisionsSystemGroupTargetStation2 = null;

        namePartRevisionsSystemTGT2566.clear();
        namePartRevisionsSystemTGT2566 = null;
        nameRevisionsSystemTGT2566.clear();
        nameRevisionsSystemTGT2566 = null;

        namePartRevisionsSubsystemTSS10802607.clear();
        namePartRevisionsSubsystemTSS10802607 = null;
        nameRevisionsSubsystemTSS10802607.clear();
        nameRevisionsSubsystemTSS10802607 = null;
    }

    /**
     * Tear down test structure - device type - name part revisions, name revisions.
     */
    private void tearDownNameRevisionDeviceType() {
        namePartRevisionsDisciplinePBI237.clear();
        namePartRevisionsDisciplinePBI237 = null;
        nameRevisionsDisciplinePBI237.clear();
        nameRevisionsDisciplinePBI237 = null;

        namePartRevisionsDeviceGroupControlElectronics1435.clear();
        namePartRevisionsDeviceGroupControlElectronics1435 = null;
        nameRevisionsDeviceGroupControllerElectronics1435.clear();
        nameRevisionsDeviceGroupControllerElectronics1435 = null;

        namePartRevisionsDeviceTypeMTCA1452.clear();
        namePartRevisionsDeviceTypeMTCA1452 = null;
        nameRevisionsDeviceTypeMTCA1452.clear();
        nameRevisionsDeviceTypeMTCA1452 = null;

        // ----------

        namePartRevisionsDisciplineCTRL502.clear();
        namePartRevisionsDisciplineCTRL502 = null;
        nameRevisionsDisciplineCTRL502.clear();
        nameRevisionsDisciplineCTRL502 = null;

        namePartRevisionsDeviceGroupEnclosures2870.clear();
        namePartRevisionsDeviceGroupEnclosures2870 = null;
        nameRevisionsDeviceGroupEnclosures2870.clear();
        nameRevisionsDeviceGroupEnclosures2870 = null;

        namePartRevisionsDeviceTypeMTCA3U2875.clear();
        namePartRevisionsDeviceTypeMTCA3U2875 = null;
        nameRevisionsDeviceTypeMTCA3U2875.clear();
        nameRevisionsDeviceTypeMTCA3U2875 = null;

        // ----------

        namePartRevisionsDeviceGroupSupportingEquipment1007.clear();
        namePartRevisionsDeviceGroupSupportingEquipment1007 = null;
        nameRevisionsDeviceGroupSupportingEquipment1007.clear();
        nameRevisionsDeviceGroupSupportingEquipment1007 = null;

        namePartRevisionsDeviceTypeSPROT3025.clear();
        namePartRevisionsDeviceTypeSPROT3025 = null;
        nameRevisionsDeviceTypeSPROT3025.clear();
        nameRevisionsDeviceTypeSPROT3025 = null;
    }

    /**
     * Tear down test structure - device - device revisions, name revisions.
     */
    private void tearDownNameRevisionDevice() {
        deviceRevisions4108InstanceIndex_FEB_050ROW_PBI_MTCA_050.clear();
        deviceRevisions4108InstanceIndex_FEB_050ROW_PBI_MTCA_050 = null;
        nameRevisions4108InstanceIndex_FEB_050ROW_PBI_MTCA_050.clear();
        nameRevisions4108InstanceIndex_FEB_050ROW_PBI_MTCA_050 = null;

        // ----------

        deviceRevisions70025InstanceIndex_TGT_TSS1080_CTRL_SPROT_08.clear();
        deviceRevisions70025InstanceIndex_TGT_TSS1080_CTRL_SPROT_08 = null;
        nameRevisions70025InstanceIndex_TGT_TSS1080_CTRL_SPROT_08.clear();
        nameRevisions70025InstanceIndex_TGT_TSS1080_CTRL_SPROT_08 = null;
    }

}
