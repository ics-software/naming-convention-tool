/*
 * Copyright (C) 2020 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.business;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * Unit tests for NameRevisionPair class. Similar setup as NameRevisionTest.
 *
 * @author Lars Johansson
 *
 * @see NameRevisionPair
 * @see NameRevision
 * @see NameRevisionTest
 * @see UtilityBusinessTestFixture
 */
public class NameRevisionPairTest {

    // Note that test fixture is handled by utility class.

    private static UtilityBusinessTestFixture testFixture;

    /**
     * One-time initialization code.
     */
    @BeforeClass
    public static void oneTimeSetUp() {
        testFixture = UtilityBusinessTestFixture.getInstance();
        testFixture.setUp();
    }

    /**
     * One-time cleanup code.
     */
    @AfterClass
    public static void oneTimeTearDown() {
        testFixture.tearDown();
        testFixture = null;
    }

    // ----------------------------------------------------------------------------------------------------

    /**
     * Test constructor of NameRevisionPair.
     */
    @Test
    public void constuctorEmpty() {
        NameRevisionPair nameRevisionPair1 = new NameRevisionPair();

        assertNotNull(nameRevisionPair1);

        assertNull(nameRevisionPair1.getApprovedRevision());
        assertNull(nameRevisionPair1.getUnapprovedRevision());
        assertNull(nameRevisionPair1.getBaseRevision());
        assertNull(nameRevisionPair1.getLatestRevision());
        assertEquals(NameStage.INITIAL, nameRevisionPair1.getNameStage());
        assertFalse(nameRevisionPair1.isApproved());
        assertFalse(nameRevisionPair1.isPending());
        assertFalse(nameRevisionPair1.isCancelled());
        assertFalse(nameRevisionPair1.isValidationNeeded());
    }

    /**
     * Test constructor of NameRevisionPair.
     */
    @Test
    public void constuctorNameRevisionPair() {
        boolean updated = false;

        NameRevisionPair nameRevisionPair1 = new NameRevisionPair();
        NameRevisionPair nameRevisionPair2 = null;

        assertNotNull(nameRevisionPair1);
        assertNull   (nameRevisionPair2);

        assertNull(nameRevisionPair1.getApprovedRevision());
        assertNull(nameRevisionPair1.getUnapprovedRevision());
        assertNull(nameRevisionPair1.getBaseRevision());
        assertNull(nameRevisionPair1.getLatestRevision());
        assertEquals(NameStage.INITIAL, nameRevisionPair1.getNameStage());
        assertFalse(nameRevisionPair1.isApproved());
        assertFalse(nameRevisionPair1.isPending());
        assertFalse(nameRevisionPair1.isCancelled());
        assertFalse(nameRevisionPair1.isValidationNeeded());

        updated = nameRevisionPair1.update(testFixture.getNameRevisionSection11());

        assertTrue(updated);
        assertNull(nameRevisionPair1.getApprovedRevision());
        assertEquals(testFixture.getNameRevisionSection11(), nameRevisionPair1.getUnapprovedRevision());
        assertEquals(testFixture.getNameRevisionSection11(), nameRevisionPair1.getBaseRevision());
        assertEquals(testFixture.getNameRevisionSection11(), nameRevisionPair1.getLatestRevision());
        assertEquals(NameStage.INITIAL_PROPOSED, nameRevisionPair1.getNameStage());
        assertFalse(nameRevisionPair1.isApproved());
        assertTrue (nameRevisionPair1.isPending());
        assertFalse(nameRevisionPair1.isCancelled());
        assertTrue (nameRevisionPair1.isValidationNeeded());

        nameRevisionPair2 = new NameRevisionPair(nameRevisionPair1);

        assertTrue(updated);
        assertNull(nameRevisionPair2.getApprovedRevision());
        assertEquals(testFixture.getNameRevisionSection11(), nameRevisionPair2.getUnapprovedRevision());
        assertEquals(testFixture.getNameRevisionSection11(), nameRevisionPair2.getBaseRevision());
        assertEquals(testFixture.getNameRevisionSection11(), nameRevisionPair2.getLatestRevision());
        assertEquals(NameStage.INITIAL_PROPOSED, nameRevisionPair2.getNameStage());
        assertFalse(nameRevisionPair2.isApproved());
        assertTrue (nameRevisionPair2.isPending());
        assertFalse(nameRevisionPair2.isCancelled());
        assertTrue (nameRevisionPair2.isValidationNeeded());
    }

    /**
     * Test update method of NameRevisionPair.
     */
    @Test
    public void updateNameRevisionSectionAscending() {
        boolean updated = false;

        NameRevisionPair nameRevisionPair1 = new NameRevisionPair();
        NameRevisionPair nameRevisionPair2 = new NameRevisionPair();
        NameRevisionPair nameRevisionPair3 = new NameRevisionPair();

        assertNotNull(nameRevisionPair1);
        assertNotNull(nameRevisionPair2);
        assertNotNull(nameRevisionPair3);

        assertNull(nameRevisionPair1.getApprovedRevision());
        assertNull(nameRevisionPair1.getUnapprovedRevision());
        assertNull(nameRevisionPair1.getBaseRevision());
        assertNull(nameRevisionPair1.getLatestRevision());
        assertEquals(NameStage.INITIAL, nameRevisionPair1.getNameStage());
        assertFalse(nameRevisionPair1.isApproved());
        assertFalse(nameRevisionPair1.isPending());
        assertFalse(nameRevisionPair1.isCancelled());
        assertFalse(nameRevisionPair1.isValidationNeeded());

        updated = nameRevisionPair1.update(testFixture.getNameRevisionSection11());

        assertTrue(updated);
        assertNull(nameRevisionPair1.getApprovedRevision());
        assertEquals(testFixture.getNameRevisionSection11(), nameRevisionPair1.getUnapprovedRevision());
        assertEquals(testFixture.getNameRevisionSection11(), nameRevisionPair1.getBaseRevision());
        assertEquals(testFixture.getNameRevisionSection11(), nameRevisionPair1.getLatestRevision());
        assertEquals(NameStage.INITIAL_PROPOSED, nameRevisionPair1.getNameStage());
        assertFalse(nameRevisionPair1.isApproved());
        assertTrue (nameRevisionPair1.isPending());
        assertFalse(nameRevisionPair1.isCancelled());
        assertTrue (nameRevisionPair1.isValidationNeeded());

        updated = nameRevisionPair1.update(testFixture.getNameRevisionSection12());

        assertTrue(updated);
        assertNull(nameRevisionPair1.getApprovedRevision());
        assertEquals(testFixture.getNameRevisionSection12(), nameRevisionPair1.getUnapprovedRevision());
        assertEquals(testFixture.getNameRevisionSection12(), nameRevisionPair1.getBaseRevision());
        assertEquals(testFixture.getNameRevisionSection12(), nameRevisionPair1.getLatestRevision());
        assertEquals(NameStage.INITIAL_PROPOSED, nameRevisionPair1.getNameStage());
        assertFalse(nameRevisionPair1.isApproved());
        assertTrue (nameRevisionPair1.isPending());
        assertFalse(nameRevisionPair1.isCancelled());
        assertTrue (nameRevisionPair1.isValidationNeeded());

        updated = nameRevisionPair1.update(testFixture.getNameRevisionSection13());

        assertTrue(updated);
        assertNull(nameRevisionPair1.getApprovedRevision());
        assertEquals(testFixture.getNameRevisionSection13(), nameRevisionPair1.getUnapprovedRevision());
        assertEquals(testFixture.getNameRevisionSection13(), nameRevisionPair1.getBaseRevision());
        assertEquals(testFixture.getNameRevisionSection13(), nameRevisionPair1.getLatestRevision());
        assertEquals(NameStage.INITIAL_PROPOSED, nameRevisionPair1.getNameStage());
        assertFalse(nameRevisionPair1.isApproved());
        assertTrue (nameRevisionPair1.isPending());
        assertFalse(nameRevisionPair1.isCancelled());
        assertTrue (nameRevisionPair1.isValidationNeeded());
        // ----------------------------------------------------------------------------------------------------
        assertNull(nameRevisionPair2.getApprovedRevision());
        assertNull(nameRevisionPair2.getUnapprovedRevision());
        assertNull(nameRevisionPair2.getBaseRevision());
        assertNull(nameRevisionPair2.getLatestRevision());
        assertEquals(NameStage.INITIAL, nameRevisionPair2.getNameStage());
        assertFalse(nameRevisionPair2.isApproved());
        assertFalse(nameRevisionPair2.isPending());
        assertFalse(nameRevisionPair2.isCancelled());
        assertFalse(nameRevisionPair2.isValidationNeeded());

        updated = nameRevisionPair2.update(testFixture.getNameRevisionSection21());

        assertTrue(updated);
        assertNull(nameRevisionPair2.getApprovedRevision());
        assertEquals(testFixture.getNameRevisionSection21(), nameRevisionPair2.getUnapprovedRevision());
        assertEquals(testFixture.getNameRevisionSection21(), nameRevisionPair2.getBaseRevision());
        assertEquals(testFixture.getNameRevisionSection21(), nameRevisionPair2.getLatestRevision());
        assertEquals(NameStage.INITIAL_PROPOSED, nameRevisionPair2.getNameStage());
        assertFalse(nameRevisionPair2.isApproved());
        assertTrue (nameRevisionPair2.isPending());
        assertFalse(nameRevisionPair2.isCancelled());
        assertTrue (nameRevisionPair2.isValidationNeeded());

        updated = nameRevisionPair2.update(testFixture.getNameRevisionSection22());

        assertTrue(updated);
        assertNull(nameRevisionPair2.getApprovedRevision());
        assertEquals(testFixture.getNameRevisionSection22(), nameRevisionPair2.getUnapprovedRevision());
        assertEquals(testFixture.getNameRevisionSection22(), nameRevisionPair2.getBaseRevision());
        assertEquals(testFixture.getNameRevisionSection22(), nameRevisionPair2.getLatestRevision());
        assertEquals(NameStage.INITIAL_PROPOSED, nameRevisionPair2.getNameStage());
        assertFalse(nameRevisionPair2.isApproved());
        assertTrue (nameRevisionPair2.isPending());
        assertFalse(nameRevisionPair2.isCancelled());
        assertTrue (nameRevisionPair2.isValidationNeeded());

        updated = nameRevisionPair2.update(testFixture.getNameRevisionSection23());

        assertTrue(updated);
        assertNull(nameRevisionPair2.getApprovedRevision());
        assertEquals(testFixture.getNameRevisionSection23(), nameRevisionPair2.getUnapprovedRevision());
        assertEquals(testFixture.getNameRevisionSection23(), nameRevisionPair2.getBaseRevision());
        assertEquals(testFixture.getNameRevisionSection23(), nameRevisionPair2.getLatestRevision());
        assertEquals(NameStage.INITIAL_PROPOSED, nameRevisionPair2.getNameStage());
        assertFalse(nameRevisionPair2.isApproved());
        assertTrue (nameRevisionPair2.isPending());
        assertFalse(nameRevisionPair2.isCancelled());
        assertTrue (nameRevisionPair2.isValidationNeeded());

        updated = nameRevisionPair2.update(testFixture.getNameRevisionSection24());

        assertTrue(updated);
        assertNull(nameRevisionPair2.getApprovedRevision());
        assertEquals(testFixture.getNameRevisionSection24(), nameRevisionPair2.getUnapprovedRevision());
        assertEquals(testFixture.getNameRevisionSection24(), nameRevisionPair2.getBaseRevision());
        assertEquals(testFixture.getNameRevisionSection24(), nameRevisionPair2.getLatestRevision());
        assertEquals(NameStage.INITIAL_PROPOSED, nameRevisionPair2.getNameStage());
        assertFalse(nameRevisionPair2.isApproved());
        assertTrue (nameRevisionPair2.isPending());
        assertFalse(nameRevisionPair2.isCancelled());
        assertTrue (nameRevisionPair2.isValidationNeeded());

        assertNull(nameRevisionPair3.getApprovedRevision());
        assertNull(nameRevisionPair3.getUnapprovedRevision());
        assertNull(nameRevisionPair3.getBaseRevision());
        assertNull(nameRevisionPair3.getLatestRevision());
        assertEquals(NameStage.INITIAL, nameRevisionPair3.getNameStage());
        assertFalse(nameRevisionPair3.isApproved());
        assertFalse(nameRevisionPair3.isPending());
        assertFalse(nameRevisionPair3.isCancelled());
        assertFalse(nameRevisionPair3.isValidationNeeded());

        updated = nameRevisionPair3.update(testFixture.getNameRevisionSection31());

        assertTrue(updated);
        assertNull(nameRevisionPair3.getApprovedRevision());
        assertEquals(testFixture.getNameRevisionSection31(), nameRevisionPair3.getUnapprovedRevision());
        assertEquals(testFixture.getNameRevisionSection31(), nameRevisionPair3.getBaseRevision());
        assertEquals(testFixture.getNameRevisionSection31(), nameRevisionPair3.getLatestRevision());
        assertEquals(NameStage.INITIAL_PROPOSED, nameRevisionPair3.getNameStage());
        assertFalse(nameRevisionPair3.isApproved());
        assertTrue (nameRevisionPair3.isPending());
        assertFalse(nameRevisionPair3.isCancelled());
        assertTrue (nameRevisionPair3.isValidationNeeded());

        updated = nameRevisionPair3.update(testFixture.getNameRevisionSection32());

        assertTrue(updated);
        assertNull(nameRevisionPair3.getApprovedRevision());
        assertEquals(testFixture.getNameRevisionSection32(), nameRevisionPair3.getUnapprovedRevision());
        assertEquals(testFixture.getNameRevisionSection32(), nameRevisionPair3.getBaseRevision());
        assertEquals(testFixture.getNameRevisionSection32(), nameRevisionPair3.getLatestRevision());
        assertEquals(NameStage.INITIAL_PROPOSED, nameRevisionPair3.getNameStage());
        assertFalse(nameRevisionPair3.isApproved());
        assertTrue (nameRevisionPair3.isPending());
        assertFalse(nameRevisionPair3.isCancelled());
        assertTrue (nameRevisionPair3.isValidationNeeded());

        updated = nameRevisionPair3.update(testFixture.getNameRevisionSection33());

        assertTrue(updated);
        assertNull(nameRevisionPair3.getApprovedRevision());
        assertEquals(testFixture.getNameRevisionSection33(), nameRevisionPair3.getUnapprovedRevision());
        assertEquals(testFixture.getNameRevisionSection33(), nameRevisionPair3.getBaseRevision());
        assertEquals(testFixture.getNameRevisionSection33(), nameRevisionPair3.getLatestRevision());
        assertEquals(NameStage.INITIAL_PROPOSED, nameRevisionPair3.getNameStage());
        assertFalse(nameRevisionPair3.isApproved());
        assertTrue (nameRevisionPair3.isPending());
        assertFalse(nameRevisionPair3.isCancelled());
        assertTrue (nameRevisionPair3.isValidationNeeded());
    }

    /**
     * Test update method of NameRevisionPair.
     */
    @Test
    public void updateNameRevisionSectionDescending() {
        boolean updated = false;

        NameRevisionPair nameRevisionPair1 = new NameRevisionPair();
        NameRevisionPair nameRevisionPair2 = new NameRevisionPair();
        NameRevisionPair nameRevisionPair3 = new NameRevisionPair();

        assertNotNull(nameRevisionPair1);
        assertNotNull(nameRevisionPair2);
        assertNotNull(nameRevisionPair3);

        assertNull(nameRevisionPair1.getApprovedRevision());
        assertNull(nameRevisionPair1.getUnapprovedRevision());
        assertNull(nameRevisionPair1.getBaseRevision());
        assertNull(nameRevisionPair1.getLatestRevision());
        assertEquals(NameStage.INITIAL, nameRevisionPair1.getNameStage());
        assertFalse(nameRevisionPair1.isApproved());
        assertFalse(nameRevisionPair1.isPending());
        assertFalse(nameRevisionPair1.isCancelled());
        assertFalse(nameRevisionPair1.isValidationNeeded());

        updated = nameRevisionPair1.update(testFixture.getNameRevisionSection13());

        assertTrue(updated);
        assertNull(nameRevisionPair1.getApprovedRevision());
        assertEquals(testFixture.getNameRevisionSection13(), nameRevisionPair1.getUnapprovedRevision());
        assertEquals(testFixture.getNameRevisionSection13(), nameRevisionPair1.getBaseRevision());
        assertEquals(testFixture.getNameRevisionSection13(), nameRevisionPair1.getLatestRevision());
        assertEquals(NameStage.INITIAL_PROPOSED, nameRevisionPair1.getNameStage());
        assertFalse(nameRevisionPair1.isApproved());
        assertTrue (nameRevisionPair1.isPending());
        assertFalse(nameRevisionPair1.isCancelled());
        assertTrue (nameRevisionPair1.isValidationNeeded());

        updated = nameRevisionPair1.update(testFixture.getNameRevisionSection12());

        assertFalse(updated);
        assertNull(nameRevisionPair1.getApprovedRevision());
        assertEquals(testFixture.getNameRevisionSection13(), nameRevisionPair1.getUnapprovedRevision());
        assertEquals(testFixture.getNameRevisionSection13(), nameRevisionPair1.getBaseRevision());
        assertEquals(testFixture.getNameRevisionSection13(), nameRevisionPair1.getLatestRevision());
        assertEquals(NameStage.INITIAL_PROPOSED, nameRevisionPair1.getNameStage());
        assertFalse(nameRevisionPair1.isApproved());
        assertTrue (nameRevisionPair1.isPending());
        assertFalse(nameRevisionPair1.isCancelled());
        assertTrue (nameRevisionPair1.isValidationNeeded());

        updated = nameRevisionPair1.update(testFixture.getNameRevisionSection11());

        assertFalse(updated);
        assertNull(nameRevisionPair1.getApprovedRevision());
        assertEquals(testFixture.getNameRevisionSection13(), nameRevisionPair1.getUnapprovedRevision());
        assertEquals(testFixture.getNameRevisionSection13(), nameRevisionPair1.getBaseRevision());
        assertEquals(testFixture.getNameRevisionSection13(), nameRevisionPair1.getLatestRevision());
        assertEquals(NameStage.INITIAL_PROPOSED, nameRevisionPair1.getNameStage());
        assertFalse(nameRevisionPair1.isApproved());
        assertTrue (nameRevisionPair1.isPending());
        assertFalse(nameRevisionPair1.isCancelled());
        assertTrue (nameRevisionPair1.isValidationNeeded());

        assertNull(nameRevisionPair2.getApprovedRevision());
        assertNull(nameRevisionPair2.getUnapprovedRevision());
        assertNull(nameRevisionPair2.getBaseRevision());
        assertNull(nameRevisionPair2.getLatestRevision());
        assertEquals(NameStage.INITIAL, nameRevisionPair2.getNameStage());
        assertFalse(nameRevisionPair2.isApproved());
        assertFalse(nameRevisionPair2.isPending());
        assertFalse(nameRevisionPair2.isCancelled());
        assertFalse(nameRevisionPair2.isValidationNeeded());

        updated = nameRevisionPair2.update(testFixture.getNameRevisionSection24());

        assertTrue(updated);
        assertNull(nameRevisionPair2.getApprovedRevision());
        assertEquals(testFixture.getNameRevisionSection24(), nameRevisionPair2.getUnapprovedRevision());
        assertEquals(testFixture.getNameRevisionSection24(), nameRevisionPair2.getBaseRevision());
        assertEquals(testFixture.getNameRevisionSection24(), nameRevisionPair2.getLatestRevision());
        assertEquals(NameStage.INITIAL_PROPOSED, nameRevisionPair2.getNameStage());
        assertFalse(nameRevisionPair2.isApproved());
        assertTrue (nameRevisionPair2.isPending());
        assertFalse(nameRevisionPair2.isCancelled());
        assertTrue (nameRevisionPair2.isValidationNeeded());

        updated = nameRevisionPair2.update(testFixture.getNameRevisionSection23());

        assertFalse(updated);
        assertNull(nameRevisionPair2.getApprovedRevision());
        assertEquals(testFixture.getNameRevisionSection24(), nameRevisionPair2.getUnapprovedRevision());
        assertEquals(testFixture.getNameRevisionSection24(), nameRevisionPair2.getBaseRevision());
        assertEquals(testFixture.getNameRevisionSection24(), nameRevisionPair2.getLatestRevision());
        assertEquals(NameStage.INITIAL_PROPOSED, nameRevisionPair2.getNameStage());
        assertFalse(nameRevisionPair2.isApproved());
        assertTrue (nameRevisionPair2.isPending());
        assertFalse(nameRevisionPair2.isCancelled());
        assertTrue (nameRevisionPair2.isValidationNeeded());

        updated = nameRevisionPair2.update(testFixture.getNameRevisionSection22());

        assertFalse(updated);
        assertNull(nameRevisionPair2.getApprovedRevision());
        assertEquals(testFixture.getNameRevisionSection24(), nameRevisionPair2.getUnapprovedRevision());
        assertEquals(testFixture.getNameRevisionSection24(), nameRevisionPair2.getBaseRevision());
        assertEquals(testFixture.getNameRevisionSection24(), nameRevisionPair2.getLatestRevision());
        assertEquals(NameStage.INITIAL_PROPOSED, nameRevisionPair2.getNameStage());
        assertFalse(nameRevisionPair2.isApproved());
        assertTrue (nameRevisionPair2.isPending());
        assertFalse(nameRevisionPair2.isCancelled());
        assertTrue (nameRevisionPair2.isValidationNeeded());

        updated = nameRevisionPair2.update(testFixture.getNameRevisionSection21());

        assertFalse(updated);
        assertNull(nameRevisionPair2.getApprovedRevision());
        assertEquals(testFixture.getNameRevisionSection24(), nameRevisionPair2.getUnapprovedRevision());
        assertEquals(testFixture.getNameRevisionSection24(), nameRevisionPair2.getBaseRevision());
        assertEquals(testFixture.getNameRevisionSection24(), nameRevisionPair2.getLatestRevision());
        assertEquals(NameStage.INITIAL_PROPOSED, nameRevisionPair2.getNameStage());
        assertFalse(nameRevisionPair2.isApproved());
        assertTrue (nameRevisionPair2.isPending());
        assertFalse(nameRevisionPair2.isCancelled());
        assertTrue (nameRevisionPair2.isValidationNeeded());

        assertNull(nameRevisionPair3.getApprovedRevision());
        assertNull(nameRevisionPair3.getUnapprovedRevision());
        assertNull(nameRevisionPair3.getBaseRevision());
        assertNull(nameRevisionPair3.getLatestRevision());
        assertEquals(NameStage.INITIAL, nameRevisionPair3.getNameStage());
        assertFalse(nameRevisionPair3.isApproved());
        assertFalse(nameRevisionPair3.isPending());
        assertFalse(nameRevisionPair3.isCancelled());
        assertFalse(nameRevisionPair3.isValidationNeeded());

        updated = nameRevisionPair3.update(testFixture.getNameRevisionSection33());

        assertTrue(updated);
        assertNull(nameRevisionPair3.getApprovedRevision());
        assertEquals(testFixture.getNameRevisionSection33(), nameRevisionPair3.getUnapprovedRevision());
        assertEquals(testFixture.getNameRevisionSection33(), nameRevisionPair3.getBaseRevision());
        assertEquals(testFixture.getNameRevisionSection33(), nameRevisionPair3.getLatestRevision());
        assertEquals(NameStage.INITIAL_PROPOSED, nameRevisionPair3.getNameStage());
        assertFalse(nameRevisionPair3.isApproved());
        assertTrue (nameRevisionPair3.isPending());
        assertFalse(nameRevisionPair3.isCancelled());
        assertTrue (nameRevisionPair3.isValidationNeeded());

        updated = nameRevisionPair3.update(testFixture.getNameRevisionSection32());

        assertFalse(updated);
        assertNull(nameRevisionPair3.getApprovedRevision());
        assertEquals(testFixture.getNameRevisionSection33(), nameRevisionPair3.getUnapprovedRevision());
        assertEquals(testFixture.getNameRevisionSection33(), nameRevisionPair3.getBaseRevision());
        assertEquals(testFixture.getNameRevisionSection33(), nameRevisionPair3.getLatestRevision());
        assertEquals(NameStage.INITIAL_PROPOSED, nameRevisionPair3.getNameStage());
        assertFalse(nameRevisionPair3.isApproved());
        assertTrue (nameRevisionPair3.isPending());
        assertFalse(nameRevisionPair3.isCancelled());
        assertTrue (nameRevisionPair3.isValidationNeeded());

        updated = nameRevisionPair3.update(testFixture.getNameRevisionSection31());

        assertFalse(updated);
        assertNull(nameRevisionPair3.getApprovedRevision());
        assertEquals(testFixture.getNameRevisionSection33(), nameRevisionPair3.getUnapprovedRevision());
        assertEquals(testFixture.getNameRevisionSection33(), nameRevisionPair3.getBaseRevision());
        assertEquals(testFixture.getNameRevisionSection33(), nameRevisionPair3.getLatestRevision());
        assertEquals(NameStage.INITIAL_PROPOSED, nameRevisionPair3.getNameStage());
        assertFalse(nameRevisionPair3.isApproved());
        assertTrue (nameRevisionPair3.isPending());
        assertFalse(nameRevisionPair3.isCancelled());
        assertTrue (nameRevisionPair3.isValidationNeeded());
    }

    /**
     * Test update method of NameRevisionPair.
     */
    @Test
    public void updateNameRevisionDeviceTypeAscending() {
        boolean updated = false;

        NameRevisionPair nameRevisionPair = new NameRevisionPair();

        assertNotNull(nameRevisionPair);

        assertNull(nameRevisionPair.getApprovedRevision());
        assertNull(nameRevisionPair.getUnapprovedRevision());
        assertNull(nameRevisionPair.getBaseRevision());
        assertNull(nameRevisionPair.getLatestRevision());
        assertEquals(NameStage.INITIAL, nameRevisionPair.getNameStage());
        assertFalse(nameRevisionPair.isApproved());
        assertFalse(nameRevisionPair.isPending());
        assertFalse(nameRevisionPair.isCancelled());
        assertFalse(nameRevisionPair.isValidationNeeded());

        updated = nameRevisionPair.update(testFixture.getNameRevisionDeviceType1());

        assertTrue(updated);
        assertNull(nameRevisionPair.getApprovedRevision());
        assertEquals(testFixture.getNameRevisionDeviceType1(), nameRevisionPair.getUnapprovedRevision());
        assertEquals(testFixture.getNameRevisionDeviceType1(), nameRevisionPair.getBaseRevision());
        assertEquals(testFixture.getNameRevisionDeviceType1(), nameRevisionPair.getLatestRevision());
        assertEquals(NameStage.INITIAL_PROPOSED, nameRevisionPair.getNameStage());
        assertFalse(nameRevisionPair.isApproved());
        assertTrue (nameRevisionPair.isPending());
        assertFalse(nameRevisionPair.isCancelled());
        assertTrue (nameRevisionPair.isValidationNeeded());

        updated = nameRevisionPair.update(testFixture.getNameRevisionDeviceType2());

        assertTrue(updated);
        assertNull(nameRevisionPair.getApprovedRevision());
        assertEquals(testFixture.getNameRevisionDeviceType2(), nameRevisionPair.getUnapprovedRevision());
        assertEquals(testFixture.getNameRevisionDeviceType2(), nameRevisionPair.getBaseRevision());
        assertEquals(testFixture.getNameRevisionDeviceType2(), nameRevisionPair.getLatestRevision());
        assertEquals(NameStage.INITIAL_PROPOSED, nameRevisionPair.getNameStage());
        assertFalse(nameRevisionPair.isApproved());
        assertTrue (nameRevisionPair.isPending());
        assertFalse(nameRevisionPair.isCancelled());
        assertTrue (nameRevisionPair.isValidationNeeded());

        updated = nameRevisionPair.update(testFixture.getNameRevisionDeviceType3());

        assertTrue(updated);
        assertNull(nameRevisionPair.getApprovedRevision());
        assertEquals(testFixture.getNameRevisionDeviceType3(), nameRevisionPair.getUnapprovedRevision());
        assertEquals(testFixture.getNameRevisionDeviceType3(), nameRevisionPair.getBaseRevision());
        assertEquals(testFixture.getNameRevisionDeviceType3(), nameRevisionPair.getLatestRevision());
        assertEquals(NameStage.INITIAL_PROPOSED, nameRevisionPair.getNameStage());
        assertFalse(nameRevisionPair.isApproved());
        assertTrue (nameRevisionPair.isPending());
        assertFalse(nameRevisionPair.isCancelled());
        assertTrue (nameRevisionPair.isValidationNeeded());
    }

    /**
     * Test update method of NameRevisionPair.
     */
    @Test
    public void updateNameRevisionDeviceTypeDescending() {
        boolean updated = false;

        NameRevisionPair nameRevisionPair = new NameRevisionPair();

        assertNotNull(nameRevisionPair);

        assertNull(nameRevisionPair.getApprovedRevision());
        assertNull(nameRevisionPair.getUnapprovedRevision());
        assertNull(nameRevisionPair.getBaseRevision());
        assertNull(nameRevisionPair.getLatestRevision());
        assertEquals(NameStage.INITIAL, nameRevisionPair.getNameStage());
        assertFalse(nameRevisionPair.isApproved());
        assertFalse(nameRevisionPair.isPending());
        assertFalse(nameRevisionPair.isCancelled());
        assertFalse(nameRevisionPair.isValidationNeeded());

        updated = nameRevisionPair.update(testFixture.getNameRevisionDeviceType3());

        assertTrue(updated);
        assertNull(nameRevisionPair.getApprovedRevision());
        assertEquals(testFixture.getNameRevisionDeviceType3(), nameRevisionPair.getUnapprovedRevision());
        assertEquals(testFixture.getNameRevisionDeviceType3(), nameRevisionPair.getBaseRevision());
        assertEquals(testFixture.getNameRevisionDeviceType3(), nameRevisionPair.getLatestRevision());
        assertEquals(NameStage.INITIAL_PROPOSED, nameRevisionPair.getNameStage());
        assertFalse(nameRevisionPair.isApproved());
        assertTrue (nameRevisionPair.isPending());
        assertFalse(nameRevisionPair.isCancelled());
        assertTrue (nameRevisionPair.isValidationNeeded());

        updated = nameRevisionPair.update(testFixture.getNameRevisionDeviceType2());

        assertFalse(updated);
        assertNull(nameRevisionPair.getApprovedRevision());
        assertEquals(testFixture.getNameRevisionDeviceType3(), nameRevisionPair.getUnapprovedRevision());
        assertEquals(testFixture.getNameRevisionDeviceType3(), nameRevisionPair.getBaseRevision());
        assertEquals(testFixture.getNameRevisionDeviceType3(), nameRevisionPair.getLatestRevision());
        assertEquals(NameStage.INITIAL_PROPOSED, nameRevisionPair.getNameStage());
        assertFalse(nameRevisionPair.isApproved());
        assertTrue (nameRevisionPair.isPending());
        assertFalse(nameRevisionPair.isCancelled());
        assertTrue (nameRevisionPair.isValidationNeeded());

        updated = nameRevisionPair.update(testFixture.getNameRevisionDeviceType1());

        assertFalse(updated);
        assertNull(nameRevisionPair.getApprovedRevision());
        assertEquals(testFixture.getNameRevisionDeviceType3(), nameRevisionPair.getUnapprovedRevision());
        assertEquals(testFixture.getNameRevisionDeviceType3(), nameRevisionPair.getBaseRevision());
        assertEquals(testFixture.getNameRevisionDeviceType3(), nameRevisionPair.getLatestRevision());
        assertEquals(NameStage.INITIAL_PROPOSED, nameRevisionPair.getNameStage());
        assertFalse(nameRevisionPair.isApproved());
        assertTrue (nameRevisionPair.isPending());
        assertFalse(nameRevisionPair.isCancelled());
        assertTrue (nameRevisionPair.isValidationNeeded());
    }

    /**
     * Test update method of NameRevisionPair.
     */
    @Test
    public void updateNameRevisionDeviceAscending() {
        boolean updated = false;

        NameRevisionPair nameRevisionPair = new NameRevisionPair();

        assertNotNull(nameRevisionPair);

        assertNull(nameRevisionPair.getApprovedRevision());
        assertNull(nameRevisionPair.getUnapprovedRevision());
        assertNull(nameRevisionPair.getBaseRevision());
        assertNull(nameRevisionPair.getLatestRevision());
        assertEquals(NameStage.INITIAL, nameRevisionPair.getNameStage());
        assertFalse(nameRevisionPair.isApproved());
        assertFalse(nameRevisionPair.isPending());
        assertFalse(nameRevisionPair.isCancelled());
        assertFalse(nameRevisionPair.isValidationNeeded());

        updated = nameRevisionPair.update(testFixture.getNameRevisionDevice1());

        assertTrue(updated);
        assertEquals(testFixture.getNameRevisionDevice1(), nameRevisionPair.getApprovedRevision());
        assertNull  (nameRevisionPair.getUnapprovedRevision());
        assertEquals(testFixture.getNameRevisionDevice1(), nameRevisionPair.getBaseRevision());
        assertEquals(testFixture.getNameRevisionDevice1(), nameRevisionPair.getLatestRevision());
        assertEquals(NameStage.STABLE, nameRevisionPair.getNameStage());
        assertTrue (nameRevisionPair.isApproved());
        assertFalse(nameRevisionPair.isPending());
        assertFalse(nameRevisionPair.isCancelled());
        assertFalse(nameRevisionPair.isValidationNeeded());

        updated = nameRevisionPair.update(testFixture.getNameRevisionDevice2());

        assertTrue(updated);
        assertEquals(testFixture.getNameRevisionDevice2(), nameRevisionPair.getApprovedRevision());
        assertNull  (nameRevisionPair.getUnapprovedRevision());
        assertEquals(testFixture.getNameRevisionDevice2(), nameRevisionPair.getBaseRevision());
        assertEquals(testFixture.getNameRevisionDevice2(), nameRevisionPair.getLatestRevision());
        assertEquals(NameStage.STABLE, nameRevisionPair.getNameStage());
        assertTrue (nameRevisionPair.isApproved());
        assertFalse(nameRevisionPair.isPending());
        assertFalse(nameRevisionPair.isCancelled());
        assertFalse(nameRevisionPair.isValidationNeeded());

        updated = nameRevisionPair.update(testFixture.getNameRevisionDevice3());

        assertTrue(updated);
        assertEquals(testFixture.getNameRevisionDevice3(), nameRevisionPair.getApprovedRevision());
        assertNull  (nameRevisionPair.getUnapprovedRevision());
        assertEquals(testFixture.getNameRevisionDevice3(), nameRevisionPair.getBaseRevision());
        assertEquals(testFixture.getNameRevisionDevice3(), nameRevisionPair.getLatestRevision());
        assertEquals(NameStage.STABLE, nameRevisionPair.getNameStage());
        assertTrue (nameRevisionPair.isApproved());
        assertFalse(nameRevisionPair.isPending());
        assertFalse(nameRevisionPair.isCancelled());
        assertFalse(nameRevisionPair.isValidationNeeded());
    }

    /**
     * Test update method of NameRevisionPair.
     */
    @Test
    public void updateNameRevisionDeviceDescending() {
        boolean updated = false;

        NameRevisionPair nameRevisionPair = new NameRevisionPair();

        assertNotNull(nameRevisionPair);

        assertNull(nameRevisionPair.getApprovedRevision());
        assertNull(nameRevisionPair.getUnapprovedRevision());
        assertNull(nameRevisionPair.getBaseRevision());
        assertNull(nameRevisionPair.getLatestRevision());
        assertEquals(NameStage.INITIAL, nameRevisionPair.getNameStage());
        assertFalse(nameRevisionPair.isApproved());
        assertFalse(nameRevisionPair.isPending());
        assertFalse(nameRevisionPair.isCancelled());
        assertFalse(nameRevisionPair.isValidationNeeded());

        updated = nameRevisionPair.update(testFixture.getNameRevisionDevice3());

        assertTrue(updated);
        assertEquals(testFixture.getNameRevisionDevice3(), nameRevisionPair.getApprovedRevision());
        assertNull  (nameRevisionPair.getUnapprovedRevision());
        assertEquals(testFixture.getNameRevisionDevice3(), nameRevisionPair.getBaseRevision());
        assertEquals(testFixture.getNameRevisionDevice3(), nameRevisionPair.getLatestRevision());
        assertEquals(NameStage.STABLE, nameRevisionPair.getNameStage());
        assertTrue (nameRevisionPair.isApproved());
        assertFalse(nameRevisionPair.isPending());
        assertFalse(nameRevisionPair.isCancelled());
        assertFalse(nameRevisionPair.isValidationNeeded());

        updated = nameRevisionPair.update(testFixture.getNameRevisionDevice2());

        assertFalse(updated);
        assertEquals(testFixture.getNameRevisionDevice3(), nameRevisionPair.getApprovedRevision());
        assertNull  (nameRevisionPair.getUnapprovedRevision());
        assertEquals(testFixture.getNameRevisionDevice3(), nameRevisionPair.getBaseRevision());
        assertEquals(testFixture.getNameRevisionDevice3(), nameRevisionPair.getLatestRevision());
        assertEquals(NameStage.STABLE, nameRevisionPair.getNameStage());
        assertTrue (nameRevisionPair.isApproved());
        assertFalse(nameRevisionPair.isPending());
        assertFalse(nameRevisionPair.isCancelled());
        assertFalse(nameRevisionPair.isValidationNeeded());

        updated = nameRevisionPair.update(testFixture.getNameRevisionDevice1());

        assertFalse(updated);
        assertEquals(testFixture.getNameRevisionDevice3(), nameRevisionPair.getApprovedRevision());
        assertNull  (nameRevisionPair.getUnapprovedRevision());
        assertEquals(testFixture.getNameRevisionDevice3(), nameRevisionPair.getBaseRevision());
        assertEquals(testFixture.getNameRevisionDevice3(), nameRevisionPair.getLatestRevision());
        assertEquals(NameStage.STABLE, nameRevisionPair.getNameStage());
        assertTrue (nameRevisionPair.isApproved());
        assertFalse(nameRevisionPair.isPending());
        assertFalse(nameRevisionPair.isCancelled());
        assertFalse(nameRevisionPair.isValidationNeeded());
    }

    // ----------------------------------------------------------------------------------------------------

    /**
     * Test update method of NameRevisionPair.
     */
    @Test(expected = IllegalArgumentException.class)
    public void updateNameRevisionSectionDeviceType() {
        NameRevisionPair nameRevisionPair1 = new NameRevisionPair();

        assertNotNull(nameRevisionPair1);

        assertNull(nameRevisionPair1.getApprovedRevision());
        assertNull(nameRevisionPair1.getUnapprovedRevision());
        assertNull(nameRevisionPair1.getBaseRevision());
        assertNull(nameRevisionPair1.getLatestRevision());
        assertEquals(NameStage.INITIAL, nameRevisionPair1.getNameStage());
        assertFalse(nameRevisionPair1.isApproved());
        assertFalse(nameRevisionPair1.isPending());
        assertFalse(nameRevisionPair1.isCancelled());
        assertFalse(nameRevisionPair1.isValidationNeeded());

        nameRevisionPair1.update(testFixture.getNameRevisionSection11());

        assertNull(nameRevisionPair1.getApprovedRevision());
        assertEquals(testFixture.getNameRevisionSection11(), nameRevisionPair1.getUnapprovedRevision());
        assertEquals(testFixture.getNameRevisionSection11(), nameRevisionPair1.getBaseRevision());
        assertEquals(testFixture.getNameRevisionSection11(), nameRevisionPair1.getLatestRevision());
        assertEquals(NameStage.INITIAL_PROPOSED, nameRevisionPair1.getNameStage());
        assertFalse(nameRevisionPair1.isApproved());
        assertTrue (nameRevisionPair1.isPending());
        assertFalse(nameRevisionPair1.isCancelled());
        assertTrue (nameRevisionPair1.isValidationNeeded());

        nameRevisionPair1.update(testFixture.getNameRevisionDeviceType1());
    }

    /**
     * Test update method of NameRevisionPair.
     */
    @Test(expected = IllegalArgumentException.class)
    public void updateNameRevisionSectionDevice() {
        NameRevisionPair nameRevisionPair1 = new NameRevisionPair();

        assertNotNull(nameRevisionPair1);

        assertNull(nameRevisionPair1.getApprovedRevision());
        assertNull(nameRevisionPair1.getUnapprovedRevision());
        assertNull(nameRevisionPair1.getBaseRevision());
        assertNull(nameRevisionPair1.getLatestRevision());
        assertEquals(NameStage.INITIAL, nameRevisionPair1.getNameStage());
        assertFalse(nameRevisionPair1.isApproved());
        assertFalse(nameRevisionPair1.isPending());
        assertFalse(nameRevisionPair1.isCancelled());
        assertFalse(nameRevisionPair1.isValidationNeeded());

        nameRevisionPair1.update(testFixture.getNameRevisionSection11());

        assertNull(nameRevisionPair1.getApprovedRevision());
        assertEquals(testFixture.getNameRevisionSection11(), nameRevisionPair1.getUnapprovedRevision());
        assertEquals(testFixture.getNameRevisionSection11(), nameRevisionPair1.getBaseRevision());
        assertEquals(testFixture.getNameRevisionSection11(), nameRevisionPair1.getLatestRevision());
        assertEquals(NameStage.INITIAL_PROPOSED, nameRevisionPair1.getNameStage());
        assertFalse(nameRevisionPair1.isApproved());
        assertTrue (nameRevisionPair1.isPending());
        assertFalse(nameRevisionPair1.isCancelled());
        assertTrue (nameRevisionPair1.isValidationNeeded());

        nameRevisionPair1.update(testFixture.getNameRevisionDevice1());
    }

    /**
     * Test update method of NameRevisionPair.
     */
    @Test(expected = IllegalArgumentException.class)
    public void updateNameRevisionDeviceTypeDevice() {
        NameRevisionPair nameRevisionPair1 = new NameRevisionPair();

        assertNotNull(nameRevisionPair1);

        assertNull(nameRevisionPair1.getApprovedRevision());
        assertNull(nameRevisionPair1.getUnapprovedRevision());
        assertNull(nameRevisionPair1.getBaseRevision());
        assertNull(nameRevisionPair1.getLatestRevision());
        assertEquals(NameStage.INITIAL, nameRevisionPair1.getNameStage());
        assertFalse(nameRevisionPair1.isApproved());
        assertFalse(nameRevisionPair1.isPending());
        assertFalse(nameRevisionPair1.isCancelled());
        assertFalse(nameRevisionPair1.isValidationNeeded());

        nameRevisionPair1.update(testFixture.getNameRevisionSection11());

        assertNull(nameRevisionPair1.getApprovedRevision());
        assertEquals(testFixture.getNameRevisionSection11(), nameRevisionPair1.getUnapprovedRevision());
        assertEquals(testFixture.getNameRevisionSection11(), nameRevisionPair1.getBaseRevision());
        assertEquals(testFixture.getNameRevisionSection11(), nameRevisionPair1.getLatestRevision());
        assertEquals(NameStage.INITIAL_PROPOSED, nameRevisionPair1.getNameStage());
        assertFalse(nameRevisionPair1.isApproved());
        assertTrue (nameRevisionPair1.isPending());
        assertFalse(nameRevisionPair1.isCancelled());
        assertTrue (nameRevisionPair1.isValidationNeeded());

        nameRevisionPair1.update(testFixture.getNameRevisionDevice1());
    }

}
