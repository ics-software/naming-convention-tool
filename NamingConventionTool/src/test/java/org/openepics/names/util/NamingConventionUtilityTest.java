/*
 * Copyright (c) 2019 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.util;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Test;
import org.openepics.names.business.NameType;
import org.openepics.names.services.EssNamingConvention;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;

/**
 * Unit tests for NamingConventionUtility class.
 *
 * @author Lars Johansson
 *
 * @see EssNamingConvention
 * @see NamingConventionUtility
 */
public class NamingConventionUtilityTest {

    // conventionname_abc_def
    //     a - 1/0 - system group   or not
    //     b - 1/0 - system         or not
    //     c - 1/0 - subsystem      or not
    //     d - 1/0 - discipline     or not
    //     e - 1/0 - device type    or not
    //     f - 1/0 - instance index or not

    private static final String NULL = null;
    private static final String EMPTY = "";
    private static final String DUMMY = "DUMMY";

    private static final String ACC           = "Acc";
    private static final String LEBT          = "LEBT";
    private static final String ZERO_ONE_ZERO = "010";


    private static final String CONVENTIONNAME_010             = "LEBT";
    private static final String CONVENTIONNAME_011             = "LEBT-010";
    private static final String CONVENTIONNAME_111             = "Acc-LEBT-010";

    private static final String CONVENTIONNAME_010_111         = "CWL:WtrC-EC-003";
    private static final String CONVENTIONNAME_011_110         = "LEBT-010:PwrC-PSChop";
    private static final String CONVENTIONNAME_011_111         = "CWL-CWSE05:WtrC-EC-003";
    private static final String CONVENTIONNAME_111_110         = "Acc-LEBT-010:PwrC-PSChop";
    private static final String CONVENTIONNAME_111_111         = "DUMMY-CWL-CWSE05:WtrC-EC-003";

    private static final String CONVENTIONNAME_011_111_EQCLASS = "CW1-CWSE5:WTRC-EC-3";
    private static final String CONVENTIONNAME_111_111_EQCLASS = "DUMMY-CW1-CWSE5:WTRC-EC-3";

    /**
     * Test of extraction of system group from convention name.
     */
    @Test
    public void extractSystemGroup() {
        assertNull(NamingConventionUtility.extractSystemGroup(NULL));
        assertNull(NamingConventionUtility.extractSystemGroup(EMPTY));
        assertNull(NamingConventionUtility.extractSystemGroup(DUMMY));

        assertEquals(NULL,      NamingConventionUtility.extractSystemGroup(CONVENTIONNAME_010));
        assertEquals(NULL,      NamingConventionUtility.extractSystemGroup(CONVENTIONNAME_011));

        assertEquals(NULL,      NamingConventionUtility.extractSystemGroup(CONVENTIONNAME_010_111));
        assertEquals(NULL,      NamingConventionUtility.extractSystemGroup(CONVENTIONNAME_011_110));
        assertEquals(NULL,      NamingConventionUtility.extractSystemGroup(CONVENTIONNAME_011_111));
        assertEquals(ACC,       NamingConventionUtility.extractSystemGroup(CONVENTIONNAME_111_110));
        assertEquals(DUMMY,     NamingConventionUtility.extractSystemGroup(CONVENTIONNAME_111_111));

        assertEquals(NULL,      NamingConventionUtility.extractSystemGroup(CONVENTIONNAME_011_111_EQCLASS));
        assertEquals(DUMMY,     NamingConventionUtility.extractSystemGroup(CONVENTIONNAME_111_111_EQCLASS));
    }

    /**
     * Test of extraction of system from convention name.
     */
    @Test
    public void extractSystem() {
        assertNull(NamingConventionUtility.extractSystem(NULL));
        assertNull(NamingConventionUtility.extractSystem(EMPTY));
        assertEquals(DUMMY,     NamingConventionUtility.extractSystem(DUMMY));

        assertEquals("LEBT",    NamingConventionUtility.extractSystem(CONVENTIONNAME_010));
        assertEquals("LEBT",    NamingConventionUtility.extractSystem(CONVENTIONNAME_011));

        assertEquals("CWL",     NamingConventionUtility.extractSystem(CONVENTIONNAME_010_111));
        assertEquals("LEBT",    NamingConventionUtility.extractSystem(CONVENTIONNAME_011_110));
        assertEquals("CWL",     NamingConventionUtility.extractSystem(CONVENTIONNAME_011_111));
        assertEquals("LEBT",    NamingConventionUtility.extractSystem(CONVENTIONNAME_111_110));
        assertEquals("CWL",     NamingConventionUtility.extractSystem(CONVENTIONNAME_111_111));

        assertEquals("CW1",     NamingConventionUtility.extractSystem(CONVENTIONNAME_011_111_EQCLASS));
        assertEquals("CW1",     NamingConventionUtility.extractSystem(CONVENTIONNAME_111_111_EQCLASS));
    }

    /**
     * Test of extraction of subsystem group from convention name.
     */
    @Test
    public void extractSubsystem() {
        assertNull(NamingConventionUtility.extractSubsystem(NULL));
        assertNull(NamingConventionUtility.extractSubsystem(EMPTY));
        assertNull(NamingConventionUtility.extractSubsystem(DUMMY));

        assertEquals(NULL,      NamingConventionUtility.extractSubsystem(CONVENTIONNAME_010));
        assertEquals("010",     NamingConventionUtility.extractSubsystem(CONVENTIONNAME_011));

        assertEquals(NULL,      NamingConventionUtility.extractSubsystem(CONVENTIONNAME_010_111));
        assertEquals("010",     NamingConventionUtility.extractSubsystem(CONVENTIONNAME_011_110));
        assertEquals("CWSE05",  NamingConventionUtility.extractSubsystem(CONVENTIONNAME_011_111));
        assertEquals("010",     NamingConventionUtility.extractSubsystem(CONVENTIONNAME_111_110));
        assertEquals("CWSE05",  NamingConventionUtility.extractSubsystem(CONVENTIONNAME_111_111));

        assertEquals("CWSE5",   NamingConventionUtility.extractSubsystem(CONVENTIONNAME_011_111_EQCLASS));
        assertEquals("CWSE5",   NamingConventionUtility.extractSubsystem(CONVENTIONNAME_111_111_EQCLASS));
    }

    /**
     * Test of extraction of discipline from convention name.
     */
    @Test
    public void extractDiscipline() {
        assertNull(NamingConventionUtility.extractDiscipline(NULL));
        assertNull(NamingConventionUtility.extractDiscipline(EMPTY));
        assertNull(NamingConventionUtility.extractDiscipline(DUMMY));

        assertEquals(NULL,      NamingConventionUtility.extractDiscipline(CONVENTIONNAME_010));
        assertEquals(NULL,      NamingConventionUtility.extractDiscipline(CONVENTIONNAME_011));

        assertEquals("WtrC",    NamingConventionUtility.extractDiscipline(CONVENTIONNAME_010_111));
        assertEquals("PwrC",    NamingConventionUtility.extractDiscipline(CONVENTIONNAME_011_110));
        assertEquals("WtrC",    NamingConventionUtility.extractDiscipline(CONVENTIONNAME_011_111));
        assertEquals("PwrC",    NamingConventionUtility.extractDiscipline(CONVENTIONNAME_111_110));
        assertEquals("WtrC",    NamingConventionUtility.extractDiscipline(CONVENTIONNAME_111_111));

        assertEquals("WTRC",    NamingConventionUtility.extractDiscipline(CONVENTIONNAME_011_111_EQCLASS));
        assertEquals("WTRC",    NamingConventionUtility.extractDiscipline(CONVENTIONNAME_111_111_EQCLASS));
    }

    /**
     * Test of extraction of device type from convention name.
     */
    @Test
    public void extractDeviceType() {
        assertNull(NamingConventionUtility.extractDeviceType(NULL));
        assertNull(NamingConventionUtility.extractDeviceType(EMPTY));
        assertNull(NamingConventionUtility.extractDeviceType(DUMMY));

        assertEquals(NULL,      NamingConventionUtility.extractDeviceType(CONVENTIONNAME_010));
        assertEquals(NULL,      NamingConventionUtility.extractDeviceType(CONVENTIONNAME_011));

        assertEquals("EC",      NamingConventionUtility.extractDeviceType(CONVENTIONNAME_010_111));
        assertEquals("PSChop",  NamingConventionUtility.extractDeviceType(CONVENTIONNAME_011_110));
        assertEquals("EC",      NamingConventionUtility.extractDeviceType(CONVENTIONNAME_011_111));
        assertEquals("PSChop",  NamingConventionUtility.extractDeviceType(CONVENTIONNAME_111_110));
        assertEquals("EC",      NamingConventionUtility.extractDeviceType(CONVENTIONNAME_111_111));

        assertEquals("EC",      NamingConventionUtility.extractDeviceType(CONVENTIONNAME_011_111_EQCLASS));
        assertEquals("EC",      NamingConventionUtility.extractDeviceType(CONVENTIONNAME_111_111_EQCLASS));
    }

    /**
     * Test of extraction of instance index from convention name.
     */
    @Test
    public void extractInstanceIndex() {
        assertNull(NamingConventionUtility.extractInstanceIndex(NULL));
        assertNull(NamingConventionUtility.extractInstanceIndex(EMPTY));
        assertNull(NamingConventionUtility.extractInstanceIndex(DUMMY));

        assertEquals(NULL,      NamingConventionUtility.extractInstanceIndex(CONVENTIONNAME_010));
        assertEquals(NULL,      NamingConventionUtility.extractInstanceIndex(CONVENTIONNAME_011));

        assertEquals("003",     NamingConventionUtility.extractInstanceIndex(CONVENTIONNAME_010_111));
        assertEquals(NULL,      NamingConventionUtility.extractInstanceIndex(CONVENTIONNAME_011_110));
        assertEquals("003",     NamingConventionUtility.extractInstanceIndex(CONVENTIONNAME_011_111));
        assertEquals(NULL,      NamingConventionUtility.extractInstanceIndex(CONVENTIONNAME_111_110));
        assertEquals("003",     NamingConventionUtility.extractInstanceIndex(CONVENTIONNAME_111_111));

        assertEquals("3",       NamingConventionUtility.extractInstanceIndex(CONVENTIONNAME_011_111_EQCLASS));
        assertEquals("3",       NamingConventionUtility.extractInstanceIndex(CONVENTIONNAME_111_111_EQCLASS));
    }

    /**
     * Test to get index style from mnemonic path.
     */
    @Test
    public void getInstanceIndexStyle() {
        final String TDS = "TDS";

        final List<String> deviceTypePath2  = ImmutableList.of("Mech", EMPTY, TDS);
        final List<String> deviceTypePath21 = ImmutableList.of("Cryo", EMPTY, TDS);

        assertEquals(NULL,
                NamingConventionUtility.getInstanceIndexStyle(deviceTypePath2,  NameType.SYSTEM_STRUCTURE, false));
        assertEquals(NULL,
                NamingConventionUtility.getInstanceIndexStyle(deviceTypePath21, NameType.SYSTEM_STRUCTURE, false));
        assertEquals(NULL,
                NamingConventionUtility.getInstanceIndexStyle(deviceTypePath2,  NameType.SYSTEM_STRUCTURE, true));
        assertEquals(NULL,
                NamingConventionUtility.getInstanceIndexStyle(deviceTypePath21, NameType.SYSTEM_STRUCTURE, true));

        assertEquals(NULL,
                NamingConventionUtility.getInstanceIndexStyle(deviceTypePath2,  NameType.DEVICE_STRUCTURE, false));
        assertEquals(NULL,
                NamingConventionUtility.getInstanceIndexStyle(deviceTypePath21, NameType.DEVICE_STRUCTURE, false));
        assertEquals(NamingConventionUtility.DISCIPLINE_SCIENTIFIC,
                NamingConventionUtility.getInstanceIndexStyle(deviceTypePath2,  NameType.DEVICE_STRUCTURE, true));
        assertEquals(NamingConventionUtility.DISCIPLINE_P_ID,
                NamingConventionUtility.getInstanceIndexStyle(deviceTypePath21, NameType.DEVICE_STRUCTURE, true));

        assertEquals(NULL,
                NamingConventionUtility.getInstanceIndexStyle(deviceTypePath2,  NameType.DEVICE_REGISTRY,  false));
        assertEquals(NULL,
                NamingConventionUtility.getInstanceIndexStyle(deviceTypePath21, NameType.DEVICE_REGISTRY,  false));
        assertEquals(NULL,
                NamingConventionUtility.getInstanceIndexStyle(deviceTypePath2,  NameType.DEVICE_REGISTRY,  true));
        assertEquals(NULL,
                NamingConventionUtility.getInstanceIndexStyle(deviceTypePath21, NameType.DEVICE_REGISTRY,  true));
    }

    /**
     * Test to check if convention name is offsite.
     */
    @Test
    public void isOffsite() {
        assertFalse(NamingConventionUtility.isOffsite(CONVENTIONNAME_010));
        assertFalse(NamingConventionUtility.isOffsite(CONVENTIONNAME_011));

        assertFalse(NamingConventionUtility.isOffsite(CONVENTIONNAME_010_111));
        assertFalse(NamingConventionUtility.isOffsite(CONVENTIONNAME_011_110));
        assertFalse(NamingConventionUtility.isOffsite(CONVENTIONNAME_011_111));
        assertTrue (NamingConventionUtility.isOffsite(CONVENTIONNAME_111_110));
        assertTrue (NamingConventionUtility.isOffsite(CONVENTIONNAME_111_111));

        assertFalse(NamingConventionUtility.isOffsite(CONVENTIONNAME_011_111_EQCLASS));
        assertTrue (NamingConventionUtility.isOffsite(CONVENTIONNAME_111_111_EQCLASS));
    }

    /**
     * Test to check conversion of mnemonic path to string.
     */
    @Test
    public void mnemonicPath2String() {
        List<String> mnemonicPath = Lists.newArrayList();

        // null, empty
        assertNull(NamingConventionUtility.mnemonicPath2String(null));
        assertEquals(EMPTY, NamingConventionUtility.mnemonicPath2String(mnemonicPath));
        mnemonicPath.clear();
        mnemonicPath.add(EMPTY);
        assertEquals(EMPTY, NamingConventionUtility.mnemonicPath2String(mnemonicPath));
        mnemonicPath.clear();
        mnemonicPath.add(EMPTY);
        mnemonicPath.add(EMPTY);
        assertEquals(EMPTY, NamingConventionUtility.mnemonicPath2String(mnemonicPath));

        // mix
        mnemonicPath.clear();
        mnemonicPath.add("-");
        mnemonicPath.add(EMPTY);
        assertEquals(EMPTY, NamingConventionUtility.mnemonicPath2String(mnemonicPath));
        mnemonicPath.clear();
        mnemonicPath.add(EMPTY);
        mnemonicPath.add("-");
        assertEquals(EMPTY, NamingConventionUtility.mnemonicPath2String(mnemonicPath));
        mnemonicPath.clear();
        mnemonicPath.add("-");
        mnemonicPath.add(EMPTY);
        mnemonicPath.add("-");
        assertEquals(EMPTY, NamingConventionUtility.mnemonicPath2String(mnemonicPath));
        mnemonicPath.clear();
        mnemonicPath.add("-");
        mnemonicPath.add(LEBT);
        assertEquals(LEBT, NamingConventionUtility.mnemonicPath2String(mnemonicPath));
        mnemonicPath.clear();
        mnemonicPath.add(LEBT);
        mnemonicPath.add("-");
        assertEquals(LEBT, NamingConventionUtility.mnemonicPath2String(mnemonicPath));
        mnemonicPath.clear();
        mnemonicPath.add("-");
        mnemonicPath.add(LEBT);
        mnemonicPath.add("-");
        assertEquals(LEBT, NamingConventionUtility.mnemonicPath2String(mnemonicPath));
        mnemonicPath.clear();
        mnemonicPath.add("--");
        mnemonicPath.add(LEBT);
        mnemonicPath.add("--");
        assertEquals(LEBT, NamingConventionUtility.mnemonicPath2String(mnemonicPath));

        // elements
        mnemonicPath.clear();
        mnemonicPath.add(LEBT);
        assertEquals(CONVENTIONNAME_010, NamingConventionUtility.mnemonicPath2String(mnemonicPath));
        mnemonicPath.clear();
        mnemonicPath.add(LEBT);
        mnemonicPath.add(ZERO_ONE_ZERO);
        assertEquals(CONVENTIONNAME_011, NamingConventionUtility.mnemonicPath2String(mnemonicPath));
        mnemonicPath.clear();
        mnemonicPath.add(ACC);
        mnemonicPath.add(LEBT);
        mnemonicPath.add(ZERO_ONE_ZERO);
        assertEquals(CONVENTIONNAME_111, NamingConventionUtility.mnemonicPath2String(mnemonicPath));
    }

}
