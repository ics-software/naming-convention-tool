/*
 * Copyright (C) 2020 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;

import java.util.UUID;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * Unit tests for Device class.
 *
 * @author Lars Johansson
 *
 * @see Device
 */
public class DeviceTest {

    private static final String UUID1 = "4262e1e7-2444-412e-83d7-aeabf58262c6";
    private static final String UUID2 = "c6e84e09-29d7-49a0-8ecd-92a6eff86da4";

    private static Device device1;
    private static Device device2;

    /**
     * One-time initialization code.
     */
    @BeforeClass
    public static void oneTimeSetUp() {
        device1 = UtilityModel.createDevice(UUID1);
        device2 = UtilityModel.createDevice(UUID2);
    }

    /**
     * One-time cleanup code.
     */
    @AfterClass
    public static void oneTimeTearDown() {
        device1 = null;
        device2 = null;
    }

    // ----------------------------------------------------------------------------------------------------

    /**
     * Test constructor of Device.
     */
    @Test
    public void constructorEmpty() {
        Device device = new Device();

        assertNotNull(device);
    }

    /**
     * Test constructor of Device.
     */
    @Test(expected = NullPointerException.class)
    public void constructorUuidNull() {
        new Device(null);
    }

    /**
     * Test constructor of Device.
     */
    @Test
    public void constructor() {
        Device device = new Device(UUID.fromString(UUID1));

        assertNotNull(device);
    }

    // ----------------------------------------------------------------------------------------------------

    /**
     * Test equals method of Device.
     */
    @Test
    public void equals() {
        Device device11 = UtilityModel.createDevice(UUID1);
        Device device12 = UtilityModel.createDevice(UUID2);

        assertNotNull  (device11);
        assertEquals   (device11, device11);
        assertNotEquals(device11, device12);
        assertEquals   (device11, device1);
        assertNotEquals(device11, device2);

        assertEquals   (device12, device12);
        assertNotEquals(device12, device1);
        assertEquals   (device12, device2);

        assertEquals   (device1,  device1);
        assertNotEquals(device1,  device2);

        assertEquals   (device2,  device2);
    }

    /**
     * Test hashcode method of Device.
     */
    @Test
    public void hashCodeTest() {
        Device device11 = UtilityModel.createDevice(UUID1);
        Device device12 = UtilityModel.createDevice(UUID2);

        assertEquals   (device11.hashCode(), device11.hashCode());
        assertNotEquals(device11.hashCode(), device12.hashCode());
        assertEquals   (device11.hashCode(), device1.hashCode());
        assertNotEquals(device11.hashCode(), device2.hashCode());

        assertEquals   (device12.hashCode(), device12.hashCode());
        assertNotEquals(device12.hashCode(), device1.hashCode());
        assertEquals   (device12.hashCode(), device2.hashCode());

        assertEquals   (device1.hashCode(),  device1.hashCode());
        assertNotEquals(device1.hashCode(),  device2.hashCode());

        assertEquals   (device2.hashCode(),  device2.hashCode());
    }

}
