/*
 * Copyright (C) 2020 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.model;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.openepics.names.business.NameRevisionStatus;

/**
 * Unit tests for NamePartRevisionStatus class.
 *
 * @author Lars Johansson
 *
 * @see NamePartRevisionStatus
 */
public class NamePartRevisionStatusTest {

    /**
     * Test asNameRevisionStatus method of NamePartRevisionStatus.
     */
    @Test
    public void asNameRevisionStatus() {
        assertEquals(
                NameRevisionStatus.APPROVED,
                NamePartRevisionStatus.asNameRevisionStatus(NamePartRevisionStatus.APPROVED));
        assertEquals(
                NameRevisionStatus.CANCELLED,
                NamePartRevisionStatus.asNameRevisionStatus(NamePartRevisionStatus.CANCELLED));
        assertEquals(
                NameRevisionStatus.REJECTED,
                NamePartRevisionStatus.asNameRevisionStatus(NamePartRevisionStatus.REJECTED));
        assertEquals(
                NameRevisionStatus.PENDING,
                NamePartRevisionStatus.asNameRevisionStatus(NamePartRevisionStatus.PENDING));
    }

    /**
     * Test get method of NamePartRevisionStatus.
     */
    @Test
    public void get() {
        assertEquals(
                NamePartRevisionStatus.APPROVED,
                NamePartRevisionStatus.get(NameRevisionStatus.APPROVED));
        assertEquals(
                NamePartRevisionStatus.CANCELLED,
                NamePartRevisionStatus.get(NameRevisionStatus.CANCELLED));
        assertEquals(
                NamePartRevisionStatus.REJECTED,
                NamePartRevisionStatus.get(NameRevisionStatus.REJECTED));
        assertEquals(
                NamePartRevisionStatus.PENDING,
                NamePartRevisionStatus.get(NameRevisionStatus.PENDING));
    }

}
