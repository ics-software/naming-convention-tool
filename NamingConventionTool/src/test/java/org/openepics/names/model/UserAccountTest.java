/*
 * Copyright (C) 2020 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * Unit tests for UserAccount class.
 *
 * @author Lars Johansson
 *
 * @see UserAccount
 */
public class UserAccountTest {

    private static final String ASDF = "asdf";
    private static final String ZXCV = "zxcv";

    private static UserAccount userAccountEditor;
    private static UserAccount userAccountSuperUser;

    /**
     * One-time initialization code.
     */
    @BeforeClass
    public static void oneTimeSetUp() {
        userAccountEditor    = UtilityModel.createUserAccountEditor   (ASDF);
        userAccountSuperUser = UtilityModel.createUserAccountSuperUser(ZXCV);
    }

    /**
     * One-time cleanup code.
     */
    @AfterClass
    public static void oneTimeTearDown() {
        userAccountEditor    = null;
        userAccountSuperUser = null;
    }

    // ----------------------------------------------------------------------------------------------------

    /**
     * Test constructor of UserAccount.
     */
    @Test
    public void constructorEmpty() {
        UserAccount userAccount = new UserAccount();

        assertNotNull(userAccount);
    }

    /**
     * Test constructor of UserAccount.
     */
    @Test(expected = IllegalArgumentException.class)
    public void constructorUsernameNull() {
        new UserAccount(null, Role.EDITOR);
    }

    /**
     * Test constructor of UserAccount.
     */
    @Test(expected = IllegalArgumentException.class)
    public void constructorUsernameEmpty() {
        new UserAccount("", Role.EDITOR);
    }

    /**
     * Test constructor of UserAccount.
     */
    @Test(expected = NullPointerException.class)
    public void constructorRoleNull() {
        new UserAccount("asdf", null);
    }

    /**
     * Test constructor of UserAccount.
     */
    @Test
    public void constructor() {
        UserAccount userAccount1 = UtilityModel.createUserAccountEditor   ("a");
        UserAccount userAccount2 = UtilityModel.createUserAccountSuperUser("b");

        assertNotNull(userAccount1);
        assertNotNull(userAccount2);
    }

    // ----------------------------------------------------------------------------------------------------

    /**
     * Test hashcode method of UserAccount.
     */
    @Test
    public void hashCodeTest() {
        assertNotEquals(userAccountEditor.hashCode(), userAccountSuperUser.hashCode());
    }

    /**
     * Test equals method of UserAccount.
     */
    @Test
    public void equals() {
        UserAccount userAccountEditor1    = UtilityModel.createUserAccountEditor   ("a");
        UserAccount userAccountEditor2    = UtilityModel.createUserAccountEditor   ("b");
        UserAccount userAccountEditor3    = UtilityModel.createUserAccountEditor   (ASDF);
        UserAccount userAccountEditor4    = UtilityModel.createUserAccountEditor   (ZXCV);
        UserAccount userAccountSuperuser1 = UtilityModel.createUserAccountSuperUser("a");
        UserAccount userAccountSuperuser2 = UtilityModel.createUserAccountSuperUser("b");
        UserAccount userAccountSuperuser3 = UtilityModel.createUserAccountSuperUser(ASDF);
        UserAccount userAccountSuperuser4 = UtilityModel.createUserAccountSuperUser(ZXCV);

        assertNotNull  (userAccountEditor1);
        assertEquals   (userAccountEditor1, userAccountEditor1);
        assertNotEquals(userAccountEditor1, userAccountEditor2);
        assertNotEquals(userAccountEditor1, userAccountEditor3);
        assertNotEquals(userAccountEditor1, userAccountEditor4);
        assertEquals   (userAccountEditor1, userAccountSuperuser1);
        assertNotEquals(userAccountEditor1, userAccountSuperuser2);
        assertNotEquals(userAccountEditor1, userAccountSuperuser3);
        assertNotEquals(userAccountEditor1, userAccountSuperuser4);
        assertNotEquals(userAccountEditor1, userAccountEditor);
        assertNotEquals(userAccountEditor1, userAccountSuperUser);

        assertEquals   (userAccountEditor2, userAccountEditor2);
        assertNotEquals(userAccountEditor2, userAccountEditor3);
        assertNotEquals(userAccountEditor2, userAccountEditor4);
        assertNotEquals(userAccountEditor2, userAccountSuperuser1);
        assertEquals   (userAccountEditor2, userAccountSuperuser2);
        assertNotEquals(userAccountEditor2, userAccountSuperuser3);
        assertNotEquals(userAccountEditor2, userAccountSuperuser4);
        assertNotEquals(userAccountEditor2, userAccountEditor);
        assertNotEquals(userAccountEditor2, userAccountSuperUser);

        assertEquals   (userAccountEditor3, userAccountEditor3);
        assertNotEquals(userAccountEditor3, userAccountEditor4);
        assertNotEquals(userAccountEditor3, userAccountSuperuser1);
        assertNotEquals(userAccountEditor3, userAccountSuperuser2);
        assertEquals   (userAccountEditor3, userAccountSuperuser3);
        assertNotEquals(userAccountEditor3, userAccountSuperuser4);
        assertEquals   (userAccountEditor3, userAccountEditor);
        assertNotEquals(userAccountEditor3, userAccountSuperUser);

        assertEquals   (userAccountEditor4, userAccountEditor4);
        assertNotEquals(userAccountEditor4, userAccountSuperuser1);
        assertNotEquals(userAccountEditor4, userAccountSuperuser2);
        assertNotEquals(userAccountEditor4, userAccountSuperuser3);
        assertEquals   (userAccountEditor4, userAccountSuperuser4);
        assertNotEquals(userAccountEditor4, userAccountEditor);
        assertEquals   (userAccountEditor4, userAccountSuperUser);

        assertEquals   (userAccountSuperuser1, userAccountSuperuser1);
        assertNotEquals(userAccountSuperuser1, userAccountSuperuser2);
        assertNotEquals(userAccountSuperuser1, userAccountSuperuser3);
        assertNotEquals(userAccountSuperuser1, userAccountSuperuser4);
        assertNotEquals(userAccountSuperuser1, userAccountEditor);
        assertNotEquals(userAccountSuperuser1, userAccountSuperUser);

        assertEquals   (userAccountSuperuser2, userAccountSuperuser2);
        assertNotEquals(userAccountSuperuser2, userAccountSuperuser3);
        assertNotEquals(userAccountSuperuser2, userAccountSuperuser4);
        assertNotEquals(userAccountSuperuser2, userAccountEditor);
        assertNotEquals(userAccountSuperuser2, userAccountSuperUser);

        assertEquals   (userAccountSuperuser3, userAccountSuperuser3);
        assertNotEquals(userAccountSuperuser3, userAccountSuperuser4);
        assertEquals   (userAccountSuperuser3, userAccountEditor);
        assertNotEquals(userAccountSuperuser3, userAccountSuperUser);

        assertEquals   (userAccountSuperuser4, userAccountSuperuser4);
        assertNotEquals(userAccountSuperuser4, userAccountEditor);
        assertEquals   (userAccountSuperuser4, userAccountSuperUser);

        assertEquals   (userAccountEditor, userAccountEditor);
        assertNotEquals(userAccountEditor, userAccountSuperUser);

        assertEquals   (userAccountSuperUser, userAccountSuperUser);
    }

    /**
     * Test toString method of UserAccount.
     */
    @Test
    public void toStringTest() {
        assertEquals(ASDF, userAccountEditor.toString());
        assertEquals(ZXCV, userAccountSuperUser.toString());
    }

}
