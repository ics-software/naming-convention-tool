/*
 * Copyright (C) 2020 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.Date;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * Unit tests for NamePartRevision class.
 *
 * @author Lars Johansson
 *
 * @see NamePartRevision
 */
public class NamePartRevisionTest {

    private static final String UUID1 = "4262e1e7-2444-412e-83d7-aeabf58262c6";
    private static final String UUID2 = "c6e84e09-29d7-49a0-8ecd-92a6eff86da4";
    private static final String UUID3 = "abc14850-594e-4924-b5e8-aba7aa7865e5";
    private static final String UUID4 = "a796360b-2634-4499-944b-8bd5c46aea16";

    private static final String ASDF = "asdf";
    private static final String ZXCV = "zxcv";

    private static final String DESCRIPTION       = "description";
    private static final String MNEMONIC          = "mnemonic";
    private static final String MNEMONIC_EQ_CLASS = "mnemonicEqClass";
    private static final String NAME              = "name";
    private static final String REQUESTER_COMMENT = "requesterComment";

    private static final Date DATE1 = new Date();
    private static final Date DATE2 = new Date();

    private static UserAccount userAccountEditor;
    private static UserAccount userAccountSuperUser;

    private static NamePart namePartSection1;
    private static NamePart namePartSection2;
    private static NamePart namePartDeviceType1;
    private static NamePart namePartDeviceType2;

    private static NamePartRevision namePartRevisionSection;
    private static NamePartRevision namePartRevisionDeviceType;

    /**
     * One-time initialization code.
     */
    @BeforeClass
    public static void oneTimeSetUp() {
        userAccountEditor    = UtilityModel.createUserAccountEditor   (ASDF);
        userAccountSuperUser = UtilityModel.createUserAccountSuperUser(ZXCV);

        namePartSection1     = UtilityModel.createNamePartSection   (UUID1);
        namePartSection2     = UtilityModel.createNamePartSection   (UUID2);
        namePartDeviceType1  = UtilityModel.createNamePartDeviceType(UUID3);
        namePartDeviceType2  = UtilityModel.createNamePartDeviceType(UUID4);

        namePartRevisionSection =
                UtilityModel.createNamePartRevision(
                        namePartSection1, DATE1, userAccountSuperUser, REQUESTER_COMMENT,
                        false, namePartSection2, NAME, MNEMONIC, DESCRIPTION, MNEMONIC_EQ_CLASS);

        namePartRevisionDeviceType =
                UtilityModel.createNamePartRevision(
                        namePartDeviceType1, DATE2, userAccountEditor, REQUESTER_COMMENT,
                        false, namePartDeviceType2, NAME, MNEMONIC, DESCRIPTION, MNEMONIC_EQ_CLASS);
    }

    /**
     * One-time cleanup code.
     */
    @AfterClass
    public static void oneTimeTearDown() {
        userAccountEditor = null;
        userAccountSuperUser = null;
        namePartSection1 = null;
        namePartSection2 = null;
        namePartDeviceType1 = null;
        namePartDeviceType2 = null;
        namePartRevisionSection = null;
        namePartRevisionDeviceType = null;
    }

    // ----------------------------------------------------------------------------------------------------

    /**
     * Test constructor of NamePartRevision.
     */
    @Test
    public void constructorEmpty() {
        NamePartRevision namePartRevision = new NamePartRevision();

        assertNotNull(namePartRevision);
    }

    /**
     * Test constructor of NamePartRevision.
     */
    @Test(expected = NullPointerException.class)
    public void constructorNamePartNull() {
        new NamePartRevision(
                null, DATE1, userAccountSuperUser, REQUESTER_COMMENT,
                false, namePartSection2, NAME, MNEMONIC, DESCRIPTION, MNEMONIC_EQ_CLASS);
    }

    /**
     * Test constructor of NamePartRevision.
     */
    @Test(expected = NullPointerException.class)
    public void constructorRequestDateNull() {
        new NamePartRevision(
                namePartSection1, null, userAccountSuperUser, REQUESTER_COMMENT,
                false, namePartSection2, NAME, MNEMONIC, DESCRIPTION, MNEMONIC_EQ_CLASS);
    }

    /**
     * Test constructor of NamePartRevision.
     */
    @Test
    public void constructorRequesterCommentNull() {
        NamePartRevision namePartRevision =
                UtilityModel.createNamePartRevision(
                        namePartSection1, DATE1, userAccountSuperUser, null,
                        false, namePartSection2, NAME, MNEMONIC, DESCRIPTION, MNEMONIC_EQ_CLASS);

        assertNotNull(namePartRevision);
    }

    /**
     * Test constructor of NamePartRevision.
     */
    @Test(expected = IllegalArgumentException.class)
    public void constructorRequesterCommentEmpty() {
        new NamePartRevision(
                namePartSection1, DATE1, userAccountSuperUser, "",
                false, namePartSection2, NAME, MNEMONIC, DESCRIPTION, MNEMONIC_EQ_CLASS);
    }

    /**
     * Test constructor of NamePartRevision.
     */
    @Test(expected = IllegalArgumentException.class)
    public void constructorNameNull() {
        new NamePartRevision(
                namePartSection1, DATE1, userAccountSuperUser, REQUESTER_COMMENT,
                false, namePartSection2, null, MNEMONIC, DESCRIPTION, MNEMONIC_EQ_CLASS);
    }

    /**
     * Test constructor of NamePartRevision.
     */
    @Test(expected = IllegalArgumentException.class)
    public void constructorNameEmpty() {
        new NamePartRevision(
                namePartSection1, DATE1, userAccountSuperUser, REQUESTER_COMMENT,
                false, namePartSection2, "", MNEMONIC, DESCRIPTION, MNEMONIC_EQ_CLASS);
    }

    // ----------------------------------------------------------------------------------------------------

    /**
     * Test getNamePart method of NamePartRevision.
     */
    @Test
    public void getNamePart() {
        assertEquals(namePartSection1,    namePartRevisionSection.getNamePart());
        assertEquals(namePartDeviceType1, namePartRevisionDeviceType.getNamePart());
    }

    /**
     * Test getParent method of NamePartRevision.
     */
    @Test
    public void getParent() {
        assertEquals(namePartSection2,    namePartRevisionSection.getParent());
        assertEquals(namePartDeviceType2, namePartRevisionDeviceType.getParent());
    }

}
