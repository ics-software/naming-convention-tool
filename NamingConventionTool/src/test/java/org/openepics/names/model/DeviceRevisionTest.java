/*
 * Copyright (C) 2020 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;

import java.util.Date;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * Unit tests for DeviceRevision class.
 *
 * @author Lars Johansson
 *
 * @see DeviceRevision
 */
public class DeviceRevisionTest {

    private static final String UUID1 = "4262e1e7-2444-412e-83d7-aeabf58262c6";
    private static final String UUID2 = "c6e84e09-29d7-49a0-8ecd-92a6eff86da4";
    private static final String UUID3 = "abc14850-594e-4924-b5e8-aba7aa7865e5";
    private static final String UUID4 = "7223e5b9-e683-46bc-bc7c-e23cb01b18f5";

    private static final String ASDF = "asdf";

    private static final String ADDITIONAL_INFO          = "additionalInfo";
    private static final String CONVENTION_NAME          = "conventionName";
    private static final String CONVENTION_NAME_EQ_CLASS = "conventionNameEqClass";
    private static final String INSTANCE_INDEX           = "instanceIndex";

    private static final Date DATE1 = new Date();

    private static UserAccount userAccountEditor;

    private static NamePart namePartSection;
    private static NamePart namePartDeviceType;

    private static Device device;

    private static DeviceRevision deviceRevision;

    /**
     * One-time initialization code.
     */
    @BeforeClass
    public static void oneTimeSetUp() {
        userAccountEditor  = UtilityModel.createUserAccountEditor(ASDF);

        namePartSection    = UtilityModel.createNamePartSection   (UUID1);
        namePartDeviceType = UtilityModel.createNamePartDeviceType(UUID2);

        device = UtilityModel.createDevice(UUID3);

        deviceRevision =
                UtilityModel.createDeviceRevision(
                        device, DATE1, userAccountEditor, false,
                        namePartSection, namePartDeviceType, INSTANCE_INDEX, CONVENTION_NAME,
                        CONVENTION_NAME_EQ_CLASS, ADDITIONAL_INFO);
    }

    /**
     * One-time cleanup code.
     */
    @AfterClass
    public static void oneTimeTearDown() {
        userAccountEditor = null;
        namePartSection = null;
        namePartDeviceType = null;
        device = null;
        deviceRevision = null;
    }

    // ----------------------------------------------------------------------------------------------------

    /**
     * Test constructor of DeviceRevision.
     */
    @Test
    public void constructorEmpty() {
        DeviceRevision deviceRevisionTest = new DeviceRevision();

        assertNotNull(deviceRevisionTest);
    }

    /**
     * Test constructor of DeviceRevision.
     */
    @Test(expected = NullPointerException.class)
    public void constructorDeviceNull() {
        new DeviceRevision(
                null, DATE1, userAccountEditor, false,
                namePartSection, namePartDeviceType, INSTANCE_INDEX, CONVENTION_NAME,
                CONVENTION_NAME_EQ_CLASS, ADDITIONAL_INFO);
    }

    /**
     * Test constructor of DeviceRevision.
     */
    @Test(expected = NullPointerException.class)
    public void constructorRequestDateNull() {
        new DeviceRevision(
                device, null, userAccountEditor, false,
                namePartSection, namePartDeviceType, INSTANCE_INDEX, CONVENTION_NAME,
                CONVENTION_NAME_EQ_CLASS, ADDITIONAL_INFO);
    }

    /**
     * Test constructor of DeviceRevision.
     */
    @Test(expected = NullPointerException.class)
    public void constructorSectionNull() {
        new DeviceRevision(
                device, DATE1, userAccountEditor, false,
                null, namePartDeviceType, INSTANCE_INDEX, CONVENTION_NAME,
                CONVENTION_NAME_EQ_CLASS, ADDITIONAL_INFO);
    }

    /**
     * Test constructor of DeviceRevision.
     */
    @Test(expected = IllegalArgumentException.class)
    public void constructorInstanceIndexEmpty() {
        new DeviceRevision(
                device, DATE1, userAccountEditor, false,
                namePartSection, namePartDeviceType, "", CONVENTION_NAME,
                CONVENTION_NAME_EQ_CLASS, ADDITIONAL_INFO);
    }

    /**
     * Test constructor of DeviceRevision.
     */
    @Test(expected = IllegalArgumentException.class)
    public void constructorConventionNameNull() {
        new DeviceRevision(
                device, DATE1, userAccountEditor, false,
                namePartSection, namePartDeviceType, INSTANCE_INDEX, null,
                CONVENTION_NAME_EQ_CLASS, ADDITIONAL_INFO);
    }

    /**
     * Test constructor of DeviceRevision.
     */
    @Test(expected = IllegalArgumentException.class)
    public void constructorConventionNameEmpty() {
        new DeviceRevision(
                device, DATE1, userAccountEditor, false,
                namePartSection, namePartDeviceType, INSTANCE_INDEX, "",
                CONVENTION_NAME_EQ_CLASS, ADDITIONAL_INFO);
    }

    /**
     * Test constructor of DeviceRevision.
     */
    @Test(expected = IllegalArgumentException.class)
    public void constructorConventionNameEqClassNull() {
        new DeviceRevision(
                device, DATE1, userAccountEditor, false,
                namePartSection, namePartDeviceType, INSTANCE_INDEX, CONVENTION_NAME,
                null, ADDITIONAL_INFO);
    }

    /**
     * Test constructor of DeviceRevision.
     */
    @Test(expected = IllegalArgumentException.class)
    public void constructorAdditionalInfoEmpty() {
        new DeviceRevision(
                device, DATE1, userAccountEditor, false,
                namePartSection, namePartDeviceType, INSTANCE_INDEX, CONVENTION_NAME,
                CONVENTION_NAME_EQ_CLASS, "");
    }

    /**
     * Test constructor of DeviceRevision.
     */
    @Test
    public void constructor() {
        DeviceRevision deviceRevisionTest =
                UtilityModel.createDeviceRevision(
                        device, DATE1, userAccountEditor, false,
                        namePartSection, namePartDeviceType, null, CONVENTION_NAME,
                        CONVENTION_NAME_EQ_CLASS, ADDITIONAL_INFO);

        assertNotNull(deviceRevisionTest);

        deviceRevisionTest =
                UtilityModel.createDeviceRevision(
                        device, DATE1, userAccountEditor, false,
                        namePartSection, namePartDeviceType, INSTANCE_INDEX, CONVENTION_NAME,
                        CONVENTION_NAME_EQ_CLASS, null);

        assertNotNull(deviceRevisionTest);

        deviceRevisionTest =
                UtilityModel.createDeviceRevision(
                        device, DATE1, userAccountEditor, false,
                        namePartSection, namePartDeviceType, INSTANCE_INDEX, CONVENTION_NAME,
                        CONVENTION_NAME_EQ_CLASS, ADDITIONAL_INFO);

        assertNotNull(deviceRevisionTest);

        deviceRevisionTest =
                UtilityModel.createDeviceRevision(
                        device, DATE1, userAccountEditor, false,
                        namePartSection, namePartDeviceType, INSTANCE_INDEX, CONVENTION_NAME,
                        CONVENTION_NAME_EQ_CLASS, ADDITIONAL_INFO, null);

        assertNotNull(deviceRevisionTest);
    }

    // ----------------------------------------------------------------------------------------------------

    /**
     * Test getDevice method of DeviceRevision.
     */
    @Test
    public void getDevice() {
        Device device1 = UtilityModel.createDevice(UUID4);

        assertNotNull(device1);
        assertNotEquals(device1, deviceRevision.getDevice());

        assertEquals(device, deviceRevision.getDevice());
    }

    /**
     * Test getSection method of DeviceRevision.
     */
    @Test
    public void getSection() {
        NamePart namePartSection1    = UtilityModel.createNamePartSection   (UUID4);

        assertNotNull(namePartSection1);
        assertNotEquals(namePartSection1, deviceRevision.getSection());

        assertEquals(namePartSection, deviceRevision.getSection());
    }

    /**
     * Test getDeviceType method of DeviceRevision.
     */
    @Test
    public void getDeviceType() {
        NamePart namePartDeviceType1 = UtilityModel.createNamePartDeviceType(UUID4);

        assertNotNull(namePartDeviceType1);
        assertNotEquals(namePartDeviceType1, deviceRevision.getDeviceType());

        assertEquals(namePartDeviceType, deviceRevision.getDeviceType());
    }

}
