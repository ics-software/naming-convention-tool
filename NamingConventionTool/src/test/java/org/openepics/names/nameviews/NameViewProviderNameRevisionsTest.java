/*
 * Copyright (C) 2020 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.nameviews;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.Set;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openepics.names.business.UtilityBusinessTestFixture;
import org.openepics.names.nameviews.NameViewProvider.NameRevisions;
import org.openepics.names.services.EssNamingConvention;
import org.powermock.reflect.Whitebox;

/**
 * Unit tests for {@link NameViewProvider}.
 * Concerns inner class {@link NameRevisions} but not {@link NameViews}.
 *
 * <br><br>
 * Note
 * <ul>
 * <li> JUnit integration test
 * </ul>
 *
 * @author Lars Johansson
 *
 * @see NameViewProvider
 * @see NameRevisions
 * @see NameRevisionTest
 * @see UtilityBusinessTestFixture
 */
public class NameViewProviderNameRevisionsTest {

    /*
       Purpose to test update method in NameViewProvider and associated parts of NameRevisions
       class, methods and variables.

       Details, test the inner workings of
           NameRevisions
               Map<String, NameRevision> nameRevisionMap

       Note
           test fixture is handled by utility class
    */

    private static UtilityBusinessTestFixture testFixture;

    private NameViewProvider nameViewProvider;

    /**
     * One-time initialization code.
     */
    @BeforeClass
    public static void oneTimeSetUp() {
        testFixture = UtilityBusinessTestFixture.getInstance();
        testFixture.setUp();
    }

    /**
     * One-time cleanup code.
     */
    @AfterClass
    public static void oneTimeTearDown() {
        testFixture.tearDown();
        testFixture = null;
    }

    /**
     * Initialization code before each test.
     */
    @Before
    public void setUp() {
        nameViewProvider = new NameViewProvider();

        Whitebox.setInternalState(nameViewProvider, "namingConvention", new EssNamingConvention());
        Whitebox.setInternalState(nameViewProvider, "nameRevisions", nameViewProvider.new NameRevisions());
    }

    /**
     * Cleanup code after each test.
     */
    @After
    public void tearDown() {
        nameViewProvider = null;
    }

    // ----------------------------------------------------------------------------------------------------

    /**
     * Test {@link NameRevisions#get(String)} method.
     *
     * @see NameViewProviderNameRevisionsTest#update()
     */
    @Test
    public void get() {
        // check content before update

        assertTrue(nameViewProvider.getNameRevisions().keySet().isEmpty());

        // update name view provider with test fixture content
        //     NameRevisions, not NameViews

        nameViewProvider.getNameRevisions().update(testFixture.getNameRevisionSection11());
        nameViewProvider.getNameRevisions().update(testFixture.getNameRevisionSection12());
        nameViewProvider.getNameRevisions().update(testFixture.getNameRevisionSection13());
        nameViewProvider.getNameRevisions().update(testFixture.getNameRevisionSection21());
        nameViewProvider.getNameRevisions().update(testFixture.getNameRevisionSection22());
        nameViewProvider.getNameRevisions().update(testFixture.getNameRevisionSection23());
        nameViewProvider.getNameRevisions().update(testFixture.getNameRevisionSection24());
        nameViewProvider.getNameRevisions().update(testFixture.getNameRevisionSection31());
        nameViewProvider.getNameRevisions().update(testFixture.getNameRevisionSection32());
        nameViewProvider.getNameRevisions().update(testFixture.getNameRevisionSection33());
        nameViewProvider.getNameRevisions().update(testFixture.getNameRevisionDeviceType1());
        nameViewProvider.getNameRevisions().update(testFixture.getNameRevisionDeviceType2());
        nameViewProvider.getNameRevisions().update(testFixture.getNameRevisionDeviceType3());

        // check content after update

        assertEquals(0, nameViewProvider.getNameRevisions().keySet().size());

        // update name view provider with test fixture content
        //     NameRevisions, not NameViews

        nameViewProvider.getNameRevisions().update(testFixture.getNameRevisionDevice1());
        nameViewProvider.getNameRevisions().update(testFixture.getNameRevisionDevice2());
        nameViewProvider.getNameRevisions().update(testFixture.getNameRevisionDevice3());

        // check content after update

        assertEquals(2, nameViewProvider.getNameRevisions().keySet().size());

        // check content of NameRevisions - nameRevisionMap
        //     conventionName
        //     conventionNameEqClass
        //     (uuid)

        Set<String> keys = nameViewProvider.getNameRevisions().keySet();
        for (String s : keys) {
            switch (s)  {
                case UtilityBusinessTestFixture.TD_D22_TS_MCH_1:
                    // deviceRevision1
                    // deviceRevision2
                    break;
                case UtilityBusinessTestFixture.TD_D22_CTR1_MCH_1:
                    // deviceRevision3
                    break;
                default:
                    fail();
            }
        }

        // check content of NameRevisions - nameRevisionMap
        //     non-existing key

        assertNull(nameViewProvider.getNameRevisions().get(null));
        assertNull(nameViewProvider.getNameRevisions().get(""));
        assertNull(nameViewProvider.getNameRevisions().get("asdf"));

        // check content of NameRevisions - nameRevisionMap
        //     id   104755
        //     id   110818
        //     id   113713

        assertEquals(
                110818, nameViewProvider.getNameRevisions().get(UtilityBusinessTestFixture.TD_D22_TS_MCH_01).getId());
        assertEquals(
                110818, nameViewProvider.getNameRevisions().get(UtilityBusinessTestFixture.TD_D22_TS_MCH_1).getId());
        assertEquals(
                113713, nameViewProvider.getNameRevisions().get(UtilityBusinessTestFixture.TD_D22_CTRL_MCH_1).getId());
        assertEquals(
                113713, nameViewProvider.getNameRevisions().get(UtilityBusinessTestFixture.TD_D22_CTR1_MCH_1).getId());
    }

    /**
     * Test {@link NameRevisions#update(org.openepics.names.business.NameRevision)} method.
     */
    @Test
    public void update() {
        // check content before update

        assertTrue(nameViewProvider.getNameRevisions().keySet().isEmpty());

        // update name view provider with test fixture content
        //     NameRevisions, not NameViews

        nameViewProvider.getNameRevisions().update(testFixture.getNameRevisionSection11());
        nameViewProvider.getNameRevisions().update(testFixture.getNameRevisionSection12());
        nameViewProvider.getNameRevisions().update(testFixture.getNameRevisionSection13());
        nameViewProvider.getNameRevisions().update(testFixture.getNameRevisionSection21());
        nameViewProvider.getNameRevisions().update(testFixture.getNameRevisionSection22());
        nameViewProvider.getNameRevisions().update(testFixture.getNameRevisionSection23());
        nameViewProvider.getNameRevisions().update(testFixture.getNameRevisionSection24());
        nameViewProvider.getNameRevisions().update(testFixture.getNameRevisionSection31());
        nameViewProvider.getNameRevisions().update(testFixture.getNameRevisionSection32());
        nameViewProvider.getNameRevisions().update(testFixture.getNameRevisionSection33());
        nameViewProvider.getNameRevisions().update(testFixture.getNameRevisionDeviceType1());
        nameViewProvider.getNameRevisions().update(testFixture.getNameRevisionDeviceType2());
        nameViewProvider.getNameRevisions().update(testFixture.getNameRevisionDeviceType3());

        // check content after update

        assertEquals(0, nameViewProvider.getNameRevisions().keySet().size());

        // update name view provider with test fixture content
        //     NameRevisions, not NameViews

        nameViewProvider.getNameRevisions().update(testFixture.getNameRevisionDevice1());
        nameViewProvider.getNameRevisions().update(testFixture.getNameRevisionDevice2());
        nameViewProvider.getNameRevisions().update(testFixture.getNameRevisionDevice3());

        // check content after update

        assertEquals(2, nameViewProvider.getNameRevisions().keySet().size());

        // check content of NameRevisions - nameRevisionMap
        //     conventionName
        //     conventionNameEqClass
        //     (uuid)

        Set<String> keys = nameViewProvider.getNameRevisions().keySet();
        for (String s : keys) {
            switch (s)  {
                case UtilityBusinessTestFixture.TD_D22_TS_MCH_1:
                    // deviceRevision1
                    // deviceRevision2
                    break;
                case UtilityBusinessTestFixture.TD_D22_CTR1_MCH_1:
                    // deviceRevision3
                    break;
                default:
                    fail();
            }
        }
    }

}
