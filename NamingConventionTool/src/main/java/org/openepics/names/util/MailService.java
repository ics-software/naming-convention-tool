/*
 * Copyright (c) 2019 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.util;

import com.google.common.base.Joiner;
import com.google.common.collect.Iterables;
import org.apache.commons.lang3.BooleanUtils;
import org.openepics.names.rbac.UserDirectoryService;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.annotation.Resource;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.inject.Inject;
import javax.mail.Address;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.util.ByteArrayDataSource;

/**
 * This is a service to send mails through configuration specified in JBOSS.
 *
 * @author Imre Toth <imre.toth@esss.se>
 * @author <a href="mailto:zoltan.runyo@ess.eu">Zoltan Runyo</a>
 **/

@Singleton
@Startup
public class MailService {

    private static final Logger LOGGER = Logger.getLogger(MailService.class.getName());

    @Inject
    private UserDirectoryService userDirectoryService;

    @Resource(name = "java:jboss/mail/Default")
    private Session mailSession;

    public static final boolean IS_MAIL_NOTIFICATION_ENABLED =
            BooleanUtils.toBoolean(System.getProperty("names.mailNotificationsEnabled"));

    /**
     * Sends plain text email to users with given parameters.
     * The mail properties are stored in standalone.xml, and the proper JNDI name has to be set for mailSession
     * variable.
     *
     * @param emailAddresses list/set of recipient mail addresses
     * @param loggedInName the name of the user invoking the notification
     * @param subject the subject of the mail address
     * @param content the email content (text)
     * @param attachments list of email-attachments (streams)
     * @param filenames attachment file-names
     * @param withAttachment should the email contain the attached files, or skip them?
     */
    public void sendMail(Iterable<String> emailAddresses, String loggedInName,
                         String subject, String content,
                         List<InputStream> attachments, List<String> filenames, boolean withAttachment) {
        if (loggedInName != null) {
            String replyToEmail = userDirectoryService.getEmail(loggedInName);
            if (replyToEmail != null && replyToEmail.trim().length() > 0) {
                sendMail(emailAddresses, Collections.singletonList(replyToEmail), subject, content, attachments,
                        filenames, withAttachment);
            }
        }
    }

    /***
     * Sends plain text email to users with given parameters.
     * The mail properties are stored in standalone.xml, and the proper JNDI name has to be set for mailSession
     * variable.
     * If TO address is empty, method will throw RuntimeException!
     *
     * @param emailAddresses list/set of recipient mail addresses
     * @param replyToEmail email addresses where others can reply to (contains operator and the requester)
     * @param subject the subject of the mail address
     * @param content the email content (text)
     * @param attachments list of email-attachments (streams)
     * @param filenames attachment file-names
     * @param withAttachment should the email contain the attached files, or skip them?
     * @throws RuntimeException if email TO list is empty
     */
    public void sendMail(Iterable<String> emailAddresses, List<String> replyToEmail,
            String subject, String content,
            List<InputStream> attachments, List<String> filenames, boolean withAttachment) {

        if (!IS_MAIL_NOTIFICATION_ENABLED) {
            LOGGER.log(Level.FINE, "Email sending is disabled, skipping sending mail");
            return;
        }

        try {

            if((emailAddresses == null) || (Iterables.isEmpty(emailAddresses))) {
                throw new RuntimeException("Email TO address is empty!");
            }

            final Message message = new MimeMessage(mailSession);

            final List<Address> toAddresses = new ArrayList<>();
            final List<Address> ccAddresses = new ArrayList<>();
            for (final String recipientAddress : emailAddresses) {

                if (recipientAddress == null || recipientAddress.trim().length() == 0) {
                    LOGGER.warning(
                            "Admin email address is empty, skipping adding it to recipient list: " + recipientAddress);
                    continue;
                }

                toAddresses.add(new InternetAddress(recipientAddress));
            }
            message.setRecipients(Message.RecipientType.TO, toAddresses.toArray(new Address[0]));

            if (toAddresses.isEmpty()) {
                LOGGER.log(Level.FINE, "Naming email TO list is empty, can not send email!");
                return;
            }

            message.setSubject("[NT] " + subject);
            message.setFrom(new InternetAddress(mailSession.getProperty("mail.from"), "Naming Database"));

            if ((replyToEmail != null) && !(replyToEmail.isEmpty())) {
                for(String addr : replyToEmail) {
                    InternetAddress userAddress = new InternetAddress(addr);
                    ccAddresses.add(userAddress);
                    message.setReplyTo(new InternetAddress[]{userAddress});
                    message.setRecipients(Message.RecipientType.CC, ccAddresses.toArray(new Address[0]));
                }
            }

            message.setSentDate(new Date());

            if (withAttachment && attachments != null && !attachments.isEmpty() && filenames != null
                    && !filenames.isEmpty() && attachments.size() == filenames.size()) {
                MimeMultipart multipart = new MimeMultipart();
                MimeBodyPart messagePart = new MimeBodyPart();
                messagePart.setContent(content, "text/html; charset=utf-8");
                multipart.addBodyPart(messagePart);
                for (int i = 0; i < attachments.size(); i++) {
                    MimeBodyPart attachmentPart = new MimeBodyPart();
                    DataSource dataSource = new ByteArrayDataSource(attachments.get(i), "application/octet-stream");
                    attachmentPart.setDataHandler(new DataHandler(dataSource));
                    attachmentPart.setFileName(filenames.get(i));
                    multipart.addBodyPart(attachmentPart);
                }
                message.setContent(multipart);
            } else {
                message.setContent(content, "text/html; charset=utf-8");
            }

            LOGGER.fine("Assembled a message with subject '" + subject + "' to:" + Joiner.on(", ").join(toAddresses)
                    + " cc:" + Joiner.on(", ").join(ccAddresses));

            LOGGER.log(Level.FINE, "Trying to send email to recipients");
            Transport.send(message);

        } catch (MessagingException | IOException e) {
            LOGGER.log(Level.WARNING, "Error while trying to sending email: " + e);
            throw new RuntimeException(e);
        }
    }
}
