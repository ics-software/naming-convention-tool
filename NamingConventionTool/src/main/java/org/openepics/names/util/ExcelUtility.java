/*-
 * Copyright (c) 2014 European Spallation Source ERIC.
 * Copyright (c) 2014 Cosylab d.d.
 *
 * This file is part of Naming Service.
 * Naming Service is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */

package org.openepics.names.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.annotation.Nullable;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

/**
 * A static utility class for handling Excel operations, import and export.
 *
 * @author Andraz Pozar
 * @author Lars Johansson
 */
public class ExcelUtility {

    /**
     * DataFormatter to help read and format Excel cell values.
     */
    private static final DataFormatter DATAFORMATTER = new DataFormatter();

    /**
     * This class is not to be instantiated.
     */
    private ExcelUtility() {
        throw new IllegalStateException("Utility class");
    }

    /**
     * Provide input stream for Excel workbook.
     *
     * @param workbook Excel workbook
     * @return input stream for Excel workbook
     */
    public static InputStream getInputStreamForWorkbook(XSSFWorkbook workbook) {
        final InputStream inputStream;
        try {
            final File temporaryFile = File.createTempFile("temp", "xlsx");
            FileOutputStream outputStream = new FileOutputStream(temporaryFile);
            workbook.write(outputStream);
            outputStream.close();
            inputStream = new FileInputStream(temporaryFile);
            temporaryFile.delete();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return inputStream;
    }

    /**
     * Read Excel file cell and return content as <tt>String</tt>.
     *
     * @param cell  Excel file cell
     * @return      Cell value as string.
     *              If cell contains numeric value, this value is cast to String.
     *              If there is no value for this cell, null is returned.
     */
    public static @Nullable String asString(@Nullable Cell cell) {
        if (cell != null) {
            if (null == cell.getCellTypeEnum()) {
                throw new UnsupportedOperationException();
            } else switch (cell.getCellTypeEnum()) {
                case NUMERIC:
                    return String.valueOf(cell.getNumericCellValue());
                case STRING:
                    return cell.getStringCellValue() != null ? cell.getStringCellValue() : null;
                case BLANK:
                    return null;
                default:
                    throw new UnsupportedOperationException();
            }
        } else {
            return null;
        }
    }

    /**
     * Read Excel file cell as string regardless of cell type and return content as <tt>String</tt>.
     *
     * @param cell Excel file cell
     * @return cell value as string regardless of cell type
     */
    public static @Nullable String asStringCell(@Nullable Cell cell) {
        if (cell != null) {
            return DATAFORMATTER.formatCellValue(cell);
        } else {
            return null;
        }
    }

    /**
     * Read Excel file cell with numeric value and return content as <tt>double</tt>.
     *
     * @param cell Excel file cell
     * @return cell value as double
     */
    public static double asNumber(Cell cell) {
        return cell.getNumericCellValue();
    }

}
