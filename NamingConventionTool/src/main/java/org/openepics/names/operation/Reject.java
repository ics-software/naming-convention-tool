/*
 * Copyright (c) 2016 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.operation;

import java.util.Set;
import org.openepics.names.business.NameElement;
import org.openepics.names.business.NameRevision;
import org.openepics.names.business.NameStage;
import org.openepics.names.business.RowData;
import org.openepics.names.nameviews.NameView;
import org.openepics.names.util.As;
import org.openepics.names.util.ValidationException;

/**
 * Class handling all data needed to reject proposals to add, delete or modify names.
 *
 * @author karinrathsman
 */
public class Reject extends MultipleNameOperation {

    /**
     * Constructs a new operation.
     *
     * @param selectedNameViews the nameViews to reject
     * @param root the root of the name tree.
     */
    public Reject(Set<NameView> selectedNameViews, NameView root) {
        super(selectedNameViews, root);
    }

    @Override
    public NameStage operatedStage(NameStage stage) {
        return stage.nextProcessedStage(false);
    }

    @Override
    public boolean isMessageRequired() {
        return true;
    }

    @Override
    public String getResult() {
        return "rejected";
    }

    @Override
    public String getTitle() {
        return "Reject proposals, modifications and/or deletions";
    }

    @Override
    protected boolean affectsDevices(NameView nameView) {
        return false;
    }

    @Override
    protected NameRevision nextApprovedRevision(NameView nameView) {
        return nameView.getRevisionPair().getApprovedRevision();
    }

    @Override
    protected NameRevision nextUnapprovedRevision(NameView nameView) {
        return nameView.getRevisionPair().getUnapprovedRevision();
    }

    @Override
    public RowData getOperatedRowData(NameView nameView) {
        if (getAffectedNameViews().contains(nameView)) {
            NameStage nextStage = operatedOrUnaffectedStage(nameView);
            NameRevision unapproved = nextUnapprovedRevision(nameView);
            NameRevision approved = nextApprovedRevision(nameView);
            NameElement unapprovedElement = unapproved != null ? unapproved.getNameElement() : null;
            NameElement approvedElement = approved != null ? approved.getNameElement() : null;

            return new RowData(
                    unapprovedElement, approvedElement, null,
                    nextStage.isPending(), nextStage.isDeleted(), nextStage.isApproved(),
                    nextStage.isArchived(), nextStage.isCancelled(), true);
        }
        return null;
    }

    @Override
    public void validateUser(boolean editor, boolean superUser) throws ValidationException {
        As.validateState(superUser, "User is not authorized");
    }

}
