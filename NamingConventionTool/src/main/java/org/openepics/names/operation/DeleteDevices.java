/*
 * Copyright (c) 2016 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.operation;

import java.util.Set;
import org.openepics.names.business.NameRevision;
import org.openepics.names.business.NameStage;
import org.openepics.names.nameviews.NameView;
import org.openepics.names.util.As;
import org.openepics.names.util.ValidationException;

/**
 * Class handling data to delete devices.
 */
public class DeleteDevices extends Delete {

    /**
     * Constructs a new operation.
     *
     * @param selectedNameViews the nameViews selected for the operation
     * @param root the root of the tree
     */
    public DeleteDevices(Set<NameView> selectedNameViews, NameView root) {
        super(selectedNameViews, root);
    }

    @Override
    public NameStage operatedStage(NameStage stage) {
        NameStage deleteStage = super.operatedStage(stage);
        return deleteStage != null ? deleteStage.nextProcessedStage(true) : null;
    }

    @Override
    public void validateOnSelect() throws ValidationException {
        super.validateOnSelect();
        for (NameView nameView : getAffectedNameViews()) {
            As.validateState(nameView.isInDeviceRegistry(), "Name is not a device name");
        }
    }

    @Override
    public String getResult() {
        return "deleted";
    }

    @Override
    public String getTitle() {
        return "Delete";
    }

    @Override
    protected boolean affectsDevices(NameView nameView) {
        return false;
    }

    @Override
    protected NameRevision nextApprovedRevision(NameView nameView) {
        return nameView.getRevisionPair().getApprovedRevision();
    }

    @Override
    protected NameRevision nextUnapprovedRevision(NameView nameView) {
        return null;
    }
}
