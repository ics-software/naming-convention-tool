/*
 * Copyright (c) 2017 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.operation;

import java.util.Set;

import org.openepics.names.business.NameRevision;
import org.openepics.names.business.NameStage;
import org.openepics.names.business.NameType;
import org.openepics.names.nameviews.NameView;
import org.openepics.names.util.As;
import org.openepics.names.util.ValidationException;

import com.google.common.collect.Sets;

/**
 * Class handling all data needed to check that device names under this names are correctly named.
 *
 * @author karinrathsman
 */
public class CheckDevices extends MultipleNameOperation {

    /**
     * Constructs a new operation.
     *
     * @param selectedNameViews the nameViews selected for the operation
     * @param root the root of the tree
     */
    public CheckDevices(Set<NameView> selectedNameViews, NameView root) {
        super(selectedNameViews, root);
    }

    @Override
    public Set<NameView> getAffectedDevices() {
        // override method
        //     if device structure or system structure level 2 or 3 then as in parent
        //     if system structure level 1 then override/replace

        Set<NameView> devices = Sets.newHashSet();
        for (NameView nameView : getAffectedNameViews()) {
            NameType nameType = nameView.getNameType();
            if (NameType.DEVICE_STRUCTURE.equals(nameType)
                    || (NameType.SYSTEM_STRUCTURE.equals(nameType)
                            && nameView.getLevel() != 1)) {
                if (affectsDevices(nameView)) {
                    for (NameView child : nameView.getDevices()) {
                        if (!child.getRevisionPair().getNameStage().isArchived()) {
                            devices.add(child);
                        }
                    }
                }
            } else if (NameType.SYSTEM_STRUCTURE.equals(nameType)
                    && nameView.getLevel() == 1) {
                if (affectsDevices(nameView)) {
                    for (NameView child : nameView.getDevicesOneLevel()) {
                        if (!child.getRevisionPair().getNameStage().isArchived()) {
                            devices.add(child);
                        }
                    }
                }
            }
        }
        return devices;
    }

    @Override
    public void validateOnSelect() throws ValidationException {
        super.validateOnSelect();
        As.validateState(getSelectedNameViews().size() == 1, "One name only to be selected for the operation");
    }

    @Override
    public NameStage operatedStage(NameStage stage) {
        return stage;
    }

    @Override
    public boolean isMessageRequired() {
        return true;
    }

    @Override
    public String getResult() {
        return "checked";
    }

    @Override
    public String getTitle() {
        return "Check devices";
    }

    @Override
    public boolean affectsDevices(NameView nameView) {
        return true;
    }

    @Override
    protected NameRevision nextApprovedRevision(NameView nameView) {
        return nameView.getRevisionPair().getApprovedRevision();
    }

    @Override
    protected NameRevision nextUnapprovedRevision(NameView nameView) {
        return nameView.getRevisionPair().getUnapprovedRevision();
    }

    @Override
    public void validateUser(boolean editor, boolean superUser) throws ValidationException {
        As.validateState(superUser, "User is not authorized");
    }
}
