/*
 * Copyright (c) 2016 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.operation;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.openepics.names.business.NameStage;
import org.openepics.names.nameviews.NameView;
import org.openepics.names.util.As;
import org.openepics.names.util.ValidationException;

/**
 * Class handling all data needed to add a name.
 *
 * @author karinrathsman
 */
public class Add extends SingleNameOperation {

    /**
     * Constructs a new operation.
     *
     * @param selectedNameViews the nameViews selected for the operation
     * @param root the root of the tree
     */
    public Add(Set<NameView> selectedNameViews, NameView root) {
        super(selectedNameViews, root);

    }

    @Override
    protected void singleUpdate() {
        NameView nameView = null;
        NameView parentView = singleSelectedNameView();
        NameView otherParentView = null;
        updateNameView(nameView);
        setParentView(parentView);
        updateOtherParentView(otherParentView);
    }

    @Override
    public void validateOnSelect() throws ValidationException {
        if (getOtherParentView() != null) {
            As.validateState(
                    acceptedAsRootParent(getParentView()) && acceptedAsOtherParent(getOtherParentView()),
                    "Parents are not accepted");
        } else {
            As.validateState(
                    acceptedAsRootParent(getParentView()),
                    "Parent is not accepted");
        }
    }

    @Override
    protected List<NameView> affectedNameViews() {
        List<NameView> affectedNameViews = new ArrayList<>();
        if (getParentView() != null) {
            affectedNameViews.add(getParentView());
        }
        return affectedNameViews;
    }

    @Override
    public boolean affects(NameView parentView) {
        return acceptedAsRootParent(parentView) || acceptedAsOtherParent(parentView);
    }

    @Override
    public NameStage operatedStage(NameStage stage) {
        return stage.nextRequestStage(false);
    }

    @Override
    public String getResult() {
        return isInStructure() ? "proposed to be added" : "added";
    }

    @Override
    public String getTitle() {
        return isInStructure() ? "Propose to add " : "Add";
    }

    @Override
    public void validate() throws ValidationException {
        // validation (also) performed as part of naming convention validation
        validateOnSelect();
        validateUpToDate();

        As.validateState(getNameElement().getFullName() != null,
                "[" + getNameElement() + "]: Full name is null");
    }
}
