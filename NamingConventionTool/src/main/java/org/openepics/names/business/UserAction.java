/*
 * Copyright (c) 2016 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.business;

import java.util.Date;

import org.openepics.names.model.UserAccount;

/**
 * Collection of information representing a user action consisting of user, date, message.
 */
public class UserAction {

    private final Date date;
    private final String message;
    private final UserAccount user;

    /**
     * Constructor
     *
     * @param date the date
     * @param message the user message
     * @param user the user
     */
    public UserAction(Date date, String message, UserAccount user) {
        this.date = date != null ? new Date(date.getTime()) : null;
        this.message = message;
        this.user = user;
    }

    /**
     * @return the date
     */
    public Date getDate() {
        return date != null ? new Date(date.getTime()) : null;
    }

    /**
     * @return the message
     */
    public String getMessage() {
        return message;
    }

    /**
     * @return the user
     */
    public UserAccount getUser() {
        return user;
    }

}
