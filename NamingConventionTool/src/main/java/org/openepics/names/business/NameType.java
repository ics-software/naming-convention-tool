/*
 * Copyright (c) 2016 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.business;

import org.openepics.names.model.NamePartType;

/**
 * Enum specifying whether it belongs to the System Structure, Device Structure or Device Registry.
 *
 * @author Karin Rathsman
 */
public enum NameType {

    SYSTEM_STRUCTURE("System Structure", "systemStructure"),
    DEVICE_STRUCTURE("Device Structure", "deviceStructure"),
    DEVICE_REGISTRY("Device Registry", "deviceName");

    private String name;
    private String value;

    /**
     * Constructor
     */
    NameType(String name, String value) {
        this.name = name;
        this.value = value;
    }

    /**
     * Return enum name.
     *
     * @return enum name
     */
    public String getName() {
        return this.name;
    }

    /**
     * Return enum value.
     *
     * @return enum value
     */
    public String getValue() {
        return this.value;
    }

    /*
     * (non-Javadoc)
     * @see java.lang.Enum#toString()
     */
    @Override
    public String toString() {
        return value;
    }

    /**
     * Return the nameType for a string.
     *
     * @param string the string
     * @return the nameType for a string
     */
    public static NameType get(String string) {
        if (string == null) {
            return SYSTEM_STRUCTURE;
        }
        for (NameType type : values()) {
            if (string.equals(type.toString())) {
                return type;
            }
        }
        throw new IllegalArgumentException();
    }

    /**
     *
     * @return true if the type is systemStructure
     */
    public boolean isSystemStructure() {
        return equals(SYSTEM_STRUCTURE);
    }

    /**
     *
     * @return true if the nameType is deviceStructure
     */
    public boolean isDeviceStructure() {
        return equals(DEVICE_STRUCTURE);
    }

    /**
     *
     * @return true if the nameType is deviceRegistry
     */
    public boolean isDeviceRegistry() {
        return equals(DEVICE_REGISTRY);
    }

    /**
     *
     * @return the corresponding NamePartType. (To be removed with the new database)
     */
    public NamePartType asNamePartType() {
        return isDeviceStructure() ? NamePartType.DEVICE_TYPE : isSystemStructure() ? NamePartType.SECTION : null;
    }

}
