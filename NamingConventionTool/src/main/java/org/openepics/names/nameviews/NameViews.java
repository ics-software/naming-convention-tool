/*
 * Copyright (c) 2017 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.nameviews;

import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.openepics.names.business.NameArtifact;
import org.openepics.names.business.NameRevision;
import org.openepics.names.business.NameType;
import org.openepics.names.util.As;
import org.openepics.names.util.NamingConventionUtility;

/**
 * This class handles map of key-value with UUID as key and name view as value.
 * It is used to build system structure and device structure data that can be used in UI and UI-related work.
 *
 * <p>
 * UUID is used as key as it remains same regardless of modification of system structure and device structure data.
 * Name view is data object with relations that contains e.g. name revision pair, type of structure, level.
 *
 * @author karinrathsman
 */
public class NameViews {

    private final NameView systemRoot;
    private final NameView deviceRoot;
    private final Map<UUID, NameView> nameViewMap;

    /**
     * Constructs new map for UUID and name view objects.
     */
    public NameViews() {
        systemRoot = new NameView(NameType.SYSTEM_STRUCTURE);
        deviceRoot = new NameView(NameType.DEVICE_STRUCTURE);
        nameViewMap = new HashMap<>();
    }

    /**
     *
     * @return the root system view
     */
    public NameView getSystemRoot() {
        return systemRoot;
    }

    /**
     *
     * @return the root device view
     */
    public NameView getDeviceRoot() {
        return deviceRoot;
    }

    private NameView newOrExistingNameView(NameArtifact nameArtifact) {
        UUID uuid = As.notNull(nameArtifact.getUuid());
        if (!contains(nameArtifact)) {
            final NameView nameView = new NameView(nameArtifact.getNameType());
            nameViewMap.put(uuid, nameView);
        }
        return As.notNull(nameViewMap.get(uuid));
    }

    /**
     * updates nameViews with a a new revision.
     *
     * @param revision the new revision
     * @return the updated nameView
     */
    public NameView update(NameRevision revision) {
        NameView nameView = newOrExistingNameView(revision.getNameArtifact());
        if (nameView.getRevisionPair().update(revision)) {
            updateParent(nameView, systemRoot);
            updateParent(nameView, deviceRoot);
        }
        return nameView;
    }

    /**
     * updates the parent child relation between names
     *
     * @param nameView
     * @param root
     */
    private void updateParent(NameView nameView, NameView root) {
        NameRevision revision = nameView.getRevisionPair().getBaseRevision();
        NameType rootType = root.getNameType();
        boolean parentRequired = nameView.getNameType().equals(rootType);
        NameArtifact parentArtifact = revision.getParent(rootType);
        NameView parentView =
                parentArtifact != null ? newOrExistingNameView(parentArtifact) : parentRequired ? root : null;
        nameView.setParent(parentView, rootType);
    }

    /**
     * Creates a clone of the specified nameView that belongs to this instance.
     * If it already exist it returns the existing one without updating.
     *
     * @param nameView  the nameView to be cloned.
     * @return          the created clone or the existing clone. null if the nameView is null.
     */
    public NameView addCloneOf(NameView nameView) {
        if (nameView == null) {
            return null;
        } else if (nameView.isRoot()) {
            return getRoot(nameView.getNameType());
        } else {
            UUID uuid = nameView.getRevisionPair().getBaseRevision().getNameArtifact().getUuid();
            if (!nameViewMap.containsKey(uuid)) {
                NameView clone = new NameView(nameView);
                nameViewMap.put(uuid, clone);

                NameView systemParent = nameView.getParent(NameType.SYSTEM_STRUCTURE);
                if (systemParent != null) {
                    clone.setParent(addCloneOf(systemParent), NameType.SYSTEM_STRUCTURE);
                }
                NameView deviceParent = nameView.getParent(NameType.DEVICE_STRUCTURE);
                if (deviceParent != null) {
                    clone.setParent(addCloneOf(deviceParent), NameType.DEVICE_STRUCTURE);
                }
                return clone;
            } else {
                return nameViewMap.get(uuid);
            }
        }
    }

    /**
     *
     * @param nameType the name Type
     * @return the root of the system or device structure specified by the NameType.
     */
    protected NameView getRoot(NameType nameType) {
        Preconditions.checkState(nameType != null && !nameType.isDeviceRegistry());
        return nameType.isDeviceStructure() ? deviceRoot : systemRoot;
    }

    /**
     * Return the name view for the specified uuid.
     *
     * @param uuid the unique identifier for the name instance.
     * @return the name view for the specified uuid
     */
    public NameView get(UUID uuid) {
        return nameViewMap.get(uuid);
    }

    /**
     * Return if name views contains the specified artifact.
     *
     * @param artifact the artifact
     * @return true if name views contains the specified artifact
     */
    public boolean contains(NameArtifact artifact) {
        return nameViewMap.containsKey(artifact.getUuid());
    }

    /**
     * Return list with name views for mnemonic path. If leaf mnemonic path, then only one name view to be returned.
     *
     * @param mnemonicPath mnemonic path
     * @return list with name views for mnemonic path
     */
    public List<NameView> getInSystemStructure(String mnemonicPath) {
        List<NameView> nameViews = getSystemRoot().getAllChildren(NameType.SYSTEM_STRUCTURE);

        String value = null;
        List<NameView> parts = Lists.newArrayList();
        for (NameView nameView : nameViews) {
            value = NamingConventionUtility.mnemonicPath2String(nameView.getMnemonicPath());
            if (mnemonicPath.equals(value)) {
                parts.add(nameView);
            }
        }
        return parts;
    }

}
