/*
 * Copyright (C) 2020 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.nameviews;

/**
 * Utility class to handle transaction result.
 *
 * @author <a href="mailto:zoltan.runyo@ess.eu">Zoltan Runyo</a>
 */
public class TransactionResult {

    /**
     * Enum to distinguish between and handle different types of transaction result.
     */
    public enum Result {
        SUCCESS, ERROR, NO_CHANGES
    }

    private final Result result;
    private final String summary;
    private final String detail;

    private TransactionResult(final Result result, final String summary, final String detail) {
        this.result = result;
        this.summary = summary;
        this.detail = detail;
    }

    /**
     * Utility method to create a transaction result for success with message.
     *
     * @param message message summary
     * @return transaction result
     */
    public static TransactionResult success(final String message) {
        return new TransactionResult(Result.SUCCESS, message, message);
    }

    /**
     * Utility method to create a transaction result for no changes without message.
     *
     * @return transaction result
     */
    public static TransactionResult noChanges() {
        return new TransactionResult(Result.NO_CHANGES, null, null);
    }

    /**
     * Utility method to create a transaction result for error with message.
     *
     * @param message message summary
     * @return transaction result
     */
    public static TransactionResult error(final String message) {
        return new TransactionResult(Result.ERROR, message, message);
    }

    /**
     * Utility method to create a transaction result for error with message and details.
     *
     * @param summary message summary
     * @param detail message detail
     * @return transaction result
     */
    public static TransactionResult error(final String summary, final String detail) {
        return new TransactionResult(Result.ERROR, summary, detail);
    }

    /**
     * Return type of transaction result.
     *
     * @return transaction result
     */
    public Result getResult() {
        return result;
    }

    /**
     * Return message summary of transaction result.
     *
     * @return message summary of transaction result
     */
    public String getSummary() {
        return summary;
    }

    /**
     * Return message details for transaction result.
     *
     * @return message details for transaction result
     */
    public String getDetail() {
        return detail;
    }

}
