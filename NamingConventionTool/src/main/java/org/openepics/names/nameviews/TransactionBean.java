/*
 * Copyright (c) 2017 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.nameviews;

import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceUnit;
import javax.transaction.HeuristicMixedException;
import javax.transaction.HeuristicRollbackException;
import javax.transaction.NotSupportedException;
import javax.transaction.RollbackException;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import org.apache.commons.lang3.StringUtils;
import org.openepics.names.business.NameArtifact;
import org.openepics.names.business.NameElement;
import org.openepics.names.business.NameRevision;
import org.openepics.names.business.NameRevisionPair;
import org.openepics.names.business.NameRevisionStatus;
import org.openepics.names.business.NameType;
import org.openepics.names.dto.MailNotificationDTO;
import org.openepics.names.dto.NamePartRevisionDTO;
import org.openepics.names.model.*;
import org.openepics.names.operation.Add;
import org.openepics.names.operation.AddDevice;
import org.openepics.names.operation.Approve;
import org.openepics.names.operation.Cancel;
import org.openepics.names.operation.CheckDevices;
import org.openepics.names.operation.Delete;
import org.openepics.names.operation.DeleteDevices;
import org.openepics.names.operation.Modify;
import org.openepics.names.operation.ModifyDevice;
import org.openepics.names.operation.NameOperation;
import org.openepics.names.operation.Reject;
import org.openepics.names.operation.SingleNameOperation;
import org.openepics.names.services.MnemonicValidation;
import org.openepics.names.services.NamingConvention;
import org.openepics.names.services.SessionService;
import org.openepics.names.util.As;
import org.openepics.names.util.NamingHelper;
import org.openepics.names.util.NamingRuntimeException;
import org.openepics.names.util.NotificationService;
import org.openepics.names.util.ValidationException;

import com.google.common.collect.Sets;

/**
 * Stateless bean acting between other beans and database.
 *
 * Bean handles actions (operations) on data for database together with various utility methods.
 *
 * @author karinrathsman
 */
@Stateless
@TransactionManagement(value = TransactionManagementType.BEAN)
public class TransactionBean {

    private static final String APPROVED = "approved";
    private static final String CONVENTION_NAME_EQ_CLASS = "conventionNameEqClass";
    private static final String DEVICE = "device";
    private static final String MNEMONIC_EQUIVALENCE_CLASS = "mnemonicEquivalenceClass";
    private static final String NAME_PART = "namePart";
    private static final String PENDING = "pending";
    private static final String STATUS = "status";

    private static final String CHANGES_NEEDS_TO_BE_APPROVED = "[Changes - Needs to be approved]";
    private static final String CHANGES_NOTIFICATION         = "[Changes - Notification]";
    private static final String DEFAULT_MAIL_SUBJECT         = "There are new changes in Naming";

    private static final String MNEMONIC_IS_NOT_UNIQUE_ACCORDING_TO_NAMING_RULES =
            "Mnemonic is not unique according to naming rules.";
    private static final String MNEMONIC_IS_NOT_VALID_ACCORDING_TO_NAMING_RULES =
            "Mnemonic is not valid according to naming rules.";

    private static final String OPERATION_IS_NEITHER_IN_DEVICE_REGISTRY_NOR_IN_STRUCTURE =
            "Operation is neither in device registry nor in structure";

    private static final String SELECT_R_FROM_NAME_PART_REVISION_R_WHERE_R_ID_EQUALS =
            "SELECT r FROM NamePartRevision r WHERE r.id = ";

    @Inject
    private NameViewProvider nameViewProvider;
    @Inject
    private NamingConvention namingConvention;
    @Inject
    private SessionService sessionService;
    @Resource
    private UserTransaction transaction;
    @Resource
    private UserTransaction transactionAddDevice;
    @PersistenceUnit
    EntityManagerFactory emf;
    @PersistenceContext
    private EntityManager em;
    @Inject
    private NotificationService notificationService;
    @Inject
    private NamingHelper namingHelper;

    // mail notification lists
    //     keep track of MailNotificationDTO
    //     keep track of cc per MailNotificationDTO
    //     comparator for MailNotificationDTO to sort
    private List<MailNotificationDTO> modificationSystemStructureApproval;
    private List<MailNotificationDTO> modificationDeviceStructureApproval;
    private List<MailNotificationDTO> modificationSystemStructureNonApproval;
    private List<MailNotificationDTO> modificationDeviceStructureNonApproval;
    private List<MailNotificationDTO> EMPTY_MODIFICATIONS = Collections.emptyList();
    private HashMap<String, List<MailNotificationDTO>> ccModificationSystemStructure;
    private HashMap<String, List<MailNotificationDTO>> ccModificationDeviceStructure;
    private Comparator<MailNotificationDTO> comparatorMailNotificationDTO = new Comparator<MailNotificationDTO>() {
        @Override
        public int compare(MailNotificationDTO first, MailNotificationDTO second) {
            List<String> list1 = first.getNodeList();
            List<String> list2 = second.getNodeList();
            if (list1.size() < list2.size()) {
                return -1;
            } else if (list2.size() < list1.size()) {
                return 1;
            } else {
                for (int i=0; i<list1.size(); i++) {
                    int val = list1.get(i).compareTo(list2.get(i));
                    if (val != 0) {
                        return val;
                    } else {
                        continue;
                    }
                }
                return 0;
            }
        }
    };

    private String nameViewStructure;

    @PostConstruct
    private void init() {
        // mail notification lists
        modificationSystemStructureApproval = new ArrayList<>();
        modificationDeviceStructureApproval = new ArrayList<>();
        modificationSystemStructureNonApproval = new ArrayList<>();
        modificationDeviceStructureNonApproval = new ArrayList<>();
        ccModificationSystemStructure = new HashMap<>();
        ccModificationDeviceStructure = new HashMap<>();
    }

    /**
     * Submits the operation
     *
     * @param nameOperation instance that carries the information needed to perform the operation
     * @return the outcome result of the transaction
     */
    public synchronized TransactionResult submit(NameOperation nameOperation) {
        Set<NameOperation> operations = new HashSet<>();
        operations.add(nameOperation);
        return submit(operations);
    }

    /**
     * Submits a set of operations
     *
     * @param operations list of operations
     * @return the outcome message of the transaction *
     */
    public synchronized TransactionResult submit(Set<NameOperation> operations) {
        try {
            transaction.begin();
            em = emf.createEntityManager();
            Action action = new Action();

            // clear mail notifications
            clearNotifications();

            for (NameOperation operation : operations) {
                if (operation instanceof Delete) {
                    action.execute((Delete) operation);
                } else if (operation instanceof Add) {
                    action.execute((Add) operation);
                } else if (operation instanceof Approve) {
                    action.execute((Approve) operation);
                } else if (operation instanceof Cancel) {
                    action.execute((Cancel) operation);
                } else if (operation instanceof Modify) {
                    action.execute((Modify) operation);
                } else if (operation instanceof Reject) {
                    action.execute((Reject) operation);
                } else if (operation instanceof CheckDevices) {
                    action.execute((CheckDevices) operation);
                } else {
                    throw new UnsupportedOperationException("NameOperation is not supported");
                }
            }

            // send mail notifications
            sendNotifications();

            // clear mail notifications
            clearNotifications();

            transaction.commit();
            nameViewProvider.update(action.getRevisions());

            // after name in system structure approved --> auto add ESS name
            //     each system name revision --> AddDevice operation
            //         prepare name operation
            //         execute action for operation
            // note. 2nd transaction independent of 1st transaction

            Set<NameOperation> operationsAddDevice = null;
            if (!action.getSystemNameRevisionsForAddDevice().isEmpty()) {
                operationsAddDevice = new HashSet<>();

                NameView systemRoot = nameViewProvider.nameStructure(NameType.SYSTEM_STRUCTURE);
                for (NameRevision nameRevision : action.getSystemNameRevisionsForAddDevice()) {
                    // AddDevice
                    //     system structure parent but not device structure parent
                    NameView systemStructureView =
                            nameViewProvider.getNameViews().get(nameRevision.getNameArtifact().getUuid());
                    String conventionName =
                            namingConvention.conventionName(
                                    systemStructureView.getMnemonicPath(), NameType.SYSTEM_STRUCTURE);

                    Set<NameView> setSystemStructureView = Sets.newHashSet(systemStructureView);
                    AddDevice add = new AddDevice(setSystemStructureView, systemRoot);
                    add.updateOtherParentView(null);
                    add.setNameElement(
                            new NameElement(
                                    conventionName,
                                    null,
                                    "System structure only"));
                    add.setMessage("Added after System structure approval");

                    operationsAddDevice.add(add);
                }
            }

            if (operationsAddDevice != null) {
                // not consider transaction result
                submitAddDevice(operationsAddDevice);
            }

            return action.getSuccessMessage();
        } catch (ValidationException ex) {
            try {
                transaction.rollback();
                return TransactionResult.error(ex.getMessage(), ex.getDescription());
            } catch (IllegalStateException | SecurityException | SystemException ex1) {
                Logger.getLogger(TransactionBean.class.getName()).log(Level.SEVERE, null, ex1);
                throw new NamingRuntimeException(ex1);
            }
        } catch (IllegalStateException
                    | SecurityException
                    | HeuristicMixedException
                    | HeuristicRollbackException
                    | NotSupportedException
                    | RollbackException
                    | SystemException ex) {
            try {
                transaction.rollback();
                return TransactionResult.error(ex.getMessage());
            } catch (IllegalStateException | SecurityException | SystemException ex1) {
                Logger.getLogger(TransactionBean.class.getName()).log(Level.SEVERE, null, ex1);
                throw new NamingRuntimeException(ex1);
            }
        }
    }

    /**
     * Only for submit of AddDevice operations after approval of Add operations in System structure.
     *
     * @param operations
     * @return
     */
    private synchronized TransactionResult submitAddDevice(Set<NameOperation> operations) {
        // note
        //     different transaction compared to submit(Set<NameOperation>)
        try {
            transactionAddDevice.begin();
            em = emf.createEntityManager();
            Action action = new Action();

            for (NameOperation operation : operations) {
                if (operation instanceof AddDevice) {
                    action.execute((AddDevice) operation);
                } else {
                    throw new UnsupportedOperationException("NameOperation is not supported");
                }
            }

            transactionAddDevice.commit();
            nameViewProvider.update(action.getRevisions());

            return action.getSuccessMessage();
        } catch (ValidationException ex) {
            try {
                transactionAddDevice.rollback();
                return TransactionResult.error(ex.getMessage(), ex.getDescription());
            } catch (IllegalStateException | SecurityException | SystemException ex1) {
                Logger.getLogger(TransactionBean.class.getName()).log(Level.SEVERE, null, ex1);
                throw new NamingRuntimeException(ex1);
            }
        } catch (IllegalStateException
                    | SecurityException
                    | HeuristicMixedException
                    | HeuristicRollbackException
                    | NotSupportedException
                    | RollbackException
                    | SystemException ex) {
            try {
                transactionAddDevice.rollback();
                return TransactionResult.error(ex.getMessage());
            } catch (IllegalStateException | SecurityException | SystemException ex1) {
                Logger.getLogger(TransactionBean.class.getName()).log(Level.SEVERE, null, ex1);
                throw new NamingRuntimeException(ex1);
            }
        }
    }

    /**
     * find the artifact and check that the revision pair is up to date
     *
     * @param nameView the artifact of the specified nameView
     * @return the name artifact
     * @throws org.openepics.names.util.ValidationException Name validation exception
     */
    public NameArtifact artifact(NameView nameView) throws ValidationException {
        NameRevisionPair pair = nameView != null ? nameView.getRevisionPair() : null;
        NameArtifact artifact = pair != null ? pair.getBaseRevision().getNameArtifact() : null;
        if (artifact != null) {
            As.validateState(
                    pair.getLatestRevision().isSameAs(latestRevision(artifact)),
                    "Latest revision of name with uuid" +artifact.getUuid().toString()+ " is not up todate");
            As.validateState(
                    pair.getBaseRevision().isSameAs(baseRevision(artifact)),
                    "BaseRevision of name with uuid" +artifact.getUuid().toString()+ " is not up todate");
        }
        return artifact;
    }

    /**
     * Check that the proposed name is valid and unique to naming convention.
     *
     * @param nameOperation containing the proposed name
     * @throws org.openepics.names.util.ValidationException Name validation exception
     */
    public void validateMnemonic(SingleNameOperation nameOperation) throws ValidationException {
        // name element length depends on system/device structure, required or not

        NameView parent = nameOperation.getParentView();
        NameView otherParent = nameOperation.getOtherParentView();
        nameOperation.validate();
        String mnemonic = As.emptyIfNull(nameOperation.getNameElement().getMnemonic());

        List<String> mnemonicPath = parent.getMnemonicPathWithChild(mnemonic);
        NameType mnemonicType = nameOperation.rootType();

        if (nameOperation.isInStructure()
                && !(nameOperation instanceof AddDevice || nameOperation instanceof ModifyDevice)) {
            MnemonicValidation mnemonicValidation = namingConvention.validateMnemonic(mnemonicPath, mnemonicType);
            if (mnemonicValidation == MnemonicValidation.NON_ACCEPTABLE_CHARS) {
                As.validateStateMnemonicRules(
                        mnemonicValidation,
                        MNEMONIC_IS_NOT_VALID_ACCORDING_TO_NAMING_RULES
                                + " Only upper and lower case alphanumeric characters (a-z, A-Z, 0-9) are allowed.");
            } else if (mnemonicValidation == MnemonicValidation.TOO_LONG){
                // same message for all name elements except system, subsystem
                int level = !(mnemonicPath == null || mnemonicPath.isEmpty()) ? mnemonicPath.size() : 0;
                String message = mnemonicType.isSystemStructure() && (level == 2 || level == 3)
                        ? MNEMONIC_IS_NOT_VALID_ACCORDING_TO_NAMING_RULES
                            + " The maximum character length of name element is 8."
                        : MNEMONIC_IS_NOT_VALID_ACCORDING_TO_NAMING_RULES
                            + " The maximum character length of name element is 6.";

                As.validateStateMnemonicRules(
                        mnemonicValidation,
                        message);
            }
            As.validateState(
                    isMnemonicUniqueOrOptional(
                            nameOperation.getNameView(),
                            nameOperation.getParentView(),
                            nameOperation.getNameElement().getMnemonic()),
                    MNEMONIC_IS_NOT_UNIQUE_ACCORDING_TO_NAMING_RULES
                    + (!StringUtils.isEmpty(StringUtils.trimToEmpty(nameViewStructure))
                            ? "<br><br>" + StringUtils.trimToEmpty(nameViewStructure)
                            : ""));

        } else if (nameOperation.isInDeviceRegistry()) {

            if (otherParent == null) {
                As.validateState(
                        StringUtils.isEmpty(mnemonic),
                        "Instance index is not valid according to naming rules.");
            } else {
                As.validateState(
                        namingConvention.isInstanceIndexValid(
                                parent.getMnemonicPath(),
                                otherParent.getMnemonicPath(),
                                mnemonic,
                                sessionService.isSuperUser()),
                        "Instance index is not valid according to naming rules.");
                As.validateState(
                        isInstanceIndexUnique(nameOperation),
                        MNEMONIC_IS_NOT_UNIQUE_ACCORDING_TO_NAMING_RULES);
            }
            As.validateState(
                    StringUtils.isNotEmpty(nameOperation.getNameElement().getDescription()),
                    "Description must not be empty.");
        } else if (nameOperation.isInSystemStructure()) {
            // check isMnemonicUnique ref isInstanceIndexUnique
            boolean isMnemonicUnique = isMnemonicUnique(nameOperation);
            As.validateState(
                    isMnemonicUnique,
                    MNEMONIC_IS_NOT_UNIQUE_ACCORDING_TO_NAMING_RULES);
        } else {
            As.validateState(false, OPERATION_IS_NEITHER_IN_DEVICE_REGISTRY_NOR_IN_STRUCTURE);
        }
    }


    /**
     * Checks if the proposed name is unique
     *
     * @param nameOperation the operation containing the proposed name.
     * @return true if the proposed changes results in a unique name.
     */
    private boolean isInstanceIndexUnique(SingleNameOperation nameOperation) throws ValidationException {
        As.validateState(nameOperation.isInDeviceRegistry(), "not in deviceRegistry");
        String conventionName =
                namingConvention.conventionName(
                        nameOperation.getParentView().getMnemonicPath(),
                        nameOperation.getOtherParentView().getMnemonicPath(),
                        nameOperation.getNameElement().getMnemonic());
        String equivalenceClass = namingConvention.equivalenceClassRepresentative(conventionName);
        if (nameOperation instanceof Modify) {
            NameArtifact artifact = artifact(nameOperation.getNameView());
            List<DeviceRevision> namesakes =
                    em.createQuery(
                            "SELECT r FROM DeviceRevision r "
                                + "WHERE r.id = (SELECT MAX(r2.id) FROM DeviceRevision r2 WHERE r2.device = r.device) "
                                + "AND r.deleted = false "
                                + "AND r.conventionNameEqClass = :conventionNameEqClass "
                                + "AND NOT (r.device =:device)",
                            DeviceRevision.class)
                        .setParameter(CONVENTION_NAME_EQ_CLASS, equivalenceClass)
                        .setParameter(DEVICE, artifact.asDevice())
                        .getResultList();
            return namesakes.isEmpty();
        } else {
            List<DeviceRevision> namesakes =
                    em.createQuery(
                            "SELECT r FROM DeviceRevision r "
                                + "WHERE r.id = (SELECT MAX(r2.id) FROM DeviceRevision r2 WHERE r2.device = r.device) "
                                + "AND r.deleted = false "
                                + "AND r.conventionNameEqClass = :conventionNameEqClass",
                            DeviceRevision.class)
                        .setParameter(CONVENTION_NAME_EQ_CLASS, equivalenceClass)
                        .getResultList();
            return namesakes.isEmpty();
        }
    }

    private boolean isMnemonicUnique(SingleNameOperation nameOperation) throws ValidationException {
        As.validateState(nameOperation.isInSystemStructure(), "not in system structure");
        String conventionName =
                namingConvention.conventionName(
                        nameOperation.getParentView().getMnemonicPath(),
                        nameOperation.getParentView().getNameType());
        String equivalenceClass = namingConvention.equivalenceClassRepresentative(conventionName);
        if (nameOperation instanceof Modify) {
            return false;
        } else {
            List<DeviceRevision> namesakes =
                    em.createQuery(
                            "SELECT r FROM DeviceRevision r "
                                + "WHERE r.id = (SELECT MAX(r2.id) FROM DeviceRevision r2 WHERE r2.device = r.device) "
                                + "AND r.deleted = false "
                                + "AND r.conventionNameEqClass = :conventionNameEqClass",
                            DeviceRevision.class)
                        .setParameter(CONVENTION_NAME_EQ_CLASS, equivalenceClass)
                        .getResultList();
            return namesakes.isEmpty();
        }
    }

    /**
     * Validates the operation data before submission
     *
     * @param operation operation data
     * @throws org.openepics.names.util.ValidationException Name validation exception
     */
    public void validate(NameOperation operation) throws ValidationException {
        operation.validate();
        operation.validateUser(sessionService.isEditor(), sessionService.isSuperUser());
        if (operation instanceof SingleNameOperation) {
            validateMnemonic((SingleNameOperation) operation);
        }
    }

    /**
     * Return if the mnemonic of a name part that is to be modified is unique according to the naming convention rules.
     *
     * @param nameView      view of the name
     * @param parentView    view of the parent
     * @param mnemonic      mnemonic of the name part to be tested for uniqueness
     * @return              <tt>true</tt> if the mnemonic of a name part that is to be modified is unique
     *                      according to the naming convention rules
     *
     * @see NameSake
     */
    private boolean isMnemonicUniqueOrOptional(
            NameView nameView, NameView parentView, String mnemonic) throws ValidationException {

        nameViewStructure = null;

        final List<String> newMnemonicPath = parentView.getMnemonicPathWithChild(mnemonic);
        if (mnemonic == null) {
            return !isMnemonicRequiredForChild(parentView);
        }

        // namesake object to hold name view and latest/base revision information
        //     pending status correspond to latest revision
        //     approved status correspond to base revision
        Set<NameSake> namesakes = namesakes(mnemonic);
        if (namesakes != null && !namesakes.isEmpty()) {
            for (NameSake namesake : namesakes) {
                if (nameView == null || !namesake.getNameView().equals(nameView)) {
                    List<String> namesakeMnemonicPath = namesake.isLatestRevision()
                            ? namesake.getNameView().getMnemonicPathLatestRevision()
                            : namesake.getNameView().getMnemonicPath();
                    NameType namesakeNameType = namesake.getNameView().getNameType();

                    if (!namingConvention.canMnemonicsCoexist(
                            newMnemonicPath,
                            parentView.getNameType(),
                            namesakeMnemonicPath,
                            namesakeNameType)) {
                        nameViewStructure = namesake.getNameView().toStringStructure();
                        return false;
                    }
                }
            }
        }
        return true;
    }

    /**
     * Checks if mnemonic is required for name view.
     *
     * @param nameView the parent of the name part, null if the name part is at the root of the hierarchy
     * @return true if the mnemonic of a child name can be null
     */
    public boolean isMnemonicRequiredForChild(NameView nameView) {
        return namingConvention.isMnemonicRequired(nameView.getMnemonicPathWithChild(""), nameView.getNameType());
    }

    /**
     * Checks if mnemonic is required for operation.
     *
     * @param operation intended operation
     * @return true if mnemonic is required
     */
    public boolean isMnemonicRequired(SingleNameOperation operation) {
        return namingConvention.isMnemonicRequired(
                operation.getParentView().getMnemonicPathWithChild(""), operation.getParentView().getNameType());
    }

    /**
     * Generate the naming convention name
     *
     * @param nameView the nameView
     * @return the convention name for the base revision of the nameView.
     */
    public String conventionName(NameView nameView) {
        if (nameView.isInDeviceRegistry()) {
            return namingConvention.conventionName(
                    nameView.getParent(NameType.SYSTEM_STRUCTURE).getMnemonicPath(),
                    nameView.getParent(NameType.DEVICE_STRUCTURE) != null
                        ? nameView.getParent(NameType.DEVICE_STRUCTURE).getMnemonicPath()
                        : null,
                    nameView.getRevisionPair().getBaseRevision().getNameElement().getMnemonic());
        } else if (nameView.isInStructure()) {
            return namingConvention.conventionName(
                    nameView.getMnemonicPath(),
                    nameView.getNameType());
        } else {
            throw new UnsupportedOperationException(OPERATION_IS_NEITHER_IN_DEVICE_REGISTRY_NOR_IN_STRUCTURE);
        }
    }

    /**
     * Generates the convention name for the proposed name
     *
     * @param nameOperation the single name operation containing the name proposal
     * @return the convention name for the operation
     */
    public String conventionName(SingleNameOperation nameOperation) {
        if (nameOperation.isInDeviceRegistry()) {
            return namingConvention.conventionName(
                    nameOperation.getParentView().getMnemonicPath(),
                    nameOperation.getOtherParentView() != null
                        ? nameOperation.getOtherParentView().getMnemonicPath()
                        : null,
                    nameOperation.getNameElement().getMnemonic());
        } else if (nameOperation.isInStructure()) {
            return namingConvention.conventionName(
                    nameOperation.getParentView().getMnemonicPath(),
                    nameOperation.rootType());
        } else if (nameOperation.getParentView() != null && nameOperation.getParentView().isInStructure()) {
            return namingConvention.conventionName(
                    nameOperation.getParentView().getMnemonicPath(),
                    nameOperation.rootType());
        } else {
            throw new UnsupportedOperationException(OPERATION_IS_NEITHER_IN_DEVICE_REGISTRY_NOR_IN_STRUCTURE);
        }
    }

    /**
     * Generates a set of namesakes (<tt>NameSake</tt>) for the proposed mnemonic.
     *
     * @param mnemonic mnemonic
     * @return         set of namesakes (<tt>NameSake</tt>) with similar mnemonics as the specified mnemonic
     * @throws org.openepics.names.util.ValidationException
     *
     * @see NameSake
     */
    private Set<NameSake> namesakes(String mnemonic) throws ValidationException {
        Set<NameSake> namesakes = new HashSet<>();
        String mnemonicEquivalenceClass = namingConvention.equivalenceClassRepresentative(mnemonic);
        List<NamePartRevision> revisions =
                em.createQuery(
                        "SELECT r FROM NamePartRevision r "
                            + "WHERE (r.id = (SELECT MAX(r2.id) FROM NamePartRevision r2 "
                                           + "WHERE r2.namePart = r.namePart AND r2.status = :approved) "
                                    + "OR r.id = (SELECT MAX(r2.id) FROM NamePartRevision r2 "
                                               + "WHERE r2.namePart = r.namePart AND r2.status = :pending)) "
                            + "AND r.deleted = FALSE "
                            + "AND r.mnemonicEqClass = :mnemonicEquivalenceClass",
                        NamePartRevision.class)
                    .setParameter(APPROVED, NamePartRevisionStatus.APPROVED)
                    .setParameter(PENDING, NamePartRevisionStatus.PENDING)
                    .setParameter(MNEMONIC_EQUIVALENCE_CLASS, mnemonicEquivalenceClass)
                    .getResultList();
        for (NamePartRevision revision : revisions) {
            NameView nameView = nameViewProvider.getNameViews().get(revision.getNamePart().getUuid());

            // no namesake object if no nameview
            if (nameView == null) {
                continue;
            }
            boolean isLatest = nameView.getRevisionPair().getLatestRevision().getId() == revision.getId();
            boolean isBase = nameView.getRevisionPair().getBaseRevision().getId() == revision.getId();

            // namesake object to hold nameview and isLatest/isBase information
            NameSake nameSake = new NameSake(nameView, isLatest, isBase);
            As.validateState(isLatest || isBase, "nameView is not up to date");
            namesakes.add(nameSake);
        }
        return namesakes;
    }

    /**
     * Returns the latest revision of the artifact
     *
     * @param artifact
     * @return
     */
    private NameRevision latestRevision(NameArtifact artifact) {
        if (artifact.getNameType().isDeviceRegistry()) {
            DeviceRevision revision =
                    em.createQuery("SELECT r FROM DeviceRevision r WHERE r.device = :device ORDER BY r.id DESC",
                            DeviceRevision.class)
                        .setParameter(DEVICE, artifact.asDevice())
                        .getResultList()
                        .get(0);
            return new NameRevision(revision);
        } else {
            NamePartRevision revision =
                    em.createQuery(
                            SELECT_R_FROM_NAME_PART_REVISION_R_WHERE_R_ID_EQUALS
                                + "(SELECT MAX(r2.id) FROM NamePartRevision r2 WHERE r2.namePart = :namePart ) ",
                            NamePartRevision.class)
                        .setParameter(NAME_PART, artifact.asNamePart())
                        .getSingleResult();
            return new NameRevision(revision);
        }
    }

    /**
     * Returns the base revision of the artifact
     *
     * @param artifact
     * @return
     */
    private NameRevision baseRevision(NameArtifact artifact) {
        if (artifact.getNameType().isDeviceRegistry()) {
            return latestRevision(artifact);
        } else {
            try {
                NamePartRevision approvedNamePartRevision =
                        em.createQuery(
                                SELECT_R_FROM_NAME_PART_REVISION_R_WHERE_R_ID_EQUALS
                                    + "(SELECT MAX(r2.id) FROM NamePartRevision r2 "
                                    + "WHERE r2.namePart = :namePart AND r2.status = :status) ",
                                NamePartRevision.class)
                            .setParameter(NAME_PART, artifact.asNamePart())
                            .setParameter(STATUS, NamePartRevisionStatus.APPROVED)
                            .getSingleResult();
                return new NameRevision(approvedNamePartRevision);
            } catch (NoResultException e) {
                return latestRevision(artifact);
            }
        }
    }


    private class Action {

        private static final String DELETED = "deleted";
        private static final String MODIFIED = "modified";

        private Map<String, Integer> resultMap;
        private List<NameRevision> revisions;
        private NameViews detachedNameViews;
        private List<NameRevision> systemNameRevisionsForAddDevice;

        public Action() {
            resultMap = new HashMap<>();
            revisions = new ArrayList<>();
            detachedNameViews = new NameViews();
            systemNameRevisionsForAddDevice = new ArrayList<>();
        }

        /**
         * Adds new or processed revision to revisions, resultMap and detachedNameViews
         *
         * @param action the action
         * @param revision new revision
         */
        public void add(String action, NameRevision revision) {
            if (revision != null) {
                revisions.add(revision);
                detachedNameViews.update(revision);
                Integer number = resultMap.containsKey(action) ? resultMap.get(action) : 0;
                resultMap.put(action, ++number);
            }
        }

        /**
         * Process the successMessage.
         */
        private TransactionResult getSuccessMessage() {
            String successMessage = null;
            for (Map.Entry<String, Integer> entry : resultMap.entrySet()) {
                String action = entry.getKey();
                Integer n = entry.getValue();
                String string;
                if (APPROVED.equals(action) || "cancelled".equals(action) || "rejected".equals(action)) {
                    string = n + (n > 1 ? " name proposals have been " : " name proposal has been ") + action;
                } else {
                    string = n + (n > 1 ? " names have been " : " name has been ") + action;
                }
                if (successMessage != null) {
                    successMessage = successMessage.concat(", " + string);
                } else {
                    successMessage = string;
                }
            }
            return StringUtils.isNotEmpty(successMessage) ? TransactionResult.success(successMessage)
                    : TransactionResult.noChanges();
        }

        /**
         *
         * @return the new or processed revisions
         */
        private List<NameRevision> getRevisions() {
            return revisions;
        }

        private List<NameRevision> getSystemNameRevisionsForAddDevice() {
            return systemNameRevisionsForAddDevice;
        }

        /**
         * Executes the deletion
         *
         * @param operation delete data
         * @throws ValidationException
         */
        private void execute(Delete operation) throws ValidationException {
            validate(operation);
            for (NameView originalNameView : operation.getAffectedNameViews()) {
                NameView nameView = detachedNameViews.addCloneOf(originalNameView);
                NameArtifact artifact = artifact(nameView);
                NameRevisionPair pair = nameView.getRevisionPair();
                NameRevision baseRevision = pair.getBaseRevision();
                NameElement el = baseRevision.getNameElement();
                if (nameView.isInDeviceRegistry() && operation instanceof DeleteDevices) {
                    NameArtifact systemParent = baseRevision.getParent(NameType.SYSTEM_STRUCTURE);
                    NameArtifact deviceParent = baseRevision.getParent(NameType.DEVICE_STRUCTURE);
                    String mnemonicEqClass = namingConvention.equivalenceClassRepresentative(el.getFullName());
                    final DeviceRevision deviceRevision =
                            new DeviceRevision(
                                    artifact.asDevice(), new Date(), sessionService.user(), true,
                                    systemParent.asNamePart(),
                                    deviceParent != null ? deviceParent.asNamePart() : null,
                                    el.getMnemonic(), el.getFullName(),
                                    mnemonicEqClass, el.getDescription(), operation.getMessage());
                    em.persist(deviceRevision);
                    add(DELETED, new NameRevision(deviceRevision));
                } else {
                    if (pair.getNameStage().isAdded()) {
                        processRevision(artifact, NameRevisionStatus.CANCELLED, operation.getMessage());
                    } else if (pair.getNameStage().isActive()) {
                        if (pair.getNameStage().isModified()) {
                            processRevision(
                                    artifact,
                                    NameRevisionStatus.CANCELLED,
                                    "proposal automatically cancelled before delete");
                        }
                        NameArtifact parent = baseRevision.getParent(artifact.getNameType());
                        NamePart parentAsNamePart = parent != null ? parent.asNamePart() : null;
                        String mnemonicEqClass = namingConvention.equivalenceClassRepresentative(el.getMnemonic());
                        final NamePartRevision namePartRevision =
                                new NamePartRevision(
                                        artifact.asNamePart(), new Date(), sessionService.user(),
                                        operation.getMessage(), true, parentAsNamePart,
                                        el.getFullName(), el.getMnemonic(), el.getDescription(), mnemonicEqClass);
                        em.persist(namePartRevision);
                        add("proposed to be deleted", new NameRevision(namePartRevision));
                    }

                    //notify admins about device deletion - approval needed
                    notifyAdminsFromChange(
                            artifact, "Deleted - Needs to be approved",
                            "[Deleted - Needs to be approved]", operation.getMessage());
                }
            }
        }

        /**
         * Executes add name
         *
         * @param operation add data
         * @throws ValidationException
         */
        private void execute(Add operation) throws ValidationException {
            validate(operation);
            NameView systemParentView =
                    detachedNameViews.addCloneOf(operation.getParentView(NameType.SYSTEM_STRUCTURE));
            NameView deviceParentView =
                    detachedNameViews.addCloneOf(operation.getParentView(NameType.DEVICE_STRUCTURE));
            NameElement el = operation.getNameElement();
            if (operation instanceof AddDevice) {
                NamePart systemParent = artifact(systemParentView).asNamePart();
                NamePart deviceParent = deviceParentView != null ? artifact(deviceParentView).asNamePart() : null;
                String conventionNameEqClass = namingConvention.equivalenceClassRepresentative(el.getFullName());
                Device device = new Device(UUID.randomUUID());
                DeviceRevision newRevision =
                        new DeviceRevision(
                                device, new Date(), sessionService.user(), false,
                                systemParent, deviceParent,
                                el.getMnemonic(), el.getFullName(), conventionNameEqClass, el.getDescription());
                em.persist(device);
                em.persist(newRevision);
                NameRevision nameRevision = new NameRevision(newRevision);
                add("added", nameRevision);
            } else {
                NameView parentView = operation.getParentView();
                NamePart parent = parentView.isInStructure() ? artifact(parentView).asNamePart() : null;
                NamePart namePart = new NamePart(UUID.randomUUID(), operation.rootType().asNamePartType());
                String mnemonicEqClass = namingConvention.equivalenceClassRepresentative(el.getMnemonic());
                NamePartRevision newRevision =
                        new NamePartRevision(
                                namePart, new Date(), sessionService.user(), operation.getMessage(), false, parent,
                                el.getFullName(), el.getMnemonic(), el.getDescription(), mnemonicEqClass);
                em.persist(namePart);
                em.persist(newRevision);
                NameRevision nameRevision = new NameRevision(newRevision);
                add("proposed to be added", nameRevision);

                String message = "Added -  Needs to be approved";

                // notify users
                addNotificationSystemStructureApproval(
                        new MailNotificationDTO(
                                namingHelper.parentListForNode(newRevision, NamePartType.SECTION),
                                message,
                                operation.getMessage()));
                addNotificationDeviceStructureApproval(
                        new MailNotificationDTO(
                                namingHelper.parentListForNode(newRevision, NamePartType.DEVICE_TYPE),
                                message,
                                operation.getMessage()));
            }
        }

        /**
         * Executes modification
         *
         * @param operation modify data
         * @throws ValidationException
         */
        private void execute(Modify operation) throws ValidationException {
            validate(operation);
            NameView nameView = detachedNameViews.addCloneOf(operation.getNameView());
            NameArtifact artifact = artifact(nameView);
            NameElement el = operation.getNameElement();

            if (operation instanceof ModifyDevice) {
                NamePart systemParent = artifact(operation.getParentView(NameType.SYSTEM_STRUCTURE)).asNamePart();
                NamePart deviceParent = operation.getParentView(NameType.DEVICE_STRUCTURE) != null
                        ? artifact(operation.getParentView(NameType.DEVICE_STRUCTURE)).asNamePart()
                        : null;
                String conventionNameEqClass = namingConvention.equivalenceClassRepresentative(el.getFullName());
                final DeviceRevision newRevision =
                        new DeviceRevision(
                                artifact.asDevice(), new Date(), sessionService.user(), false,
                                systemParent, deviceParent,
                                el.getMnemonic(), el.getFullName(), conventionNameEqClass, el.getDescription());
                em.persist(newRevision);
                add(MODIFIED, new NameRevision(newRevision));
            } else {
                NameView parentView = operation.getParentView();
                NamePart parent = parentView.isInStructure() ? artifact(parentView).asNamePart() : null;
                if (operation.getNameView().getRevisionPair().getNameStage().isPending()) {
                    processRevision(artifact, NameRevisionStatus.CANCELLED, "Automatically cancelled before modify");
                }
                String mnemonicEqClass = namingConvention.equivalenceClassRepresentative(el.getMnemonic());
                NamePartRevision namePartRevision =
                        new NamePartRevision(
                                artifact.asNamePart(), new Date(), sessionService.user(), operation.getMessage(),
                                false, parent, el.getFullName(), el.getMnemonic(),
                                el.getDescription(), mnemonicEqClass);
                em.persist(namePartRevision);
                add("proposed to be modified", new NameRevision(namePartRevision));

                String message;
                //state will change, have to Approve by admin
                if (operation.getNameView().getRevisionPair().getNameStage().isApproved()) {
                    message = "Modified -  Needs to be approved";
                    //state will not change, just notify admins
                } else {
                    message = "Modified";
                }

                // notify users
                addNotificationSystemStructureApproval(
                        new MailNotificationDTO(
                                namingHelper.parentListForNode(namePartRevision, NamePartType.SECTION),
                                message,
                                operation.getMessage()));
                addNotificationDeviceStructureApproval(
                        new MailNotificationDTO(
                                namingHelper.parentListForNode(namePartRevision, NamePartType.DEVICE_TYPE),
                                message,
                                operation.getMessage()));
            }
        }

        /**
         * Executes approve of a set of proposed names and if necessary updates and deletes affected deviceNames.
         *
         * @param operation approve data
         * @throws ValidationException
         */
        private void execute(Approve operation) throws ValidationException {
            validate(operation);
            for (NameView originalNameView : operation.getAffectedNameViews()) {
                if (originalNameView.isInStructure()) {
                    NameView nameView = detachedNameViews.addCloneOf(originalNameView);
                    NameArtifact artifact = artifact(nameView);
                    NamePartRevisionDTO originalStatus =
                            processRevision(artifact, NameRevisionStatus.APPROVED, operation.getMessage());

                    String operationSubject;
                    //NamePartRevision was marked to be deleted, and deleting was approved by an admin
                    if(originalStatus.isDeleted()) {
                        operationSubject = "Deleted - Approved";
                    } else {
                    //NamePartRevision was pending (not deleted), and was approved by an admin
                        //was modified
                        if(originalStatus.isWasApproved()) {
                            operationSubject = "Modified - Approved";
                        } else {
                            //was created
                            operationSubject = "Added - Approved";

                            // after name in system structure approved --> auto add ESS name
                            //     AddDevice operation
                            //     to set up parents for AddDevice operation,
                            //     (UUID, name view) key value map in NameViewProvider.NameView is used
                            //     which is updated after transaction has been committed
                            //     -->
                            //     AddDevice to be done after mentioned commit
                            //     data from approval kept and later used for AddDevice and related transaction
                            //     each system name revision --> AddDevice operation
                            //     --
                            //     note.
                            //         system structure
                            //         auto add ESS name when mnemonic available
                            //             (e.g. not auto add for top level system structure without mnemonic)

                            if (NameType.SYSTEM_STRUCTURE.equals(artifact.getNameType())
                                    && !StringUtils.isEmpty(originalStatus.getNameRevision().getNameElement().getMnemonic())) {
                                systemNameRevisionsForAddDevice.add(originalStatus.getNameRevision());
                            }
                        }
                    }

                    // send mail notification to admins
                    notifyAdminsFromChange(artifact, operationSubject, operation.getMessage());
                } else {
                    throw new UnsupportedOperationException();
                }
            }

            for (NameView originalDeviceView : operation.getAffectedDevices()) {
                NameView deviceView = detachedNameViews.addCloneOf(originalDeviceView);
                NameView systemParentView = deviceView.getParent(NameType.SYSTEM_STRUCTURE);
                NameView deviceParentView = deviceView.getParent(NameType.DEVICE_STRUCTURE);
                NameArtifact artifact = artifact(deviceView);
                NameRevision baseRevision = deviceView.getRevisionPair().getBaseRevision();
                NameElement el = baseRevision.getNameElement();
                String conventionName = conventionName(deviceView);
                NamePart systemParent = baseRevision.getParent(NameType.SYSTEM_STRUCTURE).asNamePart();
                NamePart deviceParent = baseRevision.getParent(NameType.DEVICE_STRUCTURE) != null
                        ? baseRevision.getParent(NameType.DEVICE_STRUCTURE).asNamePart()
                        : null;
                boolean deleted = deviceParentView != null
                        ? deviceParentView.getRevisionPair().getNameStage().isArchived()
                                || systemParentView.getRevisionPair().getNameStage().isArchived()
                        : systemParentView.getRevisionPair().getNameStage().isArchived();
                if (deleted) {
                    String mnemonicEqClass = namingConvention.equivalenceClassRepresentative(el.getFullName());
                    final DeviceRevision deviceRevision =
                            new DeviceRevision(
                                    artifact.asDevice(), new Date(), sessionService.user(), true,
                                    systemParent, deviceParent,
                                    el.getMnemonic(), el.getFullName(), mnemonicEqClass, el.getDescription());
                    em.persist(deviceRevision);
                    add(DELETED, new NameRevision(deviceRevision));
                } else if (!el.getFullName().equals(conventionName)) {
                    String mnemonicEqClass = namingConvention.equivalenceClassRepresentative(conventionName);
                    final DeviceRevision deviceRevision =
                            new DeviceRevision(
                                    artifact.asDevice(), new Date(), sessionService.user(), false,
                                    systemParent, deviceParent,
                                    el.getMnemonic(), conventionName, mnemonicEqClass, el.getDescription());
                    em.persist(deviceRevision);
                    add(MODIFIED, new NameRevision(deviceRevision));
                } else {
                    throw new ValidationException("the device is not affected");
                }
            }
        }

        /**
         * Checks and corrects corrupted devices names
         *
         * @param operation
         * @throws ValidationException
         */
        private void execute(CheckDevices operation) throws ValidationException {
            validate(operation);
            for (NameView originalDeviceView : operation.getAffectedDevices()) {
                NameView deviceView = detachedNameViews.addCloneOf(originalDeviceView);
                NameView systemParentView = deviceView.getParent(NameType.SYSTEM_STRUCTURE);
                NameView deviceParentView = deviceView.getParent(NameType.DEVICE_STRUCTURE);
                NameArtifact artifact = artifact(deviceView);
                NameRevision baseRevision = deviceView.getRevisionPair().getBaseRevision();
                NameElement el = baseRevision.getNameElement();
                String conventionName = conventionName(deviceView);
                NamePart systemParent = baseRevision.getParent(NameType.SYSTEM_STRUCTURE).asNamePart();
                NamePart deviceParent = baseRevision.getParent(NameType.DEVICE_STRUCTURE) != null
                        ? baseRevision.getParent(NameType.DEVICE_STRUCTURE).asNamePart()
                        : null;
                boolean parentDeleted = deviceParentView != null
                        ? deviceParentView.getRevisionPair().getNameStage().isArchived()
                                || systemParentView.getRevisionPair().getNameStage().isArchived()
                        : systemParentView.getRevisionPair().getNameStage().isArchived();
                if (parentDeleted) {
                    String mnemonicEqClass = namingConvention.equivalenceClassRepresentative(el.getFullName());
                    final DeviceRevision deviceRevision =
                            new DeviceRevision(
                                    artifact.asDevice(), new Date(), sessionService.user(), true,
                                    systemParent, deviceParent,
                                    el.getMnemonic(), el.getFullName(), mnemonicEqClass, el.getDescription());
                    em.persist(deviceRevision);
                    add(DELETED, new NameRevision(deviceRevision));
                } else if (!el.getFullName().equals(conventionName)) {
                    String mnemonicEqClass = namingConvention.equivalenceClassRepresentative(conventionName);
                    final DeviceRevision deviceRevision = new DeviceRevision(
                            artifact.asDevice(), new Date(), sessionService.user(), false,
                            systemParent, deviceParent,
                            el.getMnemonic(), conventionName, mnemonicEqClass, el.getDescription());
                    em.persist(deviceRevision);
                    add(MODIFIED, new NameRevision(deviceRevision));
                }
            }

        }

        /**
         * excecutes cancel of a set of names
         *
         * @param operation cancel data
         * @throws ValidationException
         */
        private void execute(Cancel operation) throws ValidationException {
            validate(operation);
            for (NameView originalNameView : operation.getAffectedNameViews()) {
                if (originalNameView.isInStructure()) {
                    NameView nameView = detachedNameViews.addCloneOf(originalNameView);
                    NameArtifact artifact = artifact(nameView);
                    processRevision(artifact, NameRevisionStatus.CANCELLED, operation.getMessage());

                    // notify admin users about changes
                    notifyAdminsFromChange(artifact, "Cancelled", operation.getMessage());
                } else {
                    throw new UnsupportedOperationException();
                }
            }
        }

        /**
         * executes rejection of a set of names
         *
         * @param operation reject data
         * @throws ValidationException
         */
        private void execute(Reject operation) throws ValidationException {
            validate(operation);
            for (NameView originalNameView : operation.getAffectedNameViews()) {
                if (originalNameView.isInStructure()) {
                    NameView nameView = detachedNameViews.addCloneOf(originalNameView);
                    NameArtifact artifact = artifact(nameView);
                    processRevision(artifact, NameRevisionStatus.REJECTED, operation.getMessage());

                    // sending mail notification to admins
                    notifyAdminsFromChange(artifact, "Rejected", operation.getMessage());
                } else {
                    throw new UnsupportedOperationException();
                }
            }
        }

        /**
         *
         * @param artifact the artifact of the device or name part
         * @param status the new status
         * @param message the successMessage with the cause for processing this name.
         *
         * @return the old state of NamePartRevision before the operation was executed on it
         * @throws ValidationException
         */
        private NamePartRevisionDTO processRevision(
                NameArtifact artifact, NameRevisionStatus status, String message) throws ValidationException {

            NamePartRevisionStatus namePartStatus = NamePartRevisionStatus.get(status);
            As.validateState(!namePartStatus.equals(NamePartRevisionStatus.PENDING), "Processed status is Pending");
            As.validateState(!artifact.getNameType().isDeviceRegistry(), "Name is in registry and cannot be processed");
            NamePart namePart = artifact.asNamePart();
            NamePartRevision namePartRevision =
                    em.createQuery(
                            SELECT_R_FROM_NAME_PART_REVISION_R_WHERE_R_ID_EQUALS
                                    + "(SELECT MAX(r2.id) FROM NamePartRevision r2 WHERE r2.namePart = :namePart ) ",
                            NamePartRevision.class)
                        .setParameter(NAME_PART, namePart)
                        .getSingleResult();
            As.validateState(
                    namePartRevision.getStatus().equals(NamePartRevisionStatus.PENDING),
                    "Name is not pending and cannot be processed");
            As.validateState(
                    message == null || !message.isEmpty(),
                    "message cannot be empty");
            em.detach(namePartRevision);

            List<NamePartRevision> approvedNamePart = em.createQuery("SELECT r FROM NamePartRevision  r WHERE " +
                                "r.namePart = :namePart AND r.status = :pStatus",
                        NamePartRevision.class)
                        .setParameter(NAME_PART, namePart)
                        .setParameter("pStatus", NamePartRevisionStatus.APPROVED)
                        .getResultList();

            NamePartRevisionDTO result = new NamePartRevisionDTO();
            result.setDeleted(namePartRevision.isDeleted());
            result.setOldStatus(namePartRevision.getStatus());
            result.setWasApproved((approvedNamePart != null) && (!approvedNamePart.isEmpty()));

            namePartRevision.setStatus(namePartStatus);
            namePartRevision.setProcessDate(new Date());
            namePartRevision.setProcessedBy(sessionService.user());
            namePartRevision.setProcessorComment(message);
            NamePartRevision attachedRevision = em.merge(namePartRevision);
            NameRevision revision = new NameRevision(attachedRevision);
            add(revision.getStatus().toString().toLowerCase(), revision);
            result.setNameRevision(revision);

            return result;
        }

    }

    /**
     * Searches entry by Artifact, and notifies admin users with Operator message/parameter,
     * and sets the mail with a default subject
     *
     * @param artifact the artifact that has to be searched (go to root element, and list nodes)
     * @param operator the operation that has been done on the element
     */
    private void notifyAdminsFromChange(NameArtifact artifact, String operator, String commitMessage) {
        notifyAdminsFromChange(artifact, operator, DEFAULT_MAIL_SUBJECT, commitMessage);
    }

    /**
     * Searches entry by Artifact, and notifies admin users with Operator message/parameter with desired mail subject
     *
     * @param artifact the artifact that has to be searched (go to root element, and list nodes)
     * @param operator the operation that has been done on the element
     * @param subject the subject of the email
     */
    private void notifyAdminsFromChange(NameArtifact artifact, String operator, String subject, String commitMessage) {
        String requester = null;

        // get info for sending mail notification
        if (NameType.DEVICE_REGISTRY.equals(artifact.getNameType())) {
            // get device info, device registry
            DeviceRevision deviceRevision =
                    em.createQuery(
                            "SELECT r FROM DeviceRevision r WHERE r.id = "
                                    + "(SELECT MAX(r2.id) FROM DeviceRevision r2 WHERE r2.device = :device ) ",
                            DeviceRevision.class)
                            .setParameter(DEVICE, artifact.asDevice())
                            .getSingleResult();

            List<String> names = new ArrayList<>();
            names.add(deviceRevision.getConventionName());

        } else {
            // get name info, system structure or device structure
            NamePartRevision namePartRevision =
                    em.createQuery(
                            SELECT_R_FROM_NAME_PART_REVISION_R_WHERE_R_ID_EQUALS
                                    + "(SELECT MAX(r2.id) FROM NamePartRevision r2 WHERE r2.namePart = :namePart ) ",
                            NamePartRevision.class)
                            .setParameter(NAME_PART, artifact.asNamePart())
                            .getSingleResult();

            requester = namePartRevision.getRequestedBy().getUsername();

            MailNotificationDTO mailNotificationSystemStructure =
                    NameType.SYSTEM_STRUCTURE.equals(artifact.getNameType())
                        ? new MailNotificationDTO(
                                namingHelper.parentListForNode(namePartRevision, NamePartType.SECTION),
                                operator,
                                commitMessage)
                        : null;
            MailNotificationDTO mailNotificationDeviceStructure =
                    NameType.DEVICE_STRUCTURE.equals(artifact.getNameType())
                        ? new MailNotificationDTO(
                                namingHelper.parentListForNode(namePartRevision, NamePartType.DEVICE_TYPE),
                                operator,
                                commitMessage)
                        : null;

            if (mailNotificationSystemStructure != null) {
                addNotificationSystemStructureNonApproval(mailNotificationSystemStructure);
                addCCNotificationSystemStructure(requester, mailNotificationSystemStructure);
            }
            if (mailNotificationDeviceStructure != null) {
                addNotificationDeviceStructureNonApproval(mailNotificationDeviceStructure);
                addCCNotificationDeviceStructure(requester, mailNotificationDeviceStructure);
            }
        }
    }

    /**
     * Clear (mail) notifications.
     */
    private void clearNotifications() {
        // clear mail notifications
        //     admin approval
        //     admin non approval
        //     cc

        modificationSystemStructureApproval.clear();
        modificationDeviceStructureApproval.clear();
        modificationSystemStructureNonApproval.clear();
        modificationDeviceStructureNonApproval.clear();
        ccModificationSystemStructure.clear();
        ccModificationDeviceStructure.clear();
    }

    /**
     * Add (mail) notification for system structure (approval notification, admin).
     *
     * @param e notification
     */
    private void addNotificationSystemStructureApproval(MailNotificationDTO e) {
        modificationSystemStructureApproval.add(e);
    }

    /**
     * Add (mail) notification for device structure (approval notification, admin).
     *
     * @param e notification
     */
    private void addNotificationDeviceStructureApproval(MailNotificationDTO e) {
        modificationDeviceStructureApproval.add(e);
    }

    /**
     * Add (mail) notification for system structure (non-approval notification, admin).
     *
     * @param e notification
     */
    private void addNotificationSystemStructureNonApproval(MailNotificationDTO e) {
        modificationSystemStructureNonApproval.add(e);
    }

    /**
     * Add (mail) notification for device structure (non-approval notification, admin).
     *
     * @param e notification
     */
    private void addNotificationDeviceStructureNonApproval(MailNotificationDTO e) {
        modificationDeviceStructureNonApproval.add(e);
    }

    /**
     * Add (mail) notification for system structure (notification, non-admin).
     *
     * @param user user for cc
     * @param e notification
     */
    private void addCCNotificationSystemStructure(String user, MailNotificationDTO e) {
        List<MailNotificationDTO> ccList = ccModificationSystemStructure.get(user);
        if (ccList == null) {
            ccList = new ArrayList<>();
        }

        ccList.add(e);
        ccModificationSystemStructure.put(user, ccList);
    }

    /**
     * Add (mail) notification for device structure (notification, non-admin).
     *
     * @param user user for cc
     * @param e notification
     */
    private void addCCNotificationDeviceStructure(String user, MailNotificationDTO e) {
        List<MailNotificationDTO> ccList = ccModificationDeviceStructure.get(user);
        if (ccList == null) {
            ccList = new ArrayList<>();
        }

        ccList.add(e);
        ccModificationDeviceStructure.put(user, ccList);
    }

    /**
     * Send (mail) notifications.
     */
    private void sendNotifications() {
        // send mail notificationss
        //     sort and send
        //     sort according to nodelist in MailNotificationDTO
        //     cc
        //         sessionService.user.getUsername (requester if approve, cancel, reject, delete
        //     subject
        //         [Changes - Needs to be approved]
        //         [Changes - Notification]
        //     send if conditions apply - content to send, non-admin cc

        // sort
        Collections.sort(modificationSystemStructureApproval, comparatorMailNotificationDTO);
        Collections.sort(modificationDeviceStructureApproval, comparatorMailNotificationDTO);
        Collections.sort(modificationSystemStructureNonApproval, comparatorMailNotificationDTO);
        Collections.sort(modificationDeviceStructureNonApproval, comparatorMailNotificationDTO);

        // send mail notification admin
        if (!modificationSystemStructureApproval.isEmpty() || !modificationDeviceStructureApproval.isEmpty()) {
            notificationService.notifyUserFromChange(
                    modificationSystemStructureApproval,
                    modificationDeviceStructureApproval,
                    null,
                    CHANGES_NEEDS_TO_BE_APPROVED);
        }

        // send mail notification cc admin
        if (!modificationSystemStructureNonApproval.isEmpty() || !modificationDeviceStructureNonApproval.isEmpty()) {
            notificationService.notifyUserFromChange(
                    modificationSystemStructureNonApproval,
                    modificationDeviceStructureNonApproval,
                    null,
                    CHANGES_NOTIFICATION);
        }

        // send mail notification cc system structure if single non-admin cc
        for (Map.Entry<String, List<MailNotificationDTO>> entry : ccModificationSystemStructure.entrySet()) {
            Collections.sort(entry.getValue(), comparatorMailNotificationDTO);
            if (!entry.getValue().isEmpty()) {
                notificationService.notifyCCFromChange(
                        entry.getValue(),
                        EMPTY_MODIFICATIONS,
                        Arrays.asList(entry.getKey()),
                        CHANGES_NOTIFICATION);
            }
        }

        // send mail notification cc device structure if single non-admin cc
        for (Map.Entry<String, List<MailNotificationDTO>> entry : ccModificationDeviceStructure.entrySet()) {
            Collections.sort(entry.getValue(), comparatorMailNotificationDTO);
            if (!entry.getValue().isEmpty()) {
                notificationService.notifyCCFromChange(
                        EMPTY_MODIFICATIONS,
                        entry.getValue(),
                        Arrays.asList(entry.getKey()),
                        CHANGES_NOTIFICATION);
            }
        }
    }

}
