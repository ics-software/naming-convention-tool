/*-
 * Copyright (c) 2014 European Spallation Source ERIC.
 * Copyright (c) 2014 Cosylab d.d.
 *
 * This file is part of Naming Service.
 * Naming Service is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */

package org.openepics.names.services;

import javax.annotation.Nullable;
import javax.ejb.Stateless;
import javax.enterprise.inject.Alternative;
import javax.inject.Inject;

import org.apache.commons.lang3.StringUtils;
import org.openepics.names.business.NameElement;
import org.openepics.names.business.NameType;
import org.openepics.names.util.NamingConventionUtility;

import java.util.List;

/**
 * A naming convention definition used by ESS.
 *
 * @author Marko Kolar
 * @author Karin Rathsman
 * @author Lars Johansson
 *
 * @see NamingConvention
 * @see NamingConventionUtility
 */
@Alternative
@Stateless
public class EssNamingConvention implements NamingConvention {

    // ----------------------------------------------------------------------------------------------------
    // rules for characters
    //     empty
    //     space
    //     value
    //     alphanumeric
    //     letter case
    //     length
    //     equivalenceClassRepresentative
    // note
    //     mnemonic can be empty
    //         system structure - system group
    //         device structure - device group
    // note
    //     mnemonic for system group may be part of ess name
    //     mnemonic for instance index may be omitted for ess name
    // ----------------------------------------------------------------------------------------------------

    // Note revision history of file in repository.

    private static final String MNEMONIC_ALPHABETIC_LOWERCASE = "^[a-z]+$";
    private static final String MNEMONIC_ALPHANUMERIC         = "^[a-zA-Z0-9]+$";
    private static final String MNEMONIC_NUMERIC              = "^[0-9]+$";
    private static final String MNEMONIC_NUMERIC_ZERO         = "^[0]+$";

    @Inject
    NamingConventionDefinition aliasManager;

    @Override
    public boolean isMnemonicRequired(List<String> mnemonicPath, NameType nameType) {
        return (new MnemonicElement(mnemonicPath, nameType)).isRequired();
    }

    @Override
    public MnemonicValidation validateMnemonic(List<String> mnemonicPath, NameType nameType) {
        // mnemonic validation for system and device structure elements
        // rules for characters
        //     depends on system/device structure, required or not
        //     alphanumeric - MNEMONIC_ALPHANUMERIC
        //     length
        //         system structure
        //             system group  - 0-6 characters, not required
        //             system        - 1-8 characters
        //             sub system    - 1-8 characters
        //         device structure
        //             discipline    - 1-6 characters
        //             device group  - not required
        //             device type   - 1-6 characters
        //
        // validation for last element in structure / path
        // accelerator as system
        //     system group as system, not required

        MnemonicElement nameElement = new MnemonicElement(mnemonicPath, nameType);
        String mnemonic = nameElement.getMnemonic();

        boolean empty = StringUtils.isEmpty(mnemonic);
        int length = empty ? 0 : mnemonic.length();
        boolean length1to6 = (length >= 1) && (length <= 6);
        boolean length1to8 = (length >= 1) && (length <= 8);
        boolean matchMnemonic = !empty && mnemonic.matches(MNEMONIC_ALPHANUMERIC);

        MnemonicValidation result = MnemonicValidation.VALID;

        if (nameElement.systemStructure) {
            // system group not required
            if (nameElement.isSystemGroup()) {
                if (empty) {
                    result = MnemonicValidation.VALID;
                } else if (!length1to6) {
                    result = MnemonicValidation.TOO_LONG;
                } else if (!matchMnemonic) {
                    result = MnemonicValidation.NON_ACCEPTABLE_CHARS;
                }
            } else if (nameElement.isSystem()) {
                if (empty) {
                    result = MnemonicValidation.EMPTY;
                } else if (!length1to8) {
                    result = MnemonicValidation.TOO_LONG;
                } else if (!matchMnemonic) {
                    result = MnemonicValidation.NON_ACCEPTABLE_CHARS;
                }
            } else if (nameElement.isSubsystem()) {
                if (empty) {
                    result = MnemonicValidation.EMPTY;
                } else if (!length1to8) {
                    result = MnemonicValidation.TOO_LONG;
                } else if (!matchMnemonic) {
                    result = MnemonicValidation.NON_ACCEPTABLE_CHARS;
                }
            }
        } else if (nameElement.deviceStructure) {
            // device group not required
            if (nameElement.isDiscipline() || nameElement.isDeviceType()) {
                if (empty) {
                    result = MnemonicValidation.EMPTY;
                } else if (!length1to6) {
                    result = MnemonicValidation.TOO_LONG;
                } else if (!matchMnemonic) {
                    result = MnemonicValidation.NON_ACCEPTABLE_CHARS;
                }
            } else if (nameElement.isDeviceGroup()) {
                if (!empty && !matchMnemonic) {
                    result = MnemonicValidation.NON_ACCEPTABLE_CHARS;
                }
            }
        }
        return result;
    }

    @Override
    public boolean isInstanceIndexValid(
            List<String> systemPath, List<String> deviceTypePath, @Nullable String instanceIndex) {

        // not override ruleset for instanceIndex
        return isInstanceIndexValid(systemPath, deviceTypePath, instanceIndex, false);
    }

    @Override
    public boolean isInstanceIndexValid(
            List<String> systemPath, List<String> deviceTypePath, @Nullable String instanceIndex,
            boolean overrideRuleset) {

        if (overrideRuleset) {
            // previous rules, less restrictions

            if (instanceIndex != null && instanceIndex.length() <= 6) {
                return instanceIndex.matches(MNEMONIC_ALPHANUMERIC);
            } else {
                return false;
            }
        } else {
            // normal path

            // p&id numeric
            // p&id
            // scientific
            if (isNamePID(deviceTypePath)) {
                if (isPIDNumeric(deviceTypePath)) {
                    // 3 numeric
                    return isNameValid(instanceIndex, 3, 3);
                }
                // 3 numeric & 0-3 alphabetic
                return isNameValidPID(instanceIndex);
            } else {
                // 1-4 numeric & non-zero
                return isNameValid(instanceIndex, 1, 4);
            }
        }
    }

    private boolean isPIDNumeric (List<String> deviceTypePath) {
        if (deviceTypePath != null && !deviceTypePath.isEmpty()) {
            for (String s : NamingConventionUtility.getMnemonicPathsPIDNumeric()) {
                if (StringUtils.equals(s, NamingConventionUtility.mnemonicPath2String(deviceTypePath))) {
                    return true;
                }
            }
        }
        return false;
    }

    private boolean isNamePID (List<String> deviceTypePath) {
        if (deviceTypePath != null && !deviceTypePath.isEmpty()) {
            String discipline = deviceTypePath.get(0);
            for (String s : NamingConventionUtility.getDisciplinesPID()) {
                if (s.equals(discipline)) {
                    return true;
                }
            }
        }
        return false;
    }

    private boolean isNameValidPID(String name) {
        if (name != null && name.length() >= 3 && name.length() <= 6) {
            if (name.length() == 3) {
                return name.matches(MNEMONIC_NUMERIC)
                        && !name.matches(MNEMONIC_NUMERIC_ZERO);
            } else {
                return name.substring(0, 3).matches(MNEMONIC_NUMERIC)
                        && name.substring(3, name.length()).matches(MNEMONIC_ALPHABETIC_LOWERCASE);
            }
        }
        return false;
    }

    private boolean isNameValid(String name, int nMin, int nMax) {
        if ((name == null || name.length() == 0)) {
            return nMin <= 0;
        } else {
            return (name.length() >= nMin && name.length() <= nMax)
                    && name.matches(MNEMONIC_NUMERIC)
                    && !name.matches(MNEMONIC_NUMERIC_ZERO);
        }
    }

    @Override
    public boolean canMnemonicsCoexist(
            List<String> mnemonicPath1, NameType nameType1,
            List<String> mnemonicPath2, NameType nameType2) {

        MnemonicElement mnemonic1 = new MnemonicElement(mnemonicPath1, nameType1);
        MnemonicElement mnemonic2 = new MnemonicElement(mnemonicPath2, nameType2);
        return mnemonic1.canCoexistWith(mnemonic2);
    }

    @Override
    public boolean canNamePartMove(
            List<String> mnemonicPathSource, NameType nameTypeSource,
            List<String> mnemonicPathDestination, NameType nameTypeDestination) {

        return (new MnemonicElement(mnemonicPathSource, nameTypeSource)
                .canMoveTo(new MnemonicElement(mnemonicPathDestination, nameTypeDestination)));
    }

    @Override
    public String equivalenceClassRepresentative(@Nullable String name) {
        return name != null
                ? name.toUpperCase()
                        .replaceAll("(?<=[A-Za-z])0+", "")
                        .replace('I', '1').replace('L', '1').replace('O', '0')
                        .replaceAll("(?<!\\d)0+(?=\\d)", "")
                : null;
    }

    @Override
    public String conventionKey(List<String> mnemonicPath, NameType nameType) {
        return equivalenceClassRepresentative(conventionName(mnemonicPath, nameType));
    }

    @Override
    public String conventionName(List<String> mnemonicPath, NameType nameType) {
        return (new MnemonicElement(mnemonicPath, nameType).conventionName());
    }

    @Override
    public String conventionName(
            List<String> systemPath, List<String> deviceTypePath, @Nullable String instanceIndex) {

        String systemConventionName = (new MnemonicElement(systemPath, NameType.SYSTEM_STRUCTURE)).conventionName();
        String deviceConventionName = (new MnemonicElement(deviceTypePath, NameType.DEVICE_STRUCTURE)).conventionName();
        if (systemConventionName != null) {
            if (deviceConventionName != null && !StringUtils.isEmpty(instanceIndex)) {
                return systemConventionName + ":" + deviceConventionName + "-" + instanceIndex;
            } else if (deviceConventionName != null) {
                return systemConventionName + ":" + deviceConventionName;
            } else {
                return systemConventionName;
            }
        }
        return null;
    }

    @Override
    public String getNamePartTypeName(List<String> mnemonicPath, @Nullable NameType nameType) {
        return nameType != null
                ? (new MnemonicElement(mnemonicPath, nameType)).getTypeName()
                : null;
    }

    @Override
    public String getNamePartTypeMnemonic(List<String> mnemonicPath, @Nullable NameType nameType) {
        return nameType != null
                ? (new MnemonicElement(mnemonicPath, nameType)).getTypeMnemonic()
                : null;
    }

    @Override
    public NameElement getNameElementDefinition(List<String> mnemonicPath, NameType nameType) {
        return nameType != null && (nameType.isSystemStructure() || nameType.isDeviceStructure())
                ? (new MnemonicElement(mnemonicPath, nameType)).getNameDefinition()
                : null;
    }

    /**
     * This class describes an element in the name structure (system, device), more precisely a mnemonic element.
     * <p>
     * Each structure may consist of one, two or three levels.
     * <ul>
     * <li>System structure
     * <ul>
     * <li>System group
     * <li>System
     * <li>Subsystem
     * </ul>
     * <li>Device structure
     * <ul>
     * <li>Discipline
     * <li>Device Group
     * <li>Device Type
     * </ul>
     * </ul>
     * <p>
     * Note
     * <ul>
     * <li><tt>MnemonicElement</tt> may represent element at any level in either name structure, i.e. it may or may not
     * describe a complete path, i.e. lowest level in either name structure
     * <li><tt>MnemonicElement</tt> may or may not have valid content
     * </ul>
     * <p>
     * A key part of class is ability to determine if element can coexist with another element.
     *
     * @see MnemonicElement#canCoexistWith(MnemonicElement)
     */
    private class MnemonicElement {

        List<String> path;
        boolean systemStructure;
        boolean deviceStructure;
        Integer level;

        /**
         * Constructor.
         *
         * @param path      list of mnemonics starting from the root of the hierarchy that describes the name structure
         * @param type      type specifying whether mnemonic belongs to the system structure or the device structure
         */
        MnemonicElement(List<String> path, NameType type) {
            this.path = path;
            this.systemStructure = type.equals(NameType.SYSTEM_STRUCTURE);
            this.deviceStructure = type.equals(NameType.DEVICE_STRUCTURE);
            this.level = !(path == null || path.isEmpty()) ? path.size() : 0;
        }

        /**
         * Return if the name part can be moved from this mnemonic element to the given name element.
         *
         * @param nameElement name element
         * @return if the name part can be moved from this mnemonic element to the given name element
         */
        public boolean canMoveTo(MnemonicElement nameElement) {
            return (this.getTypeMnemonic().equals(nameElement.getTypeMnemonic()));
        }

        /**
         * @return convention name for mnemonic element
         */
        String conventionName() {
            // no handling of offsite
            if (isDeviceType()) {
                if (!getDiscipline().isEmpty() && !getDeviceType().isEmpty()) {
                    return getDiscipline() + "-" + getDeviceType();
                } else {
                    return null;
                }
            } else if (isSubsystem()) {
                if (!getSystem().isEmpty() && !getSubsystem().isEmpty()) {
                    return getSystem() + "-" + getSubsystem();
                } else if (!getSystem().isEmpty()) {
                    return getSystem();
                } else if (!getSystemGroup().isEmpty()) {
                    return getSystemGroup();
                } else {
                    return null;
                }
            } else if (isSystem()) {
                if (!getSystem().isEmpty()) {
                    return getSystem();
                } else if (!getSystemGroup().isEmpty()) {
                    return getSystemGroup();
                } else {
                    return null;
                }
            } else if (isSystemGroup()) {
                if (!getSystemGroup().isEmpty()) {
                    return getSystemGroup();
                } else {
                    return null;
                }
            } else {
                return null;
            }
        }

        NameElement getNameDefinition() {
            if (deviceStructure) {
                return aliasManager.deviceStructureLevel(level);
            } else if (systemStructure) {
                return aliasManager.systemStructureLevel(level);
            } else {
                return null;
            }
        }

        String getTypeName() {
            return getNameDefinition() != null ? getNameDefinition().getFullName() : "";
        }

        String getTypeMnemonic() {
            return getNameDefinition() != null ? getNameDefinition().getMnemonic() : "";
        }

        boolean isSystemGroup() {
            return systemStructure && level == 1;
        }

        boolean isSystem() {
            return systemStructure && level == 2;
        }

        boolean isSubsystem() {
            return systemStructure && level == 3;
        }

        boolean isDiscipline() {
            return deviceStructure && level == 1;
        }

        boolean isDeviceGroup() {
            return deviceStructure && level == 2;
        }

        boolean isDeviceType() {
            return deviceStructure && level == 3;
        }

        boolean isRequired() {
            return !(isSystemGroup() || isDeviceGroup());
        }

        boolean isOffsite() {
            return systemStructure
                    && !getSystemGroup().isEmpty()
                    && !getSystem().isEmpty()
                    && !getSubsystem().isEmpty();
        }

        String getSystemGroup() {
            return systemStructure && !path.isEmpty() ? path.get(0) : null;
        }

        String getSystem() {
            return systemStructure && path.size() > 1 ? path.get(1) : null;
        }

        String getSubsystem() {
            return systemStructure && path.size() > 2 ? path.get(2) : null;
        }

        String getDiscipline() {
            return deviceStructure && !path.isEmpty() ? path.get(0) : null;
        }

        String getDeviceType() {
            return deviceStructure && path.size() > 2 ? path.get(2) : null;
        }

        String getMnemonic() {
            return level > 0 ? path.get(level - 1) : null;
        }

        /**
         * Return if this mnemonic element can coexist with other mnemonic element.
         *
         * @param other     other mnemonic element to compare with
         * @return          if this mnemonic element can coexist with other mnemonic element
         */
        boolean canCoexistWith(MnemonicElement other) {
            // ----------------------------------------------------------------------------------------------------
            // method answers question
            //     'if mnemonic element has been added successfully,
            //      would it be possible to add other mnemonic element?'
            // note
            //     not consider validity of MnemonicElement
            // ----------------------------------------------------------------------------------------------------
            // not consider system structure vs device structure
            // unique within structure in structure (system / device)
            //     unique within parent child relation in structure (system / device)
            //     mnemonics cannot coexist in a parent child relation
            //         system - system group, system, subsystem
            //         device - discipline, device type
            // rules for characters
            //     equivalenceClassRepresentative
            // note
            //     mnemonic can be empty
            //         system structure - system group
            //         device structure - device group
            //     mnemonic for system group may be part of ess name
            //     mnemonic for instance index may be omitted for ess name
            //     device group not part of ess name
            // ----------------------------------------------------------------------------------------------------
            // unique within parent child relation in structure
            // ----------------------------------------------------------------------------------------------------

            if (systemStructure && other.systemStructure) {
                // note
                //     check system group, system, subsystem

                String systemGroup = equivalenceClassRepresentative(getSystemGroup());
                String system = equivalenceClassRepresentative(getSystem());
                String subsystem = equivalenceClassRepresentative(getSubsystem());
                String otherSystemGroup = equivalenceClassRepresentative(other.getSystemGroup());
                String otherSystem = equivalenceClassRepresentative(other.getSystem());
                String otherSubsystem = equivalenceClassRepresentative(other.getSubsystem());

                boolean systemGroupEmpty = StringUtils.isEmpty(systemGroup);
                int levelDifference = level - other.level;

                // check
                //     equality
                //     unique within parent child relation in structure

                if (levelDifference == 0
                        && StringUtils.equalsIgnoreCase(systemGroup, otherSystemGroup)
                        && StringUtils.equalsIgnoreCase(system, otherSystem)
                        && StringUtils.equalsIgnoreCase(subsystem, otherSubsystem)) {
                    return level == 1 && systemGroupEmpty;
                } else if (
                        (StringUtils.equalsIgnoreCase(systemGroup,      system)         && system         != null)
                     || (StringUtils.equalsIgnoreCase(systemGroup,      subsystem)      && subsystem      != null)
                     || (StringUtils.equalsIgnoreCase(system,           subsystem)      && subsystem      != null)
                     || (StringUtils.equalsIgnoreCase(otherSystemGroup, otherSystem)    && otherSystem    != null)
                     || (StringUtils.equalsIgnoreCase(otherSystemGroup, otherSubsystem) && otherSubsystem != null)
                     || (StringUtils.equalsIgnoreCase(otherSystem,      otherSubsystem) && otherSubsystem != null)) {
                    return false;
                }
            } else if (deviceStructure && other.deviceStructure) {
                // note
                //     check discipline, device type
                //     not check device group - not part of device name

                String discipline = equivalenceClassRepresentative(getDiscipline());
                String deviceType = equivalenceClassRepresentative(getDeviceType());
                String otherDiscipline = equivalenceClassRepresentative(other.getDiscipline());
                String otherDeviceType = equivalenceClassRepresentative(other.getDeviceType());

                int levelDifference = level - other.level;

                // check
                //     equality
                //     unique within parent child relation in structure

                if (levelDifference == 0
                        && StringUtils.equalsIgnoreCase(discipline, otherDiscipline)
                        && StringUtils.equalsIgnoreCase(deviceType, otherDeviceType)) {
                    return level == 2;
                } else if (
                        (StringUtils.equalsIgnoreCase(discipline,      deviceType)      && deviceType      != null)
                     || (StringUtils.equalsIgnoreCase(otherDiscipline, otherDeviceType) && otherDeviceType != null)) {
                    return false;
                }
            }
            return true;
        }

    }

}
