/*-
 * Copyright (c) 2014 European Spallation Source ERIC.
 * Copyright (c) 2014 Cosylab d.d.
 *
 * This file is part of Naming Service.
 * Naming Service is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */

package org.openepics.names.services;

import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;

import org.openepics.names.business.NameElement;
import org.openepics.names.business.NameType;

/**
 * Definitions used in GUIs headers
 *
 * @author karinrathsman
 *
 */
@ManagedBean
@ApplicationScoped
public class NamingConventionDefinition {

    private final NameElement systemGroup =
            new NameElement(
                    "System Group",
                    "",
                    "High-level system group of the facility restricted to a particular use. "
                            + "Used for filtering and sorting purposes. "
                            + "Part of convention name for Accelerator as System names only.");
    private final NameElement system =
            new NameElement("System", "Sys", "High-level system in the ESS facility");
    private final NameElement subsystem =
            new NameElement("Subsystem", "Sub", "Medium-level system in the ESS facility");
    private final NameElement discipline =
            new NameElement(
                    "Discipline",
                    "Dis",
                    "Branch of knowledge indicating the context in which a device is used");
    private final NameElement deviceGroup =
            new NameElement(
                    "Device Group",
                    "",
                    "Introduced to allow certain device types to be grouped together in lists. "
                            + "Not part of the convention names. Default is Miscellaneous");
    private final NameElement deviceType =
            new NameElement("Device Type", "Dev", "Two devices of the same (generic) type provide the same function");
    private final NameElement device =
            new NameElement("ESS Name", "Sys-Sub:Dis-Dev-Idx", "Name of a single instance of a device");

    /**
     * Return the name definition for the level and name type.
     *
     * @param level the level
     * @param nameType the name type
     * @return the name definition for the level and name type
     */
    public NameElement type(int level, NameType nameType) {
        return nameType.isSystemStructure() ? systemStructureLevel(level) : deviceStructureLevel(level);
    }

    /**
     * Return the system structure definition for the level.
     *
     * @param level the level
     * @return the system structure definition for the level
     */
    public NameElement systemStructureLevel(int level) {
        switch (level) {
            case 1:
                return getSystemGroup();
            case 2:
                return getSystem();
            case 3:
                return getSubsystem();
            case 4:
                return getDevice();
            default:
                return null;
        }
    }

    /**
     * Return the device structure definition for the level.
     *
     * @param level the level
     * @return the device structure definition for the level
     */
    public NameElement deviceStructureLevel(int level) {
        switch (level) {
            case 1:
                return getDiscipline();
            case 2:
                return getDeviceGroup();
            case 3:
                return getDeviceType();
            case 4:
                return getDevice();
            default:
                return null;
        }
    }

    /**
     * @return the systemGroup
     */
    public NameElement getSystemGroup() {
        return systemGroup;
    }

    /**
     * @return the system
     */
    public NameElement getSystem() {
        return system;
    }

    /**
     * @return the subsystem
     */
    public NameElement getSubsystem() {
        return subsystem;
    }

    /**
     * @return the discipline
     */
    public NameElement getDiscipline() {
        return discipline;
    }

    /**
     * @return the deviceGroup
     */
    public NameElement getDeviceGroup() {
        return deviceGroup;
    }

    /**
     * @return the deviceType
     */
    public NameElement getDeviceType() {
        return deviceType;
    }

    /**
     * @return the device
     */
    public NameElement getDevice() {
        return device;
    }
}
