/*
 * Copyright (C) 2020 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.dto;

import org.openepics.names.business.NameRevision;
import org.openepics.names.model.NamePartRevisionStatus;

/**
 * This utility class stores (part of) state about NamePartRevision and associated NameRevision
 * after executing an operation on NamePartRevision.
 *
 * @author Imre Toth <imre.toth@ess.eu>
 */
public class NamePartRevisionDTO {

    // designates if the NamePartRevision was deleted
    private boolean isDeleted;
    // designates the old status of the NamePartRevision
    private NamePartRevisionStatus oldStatus;
    // designates that the NamePartRevision was approved before
    private boolean wasApproved;
    // associated name revision
    private NameRevision nameRevision;

    public boolean isDeleted() {
        return isDeleted;
    }

    public void setDeleted(boolean deleted) {
        isDeleted = deleted;
    }

    public NamePartRevisionStatus getOldStatus() {
        return oldStatus;
    }

    public void setOldStatus(NamePartRevisionStatus oldStatus) {
        this.oldStatus = oldStatus;
    }

    public boolean isWasApproved() {
        return wasApproved;
    }

    public void setWasApproved(boolean wasApproved) {
        this.wasApproved = wasApproved;
    }

    public NameRevision getNameRevision() {
        return nameRevision;
    }

    public void setNameRevision(NameRevision nameRevision) {
        this.nameRevision = nameRevision;
    }

}
