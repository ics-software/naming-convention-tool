/*
 * Copyright (C) 2020 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.dto;

import org.openepics.names.model.DeviceRevision;

import java.util.Date;

/**
 * Class to contain information about DeviceRevisions to send batch-notification email to admins
 *
 * @author Imre Toth <imre.toth@ess.eu>
 */
public class DeviceNotificationDTO {

    private Long id;
    private String essName;
    private String oldName;
    private Date operationDate;
    private String user;
    private String commitMessage;

    /**
     * Initializes fields from a DeviceRevision. Device old name is not known
     *
     * @param device initializes fields from the DeviceRevision parameter
     */
    public DeviceNotificationDTO(DeviceRevision device) {
        this.id = device.getId();
        this.essName = device.getConventionName();
        this.operationDate = device.getRequestDate();
        this.commitMessage = device.getProcessorComment();

        if(device.getRequestedBy() != null) {
            this.user = device.getRequestedBy().getUsername();
        }
    }

    /**
     * Initializes fields from a DeviceRevision, and the previous name of the device
     * @param device initializes fields from the DeviceRevision parameter
     * @param oldName the device previous name
     */
    public DeviceNotificationDTO(DeviceRevision device, String oldName) {
        this(device);
        this.oldName = oldName;
    }

    public Long getId() {
        return id;
    }

    public String getEssName() {
        return essName;
    }

    public String getOldName() {
        return oldName;
    }

    public Date getOperationDate() {
        return operationDate;
    }

    public String getUser() {
        return user;
    }

    public String getCommitMessage() {
        return commitMessage;
    }
}
