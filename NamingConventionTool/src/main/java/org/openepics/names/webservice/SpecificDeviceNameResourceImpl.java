/*-
 * Copyright (c) 2014 European Spallation Source ERIC.
 * Copyright (c) 2014 Cosylab d.d.
 *
 * This file is part of Naming Service.
 * Naming Service is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */

package org.openepics.names.webservice;

import java.util.Map;
import java.util.UUID;

import javax.annotation.Nullable;
import javax.ejb.Stateless;
import javax.inject.Inject;

import org.apache.commons.lang3.StringUtils;
import org.openepics.names.business.NameRevision;
import org.openepics.names.jaxb.DeviceNameElement;
import org.openepics.names.jaxb.DeviceNamesResource;
import org.openepics.names.jaxb.SpecificDeviceNameResource;
import org.openepics.names.nameviews.DeviceRecordView;
import org.openepics.names.nameviews.NameView;
import org.openepics.names.nameviews.NameViewProvider;

/**
 * This is implementation of {@link SpecificDeviceNameResource} interface.
 *
 * @author Andraz Pozar
 * @author Banafsheh Hajinasab
 * @author Lars Johansson
 *
 * @see DeviceNamesResource
 * @see SpecificDeviceNameResource
 */
@Stateless
public class SpecificDeviceNameResourceImpl implements SpecificDeviceNameResource {

    protected static final String ACTIVE   = "ACTIVE";
    protected static final String DELETED  = "DELETED";
    protected static final String OBSOLETE = "OBSOLETE";

    @Inject
    private NameViewProvider nameViewProvider;

    /**
     * @see org.openepics.names.jaxb.SpecificDeviceNameResource#getDeviceName(String)
     */
    @Override
    public @Nullable
    DeviceNameElement getDeviceName(String string) {
        // jaxb
        // ----------
        // given uuid or name, find name revision, return data transfer object
        //     representing Devices for JSON and XML serialization
        // search for uuid to return current (most recent) information (or none)
        // search for name to return information (or none)
        // ----------
        // note
        //     uuid or name
        //     not name equivalence
        //     ----------
        //     exact match
        //     case sensitive

        if (StringUtils.isEmpty(StringUtils.trimToEmpty(string))) {
            return null;
        }

        // pool of name revisions is map<equivalence class representative, name revision>
        //     a certain uuid may exist multiple times in map, with different names / equivalence class representatives
        //     a certain name may exist only once in map since same name / equivalence class representative
        //     ----------
        // search for uuid to return current (most recent) information (or none)
        // search for name to return information (or none)

        // uuid or name
        NameRevision revision = null;
        for (Map.Entry<String, NameRevision> entry : nameViewProvider.getNameRevisions().entrySet()) {
            // uuid
            if (StringUtils.equals(entry.getValue().getNameArtifact().getUuid().toString(), string)) {
                if (revision == null || entry.getValue().getId() > revision.getId()) {
                    revision = entry.getValue();
                }
                continue;
            }
            // name
            if (StringUtils.equals(entry.getValue().getNameElement().getFullName(), string) ) {
                revision = entry.getValue();
                break;
            }
        }

        if (revision == null) {
            return null;
        }
        return getDeviceNameElement(revision.getNameElement().getFullName());
    }

    /**
     * @see org.openepics.names.jaxb.SpecificDeviceNameResource#getDeviceNameElement(String)
     */
    @Override
    public DeviceNameElement getDeviceNameElement(String string) {
        // given uuid or name, retrieve name revision, return data transfer object
        //     representing Devices for JSON and XML serialization
        // ----------
        // note
        //     uuid or name or name equivalence
        //     ----------
        //     exact match
        //     case sensitive   (uuid or name)
        //     case insensitive (name equivalence)

        NameRevision revision = nameViewProvider.getNameRevisions().get(string);
        if (revision == null) {
            return null;
        } else {
            final DeviceNameElement deviceData = new DeviceNameElement();
            UUID uuid = revision.getNameArtifact().getUuid();
            String name = revision.getNameElement().getFullName();
            NameView nameView = nameViewProvider.nameView(uuid);
            NameRevision currentRevision = nameView.getRevisionPair().getApprovedRevision();
            boolean deleted = currentRevision.isDeleted();
            boolean active = !deleted && currentRevision.equals(revision);
            String status = deleted ? DELETED : active ? ACTIVE : OBSOLETE;

            deviceData.setUuid(uuid);
            deviceData.setName(name);
            deviceData.setStatus(status);

            if (active) {
                DeviceRecordView record = new DeviceRecordView(nameView);
                deviceData.setInstanceIndex(revision.getNameElement().getMnemonic());
                deviceData.setDescription(revision.getNameElement().getDescription());
                String systemGroup = mnemonic(record.getSystemGroup());
                if (systemGroup != null) {
                    deviceData.setSystemGroup(systemGroup);
                }
                deviceData.setSystem(mnemonic(record.getSystem()));
                deviceData.setSubsystem(mnemonic(record.getSubsystem()));
                deviceData.setDiscipline(mnemonic(record.getDiscipline()));
                deviceData.setDeviceType(mnemonic(record.getDeviceType()));
            }
            return deviceData;
        }
    }

    // ----------------------------------------------------------------------------------------------------

    private static String mnemonic(NameView nameView) {
        return nameView != null ? nameView.getRevisionPair().getBaseRevision().getNameElement().getMnemonic() : null;
    }

}
