/*
 * Copyright (c) 2020 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.webservice;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

import javax.ejb.Stateless;
import javax.inject.Inject;

import org.apache.commons.lang3.StringUtils;
import org.openepics.names.business.NameRevision;
import org.openepics.names.business.NameRevisionPair;
import org.openepics.names.business.NameStage;
import org.openepics.names.business.NameType;
import org.openepics.names.jaxb.PartElement;
import org.openepics.names.jaxb.PartsResource;
import org.openepics.names.nameviews.NameView;
import org.openepics.names.nameviews.NameViewProvider;
import org.openepics.names.util.NamingConventionUtility;

import com.google.common.collect.Lists;

/**
 * This is implementation of {@link PartsResource} interface.
 *
 * @author Lars Johansson
 */
@Stateless
public class PartsResourceImpl implements PartsResource {

    private static final Logger LOGGER = Logger.getLogger(PartsResourceImpl.class.getName());

    private static final String APPROVED  = "Approved";
    private static final String ARCHIVED  = "Archived";
    private static final String CANCELLED = "Cancelled";
    private static final String PENDING   = "Pending";
    private static final String REJECTED  = "Rejected";

    private static final String EMPTY_STRING   = "";

    @Inject
    private NameViewProvider nameViewProvider;

    /**
     * @see org.openepics.names.jaxb.PartsResource#getAllPartsByMnemonic(String)
     */
    @Override
    public List<PartElement> getAllPartsByMnemonic(String mnemonic) {
        // note
        //     exact match
        //     case sensitive

        // find matching NameView items in system structure and device structure hierarchies (various levels)
        // go through found items to produce return structure

        List<NameView> parts = Lists.newArrayList();
        parts.addAll(
                findNameViewsByMnemonic(
                        nameViewProvider.getNameViews().getSystemRoot().getAllChildren(NameType.SYSTEM_STRUCTURE),
                        mnemonic));
        parts.addAll(
                findNameViewsByMnemonic(
                        nameViewProvider.getNameViews().getDeviceRoot().getAllChildren(NameType.DEVICE_STRUCTURE),
                        mnemonic));

        return getPartElements(parts);
    }

    /**
     * @see org.openepics.names.jaxb.PartsResource#getAllPartsByMnemonicSearch(java.lang.String)
     */
    @Override
    public List<PartElement> getAllPartsByMnemonicSearch(String mnemonic) {
        // note
        //     search
        //     case sensitive
        //     regex

        List<NameView> parts = Lists.newArrayList();
        try {
            Pattern pattern = Pattern.compile(mnemonic);

            // find matching NameView items in system structure and device structure hierarchies (various levels)
            // go through found items to produce return structure

            parts.addAll(
                    findNameViewsByMnemonicPattern(
                            nameViewProvider.getNameViews().getSystemRoot().getAllChildren(NameType.SYSTEM_STRUCTURE),
                            pattern));
            parts.addAll(
                    findNameViewsByMnemonicPattern(
                            nameViewProvider.getNameViews().getDeviceRoot().getAllChildren(NameType.DEVICE_STRUCTURE),
                            pattern));
        } catch (PatternSyntaxException e) {
            LOGGER.log(Level.FINE, e.getMessage(), e);
        }
        return getPartElements(parts);
    }

    /**
     * see {@link org.openepics.names.jaxb.PartsResource#getAllPartsByMnemonicPathSearch(String)}
     */
    @Override
    public List<PartElement> getAllPartsByMnemonicPathSearch(String mnemonicPath) {
        // note
        //     search
        //     case sensitive
        //     regex

        List<NameView> parts = Lists.newArrayList();
        try {
            Pattern pattern = Pattern.compile(mnemonicPath);

            // find matching NameView items in system structure and device structure hierarchies (various levels)
            // go through found items to produce return structure

            parts.addAll(
                    findNameViewsByMnemonicPathPattern(
                            nameViewProvider.getNameViews().getSystemRoot().getAllChildren(NameType.SYSTEM_STRUCTURE),
                            pattern));
            parts.addAll(
                    findNameViewsByMnemonicPathPattern(
                            nameViewProvider.getNameViews().getDeviceRoot().getAllChildren(NameType.DEVICE_STRUCTURE),
                            pattern));
        } catch (PatternSyntaxException e) {
            LOGGER.log(Level.FINE, e.getMessage(), e);
        }
        return getPartElements(parts);
    }

    // ----------------------------------------------------------------------------------------------------

    /**
     * Utility method to find name views (name parts) for which mnemonic matches given mnemonic (exact match).
     * The matching is case sensitive!
     *
     * @param nameViews name views
     * @param mnemonic mnemonic
     * @return name views that matches given mnemonic (exact match)
     */
    private List<NameView> findNameViewsByMnemonic(List<NameView> nameViews, String mnemonic) {
        String value = null;
        List<NameView> parts = Lists.newArrayList();
        for (NameView nameView : nameViews) {
            value = StringUtils.trimToEmpty(
                    nameView.getRevisionPair().getBaseRevision().getNameElement().getMnemonic());
            if (StringUtils.equals(mnemonic, value)) {
                parts.add(nameView);
            }
        }
        return parts;
    }

    /**
     * Utility method to find name views (name parts) for which mnemonic matches given pattern (search).
     * The matching is case sensitive!
     *
     * @param nameViews name views
     * @param pattern search pattern
     * @return name views that matches given mnemonic pattern (search)
     */
    private List<NameView> findNameViewsByMnemonicPattern(List<NameView> nameViews, Pattern pattern) {
        String value = null;
        List<NameView> parts = Lists.newArrayList();
        for (NameView nameView : nameViews) {
            value = StringUtils.trimToEmpty(
                        nameView.getRevisionPair().getBaseRevision().getNameElement().getMnemonic());
            if (pattern.matcher(value).find()) {
                parts.add(nameView);
            }
        }
        return parts;
    }

    /**
     * Utility method to find name views (name parts) for which mnemonic path matches given pattern (search).
     * The matching is case sensitive!
     *
     * @param nameViews name views
     * @param pattern search pattern
     * @return name views that matches given mnemonic path pattern (search)
     */
    private List<NameView> findNameViewsByMnemonicPathPattern(List<NameView> nameViews, Pattern pattern) {
        String value = null;
        List<NameView> parts = Lists.newArrayList();
        for (NameView nameView : nameViews) {
            value = NamingConventionUtility.mnemonicPath2String(nameView.getMnemonicPath());
            if (pattern.matcher(value).find()) {
                parts.add(nameView);
            }
        }
        return parts;
    }

    /**
     * Utility method to convert list of name views (name parts) to list of part elements.
     *
     * <p>
     * Method to return list of part elements, i.e. list of elements with information about mnemonic and usage,
     * given a list of name views (name parts).
     *
     * @param parts name view objects
     * @return list of part elements
     */
    private List<PartElement> getPartElements(List<NameView> parts) {
        final List<PartElement> partElements = Lists.newArrayList();
        for (NameView nameView : parts) {
            PartElement partElement = new PartElement();

            partElement.setType(nameView.getNameType().getName());
            partElement.setUuid(nameView.getRevisionPair().getBaseRevision().getNameArtifact().getUuid());
            partElement.setName(nameView.getRevisionPair().getBaseRevision().getNameElement().getFullName());
            partElement.setMnemonic(nameView.getRevisionPair().getBaseRevision().getNameElement().getMnemonic());
            partElement.setDescription(nameView.getRevisionPair().getBaseRevision().getNameElement().getDescription());
            partElement.setLevel(String.valueOf(nameView.getLevel()));
            partElement.setStatus(nameRevisionPairToStatus(nameView.getRevisionPair()));

            // StringUtils.join below of name path and mnemonic path may give string that
            //     - starts with and/or ends with path separator
            //     - contains multiple path separators in a row
            // string to be adjusted so that
            //     - not starts or ends with path separator
            //     - not contains multiple path separators in a row

            partElement.setNamePath(NamingConventionUtility.mnemonicPath2String(nameView.getFullnamePath()));
            partElement.setMnemonicPath(NamingConventionUtility.mnemonicPath2String(nameView.getMnemonicPath()));

            partElements.add(partElement);
        }
        return partElements;
    }

    /**
     * Utility method to return status of revision pair in similar/same manner as what is shown in UI.
     *
     * @param revisionPair revision pair
     * @return status
     *
     * @see org.openepics.names.business.RowData#newRowData(org.openepics.names.business.NameRevisionPair, boolean, boolean)
     * @see org.openepics.names.business.RowData#RowData(org.openepics.names.business.NameElement, org.openepics.names.business.NameElement, org.openepics.names.business.UserAction, boolean, boolean, boolean, boolean, boolean, boolean)
     */
    private String nameRevisionPairToStatus(NameRevisionPair revisionPair) {
        // see RowData, newRowData and constructor

        boolean includePending = true;
        boolean includeCancelled = true;

        NameStage stage = revisionPair.getNameStage();
        NameRevision unapprovedRevision = revisionPair.getUnapprovedRevision();

        boolean cancelled = stage.isCancelled() && includeCancelled;
        boolean pending = stage.isPending() && includePending;
        boolean rejected = stage.isCancelled() && unapprovedRevision.getStatus().isRejected();

        boolean approved = stage.isApproved();
        boolean archived = stage.isArchived();
        rejected = cancelled && rejected;

        return archived
                ? ARCHIVED
                : pending
                    ? PENDING
                    : approved
                        ? APPROVED
                        : rejected
                            ? REJECTED
                            : cancelled
                                ? CANCELLED
                                : EMPTY_STRING;
    }

}
