/*
 * Copyright (C) 2021 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.webservice;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ejb.Stateless;
import javax.inject.Inject;

import org.apache.commons.lang3.StringUtils;
import org.openepics.names.business.RowData;
import org.openepics.names.business.UserAction;
import org.openepics.names.jaxb.HistoryElement;
import org.openepics.names.jaxb.HistoryResource;
import org.openepics.names.nameviews.NameView;
import org.openepics.names.nameviews.NameViewProvider;

import com.google.common.collect.Lists;

/**
 * This is implementation of {@link HistoryResource} interface.
 *
 * @author Lars Johansson
 */
@Stateless
public class HistoryResourceImpl implements HistoryResource {

    private static final Logger LOGGER = Logger.getLogger(HistoryResourceImpl.class.getName());

    private static final SimpleDateFormat SDF = new SimpleDateFormat("yyyy-MM-dd");

    @Inject
    private NameViewProvider nameViewProvider;

    @Override
    public List<HistoryElement> getPartHistoryForUuid(String uuid) {
        // check parameters
        // prepare retrieval of information
        // retrieve information
        // prepare return elements

        boolean hasUuid = !StringUtils.isEmpty(uuid);

        final List<HistoryElement> historyElements = Lists.newArrayList();
        if (!hasUuid) {
            return historyElements;
        }

        UUID uuid2 = null;
        try {
            uuid2 = UUID.fromString(uuid);
        } catch (IllegalArgumentException e) {
            LOGGER.log(Level.FINE, e.getMessage(), e);
            return historyElements;
        }

        // retrieve information
        //     System Structure or Device Structure
        //     ask NameViewProvider for revisions with search parameters

        NameView nameView = nameViewProvider.getNameViews().get(uuid2);
        if (nameView == null || !nameView.isInStructure()) {
            return historyElements;
        }
        List<RowData> historyRevisionsAsRowData = nameViewProvider.getHistoryRevisionsAsRowData(nameView);

        for (RowData rowData : historyRevisionsAsRowData) {
            UserAction userAction = rowData.getUserAction();
            String date = userAction != null && userAction.getDate() != null
                    ? SDF.format(rowData.getUserAction().getDate())
                    : "";
            String user = userAction != null && userAction.getUser() != null
                    ? rowData.getUserAction().getUser().toString()
                    : "";
            String message = userAction != null
                    ? rowData.getUserAction().getMessage()
                    : "";

            HistoryElement historyElement = new HistoryElement(
                    uuid2,
                    rowData.getFullName().toString(),
                    rowData.getMnemonic().toString(),
                    rowData.getDescription().toString(),
                    rowData.getStatus(),
                    date,
                    user,
                    message);

            historyElements.add(historyElement);
        }

        return historyElements;
    }

    // ----------------------------------------------------------------------------------------------------

    @Override
    public List<HistoryElement> getNameHistoryForUuid(String uuid) {
        // check parameters
        // prepare retrieval of information
        // retrieve information
        // prepare return elements

        boolean hasUuid = !StringUtils.isEmpty(uuid);

        final List<HistoryElement> historyElements = Lists.newArrayList();
        if (!hasUuid) {
            return historyElements;
        }

        UUID uuid2 = null;
        try {
            uuid2 = UUID.fromString(uuid);
        } catch (IllegalArgumentException e) {
            LOGGER.log(Level.FINE, e.getMessage(), e);
            return historyElements;
        }

        // retrieve information
        //     Device Registry
        //     ask NameViewProvider for revisions with search parameters

        NameView nameView = nameViewProvider.getNameViews().get(uuid2);
        if (nameView == null || !nameView.isInDeviceRegistry()) {
            return historyElements;
        }
        List<RowData> historyRevisionsAsRowData = nameViewProvider.getHistoryRevisionsAsRowData(nameView);

        for (RowData rowData : historyRevisionsAsRowData) {
            UserAction userAction = rowData.getUserAction();
            String date = userAction != null && userAction.getDate() != null
                    ? SDF.format(rowData.getUserAction().getDate())
                    : "";
            String user = userAction != null && userAction.getUser() != null
                    ? rowData.getUserAction().getUser().toString()
                    : "";
            String message = userAction != null
                    ? rowData.getUserAction().getMessage()
                    : "";

            HistoryElement historyElement = new HistoryElement(
                    uuid2,
                    rowData.getFullName().toString(),
                    rowData.getMnemonic().toString(),
                    rowData.getDescription().toString(),
                    rowData.getStatus(),
                    date,
                    user,
                    message);

            historyElements.add(historyElement);
        }

        return historyElements;
    }

}
