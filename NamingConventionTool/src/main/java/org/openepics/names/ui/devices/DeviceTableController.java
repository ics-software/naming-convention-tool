/*
 * Copyright (c) 2015 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.ui.devices;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.util.*;

import javax.annotation.Nullable;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import org.openepics.names.business.NameType;
import org.openepics.names.business.NodeStatus;
import org.openepics.names.business.RowData;
import org.openepics.names.nameviews.TransactionResult;
import org.openepics.names.operation.AddDevice;
import org.openepics.names.operation.DeleteDevices;
import org.openepics.names.operation.ModifyDevice;
import org.openepics.names.operation.NameOperation;
import org.openepics.names.operation.SingleNameOperation;
import org.openepics.names.ui.common.TreeNodeManager;
import org.openepics.names.ui.common.UIException;
import org.openepics.names.ui.export.ExcelExport;
import org.openepics.names.util.As;
import org.openepics.names.util.CookieUtility;
import org.openepics.names.util.EncodingUtility;
import org.openepics.names.util.NamingConventionUtility;
import org.openepics.names.util.UiUtility;
import org.primefaces.PrimeFaces;
import org.primefaces.component.commandbutton.CommandButton;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.event.FlowEvent;
import org.primefaces.event.NodeSelectEvent;
import org.primefaces.event.NodeUnselectEvent;
import org.primefaces.event.ToggleEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import org.primefaces.model.TreeNode;
import org.primefaces.model.Visibility;

import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import com.google.common.io.ByteStreams;
import org.openepics.names.business.NameFilter;
import org.openepics.names.business.NameFilter.Options;
import org.openepics.names.nameviews.DeviceRecordView;
import org.openepics.names.nameviews.NameView;
import org.openepics.names.nameviews.NameViewProvider;
import org.openepics.names.nameviews.TransactionBean;
import org.openepics.names.business.NameRevision;
import org.openepics.names.services.SessionService;
import org.openepics.names.util.ValidationException;

/**
 * A UI controller bean for ESS Name Registry screen.
 */
@ManagedBean
@ViewScoped
public class DeviceTableController implements Serializable {

    private static final String CONTENT_TYPE_OPENXML =
            "application/application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
    private static final String VALIDATION_ERROR = "Validation Error";
    private static final String VAR_A_PRIME_FACES_COOKIES_ENABLED_IF_A_PRIME_FACES_SET_COOKIE =
            "var a=PrimeFaces.cookiesEnabled(); if(a){PrimeFaces.setCookie('";

    private static final long serialVersionUID = -262176781057441889L;

    // initial number of entities per page
    private static final int NUMBER_OF_ENTITIES_PER_PAGE = 30;

    @Inject
    private ExcelImport excelImport;
    @Inject
    private TreeNodeManager treeNodeManager;
    @Inject
    private NameViewProvider nameViewProvider;
    @Inject
    private ExcelExport excelExport;
    @Inject
    private TransactionBean transactionBean;
    @Inject
    private SessionService sessionService;

    private byte[] importData;
    private String importFileName;
    private List<DeviceRecordView> records;
    private List<DeviceRecordView> filteredRecords;
    private List<DeviceRecordView> selectedRecords;
    private NameOperation nameOperation;
    private List<RowData> historyRevisionsAsRowData;
    private Wizard wizard;
    private FilterWizard filterWizard;
    private boolean importIsModify;

    // datatable
    //     number of view options
    //     view option visibility
    //     number of columns
    //     column visibility
    //     number of rows/entries per page in pagination component
    //     row number, if applicable, for requested entry in list of all entries
    private int numberOfViewOptions;
    private List<Boolean> viewOptionVisibility;
    private int numberOfColumns;
    private List<Boolean> columnVisibility;
    private int rows;
    private int rowNumber;
    private String deviceParamName;

    /**
     * Constructs the controller bean and handles one time setup.
     */
    public DeviceTableController() {
        // initialize datatable
        //     number of view options
        //         according to namefilter
        //     view option visibility
        //         view options initialized as default options in namefilter, may be updated
        //     number of columns
        //         according to devices.xhtml
        //     column visibility
        //         all columns initialized as visible, may be updated
        //     rows per page
        numberOfViewOptions = 4;
        viewOptionVisibility = new ArrayList<>();
        viewOptionVisibility.add(Boolean.TRUE);
        viewOptionVisibility.add(Boolean.FALSE);
        viewOptionVisibility.add(Boolean.TRUE);
        viewOptionVisibility.add(Boolean.FALSE);
        numberOfColumns = 8;
        columnVisibility = new ArrayList<>();
        for (int i=0; i<numberOfColumns; i++) {
            columnVisibility.add(Boolean.TRUE);
        }
        rows = NUMBER_OF_ENTITIES_PER_PAGE;
    }

    /**
     * Initializes the bean.
     */
    @PostConstruct
    public void init() {
        try {
            final @Nullable

            // prepare datatable
            //     cookies
            //         rows per page
            //         view option visibility
            //         column visibility
            //     row number (in all rows) for requested entry (if any)
            Cookie[] cookies =
                    ((HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest())
                        .getCookies();
            if (cookies != null) {
                for (Cookie cookie : cookies) {
                    switch (cookie.getName()) {
                        case CookieUtility.NT_DEVICEREGISTRY_PAGINATION_PAGE_SIZE:
                            initPaginationPageSize(cookie.getValue());
                            break;
                        case CookieUtility.NT_DEVICEREGISTRY_VIEW_OPTION_VISIBILITY:
                            initViewOptionVisibility(cookie.getValue());
                            break;
                        case CookieUtility.NT_DEVICEREGISTRY_COLUMN_VISIBILITY:
                            initColumnVisibility(cookie.getValue());
                            break;
                        default:
                            break;
                    }
                }
            }

            importIsModify = false;

            // view visibility updated above
            update();

        } catch (Exception e) {
            throw new UIException("DeviceTableController initialization failed: " + e.getMessage(), e);
        }
    }

    /**
     * Init pagination page size from given value.
     *
     * @param pageSize page size to be interpreted
     */
    private void initPaginationPageSize(String pageSize) {
        // keep track of page size in variable
        if (!StringUtils.isEmpty(pageSize)) {
            int value = Integer.parseInt(pageSize);
            setRows(value);
        }
    }

    /**
     * Init view option visibility from given value.
     *
     * @param visibility view option visibility to be interpreted
     *
     * @see Options#ACTIVE
     * @see Options#ARCHIVED
     * @see Options#PENDING
     * @see Options#CANCELLED
     */
    private void initViewOptionVisibility(String visibility) {
        // keep track of view option visibility in list
        // note position in array/list for entries
        if (!StringUtils.isEmpty(visibility)) {
            visibility = EncodingUtility.decode(visibility);
            String[] split = visibility.split(CookieUtility.DELIMITER_ENTRIES);
            if (split.length == numberOfViewOptions) {
                List<Options> selectedOptions = new ArrayList<>();
                for (int i=0; i<numberOfViewOptions; i++) {
                    viewOptionVisibility.set(i, Boolean.valueOf(split[i]));
                    switch (i) {
                        case 0:
                            if (viewOptionVisibility.get(i))
                                selectedOptions.add(Options.ACTIVE);
                            break;
                        case 1:
                            if (viewOptionVisibility.get(i))
                                selectedOptions.add(Options.ARCHIVED);
                            break;
                        case 2:
                            if (viewOptionVisibility.get(i))
                                selectedOptions.add(Options.PENDING);
                            break;
                        case 3:
                            if (viewOptionVisibility.get(i))
                                selectedOptions.add(Options.CANCELLED);
                            break;
                        default:
                            break;
                    }
                }
                Options[] selectedOptionsArray = new Options[selectedOptions.size()];
                for (int j=0; j<selectedOptions.size(); j++) {
                    selectedOptionsArray[j] = selectedOptions.get(j);
                }
                getViewFilter().setSelectedOptions(selectedOptionsArray);
            }
        }
    }

    /**
     * Init column visibility from given value.
     *
     * @param visibility column visibility to be interpreted
     */
    private void initColumnVisibility(String visibility) {
        // keep track of column visibility in list
        if (!StringUtils.isEmpty(visibility)) {
            visibility = EncodingUtility.decode(visibility);
            String[] split = visibility.split(CookieUtility.DELIMITER_ENTRIES);
            if (split.length == numberOfColumns) {
                for (int i=0; i<numberOfColumns; i++) {
                    columnVisibility.set(i, Boolean.valueOf(split[i]));
                }
            }
        }
    }

    /**
     * update all data
     */
    public void update() {
        filterWizard = null;
        wizard = null;
        nameOperation = null;
        records = treeNodeManager.deviceRecordViews();

        updateFilteringIcon();
    }

    /**
     * @param deviceName the device name
     * @return the row number of the specified deviceName.
     */
    private int rowNumber(String deviceName) {
        if (deviceName != null) {
            NameRevision revision = nameViewProvider.getNameRevisions().get(deviceName);
            if(revision!=null){
                int n = 0;
                for (DeviceRecordView record : records) {
                    n++;
                    if (record.getDevice().getRevisionPair().getBaseRevision().equals(revision)) {
                        setSelectedRecords(Lists.newArrayList(record));
                        return n;
                    }
                }
            }
        }
        return 0;
    }

    /**
     * Return the name operation containing all data for the operation.
     *
     * @param actionString the string specifying the selected operation
     * @return the name operation containing all data for the operation
     */
    public NameOperation nameOperation(String actionString) {
        NameView rootView = getRoot();
        Set<NameView> selectedNameViews = selectedDevices();

        switch (actionString) {
            case "add":
                return new AddDevice(Sets.newHashSet(rootView), rootView);
            case "delete":
                return new DeleteDevices(selectedNameViews, rootView);
            case "modify":
                return new ModifyDevice(selectedNameViews, rootView);
            default:
                return null;
        }
    }

    /**
     *
     * @return the filtered records
     */
    public List<DeviceRecordView> getRecords() {
        return records;
    }

    /**
     * @return the single selected record, null otherwise
     */
    public DeviceRecordView getSelectedRecord() {
        return getSelectedRecords() != null && getSelectedRecords().size() == 1 ? getSelectedRecords().get(0) : null;
    }

    /**
     *
     * @return a list of selected device records
     */
    public @Nullable
    List<DeviceRecordView> getSelectedRecords() {
        return selectedRecords;
    }

    /**
     *
     * @return the selected devices
     */
    private Set<NameView> selectedDevices() {
        Set<NameView> devices = Sets.newHashSet();
        if (getSelectedRecords() != null) {
            for (DeviceRecordView record : getSelectedRecords()) {
                devices.add(record.getDevice());
            }
        }
        return devices;
    }

    /**
     * sets the selected records.
     *
     * @param selectedRecords the list of selected records.
     */
    public void setSelectedRecords(@Nullable List<DeviceRecordView> selectedRecords) {
        this.selectedRecords = selectedRecords;
    }

    /**
     * Return if the specified operation is allowed for the selection.
     *
     * @param operationString the string specifying the operation
     * @return true if the specified operation is allowed for the selection
     */
    public boolean can(String operationString) {
        try {
            nameOperation(operationString).validateOnSelect();
            return true;
        } catch (ValidationException e) {
            return "add".equals(operationString);
        }
    }

    /**
     * Return if the user is allowed to operate.
     *
     * @param actionString indicating the required operation
     * @return true if the user is allowed to operate
     */
    public boolean rendered(String actionString) {
        try {
            nameOperation(actionString).validateUser(sessionService.isEditor(), sessionService.isSuperUser());
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    /**
     *
     * @return true if the global filtering is possible.
     */
    public boolean canFilter() {
        return true;
        //TODO remove this...
    }

    /**
     *
     * @return true if the history for the device can be shown.
     */
    public boolean canShowHistory() {
        return getSelectedRecord() != null;
    }

    /**
     * Generates the history data for the selected device.
     */
    public void prepareHistoryPopup() {
        historyRevisionsAsRowData = nameViewProvider.getHistoryRevisionsAsRowData(getSelectedRecord().getDevice());
    }

    /**
     *
     * @return the history revisions as row data to be displayed on history popup
     */
    public List<RowData> getHistoryRevisionsAsRowData() {
        return historyRevisionsAsRowData;
    }

    /**
     * Event triggered when paging for data table.
     */
    public void onPaginate() {
        unselectAllRows();
    }

    /**
     * Event triggered when paging for data table is complete. Allows keeping track of pagination page size.
     */
    public void onPaginatePageSize() {
        // set pagination page size cookie
        String statement = VAR_A_PRIME_FACES_COOKIES_ENABLED_IF_A_PRIME_FACES_SET_COOKIE
                + CookieUtility.NT_DEVICEREGISTRY_PAGINATION_PAGE_SIZE
                + "', " + getRows()
                + ", {expires:" + CookieUtility.PERSISTENCE_DAYS + "});}";
        PrimeFaces.current().executeScript(statement);
    }

    /**
     * Unselect all rows and proceed accordingly, i.e. clear selected devices and row selection.
     */
    private void unselectAllRows() {
        setSelectedRecords(null);
    }

    /**
     * Event triggered when toggling view option visibility for data table.
     * Allows keeping track of view option visibility.
     */
    public void onUpdateViewFilter() {
        // keep track of view option visibility
        // set view option visibility cookie
        //     serialize view option visibility
        //     set cookie

        viewOptionVisibility.clear();
        for (int i=0; i<numberOfViewOptions; i++) {
            viewOptionVisibility.add(Boolean.FALSE);
        }
        Options[] selectedOptions = getViewFilter().getSelectedOptions();
        for (Options options : selectedOptions) {
            if (Options.ACTIVE.equals(options))
                viewOptionVisibility.set(0, Boolean.TRUE);
            if (Options.ARCHIVED.equals(options))
                viewOptionVisibility.set(1, Boolean.TRUE);
            if (Options.PENDING.equals(options))
                viewOptionVisibility.set(2, Boolean.TRUE);
            if (Options.CANCELLED.equals(options))
                viewOptionVisibility.set(3, Boolean.TRUE);
        }

        String statement = VAR_A_PRIME_FACES_COOKIES_ENABLED_IF_A_PRIME_FACES_SET_COOKIE
                + CookieUtility.NT_DEVICEREGISTRY_VIEW_OPTION_VISIBILITY
                + "', '" + viewOptionVisibility2String()
                + "', {expires:" + CookieUtility.PERSISTENCE_DAYS + "});}";
        PrimeFaces.current().executeScript(statement);
    }

    /**
     * Event triggered when toggling column visibility for data table. Allows keeping track of column visibility.
     *
     * @param event toggle event
     */
    public void onToggle(ToggleEvent event) {
        // keep track of column visibility
        // set column visibility cookie
        //     serialize column visibility
        //     set cookie

        if (((Integer) event.getData()) < numberOfColumns) {
            columnVisibility.set((Integer) event.getData(), event.getVisibility() == Visibility.VISIBLE);

            String statement = VAR_A_PRIME_FACES_COOKIES_ENABLED_IF_A_PRIME_FACES_SET_COOKIE
                    + CookieUtility.NT_DEVICEREGISTRY_COLUMN_VISIBILITY
                    + "', '" + columnVisibility2String()
                    + "', {expires:" + CookieUtility.PERSISTENCE_DAYS + "});}";
            PrimeFaces.current().executeScript(statement);
        }
    }

    /**
     * Serializes view option visibility into string consisting of delimiter-separated string values,
     * each <code>true</code> or <code>false</code>.
     *
     * @return
     */
    private String viewOptionVisibility2String() {
        StringBuilder sb = new StringBuilder();
        for (int i=0; i<numberOfViewOptions; i++) {
            sb.append(viewOptionVisibility.get(i));
            if (i < (numberOfViewOptions-1))
                sb.append(CookieUtility.DELIMITER_ENTRIES);
        }
        return sb.toString();
    }

    /**
     * Serializes column visibility into string consisting of delimiter-separated string values,
     * each <code>true</code> or <code>false</code>.
     *
     * @return
     */
    private String columnVisibility2String() {
        StringBuilder sb = new StringBuilder();
        for (int i=0; i<numberOfColumns; i++) {
            sb.append(columnVisibility.get(i));
            if (i < (numberOfColumns-1))
                sb.append(CookieUtility.DELIMITER_ENTRIES);
        }
        return sb.toString();
    }

    /**
     * Returns column visibility for column with given index.
     *
     * @param columnIndex column index
     * @return column visibility for column with given index
     */
    public boolean isColumnVisible(int columnIndex) {
        return columnVisibility.get(columnIndex);
    }

    /**
     * Submits the name operation and updates the view.
     *
     */
    public synchronized void onSubmit() {
        TransactionResult result = transactionBean.submit(nameOperation);
        update();
        UiUtility.showMessage(result);
    }

    /**
     *
     * @return true if the operation can be submitted
     */
    public boolean canSubmit() {
        if (nameOperation.isMessageRequired() && nameOperation.getMessage() == null) {
            UiUtility.showErrorMessage(VALIDATION_ERROR, "Please enter a message");
            return false;
        } else {
            return true;
        }
    }

    /**
     * Imports devices from excel
     */
    public void onImport() {
        try (InputStream inputStream = new ByteArrayInputStream(importData)) {
            excelImport.parseDeviceImportFile(inputStream, importIsModify);
            update();
        } catch (IOException e) {
            throw new RuntimeException();
        } finally {
            importIsModify = false;
        }
    }

    /**
     * initializes for import action
     *
     * @param action specify if the action is modify or add
     */
    public void prepareImportPopup(String action) {
        importData = null;
        importFileName = null;
        importIsModify = "modify".equals(action);
    }

    public boolean isImportModify() {
        return importIsModify;
    }

    /**
     * Handles the file upload event on import
     *
     * @param event the file upload event
     */
    public void handleFileUpload(FileUploadEvent event) {
        try (InputStream inputStream = event.getFile().getInputstream()) {
            this.importData = ByteStreams.toByteArray(inputStream);
            this.importFileName = FilenameUtils.getName(event.getFile().getFileName());
        } catch (IOException e) {
            throw new RuntimeException();
        }
    }

    /**
     *
     * @return the importFileName
     */
    public String getImportFileName() {
        return importFileName;
    }

    /**
     *
     * @return the Name template excel file
     */
    public StreamedContent getDownloadableNamesTemplate() {
        if (getFilteredRecords() != null) {

            return new DefaultStreamedContent(
                    excelImport.templateForImport(getFilteredRecords(), importIsModify),
                    CONTENT_TYPE_OPENXML,
                    "NamingImportTemplate.xlsx");
        } else {

            return new DefaultStreamedContent(
                    excelImport.templateForImport(records, importIsModify),
                    CONTENT_TYPE_OPENXML,
                    "NamingImportTemplate.xlsx");
        }
    }

    /**
     *
     * @return the data on export
     */
    public StreamedContent getAllDataExport() {
        TreeNode systemStructure = As.notNull(treeNodeManager.getStructureTree(null, NameType.SYSTEM_STRUCTURE));
        TreeNode deviceStructure = As.notNull(treeNodeManager.getStructureTree(null, NameType.DEVICE_STRUCTURE));
        if (getFilteredRecords() != null) {
            return new DefaultStreamedContent(
                    excelExport.exportFile(systemStructure, deviceStructure, getFilteredRecords()),
                    CONTENT_TYPE_OPENXML,
                    "NamingConventionExport.xlsx");
        } else {
            return new DefaultStreamedContent(
                    excelExport.exportFile(systemStructure, deviceStructure, records),
                    CONTENT_TYPE_OPENXML,
                    "NamingConventionExport.xlsx");
        }
    }

    /**
     *
     * @return the filtered records in table.
     */
    public List<DeviceRecordView> getFilteredRecords() {
        return filteredRecords;
    }

    /**
     * sets the filtered records in table
     *
     * @param filteredRecords the filtered records to set
     */
    public void setFilteredRecords(List<DeviceRecordView> filteredRecords) {
        this.filteredRecords = filteredRecords;
    }

    /**
     *
     * @return the n
     */
    public int getRowNumber() {
        return rowNumber;
    }

    /**
     *
     * @return the number of rows per page
     */
    public int getRows() {
        return rows;
    }

    /**
     * Sets the number of rows per page
     *
     * @param rows number of rows per page
     */
    public void setRows(int rows) {
        this.rows = rows;
    }

    /**
     * @return the nameOperation
     */
    public NameOperation getNameOperation() {
        return nameOperation;
    }

    public NameView getRoot() {
        return nameViewProvider.nameStructure(NameType.SYSTEM_STRUCTURE);
    }

    /**
     * @return the view filter used in tables and trees.
     */
    public NameFilter getViewFilter() {
        return treeNodeManager.getNameFilter();
    }

    /**
     * @return the wizard
     */
    public Wizard getWizard() {
        return wizard;
    }

    /**
     * prepare the operation dialog
     *
     * @param actionString string specifying the operation to be performed.
     */
    public void prepareOperationDialog(String actionString) {
        this.nameOperation = nameOperation(actionString);
        if (nameOperation instanceof SingleNameOperation) {
            this.wizard = new Wizard((SingleNameOperation) nameOperation);
        }
    }

    /**
     * sets the filter wizard
     */
    public void prepareFilter() {
        this.filterWizard = new FilterWizard();
    }

    /**
     *
     * @return the filtered wizard
     */
    public FilterWizard getFilterWizard() {
        return filterWizard;
    }

    /**
     * Gets the deviceName for URL parameter
     * @return the desired deviceName for URL parameter
     */
    public String getDeviceParamName() {
        return deviceParamName;
    }

    /**
     * Sets the deviceName from the URL parameter, and selects it in dataTable,
     * or deselect rows if device can not be shown
     *
     * @param deviceParamName name of the desired device
     */
    public void setDeviceParamName(String deviceParamName) {
        this.deviceParamName = deviceParamName;

        //there is no deviceName in URL parameter
        if (StringUtils.isEmpty(deviceParamName)) {
            return;
        }

        //calculate the rowNumber of the selected device
        rowNumber = rowNumber(deviceParamName);

        //if device can be selected (is in the filtered list)
        if (rowNumber != 0) {
            //calculate the pageNumber for the device
            int pageNumber = (rowNumber - 1) / rows;

            //switch to page
            PrimeFaces.current().executeScript("PF('deviceTable').paginator.setPage(" + pageNumber + ");");

            rowNumber = (rowNumber - 1) % rows;

            //call remoteCommand to select the row
            PrimeFaces.current().executeScript("selectRow();");

        } else {
            //call remoteCommand to un-select the row
            PrimeFaces.current().executeScript("unselectRows();");
        }
    }

    /**
     * Class for the device data wizard to add and modify a single device name.
     *
     * @author karinrathsman
     *
     */
    public class Wizard {

        private TreeNode selectedRootParent;
        private TreeNode selectedOtherParent;
        private TreeNode rootTree;
        private TreeNode otherTree;
        private final List<String> tabs =
                Lists.newArrayList("rootParentTab", "otherParentTab", "nameElementTab", "finishTab");
        private SingleNameOperation singleNameOperation;
        private String header;

        /**
         * Constructor
         *
         * @param nameOperation contains the data need to add or modify a device name.
         */
        public Wizard(SingleNameOperation nameOperation) {
            this.singleNameOperation = nameOperation;
            Preconditions.checkState(singleNameOperation != null, "Single Name Operation is null");
            this.rootTree =
                    tree(singleNameOperation != null ? singleNameOperation.rootType() : NameType.SYSTEM_STRUCTURE);
            this.otherTree =
                    tree(singleNameOperation != null ? singleNameOperation.otherType() : NameType.SYSTEM_STRUCTURE);
            this.selectedRootParent = selectedParent(rootTree);
            this.selectedOtherParent = selectedParent(otherTree);
            this.header = nameOperation != null ? nameOperation.getTitle() + " ESS Name" : "";
        }

        /**
         * @param tree is the root of the system or device structure tree.
         * @return the selected parent node in the specified tree as given by the nameOperation.
         */
        private TreeNode selectedParent(TreeNode tree) {
            NameType nameType = TreeNodeManager.nameView(tree).getNameType();
            TreeNode selectedParent =
                    singleNameOperation != null ?
                            treeNodeManager.nodeOf(tree, this.singleNameOperation.getParentView(nameType)) : null;
            if (selectedParent != null) {
                selectedParent.setSelected(true);
                treeNodeManager.expandParents(selectedParent);
            }
            return selectedParent;
        }

        /**
         * @param nameType
         * @return tree root of the device registry parent tree
         */
        private TreeNode tree(NameType nameType) {
            return treeNodeManager.getDeviceRegistryParentTree(singleNameOperation, nameType);
        }

        /**
         * @return the nameOperation
         */
        public SingleNameOperation getSingleNameOperation() {
            return singleNameOperation;
        }

        /**
         *
         * @return title to be used in headers
         */
        public String getHeader() {
            return header;
        }

        /**
         *
         * @return index style to be used in devices wizard
         */
        public String getIndexStyle() {
            String indexStyle = NamingConventionUtility.getInstanceIndexStyle(singleNameOperation.getOtherParentView());
            return indexStyle != null ? indexStyle : "";
        }

        /**
         *
         * @return true if subsystem is selected
         */
        public boolean isRootParentSelected() {
            return selectedRootParent != null;
        }

        /**
         *
         * @return true if device type is selected
         */
        public boolean isOtherParentSelected() {
            return selectedOtherParent != null;
        }

        /**
         * Return if the tab exists.
         *
         * @param tab the tab number
         * @return true if the tab exists
         */
        public boolean existsTab(int tab) {
            return tab >= 0 && tab < tabs.size();
        }

        /**
         * Return next tab to be shown.
         *
         * @param event the flow event
         * @return next tab to be shown
         */
        public String onFlowProcess(FlowEvent event) {
            // step
            //     0 - system structure
            //     1 - device structure
            //     2 - instance index, description
            //     3 - review and submit
            //
            // either of
            //     - system structure, device structure, instance index, finish ---> step 0, 1, 2, 3
            //     - system structure, device structure, finish                 ---> step 0, 1, 3
            //
            // description in step for instance index,
            //     possibly move to last step to allow user to write for both cases
            //     - autogenerate

            final String oldStep = event.getOldStep();
            final String newStep = event.getNewStep();
            final int prev = tabs.indexOf(oldStep);
            final int next = tabs.indexOf(newStep);

            boolean forwards  = next > prev;
            boolean backwards = next < prev;

            if (existsTab(prev) && forwards) {
                // 0, 1, 2
                if (prev == 0) {
                    if (selectedRootParent == null || selectedRootParent.equals(rootTree)) {
                        UiUtility.showErrorMessage(VALIDATION_ERROR, "Please select from list");
                        return oldStep;
                    }
                } else if (prev == 1) {
                    // no device structure selected
                    //     set values corresponding to setMnemonic
                    //         clear if backwards
                    //     to last step in wizard
                    if (selectedOtherParent == null) {
                        String description = singleNameOperation.getNameElement().getDescription();

                        singleNameOperation.getNameElement().setMnemonic(null);
                        singleNameOperation.getNameElement().setFullName(
                                transactionBean.conventionName(singleNameOperation));
                        if (StringUtils.isEmpty(description)) {
                            singleNameOperation.getNameElement().setDescription("System structure only");
                        }
                    }
                } else if (prev == 2) {
                    try {
                        transactionBean.validateMnemonic(singleNameOperation);
                    } catch (ValidationException e) {
                        UiUtility.showErrorMessage(e.getMessage(), e.getDescription());
                        return oldStep;
                    }
                }
            } else if (existsTab(prev) && backwards) {
                // 3, 2, 1
                if (prev == 3) {
                    if (selectedOtherParent == null) {
                        singleNameOperation.getNameElement().setMnemonic(null);
                        singleNameOperation.getNameElement().setFullName(null);
                        singleNameOperation.getNameElement().setDescription(null);
                        return tabs.get(1);
                    }
                    return tabs.get(2);
                }
            }
            return existsTab(next) ? newStep : tabs.get(next + 1);
        }

        /**
         * @return the systemStructure tree
         */
        public TreeNode getRootTree() {
            return rootTree;
        }

        /**
         * @return the deviceStructure tree
         */
        public TreeNode getOtherTree() {
            return otherTree;
        }

        /**
         * Return the convention name of a name view.
         *
         * @param nameView the name view
         * @return the convention name of a name view
         */
        public String conventionName(NameView nameView) {
            return transactionBean.conventionName(nameView);
        }

        /**
         * @return the menemonic (instance index)
         */
        public String getMnemonic() {
            return As.emptyIfNull(singleNameOperation.getNameElement().getMnemonic());
        }

        /**
         * Sets the instance index and fullName.
         *
         * @param mnemonic the mnemonic (instance index) to set
         */
        public void setMnemonic(String mnemonic) {
            singleNameOperation.getNameElement().setMnemonic(mnemonic);
            if (singleNameOperation.isInDeviceRegistry()) {
                singleNameOperation.getNameElement().setFullName(transactionBean.conventionName(singleNameOperation));
            }
        }

        /**
         * set the selectedParent
         *
         * @param event the node select event
         */
        public void onSelectRootParent(NodeSelectEvent event) {
            TreeNode node = event.getTreeNode();
            if (node != null) {
                TreeNodeManager.updateSelected(this.selectedRootParent, node);
                setSelectedRootParent(node);
            } else {
                TreeNodeManager.updateSelected(this.selectedRootParent, node);
                setSelectedRootParent(rootTree);
            }
        }

        /**
         * update when unselecting the parent in root structure
         *
         * @param event the unselect node event
         */
        public void onUnselectRootParent(NodeUnselectEvent event) {
            TreeNode node = event.getTreeNode();
            node.setSelected(false);
            setSelectedRootParent(rootTree);
        }

        /**
         * @return the formSelectedSystem
         */
        public TreeNode getSelectedRootParent() {
            return selectedRootParent;
        }

        /**
         * set the selectedParent
         *
         * @param event the node select event
         */
        public void onSelectOtherParent(NodeSelectEvent event) {
            TreeNode node = event.getTreeNode();

            setSelectedOtherParent(node);
        }

        /**
         * update when unselecting the parent in other structure
         *
         * @param event the node unselect event
         */
        public void onUnselectOtherParent(NodeUnselectEvent event) {
            setSelectedOtherParent(null);
        }

        /**
         * @param selectedRootParent the formSelectedSubsystem to set
         */
        public void setSelectedRootParent(TreeNode selectedRootParent) {
            this.selectedRootParent = selectedRootParent;
            NameView parentView = TreeNodeManager.nameView(selectedRootParent);
            if (singleNameOperation != null) {
                singleNameOperation.setParentView(parentView);
            }
        }

        /**
         * @return the formSelectedDeviceType
         */
        public TreeNode getSelectedOtherParent() {
            return selectedOtherParent;
        }

        /**
         * @param selectedOtherParent the formSelectedDeviceType to set
         */
        public void setSelectedOtherParent(TreeNode selectedOtherParent) {
            TreeNodeManager.updateSelected(this.selectedOtherParent, selectedOtherParent);
            this.selectedOtherParent = selectedOtherParent;
            NameView parentView = selectedOtherParent != null ? TreeNodeManager.nameView(selectedOtherParent) : null;
            if (singleNameOperation != null) {
                singleNameOperation.updateOtherParentView(parentView);
            }
        }

    }

    /**
     * Subclass to set the global filter
     *
     * @author karinrathsman
     *
     */
    public class FilterWizard {

        private final TreeNode systemStructure;
        private final TreeNode deviceStructure;

        /**
         * constructor sets the two trees to be filtered
         */
        public FilterWizard() {
            this.systemStructure = treeNodeManager.getFilterTree(NameType.SYSTEM_STRUCTURE);
            this.deviceStructure = treeNodeManager.getFilterTree(NameType.DEVICE_STRUCTURE);
        }

        /**
         * selects all nodes in system structure
         */
        public void onSystemStructureSelectAll() {
            treeNodeManager.filterAll(systemStructure, true);
        }

        /**
         * unselects all nodes in system structure
         */
        public void onSystemStructureUnselectAll() {
            treeNodeManager.filterAll(systemStructure, false);
        }

        /**
         * selects all nodes in device structure
         */
        public void onDeviceStructureSelectAll() {
            treeNodeManager.filterAll(deviceStructure, true);
        }

        /**
         * unselects all nodes in device structure
         */
        public void onDeviceStructureUnselectAll() {
            treeNodeManager.filterAll(deviceStructure, false);
        }

        /**
         * @return the systemStructure tree
         */
        public TreeNode getSystemStructure() {
            return systemStructure;
        }

        /**
         * @return the deviceStructure tree
         */
        public TreeNode getDeviceStructure() {
            return deviceStructure;
        }

    }

    /**
     * Call this function to update icon on dataTable header according to filtering in menus
     */
    public void updateFilteringIcon() {
        NameFilter nameFilter = treeNodeManager.getNameFilter();

        if (nameFilter != null) {
            Map<UUID, NodeStatus> nodeMap = nameFilter.getNodeMap();

            int cf = 0;

            if (nodeMap != null) {
                for (NodeStatus n : nodeMap.values()) {

                    //sum all custom filters
                    if (n.isCustomizedFiltered()) {
                        cf++;
                    }
                }
                //filter, if customfilter has been set
                if (cf != nodeMap.size()) {
                    filterStatusIcon(true);
                    return;
                }
            }
        }

        //no custom filter has been set
        filterStatusIcon(false);
    }

    /**
     * Updates filter-icon on dataTable header according to parameter
     *
     * @param isFiltered tells if customFiltering has been set, or not
     */
    public void filterStatusIcon(boolean isFiltered) {
        final String noFilter = "ui-icon-closethick";
        final String filtered = "ui-icon-check";

        //search for datatable, and button
        FacesContext facesContext = FacesContext.getCurrentInstance();
        CommandButton btIcon =
                (CommandButton) facesContext.getViewRoot().findComponent("ManageNameForm:devicesTree:filterOnIcon");

        //if datatable or button was not found -> exit
        if (btIcon == null) {
            return;
        }

        if (isFiltered) {
            //filter was applied on datatable
            btIcon.setIcon(filtered);
        } else {
            //no filter were applied
            btIcon.setIcon(noFilter);
        }
    }

    /**
     * Generates title/tooltip for an operation
     *
     * @return title/tooltip for Submit button for an operation
     */
    public String operationSubmitTitle() {
        return "Submit operations";
    }

    /**
     * Collapses all trees in the Device Wizard dialog (System, and Device Structure)
     */
    public void collapseAll() {
        if (wizard != null) {
            TreeNode systemTree = wizard.rootTree;
            TreeNode deviceTree = wizard.otherTree;

            if (systemTree != null) {
                collapsingORExpanding(systemTree, false);
            }

            if (deviceTree != null) {
                collapsingORExpanding(deviceTree, false);
            }
        }
    }

    /**
     * Expands, or collapses all tree nodes
     * @param node the node for which, and which children node should be collapsed, or expanded
     * @param doExpand if <code>true</code>, nodes will be expanded, if <code>false</code> nodes will be collapsed
     */
    private void collapsingORExpanding(TreeNode node, boolean doExpand) {
        if (!node.getChildren().isEmpty()) {
            for (TreeNode s : node.getChildren()) {
                collapsingORExpanding(s, doExpand);
            }
            node.setExpanded(doExpand);
        }
        node.setSelected(false);
    }

}
