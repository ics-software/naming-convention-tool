/*
 * Copyright (C) 2020 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.ui.common;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.omnifaces.exceptionhandler.FullAjaxExceptionHandler;
import org.openepics.names.util.UiUtility;

import javax.ejb.EJBException;
import javax.el.ELException;
import javax.faces.FacesException;
import javax.faces.context.ExceptionHandler;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.event.ExceptionQueuedEvent;
import java.lang.reflect.InvocationTargetException;
import java.net.URLEncoder;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Utility class to handle custom exceptions.
 *
 * @author <a href="mailto:zoltan.runyo@ess.eu">Zoltan Runyo</a>
 * @author Lars Johansson
 */
public class CustomExceptionHandler extends FullAjaxExceptionHandler {
    private static final Logger LOGGER = Logger.getLogger(CustomExceptionHandler.class.getCanonicalName());
    private static final String UNEXPECTED_ERROR = "Unexpected error";

    /**
     * A new JSF exception handler.
     *
     * @param wrapped the original JSF exception handler
     */
    public CustomExceptionHandler(ExceptionHandler wrapped) {
        super(wrapped);
    }

    /*
     * (non-Javadoc)
     * @see org.omnifaces.exceptionhandler.FullAjaxExceptionHandler#handle()
     *
     * @throws FacesException
     */
    @Override
    public void handle() {
        final Iterator<ExceptionQueuedEvent> unhandledExceptionQueuedEvents =
                getUnhandledExceptionQueuedEvents().iterator();

        if (!unhandledExceptionQueuedEvents.hasNext()) {
            // There's no unhandled exception.
            return;
        }

        final Throwable unwrappedException = unhandledExceptionQueuedEvents.next().getContext().getException();
        // Handle UIException case which requires redirect to another page
        final InvocationTargetException ite = getInvocationTargetException(unwrappedException);
        if ((ite != null) && (ite.getTargetException() instanceof UIException)) {
            final ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
            try {
                // ExternalContext.redirect is the most low-level redirect I could find, and using uri parameters
                // seems like the most straight-forward way of doing it.
                ec.redirect(ec.getRequestContextPath() + "/error.xhtml?errorMsg=" +
                        URLEncoder.encode(getDetails(ite.getTargetException()), "UTF-8"));
            } catch (Exception e) {
                LOGGER.log(Level.SEVERE, "Failed to redirect to error page.", e);
            }
        } else {

            // Handle all other cases where redirect is not needed.
            final Throwable throwable = getExceptionNonFrameworkCause(unwrappedException);
            if (!(throwable instanceof javax.faces.application.ViewExpiredException)) {
                UiUtility.showErrorMessage(UiUtility.ErrorType.UNHANDLED,
                		getDetails(throwable), null, ExceptionUtils.getStackTrace(throwable));
                LOGGER.log(Level.SEVERE, UNEXPECTED_ERROR, throwable);
            } else {
                super.handle();
            }
        }
    }

    private String getDetails(final Throwable throwable) {
        return throwable.getClass().getSimpleName() +
                (StringUtils.isNotEmpty(throwable.getMessage()) ? ": " + throwable.getMessage() : "");
    }

    /**
     * Check for leaf {@link InvocationTargetException}.
     *
     * @param exception The top exception
     * @return The invocation target exception or <code>null</code>
     */
    private InvocationTargetException getInvocationTargetException(Throwable exception) {
        Throwable iterated = exception;
        while ((iterated != null) && (iterated.getCause() != null)) {
            if (iterated instanceof InvocationTargetException)
                return (InvocationTargetException) iterated;

            iterated = iterated.getCause();
        }

        return null;
    }

    /* Returns the nested exception cause that is not Faces or EJB exception, if it exists. */
    private Throwable getExceptionNonFrameworkCause(Throwable exception) {
        return (exception instanceof FacesException
                || exception instanceof EJBException
                || exception instanceof ELException)
                && (exception.getCause() != null)
                ? getExceptionNonFrameworkCause(exception.getCause())
                : exception;
    }
}
