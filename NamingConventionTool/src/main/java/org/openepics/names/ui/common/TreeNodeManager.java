/*-
 * Copyright (c) 2014 European Spallation Source ERIC.
 * Copyright (c) 2014 Cosylab d.d.
 *
 * This file is part of Naming Service.
 * Naming Service is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */

package org.openepics.names.ui.common;

import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

import javax.annotation.Nullable;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.inject.Inject;

import org.apache.commons.lang3.StringUtils;
import org.openepics.names.business.NameFilter;
import org.openepics.names.business.NameRevisionPair;
import org.openepics.names.business.NameType;
import org.openepics.names.business.RowData;
import org.openepics.names.nameviews.DeviceRecordView;
import org.openepics.names.nameviews.NameView;
import org.openepics.names.nameviews.NameViewProvider;
import org.openepics.names.operation.Add;
import org.openepics.names.operation.MultipleNameOperation;
import org.openepics.names.operation.SingleNameOperation;
import org.primefaces.event.NodeCollapseEvent;
import org.primefaces.event.NodeExpandEvent;
import org.primefaces.event.NodeSelectEvent;
import org.primefaces.event.NodeUnselectEvent;
import org.primefaces.model.CheckboxTreeNode;
import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.TreeNode;
import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

/**
 * A UI controller bean for tree nodes.
 *
 * It is used in ESS Name Registry, System Structure and Device Structure as well as in history and Excel handling.
 *
 * @author Karin Rathsman
 */
@ManagedBean
@ViewScoped
public class TreeNodeManager {

    // Note revision history of file in repository.

    private static final Logger LOGGER = Logger.getLogger(TreeNodeManager.class.getName());

    @Inject
    private NameViewProvider nameViewProvider;
    @Inject
    private NameFilter nameFilter;

    private final String chessURL = System.getProperty("names.chessURL");

    public String getChessURL() {
        return chessURL;
    }

    /**
     * Return the unique key (UUID) of the tree node.
     *
     * @param node tree node
     * @return the unique key (UUID)
     */
    public static @Nullable UUID key(TreeNode node) {
        return key(nameView(node));
    }

    /**
     * Return the unique key (UUID) of the name view.
     *
     * @param nameView name view
     * @return the unique key (UUID)
     */
    public static @Nullable UUID key(NameView nameView) {
        return !nameView.isRoot() ? nameView.getRevisionPair().getBaseRevision().getNameArtifact().getUuid() : null;
    }

    /**
     * Return the name view of the tree node.
     *
     * @param node treeNode containing the name view
     * @return the name view
     */
    public static NameView nameView(TreeNode node) {
        return (NameView) node.getData();
    }

    /**
     *
     * @param node tree node
     * @return list of node and successive children
     */
    private static List<TreeNode> nodeInStructureList(@Nullable TreeNode node) {
        final List<TreeNode> nodeList = Lists.newArrayList();
        if (node != null) {
            nodeList.add(node);
            for (TreeNode child : node.getChildren()) {
                nodeList.addAll(nodeInStructureList(child));
            }
        }
        return nodeList;
    }

    /**
     *
     * @param node tree node
     * @return List of successive parents of the node
     */
    private static List<TreeNode> parentList(@Nullable TreeNode node) {
        final List<TreeNode> nodeList = Lists.newArrayList();
        if (node != null && node.getParent() != null) {
            nodeList.addAll(parentList(node.getParent()));
        }
        nodeList.add(node);
        return nodeList;
    }

    /**
     * Expand treeNode and its children
     *
     * @param treeNode the tree node root
     */
    public void expandAll(TreeNode treeNode) {
        for (TreeNode node : nodeInStructureList(treeNode)) {
            setExpanded(node, true);
        }
    }

    /**
     * Collapse treeNode and its children
     *
     * @param treeNode the tree node root
     */
    public void collapseAll(TreeNode treeNode) {
        for (TreeNode node : nodeInStructureList(treeNode)) {
            setExpanded(node, false);
        }
    }

    /**
     * Set the expanded
     *
     * @param treeNode the tree node
     * @param expanded true if the tree node should be expanded
     */
    public void setExpanded(TreeNode treeNode, boolean expanded) {
        if (key(treeNode) != null) {
            treeNode.setExpanded(expanded);
            nameFilter.getNodeStatus(nameView(treeNode)).setExpanded(expanded);
        }
    }

    /**
     * Expand all parent nodes to a tree node
     *
     * @param treeNode the node to expand recursively to root node
     */
    public void expandParents(TreeNode treeNode) {
        if (treeNode != null) {
            for (TreeNode node : parentList(treeNode)) {
                setExpanded(node, true);
            }
        }
    }

    /**
     * Expands node on an event
     *
     * @param event containing the tree node
     */
    public void onNodeExpand(NodeExpandEvent event) {
        if (event != null && event.getTreeNode() != null) {
            setExpanded(event.getTreeNode(), true);
        }
    }

    /**
     * Collapses node on an even
     *
     * @param event containing the tree node
     */
    public void onNodeCollapse(NodeCollapseEvent event) {
        if (event != null && event.getTreeNode() != null) {
            setExpanded(event.getTreeNode(), false);
        }
    }

    /**
     * Generates treeNodes and attach these to one tree root.
     *
     * @param nameOperation the multiple name operation
     * @return root of the operation tree.
     */
    public static TreeNode getOperationTree(MultipleNameOperation nameOperation) {
        try {
            nameOperation.validateOnSelect();
            NameView rootView = nameOperation.getRoot();
            TreeNode root = new DefaultTreeNode(rootView);
            Map<NameView, TreeNode> map = Maps.newHashMap();
            map.put(rootView, root);
            NameType nameType = rootView.getNameType();
            List<NameView> list = nameOperation.getAffectedNameViews();
            for (NameView nameView : list) {
                treeNodeWithParents(nameView, map, nameType);
            }
            return root;
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * Generate the row data to be used to display
     *
     * @param operation the multiple name operation
     * @param nameView the nameView to create the row data for
     * @return the row data
     */
    public RowData getOperatedRowData(MultipleNameOperation operation, NameView nameView) {
        if (operation.getAffectedNameViews().contains(nameView)) {
            return operation.getOperatedRowData(nameView);
        } else {
            RowData rowData = rowData(nameView);
            rowData.setStatus("");
            return rowData;
        }
    }

    /**
     * generates the operation treeNode
     *
     * @param nameView the name view to generate the root for.
     * @param map the name view to treeNode map.
     * @param nameType branch type.
     * @return the tree node including parent nodes.
     */
    private static TreeNode treeNodeWithParents(NameView nameView, Map<NameView, TreeNode> map, NameType nameType) {
        if (nameView == null) {
            return null;
        } else if (map.containsKey(nameView)) {
            return map.get(nameView);
        } else {
            TreeNode parent = treeNodeWithParents(nameView.getParent(nameType), map, nameType);
            TreeNode nameNode = new DefaultTreeNode(nameView, parent);
            nameNode.setExpanded(true);
            nameNode.setSelectable(false);
            map.put(nameView, nameNode);
            return nameNode;
        }
    }

    /**
     *
     * @return the name filter used in views to specify which names shall be displayed.
     */
    public NameFilter getNameFilter() {
        return nameFilter;
    }

    /**
     * Set the selected node to be filtered
     *
     * @param event the event containing the selected node.
     */
    public void onFilterNode(NodeSelectEvent event) {
        TreeNode node = event.getTreeNode();
        filter(node, true);
    }

    /**
     * Set the selected node to be filtered
     *
     * @param node the selected or unselected node.
     * @param selected true if the node is selected to be filtered
     */
    public void filter(TreeNode node, boolean selected) {
        node.setSelected(selected);
        nameFilter.setCustomizedFiltered(nameView(node), selected);
    }

    /**
     * Set the selected node to be unfiltered
     *
     * @param event the event containing the tree node
     */
    public void onUnfilterNode(NodeUnselectEvent event) {
        TreeNode node = event.getTreeNode();
        filter(node, false);
    }

    /**
     * Generates tree to be used for the global filter.
     *
     * @param nameType the nameType defining the branch (system or device).
     * @return tree root.
     */
    public TreeNode getFilterTree(NameType nameType) {
        Preconditions.checkArgument(nameType.isSystemStructure() || nameType.isDeviceStructure());
        nameFilter.update();
        return getFilterTreeNode(nameViewProvider.nameStructure(nameType));
    }

    /**
     * Generates the global filter tree recursively.
     *
     * @param nameView the tree node data
     * @param parentIncluded indicator that the node is not excluded.
     * @return the tree node of the name view used by the global filter.
     */
    private TreeNode getFilterTreeNode(NameView nameView) {
        final CheckboxTreeNode node = new CheckboxTreeNode(nameView);
        if (!nameView.isChildInDeviceRegistry()) {
            for (NameView child : nameView.getChildren()) {
                TreeNode childNode = getFilterTreeNode(child);
                if (childNode != null) {
                    node.getChildren().add(childNode);
                }
            }
        }
        boolean hasAcceptedChildren = !node.getChildren().isEmpty();
        if (nameFilter.getNodeStatus(nameView).isAccepted() || hasAcceptedChildren || nameView.isRoot()) {
            node.setSelected(nameFilter.getNodeStatus(nameView).isCustomizedFiltered(), false);
            node.setExpanded(nameFilter.getNodeStatus(nameView).isExpanded());
            return node;
        } else {
            return null;
        }
    }

    /**
     * Filter/unfilters all nodes in the customized name filter
     *
     * @param treeNode root containing all treeNodes
     * @param selected is true if the nodes should be filtered, false if unfilter
     */
    public void filterAll(TreeNode treeNode, boolean selected) {
        for (TreeNode node : treeNode.getChildren()) {
            filter(node, selected);
        }
    }

    /**
     * Generates filtered the system or device structure tree
     *
     * @param operation the operation
     * @param nameType the nameType of the structure
     * @return return the tree root.
     */
    public TreeNode getStructureTree(SingleNameOperation operation, NameType nameType) {
        Preconditions.checkArgument(nameType.isSystemStructure() || nameType.isDeviceStructure());
        nameFilter.update();
        return getTreeNode(nameViewProvider.nameStructure(nameType), operation, false, (operation instanceof Add));
    }

    /**
     * Generate the filtered the system or device tree
     *
     * @param operation     the operation.
     *                      All nodes in the tree that are allowed as parents to the devices names are selecteable.
     * @param nameType      the nameType
     * @return              return the tree root.
     */
    public TreeNode getDeviceRegistryParentTree(SingleNameOperation operation, NameType nameType) {
        Preconditions.checkArgument(nameType.isSystemStructure() || nameType.isDeviceStructure());
        nameFilter.update();
        return getTreeNode(nameViewProvider.nameStructure(nameType), operation, true, true);
    }

    /**
     * Generates filtered the system or device structure tree.
     * All nodes in the tree that are allowed as parents are selectable.
     *
     * @param operation the operation
     * @param nameType the nameType
     * @return return the tree root.
     */
    public TreeNode getStructureParentTree(SingleNameOperation operation, NameType nameType) {
        Preconditions.checkArgument(nameType.isSystemStructure() || nameType.isDeviceStructure());
        nameFilter.update();
        return getTreeNode(nameViewProvider.nameStructure(nameType), operation, false, true);
    }

    /**
     * Checks if the level is acceptable for the operation
     *
     * @param inRegistry true if the tree view or operation applies to devices
     * @param parent true if the operation applies to the parent.
     * @param nameView the nameView to be checked
     * @return true if this node is accepted.
     */
    private boolean isSelectable(boolean inRegistry, boolean parent, SingleNameOperation operation, NameView nameView) {
        if (operation == null) {
            return nameView.isInStructure() || inRegistry;
        } else if (parent) {
            return operation.acceptedAsParent(nameView) && (inRegistry || nameView.isChildInStructure());
        } else {
            return operation.affects(nameView) && (inRegistry || nameView.isInStructure());
        }
    }

    /**
     * Generates the filtered tree of name views
     *
     * @param nameView          the root node
     * @param operation         the single name operation to be performed (add or modified)
     * @param parentIncluded    indicates if the parent is included in the operation.
     * @param inRegistry        indicates if tree shall include items in the device registry
     *                          (and exclude nodes that have not subsystem or device types)
     * @param selectableParent  indicates if a parent node shall be selected for the operation.
     * @return                  the name view as a tree node with name view as data.
     *                          includes children accepted by the filter settings.
     */
    private TreeNode getTreeNode(
            NameView nameView, SingleNameOperation operation, boolean inRegistry, boolean selectableParent) {

        final TreeNode node = new DefaultTreeNode(nameView);
        if (!nameView.isChildInDeviceRegistry()) {
            for (NameView child : nameView.getChildren()) {
                TreeNode childNode = getTreeNode(child, operation, inRegistry, selectableParent);
                if (childNode != null) {
                    node.getChildren().add(childNode);
                }
            }
        }
        boolean hasAcceptedChildren = !node.getChildren().isEmpty();
        boolean selectable = isSelectable(inRegistry, selectableParent, operation, nameView);

        if (nameFilter.getNodeStatus(nameView).isAccepted()
                    && nameFilter.getNodeStatus(nameView).isCustomizedFiltered()
                    && selectable
                || hasAcceptedChildren
                || nameView.isRoot()) {
            node.setExpanded(nameFilter.getNodeStatus(nameView).isExpanded());
            node.setSelectable(selectable);
            return node;
        } else {
            return null;
        }
    }

    /**
     * Generates a list of filtered subsystems or deviceTypes
     *
     * @param node the root containing the branch.
     * @return a list of nameViews at the specified level below this node.
     */
    public List<NameView> deviceParents(TreeNode node) {
        nameFilter.update();
        final List<NameView> parents = Lists.newArrayList();
        if (nameView(node).isChildInDeviceRegistry()) {
            parents.add(nameView(node));
        } else {
            for (TreeNode child : node.getChildren()) {
                parents.addAll(deviceParents(child));
            }
        }
        return parents;
    }

    /**
     * Search the tree after a the node for a name view
     *
     * @param node the root node
     * @param nameView the name view to be searched for
     * @return the tree node containing name view. Null if name view does not exist in the tree.
     */
    public TreeNode nodeOf(TreeNode node, NameView nameView) {
        TreeNode treeNode = null;
        if (nameView(node).equals(nameView)) {
            return node;
        } else {
            for (TreeNode child : node.getChildren()) {
                treeNode = nodeOf(child, nameView);
                if (treeNode != null) {
                    return treeNode;
                }
            }
        }
        return null;
    }

    /**
     * Generates a list with filtered device records
     *
     * @return list of filtered device records
     */
    public List<DeviceRecordView> deviceRecordViews() {
        List<DeviceRecordView> devices = Lists.newArrayList();
        List<NameView> deviceViews = nameViewProvider.nameStructure(NameType.SYSTEM_STRUCTURE).getDevices();
        nameFilter.update();
        for (NameView deviceView : deviceViews) {
            if (nameFilter.getNodeStatus(deviceView).isAcceptedAndFiltered()) {
                devices.add(new DeviceRecordView(deviceView));
            }
        }
        return devices;
    }

    /**
     * Update selected tree node.
     *
     * @param source node to be unselected
     * @param target node to be selected
     */
    public static void updateSelected(TreeNode source, TreeNode target) {
        if (source != null) {
            source.setSelected(false);
        }
        if (target != null) {
            target.setSelected(true);
        }
    }

    /**
     * Generates rowData for a name view.
     *
     * @param nameView          view of the name
     * @return                  row data of the specified view to be used in tables and trees
     */
    public RowData rowData(NameView nameView) {
        return nameView != null
                ? rowData(
                        nameView.getRevisionPair(),
                        getNameFilter().isIncludePending(),
                        getNameFilter().isIncludeCancelled(),
                        nameView.isInStructure() ? nameView.getMnemonicPath() : null,
                        nameView.getNameType())
                : null;
    }

    /**
     * Generates rowData for a revision pair used to display data rows in history
     *
     * @param revisionPair      the revision pair.
     * @param includePending    indicates if pending states shall be displayed
     * @param includeCancelled  indicates if cancelled states shall be displayed
     * @return                  row data of the specified revision pair.
     */
    public static RowData rowData(NameRevisionPair revisionPair, boolean includePending, boolean includeCancelled) {
        return rowData(revisionPair, includePending, includeCancelled, null, null);
    }

    /**
     * Generates row data for a revision pair used to display data rows in history
     *
     * @param revisionPair      the revision pair.
     * @param includePending    indicates if pending states shall be displayed
     * @param includeCancelled  indicates if cancelled states shall be displayed
     * @param mnemonicPath      list of mnemonics starting from the root of the hierarchy to the mnemonic to be used
     * @param nameType          type specifying whether mnemonic belongs to the system structure or the device structure
     * @return                  row data of the specified revision pair.
     */
    public static RowData rowData(NameRevisionPair revisionPair, boolean includePending, boolean includeCancelled,
            List<String> mnemonicPath, NameType nameType) {
        if (revisionPair.getNameStage().isApproved()) {
            return RowData.newRowData(revisionPair, includePending, includeCancelled, mnemonicPath, nameType);
        } else {
            return RowData.newRowData(revisionPair, true, true, mnemonicPath, nameType);
        }
    }

    /**
     * Provides filter function for regex string. Intended usage from Primefaces UI element.
     * Returns true if value encompassed by filter.
     *
     * @param value value to be considered if encompassed by filter
     * @param filter regex value as filter
     * @param locale locale
     * @return true if value encompassed by filter
     */
    public boolean filterByRegex(Object value, Object filter, Locale locale) {
        String filterText = (filter == null) ? null : filter.toString().trim();
        if (StringUtils.isEmpty(filterText)) {
            return true;
        }

        if (value == null) {
            return false;
        }

        try {
            Pattern pattern = Pattern.compile(filterText);
            return pattern.matcher((String)value).find();
        } catch (PatternSyntaxException e) {
            LOGGER.log(Level.FINEST, e.getMessage(), e);
            return false;
        }
    }

}
