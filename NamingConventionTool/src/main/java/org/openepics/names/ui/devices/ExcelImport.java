/*-
 * Copyright (c) 2014 European Spallation Source ERIC.
 * Copyright (c) 2014 Cosylab d.d.
 *
 * This file is part of Naming Service.
 * Naming Service is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package org.openepics.names.ui.devices;

import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;

import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openepics.names.business.NameType;
import org.openepics.names.nameviews.DeviceRecordView;
import org.openepics.names.nameviews.NameView;
import org.openepics.names.nameviews.NameViewProvider;
import org.openepics.names.nameviews.TransactionBean;
import org.openepics.names.nameviews.TransactionResult;
import org.openepics.names.operation.AddDevice;
import org.openepics.names.services.NamingConvention;
import org.openepics.names.util.As;
import org.openepics.names.util.ExcelUtility;
import javax.annotation.Nullable;
import javax.ejb.Stateless;
import javax.inject.Inject;
import java.io.IOException;
import java.io.InputStream;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFDataFormat;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.openepics.names.business.NameElement;
import org.openepics.names.business.NameRevision;
import org.openepics.names.operation.DeleteDevices;
import org.openepics.names.operation.ModifyDevice;
import org.openepics.names.operation.NameOperation;
import org.openepics.names.util.UiUtility;
import org.openepics.names.util.ValidationException;

/**
 * A bean for importing devices from Excel.
 *
 * @author Sunil Sah
 * @author Karin Rathsman
 * @author Lars Johansson
 */
@Stateless
public class ExcelImport {

    private static final String EMPTY = "";
    private static final String IMPORT_FAILED_ROW = "Import failed on row {0}.";

    @Inject
    private NamingConvention namingConvention;
    @Inject
    private NameViewProvider nameViewProvider;
    @Inject
    private TransactionBean transactionBean;
    private Map<String, NameView> subsystemMap;
    private Map<String, NameView> deviceTypeMap;
    private NameView systemRoot;
    private Map<String, ImportRow> keys;
    private Map<String, ImportRow> uuids;

    /**
     * Parses the input stream read from an Excel file, creating devices in the database.
     * If the device already exists, it's silently ignored.
     *
     * @param input the input stream
     * @param importIsModify true if modify names on import, false if adding on import
     */
    public void parseDeviceImportFile(InputStream input, boolean importIsModify) {
        final Set<NameOperation> operations = Sets.newHashSet();
        int rownum = -1;
        try (final XSSFWorkbook workbook = new XSSFWorkbook(input)) {
            init();

            final XSSFSheet sheet = workbook.getSheetAt(0);
            for (Row row : sheet) {
                if (row.getRowNum() > 0) {
                	rownum = row.getRowNum();
                    ImportRow importRow = new ImportRow(rownum);
                    int i = 0;
                    if (importIsModify) {
                        importRow.setKey(row.getCell(i++));
                        importRow.setDelete(row.getCell(i++));
                    } else {
                        importRow.setKey(null);
                        importRow.setDelete(null);
                    }
                    importRow.setSystemGroup(row.getCell(i++));
                    importRow.setSystem(row.getCell(i++));
                    importRow.setSubsystem(row.getCell(i++));
                    importRow.setDiscipline(row.getCell(i++));
                    importRow.setDeviceType(row.getCell(i++));
                    importRow.setInstanceIndex(row.getCell(i++));
                    importRow.setDescription(row.getCell(i++));
                    NameOperation operation = importRow.operation();
                    if (operation != null) {
                        operations.add(operation);
                    }
                }
            }

            TransactionResult result =  transactionBean.submit(operations);
            UiUtility.showMessage(result);
        } catch (ValidationException e) {
            UiUtility.showErrorMessage(
            		EMPTY, MessageFormat.format(IMPORT_FAILED_ROW, rownum + 1), e.getMessage(), e);
        } catch (IOException e) {
            UiUtility.showErrorMessage(
            		EMPTY, MessageFormat.format(IMPORT_FAILED_ROW, rownum + 1), e.getMessage(), e);
        } catch (IllegalStateException e) {
            UiUtility.showErrorMessage(
            		EMPTY, MessageFormat.format(IMPORT_FAILED_ROW, rownum + 1), e.getMessage(), e);
        }
    }

    private void init() {
        uuids = new HashMap<>();
        systemRoot = nameViewProvider.nameStructure(NameType.SYSTEM_STRUCTURE);
        subsystemMap = nameViewMap(systemRoot);
        NameView deviceStructure = nameViewProvider.nameStructure(NameType.DEVICE_STRUCTURE);
        deviceTypeMap = nameViewMap(deviceStructure);
        keys = new HashMap<>();
    }

    /**
     *
     * @param root
     * @return
     */
    private Map<String, NameView> nameViewMap(NameView root) {
        Set<NameView> parents = root.getAllowedDeviceParents();
        Map<String, NameView> namePartMap = new HashMap<>();
        for (NameView parentView : parents) {
            String conventionName =
                    namingConvention.equivalenceClassRepresentative(transactionBean.conventionName(parentView));
            Preconditions.checkState(
                    !namePartMap.containsKey(conventionName),
                    "Convention names for device parents (" + (namePartMap.containsKey(conventionName)
                            ? StringUtils.join(namePartMap.get(conventionName).getMnemonicPath().stream()
                            .filter(StringUtils::isNotEmpty).collect(Collectors.toList()), "-") : "") +
                            " and " + StringUtils.join(parentView.getMnemonicPath().stream()
                            .filter(StringUtils::isNotEmpty).collect(Collectors.toList()), "-") +
                            ") is not unique. Database contains corrupted data.");
            namePartMap.put(conventionName, parentView);
        }
        return namePartMap;
    }

    private List<String> mnemonicPath(String level1, String level2, String level3) {
        List<String> mnemonicPath = Lists.newArrayList();
        mnemonicPath.add(As.emptyIfNull(level1));
        mnemonicPath.add(As.emptyIfNull(level2));
        mnemonicPath.add(As.emptyIfNull(level3));
        return mnemonicPath;
    }

    /**
     * Exports devices from the database, producing a stream which can be streamed to the user over HTTP.
     *
     * @param records the list with filtered records
     * @param importIsModify specifies whether the import is modify
     * @return an Excel input stream containing the exported data
     */
    public InputStream templateForImport(@Nullable List<DeviceRecordView> records, boolean importIsModify) {
        final XSSFWorkbook workbook = exportWorkbook(records, importIsModify);
        return ExcelUtility.getInputStreamForWorkbook(workbook);
    }

    private XSSFWorkbook exportWorkbook(@Nullable List<DeviceRecordView> devices, boolean importIsModify) {
        final XSSFWorkbook workbook = new XSSFWorkbook();
        final XSSFSheet sheet = workbook.createSheet("ImportTemplate.csv");

        // sheet may be protected to prevent modification

        final ArrayList<String> titles = new ArrayList<>();
        if (importIsModify) {
            titles.add("Id");
            titles.add("Deleted");
        }
        titles.add("System Group");
        titles.add("System");
        titles.add("Subsystem");
        titles.add("Discipline");
        titles.add("Device Type");
        titles.add("Instance Index");
        titles.add("Description");
        XSSFFont boldFont = workbook.createFont();
        boldFont.setBold(true);
        XSSFDataFormat defaultFormat = workbook.createDataFormat();

        XSSFCellStyle headerStyle = workbook.createCellStyle();
        headerStyle.setFont(boldFont);
        headerStyle.setDataFormat(defaultFormat.getFormat("@"));
        XSSFCellStyle defaultStyle = workbook.createCellStyle();
        defaultStyle.setDataFormat(defaultFormat.getFormat("@"));
        defaultStyle.setLocked(false);

        Row head = sheet.createRow(0);
        for (int i = 0; i < titles.size(); i++) {
            if (!importIsModify) {
                sheet.getColumnHelper().setColDefaultStyle(i, defaultStyle);
            }
            Cell cell = head.createCell(i);
            cell.setCellValue(titles.get(i));
            cell.setCellStyle(headerStyle);
        }

        if (importIsModify && devices != null) {
            int rowNr = 0;
            for (DeviceRecordView record : devices) {
                if (!record.getDevice().getRevisionPair().getNameStage().isArchived()) {
                    final ArrayList<String> cells = new ArrayList<>();
                    cells.add(record.getDevice()
                            .getRevisionPair().getBaseRevision().getNameArtifact().getUuid().toString());
                    cells.add("NO");
                    cells.add(record.getSystemGroup() != null
                            ? record.getSystemGroup().getRevisionPair().getBaseRevision().getNameElement().getMnemonic()
                            : null);
                    cells.add(record.getSystem() != null
                            ? record.getSystem().getRevisionPair().getBaseRevision().getNameElement().getMnemonic()
                            : null);
                    cells.add(record.getSubsystem() != null
                            ? record.getSubsystem().getRevisionPair().getBaseRevision().getNameElement().getMnemonic()
                            : null);
                    cells.add(record.getDiscipline() != null
                            ? record.getDiscipline().getRevisionPair().getBaseRevision().getNameElement().getMnemonic()
                            : null);
                    cells.add(record.getDeviceType() != null
                            ? record.getDeviceType().getRevisionPair().getBaseRevision().getNameElement().getMnemonic()
                            : null);
                    cells.add(record.getDevice()
                            .getRevisionPair().getBaseRevision().getNameElement().getMnemonic());
                    cells.add(record.getDevice()
                            .getRevisionPair().getBaseRevision().getNameElement().getDescription());
                    final Row row = sheet.createRow(++rowNr);
                    Cell cell = row.createCell(0);
                    cell.setCellValue(cells.get(0));
                    cell.setCellStyle(headerStyle);

                    for (int i = 1; i < cells.size(); i++) {
                        cell = row.createCell(i);
                        cell.setCellValue(cells.get(i));
                        cell.setCellStyle(defaultStyle);
                    }
                }
            }
        }
        for (int i = 0; i < titles.size(); i++) {
            sheet.autoSizeColumn(i);
        }
        return workbook;
    }

    private class ImportRow {

    	// This class handles content for one row to import. In general, it does not handle which row
    	// information is about. There may be exceptions to that for validation messages although
    	// such usage may also be handled outside this class.

        private final int row;
        private String systemGroup;
        private String system;
        private String subsystem;
        private String discipline;
        private String deviceType;
        private final NameElement nameElement;
        private Action action;
        private String uuid;
        private NameView device;

        public ImportRow(int row) {
            this.row = row + 1;
            this.action = Action.ADD;
            this.uuid = null;
            this.nameElement = new NameElement(null, null, null);
        }

        /**
         * Check statement ant throws exception if it is not true
         *
         * @param statement statement to check
         * @param detail error message
         * @throws ValidationException
         */
        private void validate(boolean statement, String detail) throws ValidationException {
            if (!statement) {
                throw new ValidationException(detail);
            }
        }

        private int getRow() {
            return row;
        }

        private void setDelete(Cell cell) throws ValidationException {
            String string = As.nullIfEmpty(ExcelUtility.asString(cell));
            if (string == null && uuid == null) {
                this.action = Action.ADD;
            } else if ("YES".equalsIgnoreCase(string)) {
                validate(uuid != null, "The ID is null.");
                this.action = Action.DELETE;
            }
        }

        private void setKey(Cell cell) throws ValidationException {
            this.uuid = As.nullIfEmpty(ExcelUtility.asString(cell));
            if (uuid != null) {
                if (uuids.containsKey(uuid)) {
                    String detail = "ID " + uuid + "already exist on row " + uuids.get(uuid).getRow() + ".";
                    validate(false, detail);
                    throw new ValidationException(detail);
                } else {
                    uuids.put(uuid, this);
                }
                device = nameViewProvider.getNameViews().get(UUID.fromString(uuid));
                validate(device != null && device.isInDeviceRegistry(), "ID does not correspond to a device.");
                if (action != Action.DELETE) {
                    this.action = Action.MODIFY;
                }
            } else {
                device = null;
                this.action = Action.ADD;
            }
        }

        public void setSystemGroup(Cell cell) {
            this.systemGroup = As.nullIfEmpty(ExcelUtility.asString(cell));
        }

        private void setSystem(Cell cell) {
            this.system = As.nullIfEmpty(ExcelUtility.asString(cell));
        }

        private void setSubsystem(Cell cell) {
            this.subsystem = As.nullIfEmpty(ExcelUtility.asString(cell));
        }

        private void setDiscipline(Cell cell) {
            this.discipline = As.nullIfEmpty(ExcelUtility.asString(cell));
        }

        private void setDeviceType(Cell cell) {
            this.deviceType = As.nullIfEmpty(ExcelUtility.asString(cell));
        }

        private void setInstanceIndex(Cell cell) {
            // read cell as string regardless of cell type
            nameElement.setMnemonic(ExcelUtility.asStringCell(cell));
        }

        private void setDescription(Cell cell) {
            nameElement.setDescription(ExcelUtility.asString(cell));
        }

        private NameOperation operation() throws ValidationException {
            boolean emptyRow =
                    systemGroup == null
                            && system == null
                            && subsystem == null
                            && discipline == null
                            && deviceType == null
                            && nameElement.getDescription() == null
                            && nameElement.getMnemonic() == null;

            if (emptyRow) {
                return null;
            } else {
                // system structure key
                final String subsystemKey =
                        namingConvention.conventionKey(
                                mnemonicPath(systemGroup, system, subsystem),
                                NameType.SYSTEM_STRUCTURE);
                NameView subsystemView = subsystemMap.get(subsystemKey);
                validate(subsystemView != null,
                        " Subsystem key " + subsystemKey + " was not found in the database.");

                // device structure key
                final String deviceTypeKey =
                        namingConvention.conventionKey(
                                mnemonicPath(discipline, null, deviceType),
                                NameType.DEVICE_STRUCTURE);
                NameView deviceTypeView = deviceTypeMap.get(deviceTypeKey);

                // device name - ess name
                String deviceName =
                        namingConvention.conventionName(
                                subsystemView.getMnemonicPath(),
                                deviceTypeView != null ? deviceTypeView.getMnemonicPath() : null,
                                nameElement.getMnemonic());
                nameElement.setFullName(deviceName);

                // device key - ess name key
                // device key - device name equivalence class representative
                // device key row not already handled
                String deviceKey =
                        namingConvention.equivalenceClassRepresentative(deviceName);
                if (keys.containsKey(deviceKey)) {
                    int otherRow = keys.get(deviceKey).getRow();
                    validate(false, deviceName + " duplicates name on row " + otherRow + ".");
                }
                keys.put(deviceKey, this);

                // not handle revision that is deleted
                //     find revision (uuid) for device name
                //     find current revision for revision (uuid)
                //     check if current revision is deleted
                //     reuse device name ok for different device
                NameRevision revisionDeviceName = nameViewProvider.getNameRevisions().get(deviceName);
                if (revisionDeviceName != null) {
                    NameView nameViewDeviceName =
                            nameViewProvider.nameView(revisionDeviceName.getNameArtifact().getUuid());
                    NameRevision currentRevisionDeviceName = nameViewDeviceName.getRevisionPair().getApprovedRevision();

                    // not handle revision that is deleted
                    // reuse device name ok for different device (uuid) <--> not ok to reuse if same device (uuid)

                    if (Action.MODIFY.equals(action)
                            && currentRevisionDeviceName.isDeleted()
                            && StringUtils.equals(
                                    currentRevisionDeviceName.getNameArtifact().getUuid().toString(), uuid)) {
                        return null;
                    }
                }

                NameOperation operation = null;
                switch (action) {
                    case DELETE:
                        validate(
                                nameElement.equals(device.getRevisionPair().getBaseRevision().getNameElement()),
                                "Device to be deleted has changes.");
                        operation = new DeleteDevices(Sets.newHashSet(device), systemRoot);
                        break;
                    case ADD:
                        Set<NameView> selectedSubsystem = Sets.newHashSet(subsystemView);
                        AddDevice add = new AddDevice(selectedSubsystem, systemRoot);
                        add.updateOtherParentView(deviceTypeView);
                        add.setNameElement(nameElement);
                        operation = add;
                        break;
                    case MODIFY:
                        ModifyDevice modify = new ModifyDevice(Sets.newHashSet(device), systemRoot);
                        modify.setParentView(subsystemView);
                        modify.updateOtherParentView(deviceTypeView);
                        modify.setNameElement(nameElement);
                        operation = modify;
                        break;
                    default:
                        throw new AssertionError(action.name());
                }

                try {
                    transactionBean.validate(operation);
                } catch (ValidationException e) {
                    throw e;
                }
                return operation;
            }
        }

    }

    private enum Action {
        DELETE, ADD, MODIFY;
    }

}
