pipeline {
    agent {
        label 'docker-ce'
    }
    stages {
        stage('Build') {
            agent {
                docker {
                    image 'europeanspallationsource/oracle-jdk-maven-jenkins:8'
                    reuseNode true
                }
            }
            steps {
                script {
                    env.POM_VERSION = readMavenPom().version
                    currentBuild.displayName = env.POM_VERSION
                }
                sh 'mvn --batch-mode -Dmaven.test.failure.ignore clean install'
            }
        }
        stage('SonarQube analysis') {
            agent {
                docker {
                    image 'europeanspallationsource/oracle-jdk-maven-jenkins:8'
                    reuseNode true
                }
            }
            steps {
                withCredentials([string(credentialsId: 'sonarqube', variable: 'TOKEN')]) {
                    sh 'mvn --batch-mode -Dsonar.login=$TOKEN -Dsonar.branch=${BRANCH_NAME} sonar:sonar'
                }
            }
        }
        stage('Publish') {
            when {
                branch 'master'
            }
            agent {
                docker {
                    image 'europeanspallationsource/oracle-jdk-maven-jenkins:8'
                    reuseNode true
                }
            }
            steps {
                withCredentials([usernamePassword(credentialsId: 'artifactory', usernameVariable: 'USERNAME', passwordVariable: 'PASSWORD')]) {
                    sh 'mvn --batch-mode -Dartifactory.username=$USERNAME -Dartifactory.password=$PASSWORD deploy'
                }
            }
        }
        stage('Build Docker image') {
            when {
                branch 'master'
            }
            steps {
                sh 'docker build -t registry.esss.lu.se/ics-software/naming-convention-tool:latest -t registry.esss.lu.se/ics-software/naming-convention-tool:${POM_VERSION} NamingConventionTool'
            }
        }
        stage('Push Docker image') {
            when {
                branch 'master'
            }
            steps {
                withCredentials([usernamePassword(credentialsId: 'gitlab', usernameVariable: 'USERNAME', passwordVariable: 'PASSWORD')]) {
                    sh 'docker login registry.esss.lu.se --username ${USERNAME} --password ${PASSWORD}'
                }
                sh 'docker push registry.esss.lu.se/ics-software/naming-convention-tool:latest'
                sh 'docker push registry.esss.lu.se/ics-software/naming-convention-tool:${POM_VERSION}'
                sh 'docker logout registry.esss.lu.se'
            }
        }
        stage('Deploy to test environment') {
            when {
                branch 'master'
            }
            agent {
                docker {
                    image 'registry.esss.lu.se/ics-docker/tower-cli:3.3'
                    reuseNode true
                }
            }
            environment {
                ANSIBLE_AWX_HOST = 'torn.tn.esss.lu.se'
                TEST_ENVIRONMENT_HOST = 'icsv-wildfly02.esss.lu.se'
            }
            steps {
                withCredentials([usernamePassword(credentialsId: 'ansible-awx', usernameVariable: 'USERNAME', passwordVariable: 'PASSWORD')]) {
                    sh 'tower-cli job launch \
                          --tower-host "${ANSIBLE_AWX_HOST}" \
                          --tower-username "${USERNAME}" \
                          --tower-password "${PASSWORD}" \
                          --job-template deploy-naming-test \
                          --extra-vars "naming_docker_image_tag=${POM_VERSION}" \
                          --tags docker_container \
                          --monitor'
                }
            }
        }
    }
    post {
        failure {
            emailext (
                subject: '${DEFAULT_SUBJECT}',
                body: '${DEFAULT_CONTENT}',
                recipientProviders: [[$class: 'CulpritsRecipientProvider']]
            )
        }
    }
}
